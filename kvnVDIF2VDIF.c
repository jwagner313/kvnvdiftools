/**************************************************************************************************************
 **
 ** kvnVDIF2VDIF v1.02
 **
 ** Usage:    kvnVDIF2VDIF <input.vdif> <output.vdif> [<ID>]
 **
 ** Example:  kvnVDIF2VDIF r13340b_KVNTN_No00001.vdif r13340b_KVNTN_No00001.fixed.vdif KT
 **
 ** Converts an input file in KVN RVDB or Japanese K5 VOA format (quasi-standard "VDIF")
 ** into an output file that follows the VDIF standard. Optionally updates the station
 ** name in the VDIF header.
 **
 ** The VDIF frame size is 1280 bytes (payload) + 32 bytes (header) = 1312 bytes.
 ** This is the fixed frame size of the VOA VSI-to-10GbE hardware and is used
 ** also by the Korean RVDB Raw VLBI Data Buffer.
 **
 ** Numerous fixes can be applied:
 ** 1)  Endianness fix in the 32-bit header words (Big --> Little Endian)
 **     The endianness issue is acknowledged by the Japanese, might be fixed some day.
 ** 2)  Clears the 'Invalid' bit (sometimes data are incorrectly marked Invalid)
 ** 3a) Optional: corrects the 'Number of channels' header value
 ** 3b) TODO: some VOA data are marked 1-bit/sample but are actually 2-bit/sample
 ** 4)  Optional: fills out station ID
 ** 5)  Optional: fixes frame size to 164*8-byte = 1312 (sometimes headers say 1312*8-byte)
 ** 6)  Corrects the bit order of payload data
 ** 7)  Corrects the sample encoding from K5/Mark5B into actual VDIF
 **
 ** Open issues:
 **   - is at least the channel order correct? 
 **
 ** (C) 2014 Jan Wagner
 **
 **************************************************************************************************************/

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Compile with:
//    gcc -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE -D_LARGEFILE_SOURCE -O3 kvnVDIF2VDIF.c -o kvnVDIF2VDIF
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Infos for sample encoding issues with nonstandard "VDIF"
// 
// VDIF 1.1 standard, page 11: 10. Sample representation
//    00, 01, 10, 11 = -HiMag, -1.0, +1.0, +HiMag
//
// K5/VLBA/Mark5B in mark5 memo #019 and other documents:
//    00, 01, 10, 11 = -HiMag, +1.0, -1.0, +HiMag
//
//    The K5 and KVN "VDIF" have a further complication 
//    related to VDIF header Endianness and payload bit order.
//
// JPL DVP
//    00,  01,  10,  11 = +1.0, +HiMag, -HiMag, -1.0
//    xor with 0b10 : {00,01,10,11}^10 = {10,11,00,01} = VDIF +1.0, +HiMag, -HiMag, -1.0
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Flags to modify *HEADER*
#define VDIF_HEADER_CHANGE_ENDIAN            1   // change header endianness?
#define VDIF_HEADER_FIX_FRAMESIZE            1   // set frame size to 1312 bytes, e.g., some headers says 10496-byte
#define VDIF_HEADER_CONVERT_LEGACY           0   // TODO: conversion of 16-->32-byte header, clearing Legacy flag

// Flags to modify *PAYLOAD*
#define VDIF_PAYLOAD_CHANGE_8BITORDER        1   // flip each byte?
#define VDIF_PAYLOAD_CHANGE_2BITENCODING_K5  1   // change 2-bit sample encoding from K5 to VDIF?
#define VDIF_PAYLOAD_CHANGE_2BITENCODING_DVP 0   // if not K5 (above): change 2-bit sample encoding from JPL DVP to VDIF?
#define VDIF_PAYLOAD_CHANGE_ENDIAN           0   // change payload endianness?

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

#define NUM_BUFFERED_FRAMES     512

#if VDIF_HEADER_CONVERT_LEGACY
   #define INPUT_HEADER_SIZE     16  // legacy header size in bytes
#else
   #define INPUT_HEADER_SIZE     32  // normal header size in bytes
#endif
#define PAYLOAD_SIZE           1280  // payload in bytes
                                     // 1280 byte is built-in to VOA hardware
                                     // 8000 byte is built-in(?) into JPL DVP
#define INPUT_FRAME_SIZE  (PAYLOAD_SIZE + INPUT_HEADER_SIZE)
#define BUFSIZE           (INPUT_FRAME_SIZE * NUM_BUFFERED_FRAMES)

#include <malloc.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <endian.h>
#include <sys/types.h>
#include <sys/stat.h>

static void fix_vdif_frame(unsigned char*, const char*, const char, const int);
static unsigned char m_byte_bit_swap[256]     __attribute__ ((aligned (16)));
static unsigned char m_lut_signmagswap[256]   __attribute__ ((aligned (16)));

static int m_validity_dots = 0; // >0 to plot 'I' or '.' for each frame depending if frame Invalid bit set or not


int main(int argc, char** argv)
{
    int fdi, fdo;
    ssize_t nrd, nwr;
    int nframes;
    int i;

    unsigned char* buf = NULL;
    char* station_id = NULL;

    unsigned char zeros[32] = {0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0, 0,0,0,0,0,0,0,0};

    if (argc < 3)
    {
        printf("Program kvnVDIF2VDIF Version 1.02\n\n"
               "Usage: kvnVDIF2VDIF <input.vdif> <output.vdif> [<ID>]\n"
               "Applies numerous fixes (see kvnVDIF2VDIF.c source code) to convert KVN VDIF to standard VDIF.\n"
               "Optionally also sets the two-character station ID in the output file.\n\n");
        return -1;
    }


    /* Arguments */

    fdi = open(argv[1], O_RDONLY|O_LARGEFILE);
    if (fdi < 0)
    {
        printf("Error opening input file '%s': %s\n", argv[1], strerror(errno));
        return -1;
    }

    fdo = open(argv[2],
                O_WRONLY|O_CREAT|O_LARGEFILE,
                S_IRUSR|S_IWUSR | S_IRGRP|S_IWGRP | S_IROTH
               );
    if (fdo < 0)
    {
        printf("Error creating output file '%s': %s\n", argv[2], strerror(errno));
        return -1;
    }

    if (argc == 4)
    {
        station_id = strndup(argv[3], 2);
    }


    /* Prepare bit swapping to convert sign/magnitude bit order from Mark5B to VDIF (necessary in *some* cases) */

    for (i=0; i<256; i++)
    {
        // test case
        //   b = 0x1B; // 0b00 01 10 11  so expecting  swp = 0b00 10 01 11 = 0x27 as output
        unsigned char b = (unsigned char)i;
        unsigned char s0 = b & 0xAA;  // 0xAA = 0b101010
        unsigned char m0 = b & 0x55;  // 0x55 = 0b010101
        unsigned char swp = (s0 >> 1) | (m0 << 1);
        m_lut_signmagswap[b] = swp;
    }


    /* Copy input file to output file with fixes */

    buf = (unsigned char*)memalign(16, BUFSIZE);

    while (1)
    {
        nrd = read(fdi, (void*)buf, BUFSIZE);
        if (nrd <= 0) { break; }

        nframes = nrd / INPUT_FRAME_SIZE;

        for (i=0; i<nframes; i++)
        {
            (void)fix_vdif_frame(buf + i*INPUT_FRAME_SIZE, station_id, -1, INPUT_FRAME_SIZE);
        }

        #if VDIF_HEADER_CONVERT_LEGACY
        for (i=0; i<nframes; i++)
        {
           char* hdr = buf + i*INPUT_FRAME_SIZE;
           char* pld = buf + i*INPUT_FRAME_SIZE + INPUT_HEADER_SIZE;
           nwr = write(fdo, (void*)hdr,   INPUT_HEADER_SIZE);
           nwr = write(fdo, (void*)zeros, INPUT_HEADER_SIZE);
           nwr = write(fdo, (void*)pld,   PAYLOAD_SIZE);
        }
        #else
        nwr = write(fdo, (void*)buf, nrd);
        #endif
    }

    printf("\nDone.\n");
    close(fdi);
    close(fdo);
    return 0;
}


/**
 * fix_vdif_frame : Applies numerous changes to headers and data so that they comply with VDIF
 */
static void fix_vdif_frame(unsigned char* buf, const char* ID, const char log2_nchan, const int framesize)
{
    int i;

    /* Corrections and changes to the VDIF header */

    // Fix wrong header endianness
    #if VDIF_HEADER_CHANGE_ENDIAN
    for (i=0; i<INPUT_HEADER_SIZE; i+=4)
    {
        // TODO: could also use be32toh() for each of the 4 header words

        // swap outer 2 bytes
        unsigned char c1 = buf[i+0];
        buf[i+0] = buf[i+3]; buf[i+3] = c1;

        // swap inner 2 bytes
        unsigned char c2 = buf[i+1];
        buf[i+1] = buf[i+2]; buf[i+2] = c2;
    }
    #endif

    // Fix wrong 'Invalid' bit, optionally with printout of dot for each invalid frame
    if (buf[3] & 0x80)
    {
        if (m_validity_dots) { fputc('I', stdout); }
        buf[3] = buf[3] & ~0x80;
    }
    else
    {
        if (m_validity_dots) { fputc('.', stdout); }
    }

    // Conversion? Clear the 'Legacy' bit
    #if VDIF_HEADER_CONVERT_LEGACY
    buf[3] = buf[3] & ~0x40;
    #endif

    // Fix/change the 'Number of channels'
    if (log2_nchan >= 0)
    {
       buf[3+2*4] = (buf[3+2*4] & ~0x1F) | (log2_nchan & 0x1F);
    }

    // Insert Station ID
    if (ID != NULL)
    {
        buf[0+3*4] = ID[1];
        buf[1+3*4] = ID[0];
    }

    // Fix wrong frame size units; 8-byte not 1-byte units!
    // (e.g., VOA "VDIF" says 0x0520=1312*8=10496 bytes but uses 1312-byte frames)
    #if (VDIF_HEADER_FIX_FRAMESIZE || VDIF_HEADER_CONVERT_LEGACY)
    buf[0+2*4] = ((framesize + (VDIF_HEADER_CONVERT_LEGACY ? 16 : 0))  / 8)       & 0x00FF;
    buf[1+2*4] = (((framesize + (VDIF_HEADER_CONVERT_LEGACY ? 16 : 0)) / 8) >> 8) & 0x00FF;
    #endif

    for (i=INPUT_HEADER_SIZE; i<framesize; i+=4)
    {
        unsigned char b0 = buf[i+0];
        unsigned char b1 = buf[i+1];
        unsigned char b2 = buf[i+2];
        unsigned char b3 = buf[i+3];

        #if VDIF_PAYLOAD_CHANGE_8BITORDER
        // KVN data: 
        //   Do 8-bit mirroring but no Endianness change, as suggested by Dr. Han at KVN.
        //   Oddly, KVN VSI-H TVG test data oper@polaris:/home/oper/vdif/vdiftvg_play_test_1.dat
        //   suggest that a full 32-bit flip is needed, i.e., Endian reorder *and* 8-bit bit-swap
        b0 = m_byte_bit_swap[b0];
        b1 = m_byte_bit_swap[b1];
        b2 = m_byte_bit_swap[b2];
        b3 = m_byte_bit_swap[b3];
        #endif

        #if VDIF_PAYLOAD_CHANGE_2BITENCODING_K5
        // Convert Mark5B/K5/VLBA into VDIF
        b0 = m_lut_signmagswap[b0];
        b1 = m_lut_signmagswap[b1];
        b2 = m_lut_signmagswap[b2];
        b3 = m_lut_signmagswap[b3];
        #elif VDIF_PAYLOAD_CHANGE_2BITENCODING_DVP
        // Convert JPL DVP into VDIF (untested)
        b0 = b0 ^ 0b10101010;
        b1 = b1 ^ 0b10101010;
        b2 = b2 ^ 0b10101010;
        b3 = b3 ^ 0b10101010;
        #endif

        #if VDIF_PAYLOAD_CHANGE_ENDIAN
        // swap the endianness (suggested by KVN VSI-H TVG test file in "VDIF" format)
        buf[i+0] = b3;
        buf[i+1] = b2;
        buf[i+2] = b1;
        buf[i+3] = b0;
        #else
        // same byte order as input (suggested by Dr. Han at the KVN)
        buf[i+0] = b0;
        buf[i+1] = b1;
        buf[i+2] = b2;
        buf[i+3] = b3;
        #endif
    }

    return;
}


/** Look-up table for 8-bit bit reversal */
static unsigned char m_byte_bit_swap[256] __attribute__ ((aligned (16))) = {
            0x00, 0x80, 0x40, 0xc0, 0x20, 0xa0, 0x60, 0xe0,
            0x10, 0x90, 0x50, 0xd0, 0x30, 0xb0, 0x70, 0xf0,
            0x08, 0x88, 0x48, 0xc8, 0x28, 0xa8, 0x68, 0xe8,
            0x18, 0x98, 0x58, 0xd8, 0x38, 0xb8, 0x78, 0xf8,
            0x04, 0x84, 0x44, 0xc4, 0x24, 0xa4, 0x64, 0xe4,
            0x14, 0x94, 0x54, 0xd4, 0x34, 0xb4, 0x74, 0xf4,
            0x0c, 0x8c, 0x4c, 0xcc, 0x2c, 0xac, 0x6c, 0xec,
            0x1c, 0x9c, 0x5c, 0xdc, 0x3c, 0xbc, 0x7c, 0xfc,
            0x02, 0x82, 0x42, 0xc2, 0x22, 0xa2, 0x62, 0xe2,
            0x12, 0x92, 0x52, 0xd2, 0x32, 0xb2, 0x72, 0xf2,
            0x0a, 0x8a, 0x4a, 0xca, 0x2a, 0xaa, 0x6a, 0xea,
            0x1a, 0x9a, 0x5a, 0xda, 0x3a, 0xba, 0x7a, 0xfa,
            0x06, 0x86, 0x46, 0xc6, 0x26, 0xa6, 0x66, 0xe6,
            0x16, 0x96, 0x56, 0xd6, 0x36, 0xb6, 0x76, 0xf6,
            0x0e, 0x8e, 0x4e, 0xce, 0x2e, 0xae, 0x6e, 0xee,
            0x1e, 0x9e, 0x5e, 0xde, 0x3e, 0xbe, 0x7e, 0xfe,
            0x01, 0x81, 0x41, 0xc1, 0x21, 0xa1, 0x61, 0xe1,
            0x11, 0x91, 0x51, 0xd1, 0x31, 0xb1, 0x71, 0xf1,
            0x09, 0x89, 0x49, 0xc9, 0x29, 0xa9, 0x69, 0xe9,
            0x19, 0x99, 0x59, 0xd9, 0x39, 0xb9, 0x79, 0xf9,
            0x05, 0x85, 0x45, 0xc5, 0x25, 0xa5, 0x65, 0xe5,
            0x15, 0x95, 0x55, 0xd5, 0x35, 0xb5, 0x75, 0xf5,
            0x0d, 0x8d, 0x4d, 0xcd, 0x2d, 0xad, 0x6d, 0xed,
            0x1d, 0x9d, 0x5d, 0xdd, 0x3d, 0xbd, 0x7d, 0xfd,
            0x03, 0x83, 0x43, 0xc3, 0x23, 0xa3, 0x63, 0xe3,
            0x13, 0x93, 0x53, 0xd3, 0x33, 0xb3, 0x73, 0xf3,
            0x0b, 0x8b, 0x4b, 0xcb, 0x2b, 0xab, 0x6b, 0xeb,
            0x1b, 0x9b, 0x5b, 0xdb, 0x3b, 0xbb, 0x7b, 0xfb,
            0x07, 0x87, 0x47, 0xc7, 0x27, 0xa7, 0x67, 0xe7,
            0x17, 0x97, 0x57, 0xd7, 0x37, 0xb7, 0x77, 0xf7,
            0x0f, 0x8f, 0x4f, 0xcf, 0x2f, 0xaf, 0x6f, 0xef,
            0x1f, 0x9f, 0x5f, 0xdf, 0x3f, 0xbf, 0x7f, 0xff
};

