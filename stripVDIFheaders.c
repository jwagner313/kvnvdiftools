/********************************************************************************
 ** Strip VDIF headers and write payload to a new file.
 **
 ** The data-second and frame number are taken into account in calculating
 ** the offset in the output file at which the payload data will be written.
 ** Missing frames produce gaps in the output file; in Linux these gaps
 ** are usually filled by 0x00 by the operating system.
 **
 ** Version 1.1
 **
 ** (C) 2014 Jan Wagner 
 **
 *******************************************************************************/

// gcc -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE -D_LARGEFILE_SOURCE -O3 stripVDIFheaders.c -o stripVDIFheaders

#define BUFSIZE         32768
#define VDIF_HEADER_LEN 32      // 32 for standard VDIF, 16 for legacy

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <malloc.h>

int main(int argc, char** argv)
{
    int fdi, fdo, payloadsize, datarate;
    ssize_t nrd, nwr;

    unsigned char* buf;
    uint32_t*      buf32;

    // Values extracted from VDIF header
    int32_t sec0 = -1, framenr0 = 0;
    int32_t sec, framenr;
    off64_t framespersec;
    off64_t wroffset;
    off64_t framecount = 0;

    // Usage and cmd line arguments
    if (argc <= 4)
    {
        printf("\n"
               "Usage: stripVDIFheaders <infile> <outfile> <payloadsize> <rate in Mbps>\n\n"
               "Strips the VDIF headers from the input file, outputs headerless new file.\n"
               "If frames are missing *no* fill pattern data is inserted. Instead there\n"
               "will be gaps in the file with OS-inserted 'empty' data e.g. 0x00.\n\n");
        return -1;
    }

    fdi = open(argv[1], O_RDONLY|O_LARGEFILE);  // TODO: check for returned errors
    fdo = open(argv[2],
                O_WRONLY|O_CREAT|O_LARGEFILE,
                S_IRUSR|S_IWUSR | S_IRGRP|S_IWGRP | S_IROTH
          );
    payloadsize = atoi(argv[3]);
    datarate = atoi(argv[4]);

    // Local buffer
    buf   = memalign(16, BUFSIZE);
    buf32 = (uint32_t*)buf;

    // Frames per second : 200000 for 2048 Mbps with 1280-byte payload
    framespersec = ((double)datarate) * 1e6 / (8.0 * (double)payloadsize); 
    printf("Data rate of %d Mbps and %d-byte payload produces %d frames/sec\n", 
           datarate, payloadsize, (int)framespersec
    );

    while (1)
    {
        nrd = read(fdi, (void*)buf, VDIF_HEADER_LEN + payloadsize);
        if (nrd <= VDIF_HEADER_LEN) { break; }

        // Extract timestamp from VDIF header
        // http://vlbi.org/vdif/docs/VDIF_specification_Release_1.1.1.pdf
        sec     = buf32[0] & 0x3FFFFFFF; 
        framenr = buf32[1] & 0x00FFFFFF;
        if (sec0 == -1)	
        {
           sec0 = sec;
           framenr0 = framenr;
        }

        // Write payload at correct offset
        wroffset = ((off64_t)payloadsize) * ((off64_t)(sec - sec0) * (off64_t)framespersec + (off64_t)(framenr - framenr0));
        lseek64(fdo, wroffset, SEEK_SET);

        nwr = write(fdo, (void*)(buf + VDIF_HEADER_LEN), nrd - VDIF_HEADER_LEN);  // TODO: check nwr

        framecount++;
    }

    printf("Done. %lu frames processed.\n", framecount);
    close(fdi);
    close(fdo);

    return 0;
}

