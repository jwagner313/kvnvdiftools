#define _GNU_SOURCE
#include <arpa/inet.h>
#include <errno.h>
#include <getopt.h>
#include <malloc.h>
#include <memory.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sched.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <sys/fcntl.h>
#include <time.h>
#include <unistd.h>

#define RX_TIMEOUT_SEC       10
#define RX_SOCKET_BUFSIZE_MB 64
#define MAX_RECEIVE_SIZE     65507
#define MIN_RECEIVE_SIZE     1000

void usage(void)
{
	printf("Usage: forwardUDP <localPort> <remoteIP> <remotePort>\n");
}

void realtime_init(int cpu)
{
    cpu_set_t set;
    int rc;

    CPU_ZERO(&set);
    CPU_SET(cpu, &set);
    rc = sched_setaffinity(0, sizeof(set), &set);
    if (rc < 0)
    {
       printf("sched_setaffinity: could not set CPU affinity (maybe must run as root)?\n");
    }
    else
    {
       printf("Bound to CPU#%d\n", cpu);
    }

    return;
}

int open_udp_rx(const char* port_str)
{
   int sd, c;
   struct addrinfo  hints;
   struct addrinfo* res = 0;
   struct timeval tv_timeout;

   memset(&hints,0,sizeof(hints));
   hints.ai_family   = AF_UNSPEC;
   hints.ai_socktype = SOCK_DGRAM;
   hints.ai_protocol = 0;
   hints.ai_flags    = AI_PASSIVE | AI_ADDRCONFIG;
   getaddrinfo(NULL, port_str, &hints, &res);

   sd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
   if (bind(sd, res->ai_addr, res->ai_addrlen) == -1)
   {
      perror("bind");
      return -1;
   }
   freeaddrinfo(res);

   tv_timeout.tv_sec  = RX_TIMEOUT_SEC;
   tv_timeout.tv_usec = 0;
   setsockopt(sd, SOL_SOCKET, SO_RCVTIMEO, (char*)&tv_timeout, sizeof(struct timeval));

   c = RX_SOCKET_BUFSIZE_MB*1024*1024;
   setsockopt(sd, SOL_SOCKET, SO_RCVBUF, &c, sizeof(c));

   return sd;
}

int open_udp_tx(const char* host_str, const char* port_str, struct sockaddr_in* sdaddr)
{
    socklen_t slen;
    int sd, flag;

    sd = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sd < 0) {
        perror("socket");
        return -1;
    }
    flag = 1;
    if (setsockopt(sd, SOL_SOCKET, SO_NO_CHECK, &flag, sizeof(flag)) < 0) {
        perror("SO_NO_CHECK");
    }
    flag = RX_SOCKET_BUFSIZE_MB*1024*1024;
    if (setsockopt(sd, SOL_SOCKET, SO_SNDBUF, &flag, sizeof(flag)) < 0) {
        perror("SO_SNDBUF");
    }
    getsockopt(sd, SOL_SOCKET, SO_SNDBUF, &flag, &slen);
    printf("Socket send buffer is now %.1f kByte\n", flag/1024.0);

    // Destination IP
    memset((char*)sdaddr, 0, sizeof(struct sockaddr_in));
    sdaddr->sin_family = AF_INET;
    sdaddr->sin_port = htons(atoi(port_str));
    if (inet_aton(host_str, &(sdaddr->sin_addr)) == 0) {
       printf("Invalid IP address\n");
       return -1;
    }

    fcntl(sd, F_SETFL, O_NONBLOCK); // enable nonblocking I/O mode

    return sd;
}

int main(int argc, char** argv)
{
	struct sockaddr_in sdaddr_tx;
	struct timeval tv1, tv2;
	size_t nbyte = 0;
	int sdi, sdo;

	char* buf;
	if (argc != 4) {
		usage();
		return -1;
	}
	
	buf = memalign(4096, MAX_RECEIVE_SIZE);

	sdi = open_udp_rx(argv[1]);
	if (sdi == -1) {
		printf("Could not create UDP listener socket.\n");
		return -1;
	}

	sdo = open_udp_tx(argv[2], argv[3], &sdaddr_tx);
	if (sdo == -1) {
		printf("Could not create UDP transmit socket.\n");
		return -1;
	}


	setbuf(stdout, NULL);
	gettimeofday(&tv1, NULL);
	while (1) {

		ssize_t n = recv(sdi, buf, MAX_RECEIVE_SIZE, 0);
		if (n == -1) {
			if (errno == EAGAIN) {
				printf("\nNo UDP packets received in the last %d seconds...\n", RX_TIMEOUT_SEC);
			} else {
				perror("recv");
				exit(-1);
			}
			break;
		}

		if (n < MIN_RECEIVE_SIZE) {
			putchar('x');
			continue;
		}
		nbyte += n;

		n = sendto(sdo, buf, n, 0, (const struct sockaddr *)&sdaddr_tx, sizeof(sdaddr_tx));

		if (nbyte > RX_SOCKET_BUFSIZE_MB*1024*1024) {
			double dT;
			gettimeofday(&tv2, NULL);
			dT = (tv2.tv_sec - tv1.tv_sec) + 1e-6*(tv2.tv_usec - tv1.tv_usec);
			printf("rx->tx @ %5.3f Gbit/s    \r", (8e-9*nbyte)/dT);
			tv1 = tv2;
			nbyte = 0;
		}

	}

	return 0;
}
