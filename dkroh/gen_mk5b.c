// gen_mk5b.c : mark5b file 작성

// mark5b 파일은 data rate에 따라,
//   VSI-32MHz, 1Gbps Data rate의 경우, 1초에 12800개의 data frame이 존재
//   VSI-64MHz, 2Gbps Data rate의 경우, 1초에 25600개의 data frame이 존재
//
// Data frame은
//   16 bytes의 헤더와 10000 bytes의 데이터로 구성된다.
//
// 이 프로그램은 
// - TVG를 작성하여, mark5b 형식으로 저장한다.
// - speed 설정에 따라, 1Gbps 또는 2Gbps data rate의 파일을 생성할 수 있다.
// - date & time 정보를 Header에 삽입한다. 
// - nsec 초 길이의 파일을 만들되, TVG data는 1초 단위로 반복된다.
// - CRCC 계산 추가했음.

// 추가 작업:
// - Add a noise generating routine
// - Add a tone generating routine
// - Add a fake signal generating routine

// $ gcc -Wall -O2 -o gen_mk5b gen_mk5b.c -lm

#include <unistd.h>		// getpagesize()
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <ctype.h>

#include "stopwatch.c"
#include "calendar.c"
#include "mk5b.c"

//=========================================================================

int main(int argc, char **argv)
{
  FILE *fo;
  size_t n;

  int i,iDF,iSec;

  Mark5B_FRAMEHEADER mk5b; // Mark5B Frame Header
  unsigned char *p;
  unsigned char *data = NULL;
  int nDF;          //= speed * 12800
  int Size_DataBuf; //= nDF * LEN_MK5B_DF_DATA   = 128MB or 256MB

  unsigned int mjdn,sod,sod2; // 'KVN Mark5B recorder'

  // variables for set options : 
  //   default < 0  : "Don't change the original, unless user supplies a value." 
  //   0 <= default : "Force to change the original to this default or user supplied value."
  int  year=2014,month=1,date=1; // "date=year/month/date"
  int  hh=0,mm=0,ss=0;           // "time=hh:mm:ss"
  int  Type=0;                   // "type=TVG|Nosie|Tone|Conf"
  int  Length=1;                 // "length=#"
  int  Speed=1;                  // "speed=1|2"

  Mark5B_INFO mk5binfo= { NULL,NULL, 0,0, 0,0, 0,0,0, 0,0 }; // for debug
                                 // 오직 mk5b_HeaderTime()를 호출하기 위해 필요함.

  STOPWATCH *sw_all = alloc_stopwatch("Total       ",NULL);
  STOPWATCH *sw_gen = alloc_stopwatch("GenerateData",NULL);
  STOPWATCH *sw_wri = alloc_stopwatch("WriteData   ",NULL);

  // Command Line Parameters
  for(i=1; i<argc; i++) { // pick-up the set options first
    char *eqs, dum1,dum2;
    int i1,i2,i3;
    if((eqs = index(argv[i],'=')) != NULL) {
      if(strncasecmp(argv[i],"date",3)==0) {
        if(sscanf(eqs+1,"%d%c%d%c%d",&i1,&dum1,&i2,&dum2,&i3) == 5) {
          if(i1<2000 || (i2<1 || 12<i2) || (i3<1 || 31<i3)) {
            fprintf(stderr,"Invalid date : %s --> %d/%d/%d\n",argv[i],i1,i2,i3);
          }else{
            year=i1; month=i2; date=i3;
          }
        }else{
          fprintf(stderr,"Invalid date : %s\n",argv[i]);
        }
      }else if(strncasecmp(argv[i],"time",3)==0) {
        if(sscanf(eqs+1,"%d%c%d%c%d",&i1,&dum1,&i2,&dum2,&i3) == 5) {
          if((i1<0 || 23<i1) || (i2<0 || 59<i2) || (i3<0 || 59<i3)) {
            fprintf(stderr,"Invalid time : %s --> %d/%d/%d\n",argv[i],i1,i2,i3);
          }else{
            hh=i1; mm=i2; ss=i3;
          }
        }else{
          fprintf(stderr,"Invalid time : %s\n",argv[i]);
        }
      }else if(strncasecmp(argv[i],"type",3)==0) {
        if(strncasecmp(eqs+1,"TVG",3)==0) {
          Type = 0;
        }else if(strncasecmp(eqs+1,"Noise",3)==0) {
          Type = 1;
        }else if(strncasecmp(eqs+1,"Tone",4)==0) {
          Type = 2;
        }else if(strncasecmp(eqs+1,"Conf",4)==0) {
          Type = 3;
        }else{
          fprintf(stderr,"Invalid type : %s\n",argv[i]);
        }
      }else if(strncasecmp(argv[i],"length",3)==0) {
        i1 = atoi(eqs+1);
        if(i1<1) {
          fprintf(stderr,"Invalid length : %s --> %d\n",argv[i],i1);
        }else{
          Length = i1;
        }
      }else if(strncasecmp(argv[i],"speed",3)==0) {
        i1 = atoi(eqs+1);
        if(i1<1 || 2<i1) {
          fprintf(stderr,"Invalid speed : %s --> %d\n",argv[i],i1);
        }else{
          Speed = i1;
        }
      }else{
        fprintf(stderr,"Ignored the unknown set option, %s\n",argv[i]);
      }
      argv[i] = NULL;
    }
  }
  for(i=1; i<argc; i++) { // clean up
    if(argv[i]==NULL) {
      int k;
      for(k=i+1; k<argc; k++) {
        if(argv[k]!=NULL) { argv[i] = argv[k]; argv[k] = NULL; break; }
      }
      if(k==argc) { argc = i; break; }
    }
  }
  if(argc < 2) {
    fprintf(stderr,"Usage: $ %s out.mk5b [set options ...]\n",argv[0]);
    fprintf(stderr,
      "  where out.mk5b = output, mark5b file\n"
      "  set options:\n"
      "    date=y/m/d     Set year,month,date    : default=2014/01/01\n"
      "    time=h:m:s     Set hour,minute,second : default=00:00:00\n"
      "    type=keyword   Set data type, TVG|Noise|Tone|Conf : default=TVG\n"
      "    length=#       Set date length, # of seconds      : default=1\n"
      "    speed=#        Set recording speed, 1 or 2 Gbps   : default=1\n"
      );
    exit(1);
  }

  //============================================================================
  if(Type != 0) {
    fprintf(stderr,"Sorry, NOT yet implemented for type=%d\n",Type);
    exit(1);
  }

  //============================================================================

  // Prepare the Data buffer for 1 second.
  nDF = 12800 * Speed;	// 12800 or 25600 DFs per seconds
  Size_DataBuf = nDF * LEN_MK5B_DF_DATA;   // 128MB or 256MB per seconds
  if(posix_memalign((void **)&data,getpagesize(),Size_DataBuf) != 0) {
    fprintf(stderr,"Fail to allocate memory for Data.\n");
    exit(1);
  }//data

  // Create the output Mark5B file
  if((fo = fopen(argv[1],"w")) == NULL) {
    fprintf(stderr,"Fail to open '%s'\n",argv[1]);
    exit(1);
  }
  // Set mk5binfo about the output file (only for debug)
  mk5binfo.pathname = argv[1];
  mk5binfo.filename = argv[1];
  mk5binfo.speed = Speed;
  mk5binfo.nDF_1s = 12800*Speed;
  mjdn = MJDN(year,month,date);
  mk5binfo.MJDepoch = (mjdn/1000)*1000;
  mk5binfo.version = 1; // 1=**BEAD style** KVN Mark5B recorder

  // Check if (year,month,date) is far from 'today'
  {
  int y1,m1,d1, mjdn1,MJDepoch1;
  TODAY(&y1,&m1,&d1,NULL,NULL,NULL);
  mjdn1 = MJDN(y1,m1,d1);
  MJDepoch1 = (mjdn1/1000)*1000;
  if(MJDepoch1 != mk5binfo.MJDepoch) fprintf(stderr,
    "Be careful that requested date is too far from TODAY.\n"
    "  Requsted %4d/%02d/%02d mjdn=%d (MJDepoch=%d)\n"
    "  TODAY    %4d/%02d/%02d mjdn=%d (MJDepoch=%d)\n\n",
    year,month,date, mjdn, mk5binfo.MJDepoch,
    y1,m1,d1, mjdn1, MJDepoch1);
  }

  //----> Mark5B DF Header
  memset(&mk5b,0,LEN_MK5B_DF_HEADER);
  mk5b.sync = 0xABADDEED;               // SYNC
  mk5b.year2000 = 0xb;			//  4bits
  mk5b.userdata = 0xead;		// 12bits
  if(Type==0) mk5b.isTVG = 1;           //  1bits

  start_stopwatch(sw_all);
  for(iSec=0;iSec<Length;iSec++) {

    // Generate Header ===== 'KVN Mark5B recorder' =====
    mjdn = MJDN(year,month,date);       // mjdn
    sod = hh*3600 + mm*60 + ss + iSec;  // Seconds of Day
    //----> Mark5B DF Header
    //mk5b.frameNum = iDF;		// 15bits : 0~(nDF-1) 
    mk5b.bcdJJJ   = BCD(mjdn % 1000);	// 12bits : JJJ
    mk5b.bcdSOD   = BCD(sod);		// 20bits : sssss
    //mk5b.bcdSOD2  = 0;		// 16bits : .ssss
    //mk5b.crcc     = 0;		// 16bits

    if(iSec==0) { // Set mk5binfo (only for debug)
      mk5binfo.jjj0 = iBCD(mk5b.bcdJJJ);
      mk5binfo.sod0 = iBCD(mk5b.bcdSOD);
    }

    // Generate Data
    start_stopwatch(sw_gen);
    if(Type==0) {	//---------------- TVG
      if(iSec==0) {
        // 1초 분량의 Test Vector 열을 계산한다. 이후로는 매 초 같은 값을 반복 사용한다.
        unsigned int *tvdata = (unsigned int *)data;
        fprintf(stderr,"Generating TVG data for %d seconds at %dGbps ...\n",Length,Speed);
        tvdata[0] = tvg(1); // init TVG at first time
        for(i=1; i<Size_DataBuf/sizeof(unsigned int); i++) tvdata[i] = tvg(0);
      }
    }else if(Type==1) {	//---------------- Noise
    }else if(Type==2) {	//---------------- Tone
    }else if(Type==3) {	//---------------- Conf
    }else{
    }
    stop_stopwatch(sw_gen);

    // write "nDF" Mark5B DFs for 1 sec
    start_stopwatch(sw_wri);
    for(iDF=0; iDF<nDF; iDF++) {
      //----> Mark5B DF Header
      sod2 = (10000*iDF)/nDF;       // 0~9999
      //===== Makr5B DF Header 편집 및 쓰기
      mk5b.frameNum = iDF;		// 15bits : 0~12799 or 255999
      mk5b.bcdSOD2  = BCD(sod2);	// 16bits : .ssss
      mk5b.crcc     = crcc(&mk5b);	// 16bits

      if(/*iSec==0 &&*/ iDF==0) mk5b_HeaderTime(&mk5binfo,&mk5b,"gen_mk5b");

      if((n = fwrite(&mk5b,sizeof(unsigned char),LEN_MK5B_DF_HEADER,fo)) != LEN_MK5B_DF_HEADER) {
        fprintf(stderr,"Fail to write HEADER at iSec=%d, iDF=%d/%d, n=%lu/%u\n",iSec,iDF,nDF,n,LEN_MK5B_DF_HEADER);
        exit(1);
      }//Header
      p = data + iDF*LEN_MK5B_DF_DATA;
      if((n = fwrite(p,sizeof(unsigned char),LEN_MK5B_DF_DATA,fo)) != LEN_MK5B_DF_DATA) {
        fprintf(stderr,"Fail to write DATA at iSec=%d, iDF=%d/%d, n=%lu/%u\n",iSec,iDF,nDF,n,LEN_MK5B_DF_DATA);
        exit(1);
      }//Data
    }//iDF
    stop_stopwatch(sw_wri);

  }//iSec
  stop_stopwatch(sw_all);
  fprintf(stderr,"\nProcessed for %d seconds of Data.\n",iSec);

  // Stopwatch
  report_stopwatch(sw_gen); free_stopwatch(sw_gen);
  report_stopwatch(sw_wri); free_stopwatch(sw_wri);
  report_stopwatch(sw_all); free_stopwatch(sw_all);

  // Done.
  fclose(fo);
  free(data);

  //===== Check the status of output file
  mk5b_info(">>>",argv[1]);

  return(0);
}

