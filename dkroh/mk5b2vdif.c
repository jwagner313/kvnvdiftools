// mk5b2vdif.c
//
// Convert a mk5b file to vdif file
//
// Input mk5b file could be one of following types.
//   mk5binfo->version=1 : **BEAD style** KVN Mark5B recorder
//				H.year2000==0xb, H.userdata==0xead
//   mk5binfo->version=2 : **NULL style** KVN Mark5B recorder
//				H.year2000==0,   H.userdata==0
//   mk5binfo->version=3 : Software using year2000,bcdDOY
//				H.year2000!=0,   H.userdata==0
//   mk5binfo->version=? : unknown
//				H.year2000==?,   H.userdata==?
// mk5b file of version 2 or 3 can be fixed to version 1,
// by using 'fix_mk5b' program.
//
// Output vdif file will be type 0, Standard VDIF file.
//   type 2 = OCTA VDIF with Capture Header
//   type 1 = OCTA VDIF
//   type 0 = Standard VDIF

// in.mk5b 파일은
//   매초마다 12800*Speed 개의 DataFrame이 반복된다.
//   각 DataFrame은 16 bytes의 헤더와 10000 bytes의 데이터로 구성된다.
//
// out.vdif 파일은 Standard VDIF 이다.
//   매초마다 10만*Speed 개의 packet이 반복된다.
//   각 packet은 32 bytes의 packet 헤더와 1280 bytes의 데이터로 구성된다.
//
// 이 프로그램에서는,
//   1초 분량의 Mark5B data(12800*Speed 개의 DataFrame)를 읽어서, 
//   1초 분량의 RVDB data(10만*Speed 개의 packets)를 기록하는 작업을 반복한다.
//
// 참고로,
// OCTA VDIF (with Capture Header) 파일은
//   vdif2vdif를 이용하여 Standard VDIF로 바꾸거나,
//   vdif2mk5b를 이용하여 **BEAD** style Mark5B로 바꿀 수 있다.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "stopwatch.c"
#include "calendar.c"
#include "mk5b.c"
#include "vdif.c"

//=========================================================================
int READ_mk5b(Mark5B_FRAMEHEADER *pHeader,Mark5B_FRAMEDATA *pData, int nDF, FILE*fp)
{
  int jDF,rDF;
  size_t n;

  //===== Clear buffer
  memset((void *)pHeader,0,nDF*LEN_MK5B_DF_HEADER);
  memset((void *)pData,  0,nDF*LEN_MK5B_DF_DATA);

  //===== Try to read "nDF" Mark5B DFs
  rDF = 0;
  for(jDF=0; jDF<nDF; jDF++) {
    n = fread((void *)(pHeader+jDF),sizeof(unsigned char),LEN_MK5B_DF_HEADER,fp);
    if(n != LEN_MK5B_DF_HEADER) {
      fprintf(stderr,"  Fail to read HEADER for jDF=%d : n=%lu\n",jDF,n);
      break;
    }//Header
    n = fread((void *)(pData+jDF),sizeof(unsigned char),LEN_MK5B_DF_DATA,fp);
    if(n != LEN_MK5B_DF_DATA) {
      fprintf(stderr,"  Fail to read DATA for jDF=%d : n=%lu\n",jDF,n);
      break;
    }//Data
    rDF++;
  }//jDF
  return(rDF);
}

//=========================================================================
// Mark5B DataFrame(Header & Data)을 nDF개 넘겨 받아, Header & Data의 점검 및 수정.
// 현재 헤더만 수정한다. (지금은 필요없지만) 인수 pData는 나중에 데이터 수정이 필요할 경우를 위해 보존.
// 
// mk5binfo->version=2, '**NULL style** KVN Mark5B recorder'이거나, 
//                  =3, 'Software using year2000,bcdDOY'이면,
//           version=1, '**BEAD style** KVN Mark5B recorder'로 헤더를 변경한다.
// 이미 mk5binfo->version=1, '**BEAD style** KVN Mark5B recorder'라면, 그냥 리턴.
// 혹시 mk5binfo->version=0, 'Unknown'이라면, 오류 메시지 출력하고 리턴.
//
// 마지막으로, 모든 경우에, pH->isTVG가 mk5binfo->isTVG와 동일하게 설정한다.
int FIX_mk5b(Mark5B_FRAMEHEADER *pHeader,Mark5B_FRAMEDATA *pData, int nDF, Mark5B_INFO *mk5binfo)
{
  Mark5B_FRAMEHEADER *pH;
  //Mark5B_FRAMEDATA   *pD;
  int iDF;
  int mjdn,bcdJJJ;

  //===== Header 수정
  if(mk5binfo->version == 3) {
    // 'Software using year2000,bcdDOY'로 만든 Mark5B_FRAMEHEADER에는 
    // pH->year2000에 year2000 값이, pH->bcdJJJ에 bcdDOY 값이 들어 있다.
    // 이로부터 MJDN을 구하고,하위 세자리를 BCD 코딩하여 pH->bcdJJJ에 넣어준다.
    // 한편, '**BEAD style** KVN Mark5B recorder'의 사용 예에 맞추기 위해 
    // pH->year20000에는 0xb를, pH->userdata에는 0xead를 넣어준다.
    for(iDF=0; iDF<nDF; iDF++) {
      pH = pHeader + iDF;
      mjdn = MJDN(2000+pH->year2000,1,iBCD(pH->bcdJJJ)-1); // "-1" for fixing the BUG, introduced by "OLD software"
      bcdJJJ = BCD(mjdn % 1000);
      pH->userdata = 0xead;
      pH->year2000 = 0xb;
      pH->bcdJJJ   = bcdJJJ;
      pH->crcc     = crcc(pH); // bcdJJJ,bcdSOD,bcdSOD2의 일부가 변경되었으므로, 재계산 필요.
    }//iDF

  }else if(mk5binfo->version == 2) {
    // '**NULL style** KVN Mark5B recorder'의 Mark5B_FRAMEHEADER에는 
    // pH->year2000에 0이, pH->bcdJJJ에 bcdJJJ 값이 들어 있다.
    // 따라서, mk5binfo->MJDepoch의 값을 신뢰한다면, 실제 MJDN을 구할 수 있다.
    // 그런데 pH->bcdJJJ에는 MJDN의 하위 세자리인 bcdJJJ 값이 들어 있으므로,
    // 그냥 그대로 두면 되고, mk5binfo->MJDepoch의 값이 타당한지 고민하지 않아도 된다.
    // 오직, pH->userdata과 pH->year2000을 'BEAD'로 변경하면 끝.
    for(iDF=0; iDF<nDF; iDF++) {
      pH = pHeader + iDF;
    //mjdn = mk5binfo->MJDepoch + iBCD(pH->bcdJJJ);
    //bcdJJJ = BCD(mjdn % 1000);
      pH->userdata = 0xead;
      pH->year2000 = 0xb;
    }//iDF

  }else if(mk5binfo->version == 1) {
    // Nothing to do for '**BEAD style** KVN Mark5B recorder'

  }else{
    // What to do for 'Unknown' ???
    static int cnt=1;
    if(cnt) {
      fprintf(stderr,"Warning: What shall be done for 'Unknown' to '**BEAD style** KVN Mark5B recorder' ?\n");
      cnt--;
    }
  }//mk5binfo->version

  //===== isTVG 설정
  for(iDF=0; iDF<nDF; iDF++) {
    pH = pHeader + iDF;
  //pD = pData   + iDF;

  //pH->sync     = 0xABADDEED;
  //pH->frameNum
    pH->isTVG    = mk5binfo->isTVG;
  //pH->userdata = 0xead;
  //pH->year2000 = 0xb;
  //pH->bcdSOD
  //pH->bcdJJJ   = bcdJJJ;
  //pH->bcdSOD2
  //pH->crcc     = crcc(pH);
  }//iDF

  return(nDF);
}

//=========================================================================
int WRITE_vdif(FILE*fo, Mark5B_FRAMEDATA *pData, int nDF, 
  unsigned int RefEpoch, unsigned int Secs, 
  int Log2Nchans,int NbitsM1,int ThreadID,int StationID,int DataLen_out)
{
  //   pData,nDF   : data to be written
  //   RefEpoch    : 2000/01/01부터 6개월 단위로 1씩 증가하는 Epoch, 짝수는 1월 1일, 홀수는 7월 1일
  //   Secs        : Epoch 이후의 경과 시간, 단위는 초
  //   Log2Nchans  : default=4,  or user-supplied
  //   NbitsM1     : default=1,  or user-supplied
  //   ThreadID    : no default, or user-supplied
  //   StationID   : no default, or user-supplied
  //   DataLen_out : default=OCTAVDIF_PKTS_DATA, or user-supplied (Not tested yet!)
  VDIF_FRAMEHEADER VH;
  unsigned char   *VD;
  unsigned int iPkt,nPkt;
  int mjdn, year,month,date, hh,mm,ss, doy, sod;

  // 1sec Data = 128 or 256 MB
  // = (nDF  =  12800 or  25600) * (LEN_MK5B_DF_DATA=10000)
  // = (nPkt = 100000 or 200000) * (OCTAVDIF_PKTS_DATA=1280)
  nPkt = (nDF*LEN_MK5B_DF_DATA)/DataLen_out;

  // create packet header
  memset(&VH,0,sizeof(VDIF_FRAMEHEADER));
  //                VH.invalid = 0;		// 0=valid,1=invalid
  //                VH.legacy = 0;		// 0=standard VDIF Data Frame Header (32bytes),
						// 1=legacy Header Length mode (16bytes)
                    VH.secs = Secs;		// Seconds from reference epoch
                    VH.refepoch = RefEpoch;	// Reference Epoch for secs count
  //                VH.framenum = framenum;	// Data Frame Number within second 
  //                VH.version = 0;		// VDIF version number (possible 7 future versions)
  if(0<=Log2Nchans) VH.log2nchans = Log2Nchans;	// log2(# of channels in Data Array)
                    VH.framelen = (OCTAVDIF_PKTS_HEADER + DataLen_out)/8;
						// Data Frame Length (including Header), in 8 bytes unit
  //                VH.datatype = 0;		// 0=real, 1=complex
  if(0<=NbitsM1)    VH.nbitsm1 = NbitsM1;	// #bits/sample - 1 (1 to 32 bits/sample)
  if(0<=ThreadID)   VH.threadid = ThreadID;	// Thread ID (0 to 1023)
  if(0<=StationID)  VH.stationid = StationID;	// Station ID (2 char ASCII, or numeric)
  //                VH.EDV  = ?;		// Extended Data Version, 0 or 1~255
  //                VH.EUD1 = ?;		// EXtended User Data 1, 0 if EDV=0
  //                VH.EUD2 = ?;		// EXtended User Data 2, 0 if EDV=0
  //                VH.EUD3 = ?;		// EXtended User Data 3, 0 if EDV=0
  //                VH.EUD4 = ?;		// EXtended User Data 4, 0 if EDV=0

  for(iPkt=0;iPkt<nPkt;iPkt++) {
    VH.framenum = iPkt;				// Data Frame Number within second 
    if(iPkt==0) { //===== 매 초마다 시각정보 출력
      static int once = 1;
      char s1[2]={'\r',0}, s2[2]={0,0};
      if(once) {
        printf("Generated VDIF_PKTS_HEADER at iPkt=%u.\n",iPkt);
        print_vdif_frameheader(&VH);
        printf("\nRefEpoch Secs FrameNum >>  mjdn year/mo/dy(doy) hh:mm:ss (sec.s)\n");
        s1[0] = 0; s2[0] = '\n';
        once--;
      }
      time_vdif_frameheader(&VH,&mjdn,&year,&month,&date,&hh,&mm,&ss,&doy,&sod);
      printf("%s%3d %9d %8d >> %5d %4d/%02d/%02d(%3d) %02d:%02d:%02d (%5d)%s",
        s1, VH.refepoch,VH.secs,VH.framenum, mjdn, year,month,date,doy, hh,mm,ss, sod, s2);
      fflush(stdout);
    }
    //===== PACKET HEADER + PACKET DATA 쓰기
    VD = ((unsigned char *)pData) + iPkt*DataLen_out;
    fwrite((void *)&VH,sizeof(char),VDIF_PKTS_HEADER,fo);
    fwrite((void *)VD,sizeof(char),DataLen_out,fo);
  }//iPkt
  return nPkt;
}


int main(int argc, char **argv)
{
  Mark5B_INFO *mk5binfo=NULL, mk5binfo_fixed;
  Mark5B_FRAMEHEADER *pHeader = NULL; // buffer for Header
  Mark5B_FRAMEDATA   *pData   = NULL; // buffer for Data
  int nDF;
  size_t bufsizHEAD,bufsizDATA;

  FILE *fi,*fo;

  int iDF, iSec;
  int start,length;
  int i,j,k;
  unsigned char *pc;

  // variables for set options : 
  //   default < 0  : "Don't change the original, unless user supplies a value." 
  //   0 <= default : "Force to change the original to this default or user supplied value."
  int Log2Nchans =  4; // 0,1,2,3,4 for Nchans=1,2,4,8,16
  int NbitsM1    =  1; // force to "2 bits/sample"
  int ThreadID   = -1; // 0 ~ 1023
  int StationID  = -1; // Two char. or 16-bits integer
  int FrameLen_out = VDIF_PKTS_HEADER+OCTAVDIF_PKTS_DATA; // default = 32+1280

  STOPWATCH *sw_all = alloc_stopwatch("Total       ",NULL);
  STOPWATCH *sw_rea = alloc_stopwatch("ReadData    ",NULL);
  STOPWATCH *sw_wri = alloc_stopwatch("WriteData   ",NULL);

  //============================================================================

  // Command Line Parameters
  for(i=1; i<argc; i++) { // pick-up the set options first
    char *eqs;
    if((eqs = index(argv[i],'=')) != NULL) {
      if(strncasecmp(argv[i],"Nchans",3)==0) {
        k = ilog2( atoi(eqs+1) );
        if(0<=k && k<=4) Log2Nchans = k;
        else fprintf(stderr,"Ignored the invalid set option, %s\n",argv[i]);
      }else if(strncasecmp(argv[i],"Nbits",3)==0) {
        k = atoi(eqs+1);
        if(1<=k && k<=32) NbitsM1 = k - 1;
        else fprintf(stderr,"Ignored the invalid set option, %s\n",argv[i]);
      }else if(strncasecmp(argv[i],"ThreadID",3)==0) {
        k = atoi(eqs+1);
        if(0<=k && k<=1023) ThreadID = k;
        else fprintf(stderr,"Ignored the invalid set option, %s\n",argv[i]);
      }else if(strncasecmp(argv[i],"StationID",3)==0) {
        if(strlen(eqs+1)>=2) { StationID = 0; memcpy((void *)&StationID,(void *)(eqs+1),2); }
        else fprintf(stderr,"Ignored the invalid set option, %s\n",argv[i]);
      }else if(strncasecmp(argv[i],"PacketSize",3)==0) {
        k = atoi(eqs+1);
        j = k - VDIF_PKTS_HEADER; // data length
        if(OCTAVDIF_PKTS_DATA<=j && (DATA_1SECOND/j)*j == DATA_1SECOND) FrameLen_out = k;
        else fprintf(stderr,"Ignored the invalid set option, %s\n",argv[i]);
      }else{
        fprintf(stderr,"Ignored the unknown set option, %s\n",argv[i]);
      }
      argv[i] = NULL;
    }
  }
  for(i=1; i<argc; i++) { // clean up
    if(argv[i]==NULL) {
      for(k=i+1; k<argc; k++) {
        if(argv[k]!=NULL) { argv[i] = argv[k]; argv[k] = NULL; break; }
      }
      if(k==argc) { argc = i; break; }
    }
  }
  if(argc < 2) {
    fprintf(stderr,"Usage: $ %s in.mk5b [out.vdif] [set options ...]\n",argv[0]);
    fprintf(stderr,
      "  where in.mk5b  = input, mark5b file\n"
      "        out.vdif = output, 'standard' vdif file\n"
      "  set options:\n"
      "    Nchans=#       Number of Channels, default=16 for C5 mode\n"
      "    Nbits=#        Number of bits per sample, default=2 for normal\n"
      "    ThreadID=#     Thread ID, 0~1023, no default.\n"
      "    StaionID=XX    Two char. Station ID, no default. Use KY for KVN-Yonsei\n"
      "    PacketSize=#   output VDIF packet data size, default=%u bytes\n"
      ,VDIF_PKTS_HEADER+OCTAVDIF_PKTS_DATA);
    exit(1);
  }

  //============================================================================
  // "Mark5B file" --> "Standard VDIF"

  //===== Init global variables with the given input file
  mk5binfo = mk5b_info("<<<",argv[1]);
  if(mk5binfo == NULL) exit(1);
  // Prepare a copy of mk5binfo which is needed when showing Fixed Header
  mk5binfo_fixed = *mk5binfo;

  //===== Prepare the buffers for header and data of 1 second
  nDF = mk5binfo->nDF_1s;
  bufsizHEAD = nDF * LEN_MK5B_DF_HEADER;
  bufsizDATA = nDF * LEN_MK5B_DF_DATA;
  if(posix_memalign((void **)&pHeader,getpagesize(),bufsizHEAD) != 0) {
    fprintf(stderr,"Fail to allocate memory for Header.\n");
    exit(1);
  }//pHeader
  //fprintf(stderr,"Buffer allocated for Header : %lu bytes\n",bufsizHEAD);
  if(posix_memalign((void **)&pData,getpagesize(),bufsizDATA) != 0) {
    fprintf(stderr,"Fail to allocate memory for Data.\n");
    exit(1);
  }//pData
  //fprintf(stderr,"Buffer allocated for Data : %lu bytes\n",bufsizDATA);
  //fprintf(stderr,"\n");

  // Exit here when no output file
  if(argc < 3) {
    exit(0);
  }

  //============================================================================

  //===== Show the value of set options : forced or user-supplied only are effective.
  if(0<=Log2Nchans) {
    fprintf(stderr,"set options: Nchans = 2^%d\n",Log2Nchans);
  }
  if(0<=NbitsM1) {
    fprintf(stderr,"set options: Nbits = %d bits/sample\n",NbitsM1+1);
  }
  if(0<=ThreadID) {
    fprintf(stderr,"set options: ThreadID = %d\n",ThreadID);
  }
  if(0<=StationID) {
    pc = (unsigned char *)&StationID;
    fprintf(stderr,"set options: StationID = %c%c (%d)\n",
      isprint(*pc)?*pc:'?', isprint(*(pc+1))?*(pc+1):'?', StationID);
  }
  fprintf(stderr,"\n");

  //============================================================================

  //===== Now, set the start position and length in DF unit.
  start  = 0;                                // from beginning
  length = mk5binfo->filesize / LEN_MK5B_DF; // to end
  if(length<1) length = 1;	// read one DF at least.

  //===== open the mk5b input file
  fi = fopen(mk5binfo->pathname,"r");
  fseek(fi,mk5binfo->offset0,SEEK_SET); // "첫 정초 위치"까지 이동
  fseek(fi,start*LEN_MK5B_DF,SEEK_CUR); // 지정된 시작 위치만큼 추가 이동

  //===== Create output file
  if((fo = fopen(argv[2],"w")) == NULL) {
    fprintf(stderr,"Fail to open '%s'\n",argv[2]);
    exit(1);
  }

  // mark5b 파일 읽기 :
  //   loop 마다,  1초 분량인 최대 mk5binfo->nDF_1s 개의 Data frame을 읽는다.
  //   EOF를 만나거나 오류가 발생하면, 중단한다.
  start_stopwatch(sw_all);
  iSec = 0;
  for(iDF=start; iDF<start+length; iDF+=nDF) {
    int mDF,rDF;   // 이번 loop에서 읽을 DF 개수, 실제 읽은 DF 개수
    int ePKT,wPKT; // expected and written # of packets
    int mjdn,y,m,d;
    unsigned int RefEpoch,Secs;
    int DataLen_out  = FrameLen_out - OCTAVDIF_PKTS_HEADER;

    mDF = length - iSec*nDF;  // 앞으로 읽어야 할 전체 DF수
    if(mDF > nDF) mDF = nDF;  // loop마다 최대 nDF까지만 읽음.
    //fprintf(stderr,"iSec=%d : jDF = %d~%d (%d DFs)\n",iSec, iDF,iDF+mDF-1, mDF);

    //===== iDF번째 DF부터 mDF개의 DF를 읽자 : 리턴값= 실제 읽은 개수, mDF-리턴값= 읽지 못한 개수
    start_stopwatch(sw_rea);
    rDF = READ_mk5b(pHeader,pData,mDF, fi);
    if(rDF != mDF) {
      fprintf(stderr,"iSec=%d : Only %d/%d DFs were read.\n",iSec,rDF,mDF); 
    }
    stop_stopwatch(sw_rea);

    //===== Header & Data 점검 및 수정(헤더 구조를 version 1으로 변경함)
    FIX_mk5b(pHeader,pData,mDF, mk5binfo);
    mk5binfo_fixed.version = 1; // forced to version 1.

    //===== mDF개의 DF를 VDIF packet으로 변환하여 쓴다.
    // Calc time (RefEpoch and Secs), from the 1st Mark5B Frame Header
    mjdn = mk5binfo_fixed.MJDepoch + iBCD(pHeader->bcdJJJ); // JJJJJ
    MJDN2CDN(mjdn,&y,&m,&d);
    RefEpoch = (y-2000)*2;
    if(m<7) {
      Secs = (mjdn - MJDN(y,1,1))*86400;
    }else{
      RefEpoch += 1;
      Secs = (mjdn - MJDN(y,7,1))*86400;
    }
    Secs += iBCD(pHeader->bcdSOD); // Add the Seconds of Day
    // Write it
    //   pData,nDF   : data to be written
    //   RefEpoch    : 2000/01/01부터 6개월 단위로 1씩 증가하는 Epoch, 짝수는 1월 1일, 홀수는 7월 1일
    //   Secs        : Epoch 이후의 경과 시간, 단위는 초
    //   Log2Nchans  : default=4,  or user-supplied
    //   NbitsM1     : default=1,  or user-supplied
    //   ThreadID    : no default, or user-supplied
    //   StationID   : no default, or user-supplied
    //   DataLen_out : default=OCTAVDIF_PKTS_DATA, or user-supplied
    start_stopwatch(sw_wri);
    ePKT = (nDF*LEN_MK5B_DF_DATA)/DataLen_out;
    wPKT = WRITE_vdif(fo, pData,mDF, RefEpoch,Secs,
           Log2Nchans,NbitsM1,ThreadID,StationID, DataLen_out);
    if(wPKT != ePKT) {
      fprintf(stderr,"iSec=%d : Only %d/%d PKTs were written.\n",iSec,wPKT,ePKT); 
    }
    stop_stopwatch(sw_wri);

    iSec++;
  }//iSec
  stop_stopwatch(sw_all);
  fprintf(stderr,"\nProcessed for %d seconds of Data.\n",iSec);

  // Stopwatch
  report_stopwatch(sw_rea); free_stopwatch(sw_rea);
  report_stopwatch(sw_wri); free_stopwatch(sw_wri);
  report_stopwatch(sw_all); free_stopwatch(sw_all);

  //===== Done.
  fclose(fi);
  fclose(fo);
  free(pData);
  free(pHeader);

  //===== Show info about the created vdif file
  vdif_info(">>>",argv[2]);

  return(0);
}

