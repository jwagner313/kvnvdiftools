// vdif.c

#include "vdif.h"
#include "calendar.h"

// bit swap table for converting OCTA vdif to standard vdif
unsigned char bit_swap_tbl[256] = {
    0x00, 0x80, 0x40, 0xc0, 0x20, 0xa0, 0x60, 0xe0, 
    0x10, 0x90, 0x50, 0xd0, 0x30, 0xb0, 0x70, 0xf0, 
    0x08, 0x88, 0x48, 0xc8, 0x28, 0xa8, 0x68, 0xe8, 
    0x18, 0x98, 0x58, 0xd8, 0x38, 0xb8, 0x78, 0xf8, 
    0x04, 0x84, 0x44, 0xc4, 0x24, 0xa4, 0x64, 0xe4, 
    0x14, 0x94, 0x54, 0xd4, 0x34, 0xb4, 0x74, 0xf4, 
    0x0c, 0x8c, 0x4c, 0xcc, 0x2c, 0xac, 0x6c, 0xec, 
    0x1c, 0x9c, 0x5c, 0xdc, 0x3c, 0xbc, 0x7c, 0xfc, 
    0x02, 0x82, 0x42, 0xc2, 0x22, 0xa2, 0x62, 0xe2, 
    0x12, 0x92, 0x52, 0xd2, 0x32, 0xb2, 0x72, 0xf2, 
    0x0a, 0x8a, 0x4a, 0xca, 0x2a, 0xaa, 0x6a, 0xea, 
    0x1a, 0x9a, 0x5a, 0xda, 0x3a, 0xba, 0x7a, 0xfa, 
    0x06, 0x86, 0x46, 0xc6, 0x26, 0xa6, 0x66, 0xe6, 
    0x16, 0x96, 0x56, 0xd6, 0x36, 0xb6, 0x76, 0xf6, 
    0x0e, 0x8e, 0x4e, 0xce, 0x2e, 0xae, 0x6e, 0xee, 
    0x1e, 0x9e, 0x5e, 0xde, 0x3e, 0xbe, 0x7e, 0xfe, 
    0x01, 0x81, 0x41, 0xc1, 0x21, 0xa1, 0x61, 0xe1, 
    0x11, 0x91, 0x51, 0xd1, 0x31, 0xb1, 0x71, 0xf1, 
    0x09, 0x89, 0x49, 0xc9, 0x29, 0xa9, 0x69, 0xe9, 
    0x19, 0x99, 0x59, 0xd9, 0x39, 0xb9, 0x79, 0xf9, 
    0x05, 0x85, 0x45, 0xc5, 0x25, 0xa5, 0x65, 0xe5, 
    0x15, 0x95, 0x55, 0xd5, 0x35, 0xb5, 0x75, 0xf5, 
    0x0d, 0x8d, 0x4d, 0xcd, 0x2d, 0xad, 0x6d, 0xed, 
    0x1d, 0x9d, 0x5d, 0xdd, 0x3d, 0xbd, 0x7d, 0xfd, 
    0x03, 0x83, 0x43, 0xc3, 0x23, 0xa3, 0x63, 0xe3, 
    0x13, 0x93, 0x53, 0xd3, 0x33, 0xb3, 0x73, 0xf3, 
    0x0b, 0x8b, 0x4b, 0xcb, 0x2b, 0xab, 0x6b, 0xeb, 
    0x1b, 0x9b, 0x5b, 0xdb, 0x3b, 0xbb, 0x7b, 0xfb, 
    0x07, 0x87, 0x47, 0xc7, 0x27, 0xa7, 0x67, 0xe7, 
    0x17, 0x97, 0x57, 0xd7, 0x37, 0xb7, 0x77, 0xf7, 
    0x0f, 0x8f, 0x4f, 0xcf, 0x2f, 0xaf, 0x6f, 0xef, 
    0x1f, 0x9f, 0x5f, 0xdf, 0x3f, 0xbf, 0x7f, 0xff, 
};//bit_swap_tbl


void print_octavdif_frameheader(void *p)
{
  OCTA_VDIF_FRAMEHEADER *vh = (OCTA_VDIF_FRAMEHEADER *)p;
  printf("\tw0 0x%08x : invalid=%u secs=%u\n", vh->word0, vh->invalid,vh->secs);
  printf("\tw1 0x%08x : refepoch=%u framenum=%u\n", vh->word1, vh->refepoch,vh->framenum);
  printf("\tw2 0x%08x : framelen=%u\n", vh->word2, vh->framelen);
  printf("\tw3 0x%08x : nbitsm1=%u threadid=%u\n", vh->word3, vh->nbitsm1,vh->threadid);
  printf("\tw4 0x%08x : effbitn=%u sampling=%u split=%u timevalid=%u refepochSRC=%u\n", 
    vh->word4, vh->effbitn,vh->sampling,vh->split,vh->timevalid,vh->refepochSRC);
  printf("\tw5 0x%08x : secsSRC=%u\n", vh->word5, vh->secsSRC);
  printf("\tw6 0x%08x : count=%u\n", vh->word6, vh->count);
  printf("\tw7 0x%08x : \n", vh->word7);
}

void print_vdif_frameheader(void *p)
{
  VDIF_FRAMEHEADER *vh = (VDIF_FRAMEHEADER *)p;
  printf("\tw0 0x%08x : invalid=%u legacy=%u secs=%u\n", vh->word0, vh->invalid,vh->legacy,vh->secs);
  printf("\tw1 0x%08x : refepoch=%u framenum=%u\n", vh->word1, vh->refepoch,vh->framenum);
  printf("\tw2 0x%08x : version=%u log2nchans=%u framelen=%u\n",
    vh->word2, vh->version,vh->log2nchans,vh->framelen);
  printf("\tw3 0x%08x : datatype=%u nbitsm1=%u threadid=%u sationid=%u\n",
    vh->word3, vh->datatype,vh->nbitsm1,vh->threadid,vh->stationid);
  printf("\tw4 0x%08x : EDV=%u EUD1=%u\n", vh->word4, vh->EDV,vh->EUD1);
  printf("\tw5 0x%08x : EUD2=%u\n", vh->word5, vh->EUD2);
  printf("\tw6 0x%08x : EUD3=%u\n", vh->word6, vh->EUD3);
  printf("\tw7 0x%08x : EUD4=%u\n", vh->word7, vh->EUD4);
}

void time_vdif_frameheader(void *p,
  int *mjdn, int *y,int *m,int *d, int *hh,int *mm,int *ss, int *doy,int *sod)
{
  VDIF_FRAMEHEADER *vh = (VDIF_FRAMEHEADER *)p;
  int y2000, month_epoch, days;
  // vh->refepoch = 2000/01/01부터 6개월 단위로 1씩 증가하는 Epoch, 짝수는 1월 1일, 홀수는 7월 1일
  // vh->secs     = Epoch 이후의 경과 시간, 단위는 초
  // vh->framenum = 1초 영역 안에서 0부터 증가하는 data frame 번호, iPkt=0일 때 반드시 0이라야 함.
  y2000 = vh->refepoch/2;
  month_epoch = (vh->refepoch%2 == 0) ? 1 : 7;
  days = vh->secs/86400;
  //
  *sod  = vh->secs%86400;
  *mjdn = MJDN(2000+y2000, month_epoch, 1+days); // "1+days" is equal to "Date".
  MJDN2CDN(*mjdn, y,m,d);
  *doy = DOY(*y,*m,*d);
  *hh = *sod/3600; *ss = *sod%3600; *mm = *ss/60; *ss %= 60;
}

void vdif_HeaderTime(VDIF_INFO *vdifinfo, void *pH, char *memo)
{
  VDIF_FRAMEHEADER VH; // header copied for the time tag
                       // Because BE-to-LE needed when version 2 or 1
  unsigned int k, *pi;
  int y2000,month_epoch,days, mjdn,y,m,d, hh,mm,ss, doy,sod;

  static int first=1;
  memcpy(&VH,pH,sizeof(VH));
  switch(vdifinfo->version) {
  case 2: //'OCTA Capure vdif'
  case 1: //'OCTA vdif'
    pi = (unsigned int *)&VH;
    for(k=0;k<8;k++) pi[k] = ntohl( pi[k] ); // BE-to-LE로 endian 변환
    break;
  case 0: //'Standard vdif'
  default:
    break;
  }
  //
  y2000 = VH.refepoch/2;
  month_epoch = (VH.refepoch%2 == 0) ? 1 : 7;
  days = VH.secs/86400;
  //
  sod  = VH.secs%86400;
  mjdn = MJDN(2000+y2000, month_epoch, 1+days); // "1+days" is equal to "Date".
  MJDN2CDN(mjdn, &y,&m,&d);
  doy = DOY(y,m,d);
  hh = sod/3600; ss = sod%3600; mm = ss/60; ss %= 60;
  //
  if(first) fprintf(stderr,"RefEpoch Secs FrameNum >>  mjdn year/mo/dy(doy) hh:mm:ss (sec.s)\n");
  fprintf(stderr,"\r%3d %9d %8d >> %5d %4d/%02d/%02d(%3d) %02d:%02d:%02d (%5d) : %s",
    VH.refepoch,VH.secs,VH.framenum, mjdn, y,m,d,doy, hh,mm,ss, sod, memo);
  if(first) { fprintf(stderr,"\n"); first = 0; }
}

// ilog2(n) -- return an integer log, base 2
int ilog2(int n) 
{
  int i;
  for (i=8*sizeof(int)-1; i>=0 && ((1<<i) & n)==0; i--);
  return i;
}

//=========================================================================
// vdif 파일의 입출력 범위를 지정할 때,  Packet 단위 뿐만 아니라
// Sec 단위 또는 Byte 단위로도 입력할 수 있도록 하기 위하여 사용한다.
// Available Units : "packet", "sec" or "byte"
//    (1) "sec" or "byte" 이외의 경우, "packet" 단위로 간주한다.
//    (2) "sec" or "byte"의 경우, vdifinfo->nPkt_1s or vdifinfo->FrameLen의 값이  
//        필요하므로,  반드시 vdif_info() 함수를 호출한 다음, 사용하도록 한다.
// #s ==> # * vdifinfo->nPkt_1s  [packetNum]
// #b ==> # / vdifinfo->FrameLen [packetNum]
int vdif_PKTunit(VDIF_INFO *vdifinfo, char *s)
{
  int v = 0;
  if(s) {
    v = atoi( s );
    while(*s == '+' || *s == '-' || isdigit(*s)) s++;
    if(*s == 's' || *s == 'S') v *= vdifinfo->nPkt_1s;
    else if(*s == 'b' || *s == 'B') v /= vdifinfo->FrameLen;
  }
  return( v ); // [frameNum]
}

VDIF_INFO *vdif_info(char *prefix, char *fn)
{
  VDIF_INFO *vdifinfo=NULL;
  struct stat sb;
  FILE *fi;
  off_t nPkt,nSec,mPkt,nByte;
  char *p;
  //============================================================================
  // read buffer
#if 0
  unsigned char buf[ OCTACAPTURE_FILE_HEADER //  256
                    +OCTACAPTURE_SECD_HEADER // 1428
                    +OCTAVDIF_PKTS_HEADER    //   32
                    +OCTAVDIF_PKTS_DATA      // 1280
                ];                           // 2996 bytes in total
  CAPT_FILE_HEADER      *CF = (CAPT_FILE_HEADER      *)(buf);
  CAPT_SECD_HEADER      *CS = (CAPT_SECD_HEADER      *)(buf+OCTACAPTURE_FILE_HEADER);
  OCTA_VDIF_FRAMEHEADER *VH = (OCTA_VDIF_FRAMEHEADER *)(buf+OCTACAPTURE_FILE_HEADER+OCTACAPTURE_SECD_HEADER);
//OCTA_VDIF_DATA        *VD = (OCTA_VDIF_DATA        *)(buf+OCTACAPTURE_FILE_HEADER+OCTACAPTURE_SECD_HEADER+OCTAVDIF_PKTS_HEADER);
#endif
  CAPT_FILE_HEADER      CF; //  256
  CAPT_SECD_HEADER      CS; // 1428
  OCTA_VDIF_FRAMEHEADER VH; //   32
  //============================================================================
  unsigned int  *pi;
  size_t n;
  int k, iPkt;
  int mjdn, year,month,date, hh,mm,ss, doy, sod;
  off_t offset;

  //local function: iSec 초, iPkt 째 Header (32bytes)를 읽음
  //  offset 계산에 쓰이는 vdifinfo->FrameLen, vdifinfo->speed가 정해진 뒤에 사용할 것.
  //  (아직 speed가 정해지지 않았더라도, iSec=0인 동안에는 speed=1로 가정하고 사용해도 같은 결과를 얻는다.)
  //  또한, OCTA vdif 파일을 지원하기 위하여, vdifinfo->version도 참조하므로 주의할 것.
  int read_header(FILE *fi,off_t iSec,int iPkt,VDIF_FRAMEHEADER *H)
  {
    off_t offset,nPkt;
    nPkt = 128000000L/(vdifinfo->FrameLen - VDIF_PKTS_HEADER); // # of packets/sec @ 1 Gbps
    offset = (iSec*nPkt*vdifinfo->speed + iPkt) * vdifinfo->FrameLen;
    if(vdifinfo->version==2) {
      offset += OCTACAPTURE_FILE_HEADER + (iSec+1)*OCTACAPTURE_SECD_HEADER;
    }
    if(fseek(fi,offset,SEEK_SET) == -1) {
      fprintf(stderr,"%s Unable to fseek to the vdif packet (iPkt=%d).\n",prefix,iPkt);
      return(1);
    }//fseek
    if(fread(H,sizeof(char),OCTAVDIF_PKTS_HEADER,fi) != OCTAVDIF_PKTS_HEADER) {
      fprintf(stderr,"%s Unable to read the VDIF Frame Header (iPkt=%d).\n",prefix,iPkt);
      return(2);
    }//fread
    if(vdifinfo->version==2 || vdifinfo->version==1) {
      // special for OCTA VDIF PKTS HEADER (It is BE.)
      pi = (unsigned int *)&VH;
      for(k=0;k<8;k++) pi[k] = ntohl( pi[k] ); // BE-to-LE로 endian 변환
    }
    return(0);
  }//read_header

  fprintf(stderr,"%s================================================================\n",prefix);

  //vdifinfo용 메모리 확보 ==> 프로그램이 종료될 때까지 해제하지 않을 것임.
  if((vdifinfo = calloc(1,sizeof(VDIF_INFO))) == NULL) {
    fprintf(stderr,"%s Unable to calloc() for VDIF_INFO.\n",prefix);
    return(NULL);
  }//vdifinfo

  //vdifinfo->pathname
  //vdifinfo->filename
  vdifinfo->pathname = fn;
  vdifinfo->filename = (p = strrchr(fn,'/')) == NULL ? fn : p+1;
  fprintf(stderr,"%s Filename : %s\n",prefix,vdifinfo->filename);

  //vdifinfo->filesize
  if(stat(fn,&sb) == -1) {
    fprintf(stderr,"%s Fail to stat '%s'\n",prefix,fn);
    return(NULL);
  }//stat
  vdifinfo->filesize = sb.st_size; // filesize in bytes

  //vdifinfo->offset0  : 첫 정초 packet의 위치
  //vdifinfo->speed    : 1 or 2 Gbps
  //vdifinfo->isTVG    : OCTA Capture VDIF의 파일헤더에서 얻은 isTVG 값
  //vdifinfo->FrameLen : Frame Length [bytes]
  //vdifinfo->version  : 0=Standard VDIF,
  //                     1=OCTA VDIF,
  //                     2=OCTA VDIF with Capture Header
  vdifinfo->offset0 = 0;
  vdifinfo->speed = 1;	// speed를 확정짓기 전(초기값 0으로 설정되어 있었음), 첫 정초 packet을 발견하였을 때, 
			// FirstTic 정보출력을 위해 vdif_time1(vdifinfo,&H)를 호출하므로,
			// "devide by zero" 발생함. 정초의 경우 frameNum=0이므로 speed와 무관하게 항상 0임.
  vdifinfo->isTVG = 0;
  vdifinfo->version = -1;// Unknown

  if((fi = fopen(fn,"r")) == NULL) {
    fprintf(stderr,"%s Fail to open '%s'\n",prefix,fn);
    return(NULL);
  }//fopen

  //===== input vdif file의 종류를 판단하기
  //----- version 검사를 위해, CF 만큼 읽는다.
  //printf("Before read CF, at %lu\n", ftell(fi));
  if((n = fread(&CF,sizeof(char),sizeof(CF),fi)) != sizeof(CF)) {
    fprintf(stderr,"%s Fail to read. n=%lu/%lu\n",prefix,n,sizeof(CF));
    return(NULL);
  }
  //printf("After  read CF, at %lu\n", ftell(fi));

  p = (char *)&CF;
  if(strncmp(p,"OCTA",4) == 0) {
    //----- CF 에 OCTA CAPTURE FILE HEADER가 있다면,
    //      OCTA CAPTURE FILE이자 OCTA VDIF FILE이다. ===> Type 2 = octa capture
    vdifinfo->version = 2;
    fprintf(stderr,"%s Version  : 'OCTA VDIF FILE with Capture Header'\n",prefix);

    //----- isTVG 검사를 위해, CS 만큼 읽는다.
    if((n = fread(&CS,sizeof(char),sizeof(CS),fi)) != sizeof(CS)) {
      fprintf(stderr,"%s Fail to read. n=%lu/%lu\n",prefix,n,sizeof(CS));
      return(NULL);
    }
    // special for OCTA CAPTURE FILE : isTVG flag from OCTA SECD HEADER
    p = (char *)&CS;
    if(strncmp(p+20,"MARK",4)==0) {
      vdifinfo->isTVG = 0; // Data came from Mark5.
      fprintf(stderr,"%s           isTVG = %d\n",prefix,vdifinfo->isTVG);
    }else if(strncmp(p+20,"VSIT",4)==0) {
      vdifinfo->isTVG = 1; // Data came from VSI TVG hardware (?).
      fprintf(stderr,"%s           isTVG = %d\n",prefix,vdifinfo->isTVG);
    }else{
      fprintf(stderr,"%s\t",prefix);
      for(k=20;k<24;k++) fprintf(stderr,isprint(p[k])?"'%c' ":"0x%02x ",p[k]); 
      fprintf(stderr," : Unknown OCTACAPTURE_SECD_HEADER.\n");
    }

    //----- Header 검사를 위해, VH 만큼 읽는다.
    if((n = fread(&VH,sizeof(char),sizeof(VH),fi)) != sizeof(VH)) {
      fprintf(stderr,"%s Fail to read. n=%lu/%lu\n",prefix,n,sizeof(VH));
      return(NULL);
    }
    // special for OCTA VDIF PKTS HEADER (It is BE.)
    pi = (unsigned int *)&VH;
    for(k=0;k<8;k++) pi[k] = ntohl( pi[k] ); // BE-to-LE로 endian 변환

  }else{
    //----- 그렇지 않다면, CF 에 (OCTA/Standard) VDIF PKTS HEADER가 있을 것이다.
    memcpy(&VH,&CF,sizeof(VH)); 

    //      이 헤더가 [BigEndian]이면, OCTA VDIF FILE이다. ===> Type 1
    //      이 헤더가 [LittleEndian]이면, Standard VDIF FILE이다. ===> Type 0
    //
    // How to check the endian?
    //   vdif header의 Word 1은 아래의 세 필드로 구성되어 있다.
    //     unsigned int framenum:24;  // Data Frame Number within second 
    //     unsigned int refepoch:6;   // Reference Epoch for secs count
    //     unsigned int :2;           // un-assigned 2 bits, should be set to '00'
    //   여기서, 첫 패킷의 framenum는 반드시 0일 것이고, 
    //   refepoch는 (2000년 상반기의 관측자료가 아니라면) 0이 아닌 어떤 값일 것이다. 
    //   따라서, 읽은 그대로인 상태에서 위 조건에 맞으면, [LittleEndian]으로 간주 ===> Type 0 = standard vdif
    //   BE-to-LE로 endian 변환 후 위 조건에 맞으면, [BigEndian]으로 간주 ===> Type 1 = octa vdif
    if((VH.framenum == 0) && (VH.refepoch != 0)) {
      // Seems to be LE.
      vdifinfo->version = 0;
      vdifinfo->FrameLen = VH.framelen*8; // unit=8byte if Standard VDIF file
      fprintf(stderr,"%s Version  : 'Standard VDIF FILE' with FrameLen=%u bytes\n",prefix,vdifinfo->FrameLen);

    }else{
      // Seems to be BE.
      pi = (unsigned int *)&VH;
      for(k=0;k<8;k++) pi[k] = ntohl( pi[k] ); // BE-to-LE로 endian 변환

      // Check again
      if((VH.framenum == 0) && (VH.refepoch != 0)) {
        // Seems to be OK after BE-to-LE.
        vdifinfo->version = 1;
        fprintf(stderr,"%s Version  : 'OCTA VDIF FILE' ",prefix);
        if(VH.framelen == OCTAVDIF_PKTS_HEADER+OCTAVDIF_PKTS_DATA) {
          vdifinfo->FrameLen = VH.framelen; // unit=byte if OCTA VDIF file 
          fprintf(stderr,"with FrameLen=%u bytes\n",VH.framelen);
        }else if(VH.framelen == (OCTAVDIF_PKTS_HEADER+OCTAVDIF_PKTS_DATA)/8) {
          vdifinfo->FrameLen = VH.framelen*8; // unit=8byte if (some 2Gbps) OCTA VDIF file 
          fprintf(stderr,"with FrameLen=%u*8 bytes\n",VH.framelen);
        }else{
          vdifinfo->FrameLen = VH.framelen; // unit=byte ???
          fprintf(stderr,"with unexpected FrameLen=%u bytes\n",VH.framelen);
        }
      }else{
        fprintf(stderr,"%s Version  : Unknown. Could not determine whether a vdif file or not.\n",prefix);
        fprintf(stderr,"%s   (framelen in header = %u)\n",prefix,VH.framelen);
#if 1
        return(NULL);
#else
        if(1) {
        char ans[16];
        fprintf(stderr,"%s Do you want to continue, by ASSUMING as 'OCTA VDIF FILE'? (yes/no) : ",prefix);
        fgets(ans,sizeof(ans),stdin);
        if(strncasecmp(ans,"yes",1)!=0) exit(1);
        }
        fprintf(stderr,"%s Version  : Unknown. Assumed as 'OCTA VDIF FILE'\n",prefix);
        vdifinfo->version = 1; // Assumed
        vdifinfo->FrameLen = OCTAVDIF_PKTS_HEADER+OCTAVDIF_PKTS_DATA; // unit=byte if OCTA VDIF file 
#endif
      }
    }
  }
  //===== vdifinfo->FrameLen has been determined.

  // VH 에 (LE로 변환된 OCTA) VDIF PKTS HEADER가 있다. --> 시작시각을 표시
  time_vdif_frameheader(&VH,&mjdn,&year,&month,&date,&hh,&mm,&ss,&doy,&sod);
  fprintf(stderr,"%s             mjdn year/mo/dy(doy) hh:mm:ss (sec.s)\n",prefix);
  fprintf(stderr,"%s FirstTic : %5d %4d/%02d/%02d(%3d) %02d:%02d:%02d (%5d)\n",prefix,
    mjdn, year,month,date,doy, hh,mm,ss, sod);

  // Confirm if the # of packets within 1 seconds for input is proper.
  // nPkt will be 100000 when FrameLen=OCTAVDIF_PKTS_HEADER+OCTAVDIF_PKTS_DATA=1312,
  // or some proper integer value.
  nPkt  = DATA_1SECOND / (vdifinfo->FrameLen  - VDIF_PKTS_HEADER); // # of packets/sec @ 1Gbps
  if(nPkt*(vdifinfo->FrameLen - VDIF_PKTS_HEADER) != DATA_1SECOND) {
    double x = DATA_1SECOND / ((double)vdifinfo->FrameLen - VDIF_PKTS_HEADER);
    fprintf(stderr,"%s FrameLen of input file    = %d bytes\n",prefix,vdifinfo->FrameLen);
    fprintf(stderr,"%s # of packets/sec (@1Gbps) = %f : Something wrong!\n",prefix,x);
    return(NULL);
  }

  //===== Data rate를 판별하기
  // When HeaderSize=OCTAVDIF_PKTS_HEADER=32, DataSize=OCTAVDIF_PKTS_DATA=1280,
  //   nPkt = 100000, FrameLen=HeaderSize+DataSize.
  //   "nPkt*FrameLen" is equal to the whole data size for 1 sec @ 1Gbps.
  // 1Gbps 시의 1초 분량 다음의 첫 패킷을 읽어서, framenum을 검사하면 알 수 있다.
  // framenum이 0이면 1Gbps이고, nPkt 또는 그 이상(packet loss 발생시)이면 2Gbps이다.
  //   Mark5B 파일의 경우, 1/100 초 정도 건너 띈 위치의 DF를 읽고,  
  //   헤더의 frameNum과 SOD2의 관계를 잘 이용하여 기록 속도를 판별할 수 있었다. 
  //   그러나, vdif 파일의 경우에는 오직 framenum만 있고 SOD2에 해당하는 정보가 없으므로,
  //   오로지 1Gbps 시의 1초 분량 직후의 패킷을 읽고, 그 framenum 값을 확인해야 한다.
  //   파일 크기가 "1Gbps 시의 1초 분량" 이하라면, 아래 로직에서는 읽기 오류가 발생하므로 
  //   이럴 경우에는 기록 속도가 1Gbps라고 추정하고, "Data rate를 판별하기"를 끝내기로 한다.
  vdifinfo->speed = 1; // Assumed as 1Gbps
  if(vdifinfo->version == 2) {
    // OCTA Capture file이라면, CF + CS + nPkt*vdifinfo->FrameLen 다음에
    //   1Gbps의 경우, 2번째 CS 가 나올 것이고,
    //   2Gbps의 경우, nPkt+1 번째 packet 이 나올 것이다.
    //----- 이동
    offset = OCTACAPTURE_FILE_HEADER + OCTACAPTURE_SECD_HEADER 
             + nPkt*vdifinfo->FrameLen;
    fseek(fi,offset,SEEK_SET);
    //----- CS 만큼 읽는다.
    if((n = fread(&CS,sizeof(char),sizeof(CS),fi)) != sizeof(CS)) {
      //fprintf(stderr,"%s --------   Too short file! Continue by assuming 1Gbps.\n",prefix);
      goto end_of_check_2;
    }
    // special for OCTA CAPTURE FILE : isTVG flag from OCTA SECD HEADER
    p = (char *)&CS;
    if(strncmp(p+20,"MARK",4)==0 || strncmp(p+20,"VSIT",4)==0) {
      // Yes, it seems to be 2nd CS. Therefore this file is 1Gbps.
      vdifinfo->speed = 1; // 1Gbps
      // Read the 1st packet header for checking framenum.
      //----- VH 만큼 읽는다.
      if((n = fread(&VH,sizeof(char),sizeof(VH),fi)) != sizeof(VH)) {
        //fprintf(stderr,"%s --------   Too short file! Continue by assuming 1Gbps.\n",prefix);
        goto end_of_check_2;
      }
      // special for OCTA VDIF PKTS HEADER (It is BE.)
      pi = (unsigned int *)&VH;
      for(k=0;k<8;k++) pi[k] = ntohl( pi[k] ); // BE-to-LE로 endian 변환
      // This "VH" should have the 1st packet of 2nd second period.
      if(VH.framenum != 0) fprintf(stderr,"%s\tSomething wrong while checking Daterate : case A : VH.framenum=%u\n",prefix,VH.framenum);
    }else{
      // No, it is NOT a CS. Therefore this file is 2Gbps.
      vdifinfo->speed = 2; // 2Gbps
      // "32 bytes of CS" must be the packet header.
      memcpy(&VH,&CS,sizeof(VH));
      // special for OCTA VDIF PKTS HEADER (It is BE.)
      pi = (unsigned int *)&VH;
      for(k=0;k<8;k++) pi[k] = ntohl( pi[k] ); // BE-to-LE로 endian 변환
      // This "VH" should have the nPkt+1 -th packet header of 1st second period.
      if(VH.framenum < nPkt) fprintf(stderr,"%s\tSomething wrong while checking Daterate : case B : VH.framenum=%u\n",prefix,VH.framenum);
    }
    end_of_check_2:
    ;
  }else{ // vdifinfo->version == 1 or 0
    // OCTA vdif file 또는 Standard vdif file 이라면, nPkt*vdifinfo->FrameLen 다음에
    //   1Gbps의 경우, 2번째 초의 1 번째 packet 이 나올 것이고,
    //   2Gbps의 경우, nPkt+1 번째 packet 이 나올 것이다.
    //----- 이동
    offset = nPkt*vdifinfo->FrameLen;
    fseek(fi,offset,SEEK_SET);
    //----- VH 만큼 읽는다.
    if((n = fread(&VH,sizeof(char),sizeof(VH),fi)) != sizeof(VH)) {
      //fprintf(stderr,"%s --------   Too short file! Continue by assuming 1Gbps.\n",prefix);
      goto end_of_check_1;
    }
    // special for OCTA VDIF PKTS HEADER (It is BE.)
    if(vdifinfo->version == 1) {
      pi = (unsigned int *)&VH;
      for(k=0;k<8;k++) pi[k] = ntohl( pi[k] ); // BE-to-LE로 endian 변환
    }
    // Check the framenum
    if(VH.framenum == 0) {
      vdifinfo->speed = 1; // 1Gbps
    }else if(nPkt <= VH.framenum) {
      vdifinfo->speed = 2; // 2Gbps
    }else{
      fprintf(stderr,"%s\tSomething wrong while checking Daterate : case C : VH.framenum=%u\n",prefix,VH.framenum);
    }
    end_of_check_1:
    ;
  }
  //print_octavdif_frameheader((void*)&VH);
  //print_vdif_frameheader((void*)&VH);
  //===== vdifinfo->speed has been determined.

  // Show LastTic
  mPkt = nPkt * vdifinfo->speed; // # of packets/sec @ "speed" Gbps
  nSec = vdifinfo->filesize / vdifinfo->FrameLen / mPkt; // file length in sec
  for(iPkt=0;;iPkt++) { // framenum이 0인 packet를 찾자.
    if(read_header(fi,nSec-1,iPkt,(VDIF_FRAMEHEADER *)&VH) != 0) {
      fprintf(stderr,"%s Invalid vdif packet header at iSec=%ld iPkt=%d of '%s'\n",prefix,nSec-1,iPkt,fn);
      break;
    }//read_header
    if(VH.framenum == 0) {
      time_vdif_frameheader(&VH,&mjdn,&year,&month,&date,&hh,&mm,&ss,&doy,&sod);
      fprintf(stderr,"%s  LastTic : %5d %4d/%02d/%02d(%3d) %02d:%02d:%02d (%5d)\n",prefix,
        mjdn, year,month,date,doy, hh,mm,ss, sod);
      break;
    }
  }//iPkt

  fclose(fi);

  fprintf(stderr,"%s Datarate : %dGbps (VSI-%dMHz)",prefix,vdifinfo->speed,32*vdifinfo->speed);
  //vdifinfo->nPkt_1s : 초당 packet의 갯수   : vdifinfo->nPkt_1s [packets/s]
  //vdif                초당 header의 총 용량 : vdifinfo->nPkt_1s * VDIF_PKTS_HEADER
  //vdif                초당 data의 총 용량   : vdifinfo->nPkt_1s * (FrameLen-VDIF_PKTS_HEADER) = 128 or 256 MB/s
  vdifinfo->nPkt_1s      = nPkt * vdifinfo->speed;
  fprintf(stderr," ==> %d Packets/s (where %d bytes/packet)\n",vdifinfo->nPkt_1s,vdifinfo->FrameLen);

  // isTVG 값과 "시작시각(첫 정초 Packet)"의 offset 값
  if(vdifinfo->version == 2) {
    fprintf(stderr,"%s isTVG    : %s\n",prefix,vdifinfo->isTVG?"Yes":"No");
    //fprintf(stderr,"offset0  : %lu bytes to First Second-Tic packet.\n",vdifinfo->offset0);
  }

  // Test if the filesize is an integer multiple of "1 second"
  if(vdifinfo->version == 2) {
    fprintf(stderr,"%s Filesize : %lu bytes = ",prefix,vdifinfo->filesize);
    offset = vdifinfo->filesize - OCTACAPTURE_FILE_HEADER;
    nSec   = offset / (OCTACAPTURE_SECD_HEADER + vdifinfo->FrameLen*vdifinfo->nPkt_1s);
    offset -= nSec*OCTACAPTURE_SECD_HEADER; // total size of whole packets (excluding total size of SECD_HEADER)
    fprintf(stderr,"(%d + %d*%lu +) %lu\n",OCTACAPTURE_FILE_HEADER,OCTACAPTURE_SECD_HEADER,nSec,offset);
    nPkt  = offset / vdifinfo->FrameLen;
    nByte = offset % vdifinfo->FrameLen;
  }else{
    fprintf(stderr,"%s Filesize : %lu bytes\n",prefix,vdifinfo->filesize);
    nPkt  = vdifinfo->filesize / vdifinfo->FrameLen;
    nByte = vdifinfo->filesize % vdifinfo->FrameLen;
  }
  fprintf(stderr,"%s          = %lu packets",prefix,nPkt);
  if(nByte) fprintf(stderr," + %lu bytes",nByte);
  fprintf(stderr,"\n");
  nSec = nPkt / vdifinfo->nPkt_1s;
  mPkt = nPkt % vdifinfo->nPkt_1s;
  fprintf(stderr,"%s          = %lu Seconds",prefix,nSec);
  if(mPkt) fprintf(stderr," + %lu packets",mPkt);
  if(nByte) fprintf(stderr," + %lu bytes",nByte);
  fprintf(stderr,"\n");

  fprintf(stderr,"%s================================================================\n",prefix);
  return(vdifinfo);
}

