// vdif2mk5b.c
//
// Convert a vdif file to mk5b file
// Input file could be one of following types.
//   type 2 = OCTA VDIF with Capture Header
//   type 1 = OCTA VDIF
//   type 0 = Standard VDIF

// 2013.07.04 버전1
// 2014.09.02 버전2
//    crcc 도입 -- mark5b Header가 완벽해졌을 것.
//    bit_swap_tbl 도입 -- speed up
//    optional 3rd parameter [speed] 도입 -- 2Gbps에도 대응
//    RVDB_SECD_HEADER[20~23]이 'MARK'이면 setTVG=0, 'VSIT'이면 setTVG=1로 지정.
//    그 밖의 경우는 Unknown (setTVG=0)으로 경고를 출력함.
// 2014.10.14 버전3
//    Mark5B Header: year2000='b', userdata='ead'로 고정, bcdDOY를 bcdJJJ로 되돌림.
//    new 'calendar.c' 채용
//    input vdif file type -- one of 3 possible types

// in.vdif 파일이 OCTA VDIF 형식의 파일일 경우, (data length 고정 = 1280 bytes)
//   (256 바이트의 파일 헤더 다음에,)
//   매초마다 (1428 바이트의 1초 헤더와) 10만*Speed 개의 packet이 반복된다.
//   각 packet은 32 bytes의 packet 헤더와 1280 bytes의 데이터로 구성된다.
//
// in.vdif 파일이 standard VDIF 형식의 파일일 경우, (data length 가변)
//   매초마다 nPkt_in*Speed 개의 packet이 반복된다.
//   FrameLen_in  = 32 + data_length
//   nPkt_in      = DATA_1SECOND / data_length       // # of packets/sec @ 1Gbps
//
// out.mk5b 파일은
//   매초마다 12800*Speed 개의 DataFrame이 반복된다.
//   각 DataFrame은 16 bytes의 헤더와 10000 bytes의 데이터로 구성된다.
//
// 이 프로그램에서는,
// 1초 분량의 data (nPkt_in *Speed 개의 packet)를 읽어서, 
// 1초 분량의 Mark5B data(12800*Speed 개의 DataFrame)를 기록하는 작업을 반복한다.

// OCTA VDIF (with Capture Header) 파일은 "BigEndian", "Network Byte Order" 이다.
// - "data"는 character stream으로 이해, 즉 byte-swap이 불필요하다.
// - "vdif header"에 있는 정보를 uint형으로 접근하려면, byte-swap 필요하다 -- ntohl() 사용.
// - "1sec header"에 있는 정보와 "file header"에 있는 정보를 uint형으로 접근하려면,
//   byte-swap 필요할 것임. (아직 구조를 몰라서, 시도하지 않았음.)
// - mk5b 파일과 비교를 통해, "data" 속의 각 byte는 서로 bit-swap된 관계에 있음을 알고 있다.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <arpa/inet.h>	// ntohl()
#include <ctype.h>

#include "stopwatch.c"
#include "calendar.c"
#include "vdif.c"
#include "mk5b.c"


int main(int argc, char **argv)
{
  FILE *fi,*fo;
  size_t n,m;
  long offset;
  unsigned int iSec,iPkt, iDF;

  unsigned int  *pi;
  unsigned char *pc,*pd;

  int InputFileType = 0; // 0=Standard VDIF, 1=OCTA VDIF, 2=OCTA VDIF with Capture Header
  int setTVG = 0;        // OCTA CAPTURE FILE only may alter this value.
  int Speed = 1;         // 1 or 2 Gbps

  // variables for set options : 
  //   default < 0  : "Don't change the original, unless user supplies a value." 
  //   0 <= default : "Force to change the original to this default or user supplied value."
  int SecsOffset = 0;  // will be added to 'secs' just after read.
  int isTVG = 0;       // Set isTVG flag in Mark5b Header.

  int FrameLen_in, nPkt_in;
  int i, k;
  int mjdn, year,month,date, hh,mm,ss, doy, sod;
//  VDIF_FRAMEHEADER *vh;
  VDIF_INFO *vdifinfo = NULL;

  //============================================================================
  // read buffer
  unsigned char buf[ OCTACAPTURE_FILE_HEADER //  256
                    +OCTACAPTURE_SECD_HEADER // 1428
                    +OCTAVDIF_PKTS_HEADER    //   32
                    +OCTAVDIF_PKTS_DATA      // 1280
                ];                           // 2996 bytes in total
//  CAPT_FILE_HEADER      *CF = (CAPT_FILE_HEADER      *)(buf);
//  CAPT_SECD_HEADER      *CS = (CAPT_SECD_HEADER      *)(buf+OCTACAPTURE_FILE_HEADER);
  OCTA_VDIF_FRAMEHEADER *VH = (OCTA_VDIF_FRAMEHEADER *)(buf+OCTACAPTURE_FILE_HEADER+OCTACAPTURE_SECD_HEADER);
  OCTA_VDIF_DATA        *VD = (OCTA_VDIF_DATA        *)(buf+OCTACAPTURE_FILE_HEADER+OCTACAPTURE_SECD_HEADER+OCTAVDIF_PKTS_HEADER);

  //============================================================================
  // write buffer
  VDIF_FRAMEHEADER VH1;       // input  vdif header (copied for later use)
  Mark5B_FRAMEHEADER mk5b;    // Mark5B Frame Header
  unsigned char *data = NULL; // Data buffer for 1 second (128 or 256 MB)

  STOPWATCH *sw_all = alloc_stopwatch("Total       ",NULL);
  STOPWATCH *sw_rea = alloc_stopwatch("ReadData    ",NULL);
  STOPWATCH *sw_wri = alloc_stopwatch("WriteData   ",NULL);

  //============================================================================

  // Command Line Parameters
  for(i=1; i<argc; i++) { // pick-up the set options first
    char *eqs;
    if((eqs = index(argv[i],'=')) != NULL) {
      if(strncasecmp(argv[i],"isTVG",3)==0) {
        k = atoi(eqs+1);
        if(0<=k && k<=1) isTVG = k;
        else fprintf(stderr,"Ignored the invalid set option, %s\n",argv[i]);
      }else if(strncasecmp(argv[i],"SecsOffset",3)==0) {
        SecsOffset = atoi(eqs+1);
      }else{
        fprintf(stderr,"Ignored the unknown set option, %s\n",argv[i]);
      }
      argv[i] = NULL;
    }
  }
  for(i=1; i<argc; i++) { // clean up
    if(argv[i]==NULL) {
      for(k=i+1; k<argc; k++) {
        if(argv[k]!=NULL) { argv[i] = argv[k]; argv[k] = NULL; break; }
      }
      if(k==argc) { argc = i; break; }
    }
  }
  if(argc < 2) {
    fprintf(stderr,"Usage: $ %s in.vdif [out.mk5b] [set options ...]\n",argv[0]);
    fprintf(stderr,
      "  where in.vdif  = input, 'standard', 'octa', or 'octa with Capture Header' vdif file\n"
      "        out.mk5b = output, mark5b file\n"
      "  set options:\n"
      "    isTVG=#        Set isTVG flag if 1 (default=0, Clear isTVG flag)\n"
      "    SecsOffset=#   Secs Offset (will be added just after read)\n"
      ); 
    exit(1);
  }

  //============================================================================

  // "Standard VDIF", "OCTA VDIF", or "OCTA VDIF with Capture Header" --> "Mark5B file"

  //===== Init global variables with the given input file
  vdifinfo = vdif_info("<<<",argv[1]);
  if(vdifinfo == NULL) exit(1);

  setTVG        = vdifinfo->isTVG;
  InputFileType = vdifinfo->version;
  FrameLen_in   = vdifinfo->FrameLen;
  nPkt_in       = DATA_1SECOND / (FrameLen_in  - OCTAVDIF_PKTS_HEADER); // # of packets/sec @ 1Gbps
  Speed         = vdifinfo->speed;

  // Exit here when no output file
  if(argc < 3) {
    exit(0);
  }

  //============================================================================

  //===== Show the value of set options : forced or user-supplied only are effective.
  if(isTVG) {
    fprintf(stderr,"set options: isTVG = %d\n",isTVG);
  }
  if(SecsOffset) {
    fprintf(stderr,"set options: SecsOffset = %d\n",SecsOffset);
  }

  if((fi = fopen(argv[1],"r")) == NULL) {
    fprintf(stderr,"Fail to open '%s'\n",argv[1]);
    exit(1);
  }

  //===== Create output file
  if((fo = fopen(argv[2],"w")) == NULL) {
    fprintf(stderr,"Fail to open '%s'\n",argv[2]);
    exit(1);
  }

  // 1초간의 데이터를 모두 담을 공간을 마련한다. 128MB or 256MB
  if((data = calloc(DATA_1SECOND*Speed,sizeof(char))) == NULL) {
    fprintf(stderr,"Fail to calloc, %u bytes\n",DATA_1SECOND*Speed);
    exit(1);
  }

  // Start to convert
  offset = 0L;
  if(InputFileType == 2) offset += OCTACAPTURE_FILE_HEADER;
  fseek(fi,offset,SEEK_SET); // skip the OCTA FILE HEADER, if any

  //----- Mark5B DF Header : (1) sync만 설정
  memset(&mk5b,0,LEN_MK5B_DF_HEADER);
  mk5b.sync = 0xABADDEED;

  // 1초 단위로 파일 변환
  start_stopwatch(sw_all);
  m = FrameLen_in - VDIF_PKTS_HEADER; // legnth of data within a frame
  for(iSec=0;;iSec++) {
    int MJDepoch, jjj, nsec;

    if(InputFileType == 2) fseek(fi,OCTACAPTURE_SECD_HEADER,SEEK_CUR); // skip the OCTA SECS HEADER

    // read "nPkt_in*Speed" Packets for 1 sec
    start_stopwatch(sw_rea);
    for(iPkt=0;iPkt<nPkt_in*Speed;iPkt++) {

      // Read one input packet
      if(InputFileType) {
        //===== PACKET HEADER + PACKET DATA 읽기
        if((n = fread(VH,sizeof(unsigned char),FrameLen_in,fi)) != FrameLen_in) {
          if(feof(fi)) break; // EOF
          fprintf(stderr,"Fail to read OCTA VDIF HEADER+DATA at iSec=%u, iPkt=%u. n=%lu/%u\n",iSec,iPkt,n,FrameLen_in);
          break; // Ignore the partial packet
        }
        // PACKET HEADER : "BigEndian" to "LittleEndian"
        pi = (unsigned int *)VH;
        for(k=0;k<8;k++) pi[k] = ntohl( pi[k] );
        // PACKET DATA : Do bitswap for each byte using the pre-defined table, put into buffer.
        pc = (unsigned char *)VD;
        pd = data + iPkt*m;
        for(k=0; k<m; k++) pd[k] = bit_swap_tbl[ pc[k] ];
      }else{
        //===== PACKET HEADER 읽기
        if((n = fread(VH,sizeof(unsigned char),VDIF_PKTS_HEADER,fi)) != VDIF_PKTS_HEADER) {
          if(feof(fi)) break; // EOF
          fprintf(stderr,"Fail to read VDIF HEADER at iSec=%u, iPkt=%u. n=%lu/%u\n",iSec,iPkt,n,VDIF_PKTS_HEADER);
          break; // Ignore the partial header
        }
        //===== PACKET DATA 읽기
        pd = data + iPkt*m;
        if((n = fread(pd,sizeof(unsigned char),m,fi)) != m) {
          if(feof(fi)) break; // EOF
          fprintf(stderr,"Fail to read VDIF DATA at iSec=%u, iPkt=%u. n=%lu/%lu\n",iSec,iPkt,n,m);
          break; // Ignore the partial data
        }
      }

      // Makeup the output Mark5B Header

      // 매 초 첫 패킷의 헤더를 이용하여, 시각 관리를 한다.
      //   사본 VH1 = 읽어들인 원본 헤더값 확인을 위해 사용. 현재, 맨 처음 한번만 출력함.
      //   mk5b    = 출력용 헤더, 각종 수정사항이 반영되도록 적절히 변경해야 함.
      if(iPkt==0) {

        // Save the (original) input vdif packet header for later printing.
        memcpy(&VH1,VH,VDIF_PKTS_HEADER); // copied for later use

        // Adjust VH.secs by SecsOffset ===== very special options =====
        //   OCTADDB/OCTADISK에서 직접 vdif packet을 받아 기록한 경우, 
        //   "secs" 값이 기대치+86400 인 문제점이 있음을 알게 되었다.
        //   이러한 경우에, set option으로 "SecsOffset=-86400"을 지정하면, 여기서 해결.
        if(SecsOffset) {
          int y2000,month_epoch,days, y,m,d, mjdn,sod;
          int new_secs = (int)VH->secs + SecsOffset; // Adjust
              // Use "int", instead of "unsigned int".
              // (It should be able to hold a possible negative value.
          // (VH->refepoch,VH->secs+SecsOffset) --> (y2000,month_epoch,days,sod)
          y2000 = VH->refepoch/2;
          month_epoch = (VH->refepoch%2 == 0) ? 1 : 7;
          days = 0;
          while(new_secs < 0) { // make sure that not-negative new_secs
            new_secs += 86400;
            days -= 1;
          }
          days += new_secs/86400;
          sod   = new_secs%86400;
          // (y2000,month_epoch,1+days) --> (mjdn) --> (y,m,d)
          mjdn = MJDN(2000+y2000, month_epoch, 1+days); // "1+days" is equal to "Date".
          MJDN2CDN(mjdn, &y,&m,&d);
          // (mjdn,sod; y,m) --> (VH->refepoch,VH->secs) 
          VH->refepoch = (y-2000)*2;
          if(m<7) {
            VH->secs = (mjdn - MJDN(y,1,1))*86400;
          }else{
            VH->refepoch += 1;
            VH->secs = (mjdn - MJDN(y,7,1))*86400;
          }
          VH->secs += sod; // Add the Seconds of Day
        }

        // ---> Mark5B DF Header : (2) 초 단위로 변경되는 부분을 설정
        time_vdif_frameheader((VDIF_FRAMEHEADER *)VH, &mjdn,&year,&month,&date,&hh,&mm,&ss,&doy,&sod);
        if(iSec==0) {
          printf("RefEpoch Secs FrameNum >>  mjdn year/mo/dy(doy) hh:mm:ss (sec.s)\n");
          printf("%3d %9d %8d >> %5d %4d/%02d/%02d(%3d) %02d:%02d:%02d (%5d)\n\n",
            VH->refepoch,VH->secs,VH->framenum, mjdn, year,month,date,doy, hh,mm,ss, sod);
        }
        MJDepoch = (mjdn/1000)*1000;
        mk5b.year2000 = 0xb;			//  4bits
        mk5b.userdata = 0xead;			// 12bits
        if(setTVG || isTVG) mk5b.isTVG = 1;	//  1bits
      //mk5b.frameNum = iDF;			// 15bits : 0~12799
        mk5b.bcdJJJ   = BCD(mjdn % 1000);	// 12bits : JJJ
        mk5b.bcdSOD   = BCD(sod);		// 20bits : sssss
      //mk5b.bcdSOD2  = 0;			// 16bits : .ssss
      //mk5b.crcc     = 0;			// 16bits
      }
    }//iPkt
    stop_stopwatch(sw_rea);

    // Data ready ?
    if(iPkt < nPkt_in*Speed) {
      if(iPkt == 0) break; // No more data to be processed

      // Got a patial data (less than 1 second). Stop anyway. (Throw it away.)
      fprintf(stderr,"Warning: Partial second data, %d/%d packets.\n",iPkt,nPkt_in*Speed);
      break;
    }

    //===== write 12800*Speed Mark5B DFs for 1 sec
    start_stopwatch(sw_wri);
    for(iDF=0;iDF<12800*Speed;iDF++) {
      unsigned int sod2 = (10000*iDF)/(12800*Speed); // 0~9999

      // ---> Mark5B DF Header : (3) 초 단위 이하로 변경되는 부분을 설정
      mk5b.frameNum = iDF;		// 15bits : 0~12799
      mk5b.bcdSOD2  = BCD(sod2);	// 16bits : .ssss
      mk5b.crcc     = crcc(&mk5b);	// 16bits

      //===== 매 초마다 시각정보 출력
      if(iDF==0) {
        char s1[2]={'\r',0}, s2[2]={0,0};

        // Once at first time
        if(iSec==0) {
          // Report the possible framenum error on the beginning of input file
          if(VH1.framenum != 0) printf("\t!!! WARNING -- 1PP mismatch %u\n",VH1.framenum);

          // input/output VDIF Header 출력
          printf("Input  VDIF_PKTS_HEADER\n");
          if(InputFileType) print_octavdif_frameheader((OCTA_VDIF_FRAMEHEADER *)&VH1);
          else              print_vdif_frameheader(&VH1);

          // Time-info Lookup header
          printf("\nOutput Mark5B_HEADER\n");
          printf("SYNCWORD YR UD TVG FramN JJJ Sec_of_Day CRCC : Calendar_dates_and_exact-time\n");
          s1[0] = 0; s2[0] = '\n';
        }

        //===== Time-info from mk5b header
        jjj = iBCD(mk5b.bcdJJJ); 
        sod = iBCD(mk5b.bcdSOD);
        mjdn = MJDepoch + jjj;
        MJDN2CDN(mjdn, &year,&month,&date);
        ss = sod;
        hh = ss/3600; ss %= 3600; mm = ss/60; ss %= 60;
        nsec = (mk5b.frameNum * 78125) / Speed; // 78125=10^9/12800 : VSI-32MHz에 해당
        //
        printf("%s%08X %02u %04u %1u %5u ",
          s1, mk5b.sync,mk5b.year2000,mk5b.userdata,mk5b.isTVG,mk5b.frameNum);
        printf("%3d %5d.%04u %04x : ",
          jjj,sod,iBCD(mk5b.bcdSOD2),mk5b.crcc);
        printf("%4d/%02d/%02d %2d:%02d:%02d.%09d%s",year,month,date,hh,mm,ss,nsec, s2);
        fflush(stdout);
      }

      //===== DF Header 쓰기
      if((n = fwrite(&mk5b,sizeof(char),LEN_MK5B_DF_HEADER,fo)) != LEN_MK5B_DF_HEADER) {
        fprintf(stderr,"Fail to write LEN_MK5B_DF_HEADER at iSec=%u, iDF=%u. n=%lu/%u\n",
          iSec,iDF,n,LEN_MK5B_DF_HEADER);
        break; // Stop here
      }
      //===== DF Data 쓰기
      pc = data + iDF*LEN_MK5B_DF_DATA;
      if((n = fwrite(pc,sizeof(char),LEN_MK5B_DF_DATA,fo)) != LEN_MK5B_DF_DATA) {
        fprintf(stderr,"Fail to write LEN_MK5B_DF_DATA at iSec=%u, iDF=%u. n=%lu/%u\n",
          iSec,iDF,n,LEN_MK5B_DF_DATA);
        break; // Stop here
      }
    }//iDF
    stop_stopwatch(sw_wri);

  }//iSec
  stop_stopwatch(sw_all);
  fprintf(stderr,"\nProcessed for %d seconds of Data.\n",iSec);

  // Stopwatch
  report_stopwatch(sw_rea); free_stopwatch(sw_rea);
  report_stopwatch(sw_wri); free_stopwatch(sw_wri);
  report_stopwatch(sw_all); free_stopwatch(sw_all);

  // Done.
  fclose(fi);
  fclose(fo);
  free(data);

  //===== Show info about the created mk5b file
  mk5b_info(">>>",argv[2]);

  return(0);
}

