// vdifcp.h

// VDIFCP SR (Sender Report)
struct VDIFCP_SR {
  struct {
    unsigned int plen:16;	// packet length, 28 fixed
    unsigned int :4;		// reserved, 0 fixed
    unsigned int VSIport:4;	// VSI port number, 1~4
    unsigned int class:4;	// Classification, 3 fixed
    unsigned int version:4;	// Version, 1 fixed
  };
  struct {
    unsigned int seqno:24;	// Sequence Number
    unsigned int :8;		// reserved, 0 fixed
  };
  struct {
    unsigned int d100:4;	// Time 100D, 4bit-BCD for YYYYDDDhhmmss
    unsigned int y1:4;		// Time 1Y
    unsigned int y10:4;		// Time 10Y
    unsigned int y100:4;	// Time 100Y
    unsigned int y1000:4;	// Time 1000Y
    unsigned int mode:4;	// VDIF Mode, 1 or 2
    unsigned int :8;		// reserved, 0 fixed
  };
  struct {
    unsigned int s1:4;		// Time 1S
    unsigned int s10:4;		// Time 10S
    unsigned int m1:4;		// Time 1M
    unsigned int m10:4;		// Time 10M
    unsigned int h1:4;		// Time 1H
    unsigned int h10:4;		// Time 10H
    unsigned int d1:4;		// Time 1D
    unsigned int d10:4;		// Time 10D
  };
  struct {
    unsigned int :32;		// reserved, 0 fixed
  };
  unsigned char   aux[8];	// AUX (NULL fill)
  unsigned char   pdata[52];	// PDATA (variable length)
//   unsigned char   ver_kind;           // version and packet-kind
//   unsigned char   port;               // vsi-port
//   unsigned short  plen;               // packet length
//   unsigned int  seqno;              // seq-number
//   unsigned char   reserve1;
//   unsigned char   mode_timeupper;       // mode and time
//   unsigned char   time[6];       // mode and time
//   unsigned int  reserve2;
//   unsigned char   aux[8];             // AUX (NULL fill)
//   unsigned char   pdata[52];           // PDATA (variable length)
};

// VDIFCP RR (Receiver Report)
struct VDIFCP_RR {
   unsigned char   ver_kind;           // version and packet-kind
   unsigned char   port;               // vsi-port
   unsigned short  plen;               // packet length
   unsigned int  seqno;              // seq-number
   unsigned char   tv;                 // time valid flag (MSB)
   unsigned char   mode_timeupper;     // mode and time
   unsigned char   time[6];            // mode and time
   unsigned int  h_fcnt;             // H-frame frame count
   unsigned int  l_fcnt;             // L-frame frame count
   unsigned int  reserve;
};

// VDIFCP VR (VDIF Request)
struct VDIFCP_VR {
  struct {
    unsigned int plen:16;	// packet length, 28 fixed
    unsigned int :4;		// reserved, 0 fixed
    unsigned int VSIport:4;	// VSI port number, 1~4
    unsigned int class:4;	// Classification, 3 fixed
    unsigned int version:4;	// Version, 1 fixed
  };
  struct {
    unsigned int seqno:24;	// Sequence Number
    unsigned int :8;		// reserved, 0 fixedd
  };
  struct {
    unsigned int d100:4;	// Time 100D, 4bit-BCD for YYYYDDDhhmmss
    unsigned int y1:4;		// Time 1Y
    unsigned int y10:4;		// Time 10Y
    unsigned int y100:4;	// Time 100Y
    unsigned int y1000:4;	// Time 1000Y
    unsigned int mode:4;	// VDIF Mode, 1 or 2
    unsigned int :8;		// reserved, 0 fixed
  };
  struct {
    unsigned int s1:4;		// Time 1S
    unsigned int s10:4;		// Time 10S
    unsigned int m1:4;		// Time 1M
    unsigned int m10:4;		// Time 10M
    unsigned int h1:4;		// Time 1H
    unsigned int h10:4;		// Time 10H
    unsigned int d1:4;		// Time 1D
    unsigned int d10:4;		// Time 10D
  };
  struct {
    unsigned int :32;		// reserved, 0 fixed
  };
  struct {
    unsigned int :32;		// reserved, 0 fixed
  };
  struct {
    unsigned int :32;		// reserved, 0 fixed
  };
//   unsigned char   ver_kind;           // version and packet-kind
//   unsigned char   port;               // vsi-port
//   unsigned short  plen;               // packet length
//   unsigned int  seqno;              // seq-number
//   unsigned char   reserve1;
//   unsigned char   mode_timeupper;     // mode and time
//   unsigned char   time[6];            // mode and time
//   unsigned int  reserve2[3];
};


