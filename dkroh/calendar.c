// calendar.c
// -- Using the standard modules : time(), timegm(), gmtime_r()
// -- Epoch = 0h0m0s of Jan. 1, 1970
// -- "time_t" is equivalent to "long int".

#include "calendar.h"

void CheckYearZero(int y)
{
  if(y==0) { fprintf(stderr,"Year Zero!!!\n"); exit(1); }
}

int JDN(int y, int m, int d)
{
  struct tm tm;
  time_t t;
  int jdn;
  CheckYearZero(y);
  memset(&tm,0,sizeof(struct tm));
  tm.tm_year = y-1900;
  tm.tm_mon  = m-1;
  tm.tm_mday = d;
  t = timegm(&tm); // seconds since Epoch
  jdn = t/86400L + JDN_1970_01_01_00H;
  return(jdn);
}
void JDN2CDN(int jdn, int *y,int *m,int *d)
{
  struct tm tm;
  time_t t;
  t = (jdn - JDN_1970_01_01_00H)*86400L;
  gmtime_r(&t,&tm);
  *y = tm.tm_year + 1900;
  *m = tm.tm_mon + 1;
  *d = tm.tm_mday;
}

int MJDN(int y, int m, int d)
{
  struct tm tm;
  time_t t;
  int mjdn;
  CheckYearZero(y);
  memset(&tm,0,sizeof(struct tm));
  tm.tm_year = y-1900;
  tm.tm_mon  = m-1;
  tm.tm_mday = d;
  t = timegm(&tm); // seconds since Epoch
  mjdn = t/86400L + MJDN_1970_01_01_00H;
  return(mjdn);
}
void MJDN2CDN(int mjdn, int *y,int *m,int *d)
{
  struct tm tm;
  time_t t;
  t = (mjdn - MJDN_1970_01_01_00H)*86400L;
  gmtime_r(&t,&tm);
  *y = tm.tm_year + 1900;
  *m = tm.tm_mon + 1;
  *d = tm.tm_mday;
}

double JD(int y, int m, int d, double sod)
{
  struct tm tm;
  time_t t;
  double jd;
  CheckYearZero(y);
  memset(&tm,0,sizeof(struct tm));
  tm.tm_year = y-1900;
  tm.tm_mon  = m-1;
  tm.tm_mday = d;
  t = timegm(&tm); // seconds since Epoch
  jd = (t/86400.0 + JD_1970_01_01_00H) + sod/86400.0;
  return(jd);
}
void JD2CD(double jd, int *y,int *m,int *d,double *sod)
{
  struct tm tm;
  time_t t;
  double x = jd - JD_1970_01_01_00H;
  t = (int)x;
  *sod = (x - (double)t)*86400.0;
  t *= 86400L;
  gmtime_r(&t,&tm);
  *y = tm.tm_year + 1900;
  *m = tm.tm_mon + 1;
  *d = tm.tm_mday;
}

double MJD(int y, int m, int d, double sod)
{
  struct tm tm;
  time_t t;
  double mjd;
  CheckYearZero(y);
  memset(&tm,0,sizeof(struct tm));
  tm.tm_year = y-1900;
  tm.tm_mon  = m-1;
  tm.tm_mday = d;
  t = timegm(&tm); // seconds since Epoch
  mjd = (t/86400.0 + MJD_1970_01_01_00H) + sod/86400.0;
  return(mjd);
}
void MJD2CD(double mjd, int *y,int *m,int *d,double *sod)
{
  struct tm tm;
  time_t t;
  double x = mjd - MJD_1970_01_01_00H;
  t = (int)x;
  *sod = (x - (double)t)*86400.0;
  t *= 86400L;
  gmtime_r(&t,&tm);
  *y = tm.tm_year + 1900;
  *m = tm.tm_mon + 1;
  *d = tm.tm_mday;
}

int DOY(int y, int m, int d)
{
  struct tm tm;
  time_t t;
  CheckYearZero(y);
  memset(&tm,0,sizeof(struct tm));
  tm.tm_year = y-1900;
  tm.tm_mon  = m-1;
  tm.tm_mday = d;
  t = timegm(&tm); // seconds since Epoch
  gmtime_r(&t,&tm);
  return(tm.tm_yday + 1);
}

int NDAYS(int y)
{
  struct tm tm;
  time_t t1,t0;
  int ndays;
  CheckYearZero(y);
  memset(&tm,0,sizeof(struct tm));
  tm.tm_year = y-1900;
  tm.tm_mon  = 0; // Jan
  tm.tm_mday = 1;
  t0 = timegm(&tm); // seconds since Epoch
  tm.tm_year += (y==-1) ? 2 : 1; // next year
  tm.tm_mon  = 0; // Jan
  tm.tm_mday = 1;
  t1 = timegm(&tm);
  ndays = (t1-t0)/86400L;
  return(ndays);
}

int Leapyear(int y)
{
  return( (NDAYS(y)==366) ? 1 : 0 );
}

double SOD(double hou, double min, double sec)
{
  int sgn=1; // hh may be positive/negative, but mm & sec must be positive. 
  double sod;
  if(hou<0.0) { sgn = -1; hou = -hou; }
  if(min<0.0) { fprintf(stderr,"%f : ignored the sign of minute\n",min); min = -min; } 
  if(sec<0.0) { fprintf(stderr,"%f : ignored the sign of second\n",sec); sec = -sec; } 
  sod = sec + min*60.0 + hou*3600.0;
  sod *= sgn;
  return(sod);
}
void HMS(double sod, int *hh,int *mm,double *sec)
{
  int sgn=1; // hh takes the sign of sod, and both mm & sec are always positive.
  if(sod<0.0) { sgn=-1; sod = -sod; }
  *sec = sod/3600.0;
  *hh = *sec; *sec -= *hh; *sec *= 60.0;
  *mm = *sec; *sec -= *mm; *sec *= 60.0;
  *hh *= sgn;
}

void TODAY(int *y,int *m,int *d, int *hh,int *mm,int *ss)
{
  time_t t;
  struct tm *tm;
  time(&t);
  tm = gmtime(&t);
  if(y) *y = tm->tm_year+1900;
  if(m) *m = tm->tm_mon+1;
  if(d) *d = tm->tm_mday;
  if(hh) *hh = tm->tm_hour;
  if(mm) *mm = tm->tm_min;
  if(ss) *ss = tm->tm_sec;
}




