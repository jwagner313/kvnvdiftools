// calendar.h
// -- Using the standard modules : time(), timegm(), gmtime_r()
// -- Epoch = 0h0m0s of Jan. 1, 1970
// -- "time_t" is equivalent to "long int".

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define   JD_1970_01_01_00H 2440587.5
#define  JDN_1970_01_01_00H 2440588L

#define  MJD_1970_01_01_00H   40587.0
#define MJDN_1970_01_01_00H   40587L

void CheckYearZero(int y);

int JDN(int y, int m, int d);
int MJDN(int y, int m, int d);
void JDN2CDN(int jdn, int *y,int *m,int *d);
void MJDN2CDN(int mjdn, int *y,int *m,int *d);

double JD(int y, int m, int d, double sod);
double MJD(int y, int m, int d, double sod);
void JD2CD(double jd, int *y,int *m,int *d,double *sod);
void MJD2CD(double mjd, int *y,int *m,int *d,double *sod);

int DOY(int y, int m, int d);
int NDAYS(int y);
int Leapyear(int y);

double SOD(double hou, double min, double sec);
void   HMS(double sod, int *hh,int *mm,double *sec);


