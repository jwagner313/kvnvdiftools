// cut_mk5b.c
//
// Cut a part from a mk5b file
//

// .mk5b 파일은
//   매초마다 12800*Speed 개의 DataFrame이 반복된다.
//   각 DataFrame은 16 bytes의 헤더와 10000 bytes의 데이터로 구성된다.
//
// 이 프로그램에서는,
//   1초 분량의 Mark5B data(12800*Speed 개의 DataFrame)를 읽어서, 
//   기록하는 작업을 반복한다.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "stopwatch.c"
#include "calendar.c"
#include "mk5b.c"

//=========================================================================
int READ_mk5b(Mark5B_FRAMEHEADER *pHeader,Mark5B_FRAMEDATA *pData, int nDF, FILE*fi)
{
  int jDF,rDF;
  size_t n;

  //===== Clear buffer
  memset((void *)pHeader,0,nDF*LEN_MK5B_DF_HEADER);
  memset((void *)pData,  0,nDF*LEN_MK5B_DF_DATA);

  //===== Try to read "nDF" Mark5B DFs
  rDF = 0;
  for(jDF=0; jDF<nDF; jDF++) {
    n = fread((void *)(pHeader+jDF),sizeof(unsigned char),LEN_MK5B_DF_HEADER,fi);
    if(n != LEN_MK5B_DF_HEADER) {
      fprintf(stderr,"  Fail to read HEADER for jDF=%d : n=%lu\n",jDF,n);
      break;
    }//Header
    n = fread((void *)(pData+jDF),sizeof(unsigned char),LEN_MK5B_DF_DATA,fi);
    if(n != LEN_MK5B_DF_DATA) {
      fprintf(stderr,"  Fail to read DATA for jDF=%d : n=%lu\n",jDF,n);
      break;
    }//Data
    rDF++;
  }//jDF
  return(rDF);
}
//=========================================================================
int WRITE_mk5b(Mark5B_FRAMEHEADER *pHeader,Mark5B_FRAMEDATA *pData, int nDF, FILE*fo)
{
  int jDF,wDF;
  size_t n;

  //===== Try to write "nDF" Mark5B DFs
  wDF = 0;
  for(jDF=0; jDF<nDF; jDF++) {
    n = fwrite((void *)(pHeader+jDF),sizeof(unsigned char),LEN_MK5B_DF_HEADER,fo);
    if(n != LEN_MK5B_DF_HEADER) {
      fprintf(stderr,"  Fail to write HEADER for jDF=%d : n=%lu\n",jDF,n);
      break;
    }//Header
    n = fwrite((void *)(pData+jDF),sizeof(unsigned char),LEN_MK5B_DF_DATA,fo);
    if(n != LEN_MK5B_DF_DATA) {
      fprintf(stderr,"  Fail to write DATA for jDF=%d : n=%lu\n",jDF,n);
      break;
    }//Data
    wDF++;
  }//jDF
  return(wDF);
}


int main(int argc, char **argv)
{
  Mark5B_INFO *mk5binfo=NULL;
  Mark5B_FRAMEHEADER *pHeader = NULL; // buffer for Header
  Mark5B_FRAMEDATA   *pData   = NULL; // buffer for Data
  size_t bufsizHEAD,bufsizDATA;
  int nDF;

  FILE *fi,*fo=NULL;

  int iDF,sDF,eDF, nSec;

  STOPWATCH *sw_all = alloc_stopwatch("Total       ",NULL);
  STOPWATCH *sw_rea = alloc_stopwatch("ReadData    ",NULL);
  STOPWATCH *sw_wri = alloc_stopwatch("WriteData   ",NULL);

  //============================================================================

  if(argc < 2) {
    fprintf(stderr,"Usage: $ %s in.mk5b [out.mk5b [from [length]]]\n",argv[0]);
    fprintf(stderr,
      "  where in.mk5b  = input, mark5b file\n"
      "        out.mk5b = output, mark5b file\n"
      "        from     = offset from beginning, default=0s\n"
      "        length   = cut length, default=1s\n"
      "  Suffix of 'from' & 'length' for specifing the units :\n"
      "        's' = Seconds, 'DF' = DataFrames, 'b' = Bytes\n"
      "        else or none = considered as 'DF'\n");
    exit(1);
  }

  //============================================================================
  // "Mark5B file" --> "Mark5B file" 

  //===== Init global variables with the given input file
  mk5binfo = mk5b_info("<<<",argv[1]);
  if(mk5binfo == NULL) exit(1);

  //===== Prepare the buffers for header and data of 1 second
  nDF = mk5binfo->nDF_1s;
  bufsizHEAD = nDF * LEN_MK5B_DF_HEADER;
  bufsizDATA = nDF * LEN_MK5B_DF_DATA;
  if(posix_memalign((void **)&pHeader,getpagesize(),bufsizHEAD) != 0) {
    fprintf(stderr,"Fail to allocate memory for Header.\n");
    exit(1);
  }//pHeader
  //fprintf(stderr,"Buffer allocated for Header : %lu bytes\n",bufsizHEAD);
  if(posix_memalign((void **)&pData,getpagesize(),bufsizDATA) != 0) {
    fprintf(stderr,"Fail to allocate memory for Data.\n");
    exit(1);
  }//pData

  //============================================================================

  //===== Now, set the start position and length in DF unit.
  sDF = (argc>3) ? mk5b_DFunit(mk5binfo,argv[3]) : mk5b_DFunit(mk5binfo,"0s"); // from
  iDF = (argc>4) ? mk5b_DFunit(mk5binfo,argv[4]) : mk5b_DFunit(mk5binfo,"1s"); // length
  eDF = sDF + iDF;

  //===== open the mk5b input file
  fi = fopen(mk5binfo->pathname,"r");
  fseek(fi, mk5binfo->offset0 + sDF*LEN_MK5B_DF, SEEK_SET);

  //===== Create output file
  if(argc>2) 
  if((fo = fopen(argv[2],"w")) == NULL) {
    fprintf(stderr,"Fail to open '%s'\n",argv[2]);
    exit(1);
  }

  // mark5b 파일 읽기 :
  //   loop 마다,  1초 분량인 mk5binfo->nDF_1s 개의 Data frame을 읽는다.
  //   EOF를 만나거나 오류가 발생하면, 중단한다.
  start_stopwatch(sw_all);
  nSec = 0;
  for(iDF=sDF; iDF<eDF; iDF+=nDF) {
    int rDF,wDF, iSec=iDF/nDF;
    char memo[80];

    // read
    start_stopwatch(sw_rea);
    rDF = READ_mk5b(pHeader,pData,nDF, fi);
    if(rDF != nDF) {
      fprintf(stderr,"Warning : read %d/%d DFs at %ds.\n",rDF,nDF,iSec); 
    }
    stop_stopwatch(sw_rea);

    // Show time (once per second)
    sprintf(memo,": %ds",iSec);
    mk5b_HeaderTime(mk5binfo,pHeader,memo);

    // write
    start_stopwatch(sw_wri);
    if(fo) {
      wDF = WRITE_mk5b(pHeader,pData,rDF, fo);
      if(wDF != rDF) {
        fprintf(stderr,"Warning : written %d/%d DFs at %ds.\n",wDF,rDF,iSec); 
      }
    }
    stop_stopwatch(sw_wri);

    nSec++;
  }//iSec
  stop_stopwatch(sw_all);
  fprintf(stderr,"\nProcessed for %d seconds of Data.\n",nSec);

  // Stopwatch
  report_stopwatch(sw_rea); free_stopwatch(sw_rea);
  report_stopwatch(sw_wri); free_stopwatch(sw_wri);
  report_stopwatch(sw_all); free_stopwatch(sw_all);

  //===== Done.
  fclose(fi);
  if(fo) fclose(fo);
  free(pData);
  free(pHeader);

  //===== Show info about the output file
  if(fo) mk5b_info(">>>",argv[2]);

  return(0);
}

