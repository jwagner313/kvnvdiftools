// mk5b.c : mark5b 파일을 읽고 쓸 때, 필요한 정의와 유용한 모듈들 

#include "calendar.h"	// MJDN(), MJDN2CDN()
#include "mk5b.h"


//=========================================================================
// Calculate the test-vector-generator (TVG) pattern. 
//
// Reference: VSI-H Specification, Rev 1.0, 7 August 2000, 
//   available at http://web.haystack.mit.edu/vsi/.
//   See especially Figure 6, page 29 and Table 13, page 20.
//   Referring to Figure 6, 
//   q (below) represents the Q outputs of the flip-flops, 
//   r the upper inputs of the exors, and 
//   u the lower 16 bits of the answer (output). 
//
// Revised:  2005 March 17, JAB
unsigned /*long*/ tvg(int init) 
{ 
  static unsigned short q = 0xffff; 
  unsigned short r, u; 

  if(init) q = 0xffff; // Initialize? 

  /* Set r to a copy of q right shifted by 1 bit */ 
  r = q >> 1; 
  /* And set r bit 15 to a copy of q bit 2 */ 
  if (q & 4) /* Bit 2 set? */ 
    r |= 0x8000; /* Yes */ 
  /* We need to overwrite r bit 14 with a copy of u bit 0, 
   * so we need to do q exor r temporarily to get this bit */ 
  if ((q ^ r) & 1) /* Bit 0 set? */ 
    r |= 0x4000; /* Yes */ 
  else /* Not set */ 
    r &= 0xbfff; 
  /* Now set u to q exor r */ 
  u = q ^ r; /* Lower 16 bits of the answer */ 
  /* For next time, set q to a copy of u right shifted by 1 bit */ 
  q = u >> 1; 
  /* And set q bit 15 (invisible) to a copy of q bit 0 */ 
  if (q & 1) /* Bit 0 set? */ 
    q |= 0x8000; /* Yes */ 
  /* u has the lower 16 bits of the answer; the upper 16 bits 
   * are the lower 16 bits inverted and shifted up there */ 
  return(((unsigned /*long*/) ~u << 16) + u); 
}
// ret값/*의 하위*/ 32비트가 test vector b31~b00에 해당한다.

//=========================================================================
// Calculate the CRC (cyclic redundancy check) of the given bit stream.
//   nbits_buf = 48    : because that Mark5B time codes = 6 bytes
//   mask_CRC  = 0x4003
//   nbits_CRC = 16
int crcc(Mark5B_FRAMEHEADER *pH)
{
  static int nbits = 48;
  static int mask_CRC = 0x4003;
  static int nbits_CRC = 16;

  int ibit,iB,iC, p, q, r=0;
  unsigned char *x,buf[8];

  // copy the word 2 & 3 with byte-swap
  x = (unsigned char *)pH + 8;
  for(iC=0; iC<8; iC++) {
    if(iC<4) buf[ 3-iC] = x[iC]; // 0123 --> 3210 
    else     buf[11-iC] = x[iC]; // 4567 --> 7654 
  }//swap

  for(ibit=0; ibit<nbits; ibit++) {
    iC = ibit/8;	// char index
    iB = 7-ibit%8;	// bit index within char, 7 to 0 for MSB to LSB 

    p = (buf[iC] >> iB) & 1;	// iB-th bit of iC-th char
    q = r & 1;					// LSB of current crcc
    if((p ^ q) == 0) {
      r &= -2;					// Clear LSB of crcc
    }else{
      r ^= mask_CRC;			// Invert bits with 1s in mask 
      r |= 1;					// Set LSB of crcc
    }//compare p and q
    r = (r >> 1) | (r & 1) << (nbits_CRC-1); // Rotate one bit right direction
  }//ibit

  // bit-reverse : 비트를 뒤집어야 mark5b header의 crcc값과 일치한다.
  // (cf., Parse5A.c, lines 5890--5900)
  for(q=0,iB=0; iB<nbits_CRC; iB++) {
    if(r & (1<<iB)) q |= 1 << (nbits_CRC-1-iB);
  }//bit-reverse
  return( q );
}

//=========================================================================
// (10진법로 표현했을 때, 최대 8 유효숫자까지) unsigned int를 BCD로 인코딩
unsigned int BCD(unsigned int n)
{
  char s[16];
  unsigned int i,m;
  n = n%100000000;
  sprintf(s,"%08u",n);
  for(m=i=0;i<8;i++) m = (m << 4) | (s[i] & 0x0f);
  return m;
}

// (최대 8 유효숫자의) BCD를 unsigned int로 변환
unsigned int iBCD(unsigned int bcd)
{
  unsigned int v,i,n,mask=0x0f,mult;
  for(n=0,mult=1,v=i=0; i<8; mult*=10,n+=4,i++) {
    v = ((bcd & (mask<<n))>>n)*mult + v;
  }
  return v;
}
//=========================================================================

// DF number from beginning. 시작시각(파일에서 첫 정초 frame의 시각)부터 헤아린 전체 DF 번호
// (delta_JJJ*86400 + delta_SOD) = # of seconds
// mk5binfo->nDF_1s = 12800 or  25600 DFs/s
// pH->frameNum = 12800 or  25600
// Max.Value for 60sec = 60*25600 = 1536000
int mk5b_DFindex(Mark5B_INFO *mk5binfo, Mark5B_FRAMEHEADER *pH)
{
  int delta_JJJ = iBCD(pH->bcdJJJ) - mk5binfo->jjj0;
  if(delta_JJJ<0) delta_JJJ += 1000;
  return( (delta_JJJ*86400 + (iBCD(pH->bcdSOD)-mk5binfo->sod0))*mk5binfo->nDF_1s + pH->frameNum );
}

//Calc time using year2000 and DOY : ONLY for version 3
//  "iBCD(pH->bcdJJJ)" is expected to be equal to "DOY" (Date-of-Year),
//  but "OLD software" have a BUG : "put doy+1 instead of true doy".
//  Nano-second accuracy is achived using pH->frameNum (and mk5binfo->speed).
char *mk5b_time1_doy(Mark5B_INFO *mk5binfo, Mark5B_FRAMEHEADER *pH)
{
  static char timemark[80];
  int mjdn, year,month,date,hh,mm,ss,nsec, doy;
  doy = iBCD(pH->bcdJJJ) - 1; // "-1" for fixing the BUG, introduced by "OLD software"
  mjdn = MJDN(2000+pH->year2000, 1, doy);
  MJDN2CDN(mjdn, &year,&month,&date); 
#if 0
  fprintf(stderr,"------------------------------------------- mk5b_time1_doy\n");
  fprintf(stderr,"Using year=2000+%u=%u and doy=%d,\n",pH->year2000,2000+pH->year2000,doy);
  fprintf(stderr,"  MJDN(%u,1,%d) gives mjdn=%d\n", 2000+pH->year2000,doy, mjdn);
  fprintf(stderr,"  MJDN2CDN(%u, y,m,d) gives y=%d, m=%d, d=%d\n", mjdn, year,month,date);
#endif
  ss = iBCD(pH->bcdSOD);
  hh = ss/3600; ss %= 3600; mm = ss/60; ss %= 60;

  nsec = pH->frameNum * 78125; // 78125=10^9/12800 : VSI-32MHz에 해당
  if(mk5binfo->speed > 0) nsec /= mk5binfo->speed;

  sprintf(timemark,"%4d/%02d/%02d %2d:%02d:%02d.%09d",year,month,date,hh,mm,ss,nsec);
  return( timemark );
}

//Calc time using MJDepoch and bcdJJJ : ONLY for version 1 or 2 
//  iBCD(pH->bcdJJJ) is equal to JJJ (lower-3-digits-of-MJDN).
//  Nano-second accuracy is achived using pH->frameNum (and mk5binfo->speed).
char *mk5b_time1(Mark5B_INFO *mk5binfo, Mark5B_FRAMEHEADER *pH)
{
  static char timemark[80];
  int mjdn, year,month,date,hh,mm,ss,nsec, jjj;
  jjj = iBCD(pH->bcdJJJ);
  mjdn = mk5binfo->MJDepoch + jjj;
  MJDN2CDN(mjdn, &year,&month,&date);
#if 0
  fprintf(stderr,"------------------------------------------- mk5b_time1\n");
  fprintf(stderr,"Using mk5binfo.MJDepoch=%d and jjj=%d,\n",mk5binfo->MJDepoch,jjj);
  fprintf(stderr,"  %d + %d gives mjdn=%d\n", mk5binfo->MJDepoch, jjj, mjdn);
  fprintf(stderr,"  MJDN2CDN(%u, y,m,d) gives y=%d, m=%d, d=%d\n", mjdn, year,month,date);
#endif
  ss = iBCD(pH->bcdSOD);
  hh = ss/3600; ss %= 3600; mm = ss/60; ss %= 60;

  nsec = pH->frameNum * 78125; // 78125=10^9/12800 : VSI-32MHz에 해당
  if(mk5binfo->speed > 0) nsec /= mk5binfo->speed;

  sprintf(timemark,"%4d/%02d/%02d %2d:%02d:%02d.%09d",year,month,date,hh,mm,ss,nsec);
  return( timemark );
}

void mk5b_HeaderTime(Mark5B_INFO *mk5binfo, Mark5B_FRAMEHEADER *pH, char *memo)
{
  // Mark5B DF Header 16byte 정보 출력
  //   Years since 2000    : year-2000 : NOT USED in the **BEAD style** Mark5B recorder
  //   User-specified data : 0
  //   internal TVG flag   : 0 for data, 1 for TVG
  //   Frame number within second : 0 ~ 12799 or 25599
  //   JJJ  = lower 3 digits of MJDN : 000~999 : NEED the external information
  //          DOY+1 : For version 3 ("+1" is BUG, introduced by "OLD software")
  //   SOD  = Seconds of Day    : 0 ~ 86399
  //   SOD2 = fractional 1/10000 seconds  : 0 ~ 9999
  //   CRCC = 

#if 0
  //As four unsigned integers
  unsigned int *p = (unsigned int *)pH;
  fprintf(stderr,"%08X %08X %08X %08X %s\n",*p,*(p+1),*(p+2),*(p+3),memo?memo:"");
#endif

#if 0
  //Style as NAME=value 
  if(pH->sync == 0xABADDEED) fprintf(stderr,"SYNC_OK  "); else fprintf(stderr,"%08X ",pH->sync);
  fprintf(stderr,"YR=%2u UD=%4u TVG=%1u FN=%5u ",pH->year2000,pH->userdata,pH->isTVG,pH->frameNum);
  if(mk5binfo->version==3) fprintf(stderr,"DOY=%3u ",iBCD(pH->bcdJJJ)-1); // DOY, "-1" for fixing BUG
  else                     fprintf(stderr,"JJJ=%3u ",iBCD(pH->bcdJJJ));   // JJJ
  fprintf(stderr,"SOD=%5u.%04u CRCC=%04x ",iBCD(pH->bcdSOD),iBCD(pH->bcdSOD2),pH->crcc);
  // YR/DOY/SOD.SOD2로 1/10000초 단위로 표현 가능하지만, 
  // DF는 1/(12800*mk5binfo->speed)초 간격이므로 정확한 시각은 아님.
  // Frame number를 이용하여 정확한 시각을 계산할 수 있음.
  if(mk5binfo->version==3) fprintf(stderr,": %s %s\n",mk5b_time1_doy(mk5binfo,pH),memo?memo:"");
  else                     fprintf(stderr,": %s %s\n",mk5b_time1(mk5binfo,pH),memo?memo:"");
#endif

  //Separate NAME and value (for short line)
  //  Nano-seconds accuray with mk5b_time1() or mk5b_time1_doy().
  //  After all done, print one newline to stderr for better looking.
  static int first=1;
  unsigned int *p = (unsigned int *)pH;
  if(first) fprintf(stderr,"Using MJDepoch=%u :\n",mk5binfo->MJDepoch);
  switch(mk5binfo->version) {
  case 3: // 'Software using year2000,bcdDOY'
    if(first) fprintf(stderr,"SYNCWORD YR UD TVG FramN DOY Sec_of_Day CRCC : Calendar_dates_and_exact-time\n");
    if(pH->sync == 0xABADDEED) fprintf(stderr,"\r%08x ",pH->sync); else fprintf(stderr,"\r%08X ",pH->sync);
    fprintf(stderr,"%02u %04u %1u %5u ",pH->year2000,pH->userdata,pH->isTVG,pH->frameNum);
    fprintf(stderr,"%3u ",iBCD(pH->bcdJJJ)-1); // DOY, "-1" for fixing BUG
    fprintf(stderr,"%5u.%04u %04x ",iBCD(pH->bcdSOD),iBCD(pH->bcdSOD2),pH->crcc);
    fprintf(stderr,": %s %s",mk5b_time1_doy(mk5binfo,pH),memo?memo:"");
    break;
  case 2: // '**NULL style** KVN Mark5B recorder'
  case 1: // '**BEAD style** KVN Mark5B recorder'
    if(first) fprintf(stderr,"SYNCWORD B EAD TVG FramN JJJ Sec_of_Day CRCC : Calendar_dates_and_exact-time\n");
    if(pH->sync == 0xABADDEED) fprintf(stderr,"\r%08x ",pH->sync); else fprintf(stderr,"\r%08X ",pH->sync);
    fprintf(stderr,"%1x %03x %3u %5u ",pH->year2000,pH->userdata,pH->isTVG,pH->frameNum);
    fprintf(stderr,"%3u ",iBCD(pH->bcdJJJ));   // JJJ
    fprintf(stderr,"%5u.%04u %04x ",iBCD(pH->bcdSOD),iBCD(pH->bcdSOD2),pH->crcc);
    fprintf(stderr,": %s %s",mk5b_time1(mk5binfo,pH),memo?memo:"");
    break;
  case 0: // 'Unknown'
  default:
    if(first) fprintf(stderr,"SYNCWORD Word1    Word2    Word3    Memo\n");
    fprintf(stderr,"\r%08X %08X %08X %08X %s",*p,*(p+1),*(p+2),*(p+3),memo?memo:"");
    break;
  }
  if(first) { fprintf(stderr,"\n"); first = 0; }
}

//=========================================================================
// mk5b 파일의 입출력 범위를 지정할 때,  Frame 단위 뿐만 아니라
// Sec 단위 또는 Byte 단위로도 입력할 수 있도록 하기 위하여 사용한다.
// Available Units : "frame", "sec" or "byte"
//    (1) "sec" or "byte" 이외의 경우, "frame" 단위로 간주한다.
//    (2) "sec"의 경우, mk5binfo->nDF_1s의 값이 필요하므로,  반드시 
//        mk5b_info() 함수를 호출한 다음, 사용하도록 한다.
// #s ==> # * mk5binfo->nDF_1s [frameNum]
// #b ==> # / LEN_MK5B_DF [frameNum]
int mk5b_DFunit(Mark5B_INFO *mk5binfo, char *s)
{
  int v = 0;
  if(s) {
    v = atoi( s );
    while(*s == '+' || *s == '-' || isdigit(*s)) s++;
    if(*s == 's' || *s == 'S') v *= mk5binfo->nDF_1s;
    else if(*s == 'b' || *s == 'B') v /= LEN_MK5B_DF;
  }
  return( v ); // [frameNum]
}

//=========================================================================
Mark5B_INFO *mk5b_info(char *prefix, char *fn)
{
  Mark5B_INFO *mk5binfo=NULL;
  Mark5B_FRAMEHEADER H,H2; // temporary buffer for Mark5B DataFrame Header
  struct stat sb;
  FILE *fi;
  int n,iDF;
  off_t nDF,nSec,mDF,nByte;
  char *p;

  //local function: iSec 초, iDF 째 Header (16bytes)를 읽음
  //  mk5binfo->speed 가 정해진 뒤에 사용할 것.
  //  (아직 speed가 정해지지 않았더라도, iSec=0인 동안에는 speed=1로 가정하고 사용해도 같은 결과를 얻는다.)
  int read_header(FILE *fi,off_t iSec,int iDF,Mark5B_FRAMEHEADER *H)
  {
    off_t offset;
    offset = (iSec*12800L*mk5binfo->speed + iDF) * LEN_MK5B_DF;
    if(fseek(fi,offset,SEEK_SET) == -1) {
      fprintf(stderr,"%s Unable to fseek to the Data Frame (iDF=%d).\n",prefix,iDF);
      return(1);
    }//fseek
    if(fread(H,sizeof(char),LEN_MK5B_DF_HEADER,fi) != LEN_MK5B_DF_HEADER) {
      fprintf(stderr,"%s Unable to read the Data Frame Header (iDF=%d).\n",prefix,iDF);
      return(2);
    }//fread
    if(H->sync != 0xABADDEED) { // Header가 아닌 듯.
      fprintf(stderr,"%s Data Frame Header (iDF=%d) is INVALID.\n",prefix,iDF);
      return(3);
    }//sync
    return(0);
  }//read_header

  fprintf(stderr,"%s================================================================\n",prefix);

  //mk5binfo용 메모리 확보 ==> 프로그램이 종료될 때까지 해제하지 않을 것임.
  if((mk5binfo = calloc(1,sizeof(Mark5B_INFO))) == NULL) {
    fprintf(stderr,"%s Unable to calloc() for Mark5B_INFO.\n",prefix);
    return(NULL);
  }//mk5binfo

  // Set MJDepoch
  n = 56; // MJDepoch 56000 = 2012/03/14(074) ~ 2014/12/08(342)
  p = getenv("MARK5B_MJD_EPOCH");
  if(p) {
    n = atoi(p);
    if(n>1000) n = n/1000;
    if(n<54 || 58<n ) n = 56;
  }
  mk5binfo->MJDepoch = n*1000;

  //mk5binfo->pathname
  //mk5binfo->filename
  mk5binfo->pathname = fn;
  mk5binfo->filename = (p = strrchr(fn,'/')) == NULL ? fn : p+1;
  fprintf(stderr,"%s Filename : %s\n",prefix,mk5binfo->filename);

  //mk5binfo->filesize
  if(stat(fn,&sb) == -1) {
    fprintf(stderr,"%s Fail to stat '%s'\n",prefix,fn);
    return(NULL);
  }//stat
  mk5binfo->filesize = sb.st_size; // filesize in bytes

  //mk5binfo->offset0 : 첫 정초 DF의 위치
  //mk5binfo->speed   : 1 or 2 Gbps
  //mk5binfo->version : 0=Unknown,
  //                    1=**BEAD style** KVN Mark5B recorder,
  //                    2=**NULL style** KVN Mark5B recorder,
  //                    3=Software using year2000,bcdDOY
  mk5binfo->offset0 = 0;
  mk5binfo->speed = 1;	// speed를 확정짓기 전(초기값 0으로 설정되어 있었음), 첫 정초 DF을 발견하였을 때, 
			// FirstTic 정보출력을 위해 mk5b_time1(mk5binfo,&H)를 호출하므로,
			// "devide by zero" 발생함. 정초의 경우 frameNum=0이므로 speed와 무관하게 항상 0임.
  mk5binfo->version = 0;// Unknown
  if((fi = fopen(fn,"r")) == NULL) {
    fprintf(stderr,"%s Fail to open '%s'\n",prefix,fn);
    return(NULL);
  }//fopen
  for(iDF=0;;iDF++) { // frameNum이 0인 DF를 찾고, 128 DF 뒤의 시각을 점검.
    if(read_header(fi,0,iDF,&H) != 0) {
      fprintf(stderr,"%s Invalid mark5b header at iSec=0 iDF=%d of '%s'\n",prefix,iDF,fn);
      return(NULL);
    }//read_header
    if(H.frameNum == 0) {
      //x 정초 frame 발견
      mk5binfo->offset0 = iDF * LEN_MK5B_DF;

      //x   이 시점에서 가장 먼저 기록속도와 파일 버전을 판별한다.
      //x   헤더의 내용(특히, 시각)을 출력하려고 하면, speed 및 version을 알아야 하기 때문에.
      //x 
      //
      //x 기록속도를 판별하는 방법
      //x 정밀도가 떨어지는 SOD2값을 이용하여 speed를 판단하기 위하여,
      //x 적어도 파일 길이가 1/100초 분량 (128 DF) 이상은 있을 것이라는 가정하에 
      //x 128 DF 다음의 frameNum과 SOD2값을 비교하여, speed를 판단하자.
      //x   (a) (int)(H.frameNum*0.78125) = 100, always
      //x   (b) iBCD(pH->bcdSOD2) = 100 if 1Gbps, 50 if 2Gbps
      //x       일부 DF가 사라졌다면, 이 값은 100 또는 50 보다 작을 수 있다.
      //x       그렇지만, 기껏해야 1~2 정도일 테니, 나눗셈의 몫이 바뀌지는 않을 것이다.
      if(read_header(fi,0,iDF+128,&H2) != 0) {
        fprintf(stderr,"%s Invalid mark5b header at iSec=0 iDF=%d of '%s'\n",prefix,iDF+128,fn);
        return(NULL);
      }//read_header
      mk5binfo->speed = (int)(H2.frameNum*0.78125) / iBCD(H2.bcdSOD2);
      if(mk5binfo->speed != 1 && mk5binfo->speed != 2) {
        fprintf(stderr,"%s Fail to determine Datarate : %d Gbps ?\n",prefix,mk5binfo->speed);
        return(NULL);
      }

      //x 시작시각을 기억해 둔다. ==> 시각시각부터 헤아린 frameNumTotal을 계산할 때 사용
      mk5binfo->jjj0 = iBCD(H.bcdJJJ);
      mk5binfo->sod0 = iBCD(H.bcdSOD);

      //x isTGV 값을 기억해 둔다.
      mk5binfo->isTVG = H.isTVG;

      //x 파일 버전을 판별하는 방법
      //x version                                           year2000 userdata bcdJJJ
      //x 1 = '**BEAD style** KVN Mark5B recorder'          0xb      0xead    JJJ
      //x 2 = '**NULL style** KVN Mark5B recorder'          0x0      0x000    JJJ
      //x 3 = 'Software which is using year2000 and bcdDOY' year2000 0x000    DOY
      //x 0 = 'Unknown'                                     ?        ?        ?
      //x 
      //x unsigned int userdata : 12; // BIT27~16 User-specified data
      //x unsigned int year2000 :  4; // BIT31~28 Integer Years since 2000
      //x 
      //x KVN site의 Mark5B는 요즘(firmware update 후?)에는 version 1 파일을 만든다.
      //x                    예전(firmware update 전?)에는 version 2 파일이 있더라.
      //x 내가 만든 프로그램에서, 실수(?)로 version 3 형태의 파일을 만들었다. 따지고 보면,
      //x year2000 + DOY 조합으로 "독립적"으로 시각을 나타낼 수 있는 좋은 방식이다.
      //x (version 1 형식의 파일은 외부에서 MJD의 상위 두 자리수(MJDepoch)를 줘야만 한다.)
      //x 
      //x 버전에 맞춰, 시각을 표시한다.
      if(H.year2000==0xb && H.userdata==0xead) {
        mk5binfo->version = 1; // 1=KVN Mark5B recorder
        fprintf(stderr,"%s Version  : Seems to be written by **BEAD style** KVN Mark5B recorder\n",prefix);
        fprintf(stderr,"%s            (using MJDepoch=%u and JJJ=%u)\n",prefix,mk5binfo->MJDepoch, mk5binfo->jjj0);
        fprintf(stderr,"%s FirstTic : %s at offset %lu\n",prefix,mk5b_time1(mk5binfo,&H),mk5binfo->offset0);

      }else if(H.year2000==0 && H.userdata==0) {
        mk5binfo->version = 2; // 2=KVN Mark5B recorder
        fprintf(stderr,"%s Version  : Seems to be written by **NULL style** KVN Mark5B recorder\n",prefix);
        fprintf(stderr,"%s            (using MJDepoch=%u and JJJ=%u)\n",prefix,mk5binfo->MJDepoch, mk5binfo->jjj0);
        fprintf(stderr,"%s FirstTic : %s at offset %lu\n",prefix,mk5b_time1(mk5binfo,&H),mk5binfo->offset0);

      }else if(H.year2000!=0 && H.userdata==0) {
        int mjdn, doy;
        mk5binfo->version = 3; // 3=Software using year2000,bcdDOY
        doy = iBCD(H.bcdJJJ) - 1; // "-1" for fixing the BUG, introduced by "OLD software"
        fprintf(stderr,"%s Version  : Seems to be written by Software which is using year2000 and bcdDOY\n",prefix);
        fprintf(stderr,"%s            (using year2000=%u and DOY=%u)\n",prefix,H.year2000,doy);
        fprintf(stderr,"%s FirstTic : %s at offset %lu\n",prefix,mk5b_time1_doy(mk5binfo,&H),mk5binfo->offset0);
        // Calc jjj0
        mjdn = MJDN(2000+H.year2000,1,doy);
        mk5binfo->jjj0 = mjdn % 1000;

      }else{
        fprintf(stderr,"%s Fail to determine Version : year2000=0x%1x and userdata=0x%03x\n",prefix,H.year2000,H.userdata);
        return(NULL);
      }

      break;
    }//H.frameNum
  }//iDF
  //======== mk5binfo->speed has been determined.

  // Show LastTic
  nSec = mk5binfo->filesize / LEN_MK5B_DF / (12800*mk5binfo->speed);
  for(iDF=0;;iDF++) { // frameNum이 0인 DF를 찾자.
    if(read_header(fi,nSec-1,iDF,&H) != 0) {
      fprintf(stderr,"%s Invalid mark5b header at iSec=%ld iDF=%d of '%s'\n",prefix,nSec-1,iDF,fn);
      break;
    }//read_header
    if(H.frameNum == 0) {
      if(mk5binfo->version==3) fprintf(stderr,"%s  LastTic : %s ",prefix,mk5b_time1_doy(mk5binfo,&H));
      else                     fprintf(stderr,"%s  LastTic : %s ",prefix,mk5b_time1(mk5binfo,&H));
      fprintf(stderr,"at offset %lu\n",ftell(fi)-sizeof(H));
      break;
    }//H.frameNum
  }//iDF

  fclose(fi);

  fprintf(stderr,"%s Datarate : %dGbps (VSI-%dMHz)",prefix,mk5binfo->speed,32*mk5binfo->speed);
  //mk5binfo->nDF_1s : 초당 DF의 갯수		:  12800 or  25600 DFs/s
  //mk5b               초당 header의 총 용량	: 204800 or 409600 bytes/s = mk5binfo->nDF_1s * LEN_MK5B_DF_HEADER
  //mk5b               초당 data의 총 용량		:    128 or    256 MB/s    = mk5binfo->nDF_1s * LEN_MK5B_DF_DATA
  mk5binfo->nDF_1s      = 12800 * mk5binfo->speed;
  fprintf(stderr," ==> %d DF/s (where %d bytes/DF)\n",mk5binfo->nDF_1s,LEN_MK5B_DF);

  // isTVG 값과 "시작시각(첫 정초 DF)"의 offset 값
  fprintf(stderr,"%s isTVG    : %s\n",prefix,mk5binfo->isTVG?"Yes":"No");
  //fprintf(stderr,"offset0  : %lu bytes to First Second-Tic DF.\n",mk5binfo->offset0);

  // Test if the filesize is an integer multiple of "1 second"
  fprintf(stderr,"%s Filesize : %lu bytes\n",prefix,mk5binfo->filesize);
  nDF   = mk5binfo->filesize / LEN_MK5B_DF;
  nByte = mk5binfo->filesize % LEN_MK5B_DF;
  fprintf(stderr,"%s          = %lu DFs",prefix,nDF);
  if(nByte) fprintf(stderr," + %lu bytes",nByte);
  fprintf(stderr,"\n");
  nSec = nDF / mk5binfo->nDF_1s;
  mDF  = nDF % mk5binfo->nDF_1s;
  fprintf(stderr,"%s          = %lu Seconds",prefix,nSec);
  if(mDF) fprintf(stderr," + %lu DFs",mDF);
  if(nByte) fprintf(stderr," + %lu bytes",nByte);
  fprintf(stderr,"\n");

  fprintf(stderr,"%s================================================================\n",prefix);
  return(mk5binfo);
}


