// stopwatch.c

// How to use :
// (1) Include this file
//     #include "stopwatch.c"
//
// (2) Preapre a pointer to your stopwatch
//     STOPWATCH *sw1;
//
// (3) Allocate your stopwatch with name and the FILE pointer for message output.
//     sw1 = alloc_stopwatch("first",stdout);
//
// (4) Control your stopwatch before and after the job what to be measured
//     as any sequence of start/job/stop or start/job/stop/start/job/stop.
//     Whenever you want to know the stopwatch value, you can ask to report.
//     start_stopwatch(sw1);
//     stop_stopwatch(sw1);
//     report_stopwatch(sw1);
//
//     If you want to restart an existing stopwatch, use this.
//     clear_stopwatch(sw1);
//
// (5) Free it when you do not need anymore.
//     free_stopwatch(sw1);
//
#include <stdio.h>
#include <stdlib.h> // exit()
#include <string.h> // strncmp(), strlen()
#include <sys/time.h>

typedef struct stopwatch_t{
  struct timeval ts;
  double sec;
  char name[16];  // Name of this Stopwatch
  int  stopped;   // 1=stopped 0=running
}STOPWATCH;

//------------------------------------------------------------------------------

STOPWATCH *alloc_stopwatch(char *name, FILE *fp)
{
  STOPWATCH *stopwatch = (STOPWATCH *)calloc(1,sizeof(STOPWATCH));
  if(stopwatch) {
    strncpy(stopwatch->name,name,15); stopwatch->name[15] = 0;
    stopwatch->stopped = 1;
  }else{
    perror("Fail to allocate a StopWatch");
  }
  return(stopwatch);
}

void clear_stopwatch(STOPWATCH *stopwatch)
{
  stopwatch->stopped = 1;    // stopped
  stopwatch->sec     = 0.0;  // initialize
  stopwatch->ts.tv_sec = stopwatch->ts.tv_usec = 0L; // clear start time
}

void start_stopwatch(STOPWATCH *stopwatch)
{
  if(stopwatch->stopped) {
    gettimeofday(&stopwatch->ts, NULL); // mark start time
    stopwatch->stopped = 0;
  }
}

void stop_stopwatch(STOPWATCH *stopwatch)
{
  struct timeval te;
  if(stopwatch->stopped==0) {
    gettimeofday(&te, NULL); // mark stop time
    stopwatch->sec += (te.tv_sec - stopwatch->ts.tv_sec) +
      (te.tv_usec - stopwatch->ts.tv_usec)*1.e-6;      // accumulate this duration [sec]
    stopwatch->ts.tv_sec = stopwatch->ts.tv_usec = 0L; // clear start time
    stopwatch->stopped = 1;
  }
}

void report_stopwatch(STOPWATCH *stopwatch)
{
  fprintf(stderr,"StopWatch %s : %.6f sec%s\n",stopwatch->name,stopwatch->sec,
    stopwatch->stopped ? "" : " -- running");
}

void free_stopwatch(STOPWATCH *stopwatch)
{
  free(stopwatch);
}

//------------------------------------------------------------------------------

