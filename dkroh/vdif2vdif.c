// vdif2vdif.c
//
// Convert a vdif file to 'standard' vdif file.
// Input file could be one of following types.
//   type 2 = OCTA VDIF with Capture Header
//   type 1 = OCTA VDIF
//   type 0 = Standard VDIF

// in.vdif 파일이 OCTA VDIF 형식의 파일일 경우, (data length 고정 = 1280 bytes)
//   (256 바이트의 파일 헤더 다음에,)
//   매초마다 (1428 바이트의 1초 헤더와) 10만*Speed 개의 packet이 반복된다.
//   각 packet은 32 bytes의 packet 헤더와 1280 bytes의 데이터로 구성된다.
//
// in.vdif 파일이 standard VDIF 형식의 파일일 경우, (data length 가변)
//   매초마다 nPkt_in*Speed 개의 packet이 반복된다.
//   FrameLen_in  = 32 + data_length
//   nPkt_in      = DATA_1SECOND / data_length       // # of packets/sec @ 1Gbps
//
// out.vdif 파일은 항상 standard VDIF 형식의 파일로 되며, (data length 가변)
//   매초마다 nPkt_out*Speed 개의 packet이 반복된다.
//   FrameLen_out = 32 + data_length
//   nPkt_out     = DATA_1SECOND / data_length       // # of packets/sec @ 1Gbps
//
// 이 프로그램에서는,
// 1초 분량의 data (nPkt_in *Speed 개의 packet)를 읽어서, 
// 1초 분량의 data (nPkt_out*Speed 개의 packet)를 기록하는 작업을 반복한다.

// OCTA VDIF (with Capture Header) 파일은 "BigEndian", "Network Byte Order" 이다.
// - "data"는 character stream으로 이해, 즉 byte-swap이 불필요하다.
// - "vdif header"에 있는 정보를 uint형으로 접근하려면, byte-swap 필요하다 -- ntohl() 사용.
// - "1sec header"에 있는 정보와 "file header"에 있는 정보를 uint형으로 접근하려면,
//   byte-swap 필요할 것임. (아직 구조를 몰라서, 시도하지 않았음.)
// - mk5b 파일과 비교를 통해, "data" 속의 각 byte는 서로 bit-swap된 관계에 있음을 알고 있다.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <arpa/inet.h>	// ntohl()
#include <ctype.h>

#include "stopwatch.c"
#include "calendar.c"
#include "vdif.c"

int main(int argc, char **argv)
{
  FILE *fi,*fo;
  size_t n,m;
  long offset;
  unsigned int iSec,iPkt;

  unsigned int  *pi;
  unsigned char *pc,*pd;

  int InputFileType = 0; // 0=Standard VDIF, 1=OCTA VDIF, 2=OCTA VDIF with Capture Header
//  int setTVG = 0;        // OCTA CAPTURE FILE only may alter this value.
  int Speed = 1;         // 1 or 2 Gbps

  // variables for set options : 
  //   default < 0  : "Don't change the original, unless user supplies a value." 
  //   0 <= default : "Force to change the original to this default or user supplied value."
  int Log2Nchans =  4; // 0,1,2,3,4 for Nchans=1,2,4,8,16
  int NbitsM1    =  1; // force to "2 bits/sample"
  int ThreadID   = -1; // 0 ~ 1023
  int StationID  = -1; // Two char. or 16-bits integer
  int FrameLen_out = VDIF_PKTS_HEADER+OCTAVDIF_PKTS_DATA; // default = 32 + 1280
  int SecsOffset = 0;  // will be added to 'secs' just after read.

  int FrameLen_in, nPkt_in,nPkt_out;
  int i,j,k;
  int mjdn, year,month,date, hh,mm,ss, doy, sod;
//  VDIF_FRAMEHEADER *vh;
  VDIF_INFO *vdifinfo = NULL;

  //============================================================================
  // read buffer
  unsigned char buf[ OCTACAPTURE_FILE_HEADER //  256
                    +OCTACAPTURE_SECD_HEADER // 1428
                    +OCTAVDIF_PKTS_HEADER    //   32
                    +OCTAVDIF_PKTS_DATA      // 1280
                ];                           // 2996 bytes in total
//CAPT_FILE_HEADER      *CF = (CAPT_FILE_HEADER      *)(buf);
//CAPT_SECD_HEADER      *CS = (CAPT_SECD_HEADER      *)(buf+OCTACAPTURE_FILE_HEADER);
  OCTA_VDIF_FRAMEHEADER *VH = (OCTA_VDIF_FRAMEHEADER *)(buf+OCTACAPTURE_FILE_HEADER+OCTACAPTURE_SECD_HEADER);
  OCTA_VDIF_DATA        *VD = (OCTA_VDIF_DATA        *)(buf+OCTACAPTURE_FILE_HEADER+OCTACAPTURE_SECD_HEADER+OCTAVDIF_PKTS_HEADER);

  //============================================================================
  // write buffer
  VDIF_FRAMEHEADER VH1;      // input  vdif header (copied for later use)
  VDIF_FRAMEHEADER VH2;      // output vdif header (for writing)
  void             *vd;      // pointer to vdif data 

  unsigned char *data = NULL; // Data buffer for 1 second (128 or 256 MB)

  STOPWATCH *sw_all = alloc_stopwatch("Total       ",NULL);
  STOPWATCH *sw_rea = alloc_stopwatch("ReadData    ",NULL);
  STOPWATCH *sw_wri = alloc_stopwatch("WriteData   ",NULL);

  //============================================================================

  // Command Line Parameters
  for(i=1; i<argc; i++) { // pick-up the set options first
    char *eqs;
    if((eqs = index(argv[i],'=')) != NULL) {
      if(strncasecmp(argv[i],"Nchans",3)==0) {
        k = ilog2( atoi(eqs+1) );
        if(0<=k && k<=4) Log2Nchans = k;
        else fprintf(stderr,"Ignored the invalid set option, %s\n",argv[i]);
      }else if(strncasecmp(argv[i],"Nbits",3)==0) {
        k = atoi(eqs+1);
        if(1<=k && k<=32) NbitsM1 = k - 1;
        else fprintf(stderr,"Ignored the invalid set option, %s\n",argv[i]);
      }else if(strncasecmp(argv[i],"ThreadID",3)==0) {
        k = atoi(eqs+1);
        if(0<=k && k<=1023) ThreadID = k;
        else fprintf(stderr,"Ignored the invalid set option, %s\n",argv[i]);
      }else if(strncasecmp(argv[i],"StationID",3)==0) {
        if(strlen(eqs+1)>=2) { StationID = 0; memcpy((void *)&StationID,(void *)(eqs+1),2); }
        else fprintf(stderr,"Ignored the invalid set option, %s\n",argv[i]);
      }else if(strncasecmp(argv[i],"FrameLen",3)==0) {
        k = atoi(eqs+1);
        j = k - OCTAVDIF_PKTS_HEADER; // data length
        if(OCTAVDIF_PKTS_DATA<=j && (DATA_1SECOND/j)*j == DATA_1SECOND) FrameLen_out = k;
        else fprintf(stderr,"Ignored the invalid set option, %s\n",argv[i]);
      }else if(strncasecmp(argv[i],"SecsOffset",3)==0) {
        SecsOffset = atoi(eqs+1);
      }else{
        fprintf(stderr,"Ignored the unknown set option, %s\n",argv[i]);
      }
      argv[i] = NULL;
    }
  }
  for(i=1; i<argc; i++) { // clean up
    if(argv[i]==NULL) {
      for(k=i+1; k<argc; k++) {
        if(argv[k]!=NULL) { argv[i] = argv[k]; argv[k] = NULL; break; }
      }
      if(k==argc) { argc = i; break; }
    }
  }
  if(argc < 2) {
    fprintf(stderr,"Usage: $ %s in.vdif [out.vdif] [set options ...]\n",argv[0]);
    fprintf(stderr,
      "  where in.vdif  = input, 'standard', 'octa', or 'octa with Capture Header' vdif file\n"
      "        out.vdif = output, 'standard' vdif file\n"
      "  set options:\n"
      "    Nchans=#       Number of Channels, default=16 for C5 mode\n"
      "    Nbits=#        Number of bits per sample, default=2 for normal\n"
      "    ThreadID=#     Thread ID, 0~1023, no default.\n"
      "    StaionID=XX    Two char. Station ID, no default. Use KY for KVN-Yonsei\n"
      "    FrameLen=#     output VDIF packet frame size, default=%u bytes\n"
      "    SecsOffset=#   Secs Offset (will be added just after read)\n"
      ,VDIF_PKTS_HEADER+OCTAVDIF_PKTS_DATA);
    exit(1);
  }

  //============================================================================

  // "Standard VDIF", "OCTA VDIF", or "OCTA VDIF with Capture Header" --> "Standard VDIF file"

  //===== Init global variables with the given input file
  vdifinfo = vdif_info("<<<",argv[1]);
  if(vdifinfo == NULL) exit(1);

//  setTVG        = vdifinfo->isTVG;
  InputFileType = vdifinfo->version;
  FrameLen_in   = vdifinfo->FrameLen;
  nPkt_in       = DATA_1SECOND / (FrameLen_in  - OCTAVDIF_PKTS_HEADER); // # of packets/sec @ 1Gbps
  Speed         = vdifinfo->speed;

  // Exit here when no output file
  if(argc < 3) {
    exit(0);
  }

  //============================================================================

  //===== Show the value of set options : forced or user-supplied only are effective.
  if(0<=Log2Nchans) {
    fprintf(stderr,"set options: Nchans = 2^%d\n",Log2Nchans);
  }
  if(0<=NbitsM1) {
    fprintf(stderr,"set options: Nbits = %d bits/sample\n",NbitsM1+1);
  }
  if(0<=ThreadID) {
    fprintf(stderr,"set options: ThreadID = %d\n",ThreadID);
  }
  if(0<=StationID) {
    pc = (unsigned char *)&StationID;
    fprintf(stderr,"set options: StationID = %c%c (%d)\n",
      isprint(*pc)?*pc:'?', isprint(*(pc+1))?*(pc+1):'?', StationID);
  }
  fprintf(stderr,"set options: FrameLen = %d bytes%s\n",FrameLen_out,
    (FrameLen_in==FrameLen_out) ? "" : " (changed)");
  if(SecsOffset) {
    fprintf(stderr,"set options: SecsOffset = %d\n",SecsOffset);
  }

  // Confirm if the # of packets within 1 seconds for output is proper.
  // nPkt_out will be 100000 when FrameLen_out=OCTAVDIF_PKTS_HEADER+OCTAVDIF_PKTS_DATA=1312,
  // or some proper integer value.
  nPkt_out = DATA_1SECOND / (FrameLen_out - OCTAVDIF_PKTS_HEADER); // # of packets/sec @ 1Gbps
  if(nPkt_out*(FrameLen_out - OCTAVDIF_PKTS_HEADER) != DATA_1SECOND) {
    double x = DATA_1SECOND / ((double)FrameLen_out - OCTAVDIF_PKTS_HEADER);
    fprintf(stderr,"FrameLen of output file    = %d bytes\n",FrameLen_out);
    fprintf(stderr,"# of packets/sec (@1Gbps)  = %f : Something wrong!\n",x);
    exit(1);
  }
  fprintf(stderr,"\n");

  //============================================================================

  if((fi = fopen(argv[1],"r")) == NULL) {
    fprintf(stderr,"Fail to open '%s'\n",argv[1]);
    exit(1);
  }

  //===== Create output file
  if((fo = fopen(argv[2],"w")) == NULL) {
    fprintf(stderr,"Fail to open '%s'\n",argv[2]);
    exit(1);
  }

  // 1초간의 데이터를 모두 담을 공간을 마련한다. 128MB or 256MB
  if((data = calloc(DATA_1SECOND*Speed,sizeof(char))) == NULL) {
    fprintf(stderr,"Fail to calloc, %u bytes\n",DATA_1SECOND*Speed);
    exit(1);
  }

  // Start to convert
  offset = 0L;
  if(InputFileType == 2) offset += OCTACAPTURE_FILE_HEADER;
  fseek(fi,offset,SEEK_SET); // skip the OCTA FILE HEADER, if any

  // 1초 단위로 파일 변환
  start_stopwatch(sw_all);
  for(iSec=0;;iSec++) {

    if(InputFileType == 2) fseek(fi,OCTACAPTURE_SECD_HEADER,SEEK_CUR); // skip the OCTA SECS HEADER

    // read "nPkt_in*Speed" Packets for 1 sec
    start_stopwatch(sw_rea);
    m = FrameLen_in - OCTAVDIF_PKTS_HEADER; // (input) vdif data length
    for(iPkt=0; iPkt<nPkt_in*Speed; iPkt++) {

      // Read one input packet
      if(InputFileType) {
        //===== PACKET HEADER + PACKET DATA 읽기
        if((n = fread(VH,sizeof(unsigned char),FrameLen_in,fi)) != FrameLen_in) {
          if(feof(fi)) break; // EOF
          fprintf(stderr,"Fail to read OCTA VDIF HEADER+DATA at iSec=%u, iPkt=%u. n=%lu/%u\n",iSec,iPkt,n,FrameLen_in);
          break; // Ignore the partial packet
        }
        // PACKET HEADER : "BigEndian" to "LittleEndian"
        pi = (unsigned int *)VH;
        for(k=0;k<8;k++) pi[k] = ntohl( pi[k] );
        // PACKET DATA : Do bitswap for each byte using the pre-defined table, put into buffer.
        pc = (unsigned char *)VD;
        pd = data + iPkt*m;
        for(k=0; k<m; k++) pd[k] = bit_swap_tbl[ pc[k] ];
      }else{
        //===== PACKET HEADER 읽기
        if((n = fread(VH,sizeof(unsigned char),VDIF_PKTS_HEADER,fi)) != VDIF_PKTS_HEADER) {
          if(feof(fi)) break; // EOF
          fprintf(stderr,"Fail to read VDIF HEADER at iSec=%u, iPkt=%u. n=%lu/%u\n",iSec,iPkt,n,VDIF_PKTS_HEADER);
          break; // Ignore the partial header
        }
        //===== PACKET DATA 읽기
        pd = data + iPkt*m;
        if((n = fread(pd,sizeof(unsigned char),m,fi)) != m) {
          if(feof(fi)) break; // EOF
          fprintf(stderr,"Fail to read VDIF DATA at iSec=%u, iPkt=%u. n=%lu/%lu\n",iSec,iPkt,n,m);
          break; // Ignore the partial data
        }
      }

      // 매 초 첫 패킷의 헤더를 이용하여, 시각 관리를 한다.
      //   사본 VH1 = 읽어들인 원본 헤더값 확인을 위해 사용. 현재, 맨 처음 한번만 출력함.
      //   사본 VH2 = 출력용 헤더, 각종 수정사항이 반영되도록 적절히 변경해야 함.
      if(iPkt==0) {

        // Save the (original) input vdif packet header for later printing.
        memcpy(&VH1,VH,VDIF_PKTS_HEADER);

        // Adjust VH.secs by SecsOffset ===== very special options =====
        //   OCTADDB/OCTADISK에서 직접 vdif packet을 받아 기록한 경우, 
        //   "secs" 값이 기대치+86400 인 문제점이 있음을 알게 되었다.
        //   이러한 경우에, set option으로 "SecsOffset=-86400"을 지정하면, 여기서 해결.
        if(SecsOffset) {
          int y2000,month_epoch,days, y,m,d, mjdn,sod;
          int new_secs = (int)VH->secs + SecsOffset; // Adjust
              // Use "int", instead of "unsigned int".
              // (It should be able to hold a possible negative value.
          // (VH->refepoch,VH->secs+SecsOffset) --> (y2000,month_epoch,days,sod)
          y2000 = VH->refepoch/2;
          month_epoch = (VH->refepoch%2 == 0) ? 1 : 7;
          days = 0;
          while(new_secs < 0) { // make sure that not-negative new_secs
            new_secs += 86400;
            days -= 1;
          }
          days += new_secs/86400;
          sod   = new_secs%86400;
          // (y2000,month_epoch,1+days) --> (mjdn) --> (y,m,d)
          mjdn = MJDN(2000+y2000, month_epoch, 1+days); // "1+days" is equal to "Date".
          MJDN2CDN(mjdn, &y,&m,&d);
          // (mjdn,sod; y,m) --> (VH->refepoch,VH->secs) 
          VH->refepoch = (y-2000)*2;
          if(m<7) {
            VH->secs = (mjdn - MJDN(y,1,1))*86400;
          }else{
            VH->refepoch += 1;
            VH->secs = (mjdn - MJDN(y,7,1))*86400;
          }
          VH->secs += sod; // Add the Seconds of Day
        }

        // Makeup the output vdif packet header
        memcpy(&VH2,VH,VDIF_PKTS_HEADER);
#if 0
    //==========================// Standard vdif packet header
    //VH->invalid = 0		// 0=valid,1=invalid
    //VH->legacy = 0		// 0=standard VDIF Data Frame Header (32bytes), 1=legacy Header Length mode (16bytes)
  ! //VH->secs = #		// Seconds from reference epoch
  ! //VH->refepoch = #		// Reference Epoch for secs count
  ! //VH->framenum = #		// Data Frame Number within second 
    //VH->version = 0		// VDIF version number (possible 7 future versions)
  o   VH->log2nchans = 4;	// log2(# of channels in Data Array)
  o //VH->framelen = 1312/8	// Data Frame Length (including Header), in 8 bytes unit
    //VH->datatype = 0		// 0=real, 1=complex
  o   VH->nbitsm1 = 1;		// #bits/sample - 1 (1 to 32 bits/sample)
  ?   VH->threadid		// Thread ID (0 to 1023)
  o   VH->stationid = 'XX';	// Station ID (2 char ASCII, or numeric)
  c //VH->EDV			// Extended Data Version, 0 or 1~255
  c //VH->EUD1			// EXtended User Data 1, 0 if EDV=0
  c //VH->EUD2			// EXtended User Data 2, 0 if EDV=0
  c //VH->EUD3			// EXtended User Data 3, 0 if EDV=0
  c //VH->EUD4			// EXtended User Data 4, 0 if EDV=0
#endif
        if(0<=Log2Nchans) VH2.log2nchans = Log2Nchans;     // default=4 for 2^4=16 channels
                          VH2.framelen   = FrameLen_out/8; // [bytes] to [8 bytes] for standard vdif
        if(0<=NbitsM1)    VH2.nbitsm1    = NbitsM1;        // force to "2 bits/sample"
        if(0<=ThreadID)   VH2.threadid   = ThreadID;       // 0~1023
        if(0<=StationID)  VH2.stationid  = StationID;      // Two char.
        VH2.EDV = VH2.EUD1 = VH2.EUD2 = VH2.EUD3 = VH2.EUD4 = 0;
      }
    }//iPkt
    stop_stopwatch(sw_rea);

    // Data ready ?
    if(iPkt < nPkt_in*Speed) {
      if(iPkt == 0) break; // No more data to be processed

      // Got a patial data (less than 1 second). Stop anyway. (Throw it away.)
      fprintf(stderr,"Warning: Partial second data, %d/%d packets.\n",iPkt,nPkt_in*Speed);
      break;
    }

    // write "nPkt_out*Speed" Packets for 1 sec
    start_stopwatch(sw_wri);
    m = FrameLen_out - VDIF_PKTS_HEADER; // (output) vdif data length
    for(iPkt=0; iPkt<nPkt_out*Speed; iPkt++) {
      //===== output VDIF Header
      VH2.framenum = iPkt;

      //===== 매 초마다 시각정보 출력
      if(iPkt==0) {
        char s1[2]={'\r',0}, s2[2]={0,0};

        // Once at first time
        if(iSec==0) {
          // Report the possible framenum error on the beginning of input file
          if(VH1.framenum != 0) printf("\t!!! WARNING -- 1PP mismatch %u\n",VH1.framenum);

          // input/output VDIF Header 출력
          printf("Input  VDIF_PKTS_HEADER\n");
          if(InputFileType) print_octavdif_frameheader((OCTA_VDIF_FRAMEHEADER *)&VH1);
          else              print_vdif_frameheader(&VH1);

          printf("Output VDIF_PKTS_HEADER\n");
          print_vdif_frameheader(&VH2);

          // Time-info Lookup header
          printf("\nRefEpoch Secs FrameNum >>  mjdn year/mo/dy(doy) hh:mm:ss (sec.s)\n");
          s1[0] = 0; s2[0] = '\n';
        }

        //===== Time-info
        time_vdif_frameheader(&VH2, &mjdn,&year,&month,&date,&hh,&mm,&ss,&doy,&sod);
        printf("%s%3d %9d %8d >> %5d %4d/%02d/%02d(%3d) %02d:%02d:%02d (%5d)%s",
          s1, VH2.refepoch,VH2.secs,VH2.framenum, mjdn, year,month,date,doy, hh,mm,ss, sod, s2);
        fflush(stdout);
      }

      //===== PACKET HEADER 쓰기
      if((n = fwrite(&VH2,sizeof(char),OCTAVDIF_PKTS_HEADER,fo)) != OCTAVDIF_PKTS_HEADER) {
        fprintf(stderr,"Fail to write a vdif packet header at iSec=%u, iPkt=%u. n=%lu/%u\n",iSec,iPkt,n,OCTAVDIF_PKTS_HEADER);
        break; // Stop here
      }
      //===== PACKET DATA 쓰기
      vd = (void *)(data + iPkt*m);
      if((n = fwrite(vd,sizeof(char),m,fo)) != m) {
        fprintf(stderr,"Fail to write a vdif packet data at iSec=%u, iPkt=%u. n=%lu/%lu\n",iSec,iPkt,n,m);
        break; // Stop here
      }
    }//iPkt
    start_stopwatch(sw_wri);

  }//iSec
  stop_stopwatch(sw_all);
  fprintf(stderr,"\nProcessed for %d seconds of Data.\n",iSec);

  // Stopwatch
  report_stopwatch(sw_rea); free_stopwatch(sw_rea);
  report_stopwatch(sw_wri); free_stopwatch(sw_wri);
  report_stopwatch(sw_all); free_stopwatch(sw_all);

  // Done.
  fclose(fi);
  fclose(fo);
  free(data);

  //===== Show info about the created vdif file
  vdif_info(">>>",argv[2]);

  return(0);
}


