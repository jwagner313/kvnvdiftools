// mk5b.h

// mark5b 파일은 data rate에 따라,
//   VSI-32MHz, 1Gbps Data rate의 경우, 1초에 12800개의 data frame이 존재
//   VSI-64MHz, 2Gbps Data rate의 경우, 1초에 25600개의 data frame이 존재
//
// Data frame은
//   16 bytes의 헤더와 10000 bytes의 데이터로 구성된다.

#include <sys/types.h>
#include <sys/stat.h>	// stat()
#include <unistd.h>		// getpagesize()
#include <ctype.h>		// isdigit()
#include <stdio.h>
#include <stdlib.h>

#define DATA_1SECOND   128000000 // Data rate at 1Gbps
//      mk5b = LEN_MK5B_DF_DATA [bytes/DataFrame] * (12800*Speed)            [DataFrames]
// octa vdif = OCTAVDIF_PKTS_DATA [bytes/packet] * (100000*Speed)            [packets]
//  std vdif = (FrameLen_out-VDIF_PKTS_HEADER) [bytes/packet] * (nPkt*Speed) [packets]

#ifndef __mk5b_h__
#define __mk5b_h__

// Header definition:
//
//  4-bits of 2nd word : defined as 'year2000', but not used in Mark5B
//
// 12-bits of 3rd word : last 3 digits of MJD
//    Therefore, we need an "external" information about current MJD epoch.
//    MJDepoch 54000 = 2006/09/22(265) ~ 2009/06/17(168)
//    MJDepoch 55000 = 2009/06/18(169) ~ 2012/03/13(073)
//    MJDepoch 56000 = 2012/03/14(074) ~ 2014/12/08(342)
//    MJDepoch 57000 = 2014/12/09(343) ~ 2017/09/03(246)
//    MJDepoch 58000 = 2017/09/04(247) ~ 2020/05/30(151)

typedef struct Mark5B_frameheader_t {
  unsigned int sync;          // 0xABADDEED

  unsigned int frameNum : 15; // BIT14~00 Frame number within second (starting at 0)
  unsigned int isTVG    :  1; // BIT15    data is from internal TVG
  unsigned int userdata : 12; // BIT27~16 User-specified data
  unsigned int year2000 :  4; // BIT31~28 Integer Years since 2000

  unsigned int bcdSOD   : 20; // BIT19~00 VLBA BCD Time Code Word 1 ('---SSSSS')
  unsigned int bcdJJJ   : 12; // BIT31~20 VLBA BCD Time Code Word 1 ('JJJ-----')

  unsigned int crcc     : 16; // BIT15~00 16-bit CRCC
  unsigned int bcdSOD2  : 16; // BIT31~16 VLBA BCD Time Code Word 2 ('.SSSS')
} Mark5B_FRAMEHEADER;

typedef struct Mark5B_framedata_t {
  unsigned int data[2500]; // Frame Data
} Mark5B_FRAMEDATA;

typedef struct Mark5_diskframe_t {
  union {                  // Frame Header
  unsigned int head[4];
  Mark5B_FRAMEHEADER H;
  };
  Mark5B_FRAMEDATA D;      // Frame Data
} Mark5B_DATAFRAME;

#define LEN_MK5B_DF_HEADER    16 // sizeof(unsigned int)*4
#define LEN_MK5B_DF_DATA   10000 // sizeof(unsigned int)*2500
#define LEN_MK5B_DF        10016 // Header(16) + Data(10000)

//                 1 DF   = 10000byte * 8bits/byte / 2bits/sample = 40000 samples
// VSI-32MHz   12800 DF/s = 40000samples/DF * 12800DF/s =  512x10^6 samples/s (1024Mbps / 2bits/sample)
// VSI-64MHz   25600 DF/s = 40000samples/DF * 25600DF/s = 1024x10^6 samples/s (2048Mbps / 2bits/sample)
//
// Data size for 1 second : 128MB or 256MB
// = Speed * 1024Mbits
// = Speed * 128MB
// = Speed * 10000 [bytes/DataFrame] *  12800 [DataFrames] ==> LEN_MK5B_DF_DATA = 10000
// = Speed *  1280 [bytes/packet]    * 100000 [packets]    ==> RVDB_PKTS_DATA   =  1280
//
// # of DFs per second = Speed * 12800

typedef struct Mark5B_info_t {
  char   *pathname;		// mk5b_info()가 받은 파일경로 문자열 포인터
  char   *filename;		// pathname에서 디렉터리 부분을 제외한 순수 파일이름 포인터
  off_t  filesize;		// in bytes
  off_t  offset0;		// 첫 정초 DF까지의 오프셋
  int    jjj0,sod0;		// (첫 정초 DF의) 시작시각, frameNumTotal 계산용
  int    speed;			// Recording Speed, 1 or 2 Gbps
  int    nDF_1s;		// 1초에 속하는 DF의 갯수, 12800*speed 
  int    isTVG;			// 시작시각 Header의 isTVG 값
  int    MJDepoch;		// current MJD epoch,
  int    version;		// 0=Unknown, 1=**BEAD style** KVN Mark5B recorder, 
                                // 2=**NULL style** KVN Mark5B recorder, 3=Software using year2000,bcdDOY
  //----------------------- runtime options
  //int CmpsMode; // 1,2,3,4,5
  //int nStrm;    // 1,2,4,8,16
} Mark5B_INFO;

#endif//__mk5b_h__

unsigned tvg(int init);
int crcc(Mark5B_FRAMEHEADER *pH);

unsigned int  BCD(unsigned int n);
unsigned int iBCD(unsigned int bcd);

int mk5b_DFindex(Mark5B_INFO *mk5binfo, Mark5B_FRAMEHEADER *pH); // DF number from beginning

#if 0
char *mk5b_time0(Mark5B_FRAMEHEADER *pH)
#endif
char *mk5b_time1(Mark5B_INFO *mk5binfo, Mark5B_FRAMEHEADER *pH);
void mk5b_HeaderTime(Mark5B_INFO *mk5binfo, Mark5B_FRAMEHEADER *pH, char *memo);

int mk5b_DFunit(Mark5B_INFO *mk5binfo, char *s);

Mark5B_INFO *mk5b_info(char *prefix, char *fn);

int read_header_and_data(Mark5B_INFO *mk5binfo,Mark5B_FRAMEHEADER *pHeader,Mark5B_FRAMEDATA *pData, int iDF,int nDF, FILE*fp);
int fix_header_and_data(Mark5B_INFO *mk5binfo,Mark5B_FRAMEHEADER *pHeader,Mark5B_FRAMEDATA *pData, int nDF, int nStrm, int fix_data);


/*
초당 DF수 = mk5b_nDF_1s = 12800 또는 25600
일당 DF수 = mk5b_nDF_1s*86400 = 1105920000 또는 2211840000 (32bit unsigned int면 충분히 표현 가능)
한 관측이 24시간을 넘지 않을 것이기 때문에, 

frameNumTotal : 시작시각 정초부터 DF를 카운트하여 절대적인 시각으로 사용하여도 될 듯.
이 때 H.frameNum 는  항상 frameNumTotal % mk5b_nDF_1s 과 같다.

read buffer가 1초 분량보다 짧은 경우, 
index 는 0 ~ frameNumTotal % nDF_buffer 로 사용하면 될 것이다.

헤더 정보에서 24시간 이하의 상대 시각을 구하려면,  doy,sod,frameNum이 필요하다.
시각시각은 항상 정초이므로 frameNum0는 0이다. 따라서 
frameNumTotal = ((doy-doy0)*86400 + (sod-sod0))*mk5b_nDF_1s + frameNum 로 계산된다.

*/


 
