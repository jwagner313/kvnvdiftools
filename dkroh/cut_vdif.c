// cut_vdif.c
//
// Cut a part from a vdif file
//

// .vdif 파일은
//   legacy=0 일 때, 32bytes 길의의 packet header와 
//   헤더 정보 framelen의 정의에 따른, framelen-32 bytes 길이의 packet data로 구성된 
//   128000000*speed / (framelen-32) 개의 packet이 매초마다 반복된다.
//
// 이 프로그램에서는,
//   1초 분량의 vdif packet을 읽어서, 
//   기록하는 작업을 반복한다.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "stopwatch.c"
#include "calendar.c"
#include "vdif.c"

unsigned int LenPktHeader = sizeof(VDIF_FRAMEHEADER);
unsigned int LenPktData   = 0;

//=========================================================================
int READ_vdif(VDIF_FRAMEHEADER *pHeader,void *pData, int nPkt, FILE*fi)
{
  int jPkt,rPkt;
  size_t n;

  //===== Clear buffer
  memset((void *)pHeader,0,nPkt*LenPktHeader);
  memset((void *)pData,  0,nPkt*LenPktData);

  //===== Try to read "nPkt" VDIF packets
  rPkt = 0;
  for(jPkt=0; jPkt<nPkt; jPkt++) {
    n = fread((void *)(pHeader+jPkt),sizeof(unsigned char),LenPktHeader,fi);
    if(n != LenPktHeader) {
      fprintf(stderr,"  Fail to read HEADER for jPkt=%d : n=%lu\n",jPkt,n);
      break;
    }//Header
    n = fread(pData+jPkt*LenPktData,sizeof(unsigned char),LenPktData,fi);
    if(n != LenPktData) {
      fprintf(stderr,"  Fail to read DATA for jPkt=%d : n=%lu\n",jPkt,n);
      break;
    }//Data
    rPkt++;
  }//jPkt
  return(rPkt);
}
//=========================================================================
int WRITE_vdif(VDIF_FRAMEHEADER *pHeader,void *pData, int nPkt, FILE*fo)
{
  int jPkt,wPkt;
  size_t n;

  //===== Try to write "nPkt" VDIF packets
  wPkt = 0;
  for(jPkt=0; jPkt<nPkt; jPkt++) {
    n = fwrite((void *)(pHeader+jPkt),sizeof(unsigned char),LenPktHeader,fo);
    if(n != LenPktHeader) {
      fprintf(stderr,"  Fail to write HEADER for jPkt=%d : n=%lu\n",jPkt,n);
      break;
    }//Header
    n = fwrite(pData+jPkt*LenPktData,sizeof(unsigned char),LenPktData,fo);
    if(n != LenPktData) {
      fprintf(stderr,"  Fail to write DATA for jPkt=%d : n=%lu\n",jPkt,n);
      break;
    }//Data
    wPkt++;
  }//jPkt
  return(wPkt);
}


int main(int argc, char **argv)
{
  VDIF_INFO *vdifinfo=NULL;
  VDIF_FRAMEHEADER *pHeader = NULL; // buffer for Header
  void             *pData   = NULL; // buffer for Data
  size_t bufsizHEAD,bufsizDATA;
  int nPkt;
  off_t offset;

  FILE *fi,*fo=NULL;

  int iPkt,sPkt,ePkt, nSec;

  STOPWATCH *sw_all = alloc_stopwatch("Total       ",NULL);
  STOPWATCH *sw_rea = alloc_stopwatch("ReadData    ",NULL);
  STOPWATCH *sw_wri = alloc_stopwatch("WriteData   ",NULL);

  //============================================================================

  if(argc < 2) {
    fprintf(stderr,"Usage: $ %s in.vdif [out.vdif [from [length]]]\n",argv[0]);
    fprintf(stderr,
      "  where in.vdif  = input, vdif file\n"
      "        out.vdif = output, vdif file\n"
      "        from     = offset from beginning, default=0s\n"
      "        length   = cut length, default=1s\n"
      "  Suffix of 'from' & 'length' for specifing the units :\n"
      "        's' = Seconds, 'p' = Packets, 'b' = Bytes\n"
      "        else or none = considered as 'p'\n");
    exit(1);
  }

  //============================================================================
  // "Mark5B file" --> "Mark5B file" 

  //===== Init global variables with the given input file
  vdifinfo = vdif_info("<<<",argv[1]);
  if(vdifinfo == NULL) exit(1);

  //===== Prepare the buffers for header and data of 1 second
  LenPktData = vdifinfo->FrameLen - LenPktHeader;
  nPkt = vdifinfo->nPkt_1s;
  bufsizHEAD = nPkt * LenPktHeader;
  bufsizDATA = nPkt * LenPktData;
  if(posix_memalign((void **)&pHeader,getpagesize(),bufsizHEAD) != 0) {
    fprintf(stderr,"Fail to allocate memory for Header.\n");
    exit(1);
  }//pHeader
  //fprintf(stderr,"Buffer allocated for Header : %lu bytes\n",bufsizHEAD);
  if(posix_memalign((void **)&pData,getpagesize(),bufsizDATA) != 0) {
    fprintf(stderr,"Fail to allocate memory for Data.\n");
    exit(1);
  }//pData

  //============================================================================

  //===== Now, set the start position and length in DF unit.
  sPkt = (argc>3) ? vdif_PKTunit(vdifinfo,argv[3]) : vdif_PKTunit(vdifinfo,"0s"); // from
  iPkt = (argc>4) ? vdif_PKTunit(vdifinfo,argv[4]) : vdif_PKTunit(vdifinfo,"1s"); // length
  ePkt = sPkt + iPkt;

  //===== open the vdif input file
  fi = fopen(vdifinfo->pathname,"r");
  offset = vdifinfo->offset0 + sPkt*vdifinfo->FrameLen;
  if(vdifinfo->version == 2) offset += OCTACAPTURE_FILE_HEADER + (sPkt/nPkt - 1)*OCTACAPTURE_SECD_HEADER;
  fseek(fi, offset, SEEK_SET);

  //===== Create output file
  if(argc>2) 
  if((fo = fopen(argv[2],"w")) == NULL) {
    fprintf(stderr,"Fail to open '%s'\n",argv[2]);
    exit(1);
  }

  // vdif 파일 읽기 :
  //   loop 마다,  1초 분량인 vdifinfo->nPkt_1s 개의 Data frame을 읽는다.
  //   EOF를 만나거나 오류가 발생하면, 중단한다.
  start_stopwatch(sw_all);
  nSec = 0;
  for(iPkt=sPkt; iPkt<ePkt; iPkt+=nPkt) {
    int rPkt,wPkt, iSec=iPkt/nPkt;
    char memo[80];

    // read
    start_stopwatch(sw_rea);
    if(vdifinfo->version == 2) fseek(fi, OCTACAPTURE_SECD_HEADER, SEEK_CUR); // skip SECD_HEADER
    rPkt = READ_vdif(pHeader,pData,nPkt, fi);
    if(rPkt != nPkt) {
      fprintf(stderr,"Warning : read %d/%d DFs at %ds.\n",rPkt,nPkt,iSec); 
    }
    stop_stopwatch(sw_rea);

    // Show time (once per second)
    sprintf(memo,"%ds",iSec);
    vdif_HeaderTime(vdifinfo,pHeader,memo);


    // write
    start_stopwatch(sw_wri);
    if(fo) {
      wPkt = WRITE_vdif(pHeader,pData,rPkt, fo);
      if(wPkt != rPkt) {
        fprintf(stderr,"Warning : written %d/%d DFs at %ds.\n",wPkt,rPkt,iSec); 
      }
    }
    stop_stopwatch(sw_wri);

    nSec++;
  }//iSec
  stop_stopwatch(sw_all);
  fprintf(stderr,"\nProcessed for %d seconds of Data.\n",nSec);

  // Stopwatch
  report_stopwatch(sw_rea); free_stopwatch(sw_rea);
  report_stopwatch(sw_wri); free_stopwatch(sw_wri);
  report_stopwatch(sw_all); free_stopwatch(sw_all);

  //===== Done.
  fclose(fi);
  if(fo) fclose(fo);
  free(pData);
  free(pHeader);

  //===== Show info about the output file
  if(fo) vdif_info(">>>",argv[2]);

  return(0);
}

