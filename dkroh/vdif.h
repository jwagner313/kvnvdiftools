// vdif.h

// ELECS사의 OCTAVIA/OCTADDB에서 사용하는 vdif packet을 다루기 위하여

// RVDB(Raw VLBI Data Buffer) system은 
// DIO 1대 및 DDB 4대, 그리고 상호연결을 위한 10GbE switch 1대로 구성된다.
// DIO는 OCTAVIA로 업그레이드되었다.
// DDB는 OCTADDB로 업그레이드되었으며, 현재 OCTADISK로 개조된 것도 있다.
// 
// RVDB 내부에서,  OCTAVIA와 OCTADDB 사이의 데이터 전송은 
// Header 32bytes와 Payload(Data) 1280bytes로 구성된 
// vdif packet을 이용한다. 연결 매체는 10GbE 광케이블이다.
// Data structure는 network order (BigEndian)를 따른다.
//
// OCTA CAPTURE FILE:
// VERA-2000 및 Mark5B playback으로부터 RVDB로 데이터를 복사하는 소프트웨어에서 
// DDB 파티션의 일부를 읽어내어, 데이터를 검사할 수 있다. 이 때 만들어지는 임시 파일이 
// octa-capture file이다. 이 파일은 256바이트 짜리 파일 헤더와, 매초 1428 바이트 짜리
// 데이터 헤더가 존재한다. (현재, 이 두 종류의 헤더는 그 내용이 파악되어 있지 않다.)
// 이를 제외하면, 아래의 octavdif file과 동일하다.
//
// OCTA VDIF FILE:
// OCTAVIA 대신에, PC로 OCTADDB를 제어하면서  
// 이 vdif packet을 수신하여 "그대로" 기록한 것이 octavdif file이다. 
// 따라서, LittleEndian의 PC에서 이 파일을 다룰 때에는
// Header에 대해서는 
// read 시 BE-->LE로, write 시 LE-->BE로 byteswap하는 것이 필요하다.
// Data는
// 각 byte 단위로 bit-reverse하면 mark5b 포맷의 data와 일치한다.
// (염재환)

// 참고:
// octavdif 파일의 packet(data frame)은
//   32 bytes의 헤더와 1280 bytes의 데이터로 구성된다. (고정)
// mark5b 파일의 data frame은
//   16 bytes의 헤더와 10000 bytes의 데이터로 구성된다. (고정)
//
//c  VSIclock DataRate      mark5bDF octavdifDF
//c  -------- ------------- -------- ----------
//c  32MHz    1Gbps=128MB/s   12,800    100,000
//c  64MHz    2Gbps=256MB/s   25,600    200,000
 
#include <sys/types.h>
#include <sys/stat.h>	// stat()
#include <unistd.h>		// getpagesize()
#include <ctype.h>		// isdigit()
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>	// ntohl()

#define DATA_1SECOND   128000000 // Data rate at 1Gbps
//      mk5b = LEN_MK5B_DF_DATA [bytes/DataFrame] * (12800*Speed)            [DataFrames]
// octa vdif = OCTAVDIF_PKTS_DATA [bytes/packet] * (100000*Speed)            [packets]
//  std vdif = (FrameLen_out-VDIF_PKTS_HEADER) [bytes/packet] * (nPkt*Speed) [packets]

#ifndef __vdif_h__
#define __vdif_h__

#define OCTACAPTURE_FILE_HEADER  256 // 
#define OCTACAPTURE_SECD_HEADER 1428 // 
#define OCTAVDIF_PKTS_HEADER      32 // 
#define OCTAVDIF_PKTS_DATA      1280 // 

typedef struct capt_file_header_t {  //<---
  unsigned char c[OCTACAPTURE_FILE_HEADER];
} CAPT_FILE_HEADER;

typedef struct capt_secd_header_t {  //<---
  unsigned char c[OCTACAPTURE_SECD_HEADER];
} CAPT_SECD_HEADER;

// OCTA "32-byte VDIF Data Frame Header" 
// BigEndian : unsigned int (4-bytes) 
typedef struct octa_vdif_frameheader_t {  //<---
  // Word 0
  union {
    unsigned int words[1];
    unsigned int word0;
    struct {
      unsigned int secs:30;	// Seconds from reference epoch, 
      unsigned int :1;		// reserved, 0 fixed
      unsigned int invalid:1;	// 0=valid,1=invalid
    };
  };
  // Word 1
  union {
    unsigned int word1;
    struct {
      unsigned int framenum:24;  // Data Frame Number within second 
      unsigned int refepoch:6;   // Reference Epoch for secs count
      unsigned int :2;           // un-assigned 2 bits, should be set to '00'
    };
  };
  // Word 2
  union {
    unsigned int word2;
    struct {
      unsigned int framelen:24;  // Data Frame Length (including Header), in units of Bytes
      unsigned int :8;           // un-assigned 8 bits
    };
  };
  // Word 3
  union {
    unsigned int word3;
    struct {
      unsigned int :16;          // un-assigned 16 bits,
      unsigned int threadid:10;  // Thread ID (0 to 1023)
      unsigned int nbitsm1:5;    // #bits/sample - 1 (1 to 32 bits/sample)
      unsigned int :1;           // un-assigned 1 bits
    };
  };
  // Word 4
  union {
    unsigned int word4;
    struct {
      unsigned int refepochSRC:6;   // Reference Epoch for SRC time
      unsigned int timevalid:2;     // Time Valid (CTL time and SRC time)
      unsigned int split:4;         // H/L split mode, 0~5
      unsigned int sampling:4;      // H/L smapling, 0~1
      unsigned int effbitn:4;       // Effective bit #, 0~31
      unsigned int :4;              // un-assigned 4 bits
      unsigned int :8;              // un-assigned 8 bits
    };
  };
  // Word 5
  union {
    unsigned int word5;
    struct {
      unsigned int secsSRC:31;	// Seconds from reference epoch for SRC time
      unsigned int :1;          // un-assigned 1 bits
    };
  };
  // Word 6
  union {
    unsigned int word6;
    struct {
      unsigned int :28;         // un-assigned 28 bits
      unsigned int count:4;     // Second count
    };
  };
  // Word 7
  union {
    unsigned int word7;
    struct {
    unsigned int :32;		// reserved, 0 fixed
    };
  };
} OCTA_VDIF_FRAMEHEADER;

typedef struct octa_vdif_data_t {  //<--- fixed length
  union {                  // Frame Payload
    unsigned char c[OCTAVDIF_PKTS_DATA];               // 1280 bytes
    unsigned int data[OCTAVDIF_PKTS_DATA/sizeof(int)]; //  320 uint
  };
} OCTA_VDIF_DATA;

//==============================================================================

#define VDIF_PKTS_HEADER      32 // 

// Standard (legacy=0) "32-byte VDIF Data Frame Header" 
typedef struct vdif_frameheader_t {  //<---
  // Word 0
  union {
    unsigned int words[1];
    unsigned int word0;
    struct {
      unsigned int secs:30;      // Seconds from reference epoch, 
      unsigned int legacy:1;     // 0=standard VDIF Data Frame Header (32bytes), 
                                 // 1=legacy Header Length mode (16bytes)
      unsigned int invalid:1;    // 0=valid,1=invalid
    };
  };
  // Word 1
  union {
    unsigned int word1;
    struct {
      unsigned int framenum:24;  // Data Frame Number within second 
      unsigned int refepoch:6;   // Reference Epoch for secs count
      unsigned int :2;           // un-assigned 2 bits, should be set to '00'
    };
  };
  // Word 2
  union {
    unsigned int word2;
    struct {
      unsigned int framelen:24;  // Data Frame Length (including Header), in units of 8Bytes
      unsigned int log2nchans:5; // log2(# of channels in Data Array)
      unsigned int version:3;    // VDIF version number (possible 7 future versions)
    };
  };
  // Word 3
  union {
    unsigned int word3;
    struct {
      unsigned int stationid:16; // Station ID (2 char ASCII, or numeric)
      unsigned int threadid:10;  // Thread ID (0 to 1023)
      unsigned int nbitsm1:5;    // #bits/sample - 1 (1 to 32 bits/sample)
      unsigned int datatype:1;   // 0=real, 1=complex
    };
  };
  // Word 4
  union {
    unsigned int word4;
    struct {
      unsigned int EUD1:24;      // EXtended User Data 1, 0 if EDV=0
      unsigned int EDV:8;        // Extended Data Version, 0 or 1~255
    };
  };
  // Word 5
  union {
    unsigned int word5;
    unsigned int EUD2;           // EXtended User Data 2, 0 if EDV=0
  };
  // Word 6
  union {
    unsigned int word6;
    unsigned int EUD3;           // EXtended User Data 3, 0 if EDV=0
  };
  // Word 7
  union {
    unsigned int word7;
    unsigned int EUD4;           // EXtended User Data 4, 0 if EDV=0
  };
} VDIF_FRAMEHEADER;


typedef struct VDIF_info_t {
  char   *pathname;		// mk5b_info()가 받은 파일경로 문자열 포인터
  char   *filename;		// pathname에서 디렉터리 부분을 제외한 순수 파일이름 포인터
  off_t  filesize;		// in bytes
  off_t  offset0;		// 첫 정초 packet까지의 오프셋

  int    speed;			// Recording Speed, 1 or 2 Gbps
  int    nPkt_1s;		// 1초에 속하는 packet의 갯수, 12800*speed 

  int    isTVG;			// OCTA Capture VDIF의 파일헤더에서 얻은 isTVG 값
  int    FrameLen;		// Frame Length [bytes]
  int    version;		// 0=Standard VDIF, 1=OCTA VDIF, 2=OCTA Capture VDIF
  //----------------------- runtime options
  //int CmpsMode; // 1,2,3,4,5
  //int nStrm;    // 1,2,4,8,16
} VDIF_INFO;


#endif//__vdif_h__


void print_octavdif_frameheader(void *p);
void print_vdif_frameheader(void *p);

void time_vdif_frameheader(void *p,
  int *mjdn, int *y,int *m,int *d, int *hh,int *mm,int *ss, int *doy,int *sod);

int ilog2(int n);







