// recv_octa.c

// Created by JH Yeom (ctl_octa.c)
// Modified by DG Roh, 2014/11/10

// Purpose : To create a standard vdif file
//           by receiving the vdif data frame packets from OCTADISK/OCTADDB.
//
// Notes   : (1) VDIFCP/VR and VDIF/VDIF_DF over UDP/IP
//           (2) Converting form "octa vdif data frame" of OCTADISK/OCTADDB
//               to "standard vdif data frame"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h> // time(), localtime(), strftime(), clock()
#include <sys/time.h> // gettimeofday()

#include <sys/types.h>
#include <fcntl.h> // open(), 
#include <unistd.h> // lseek(), write(), close()
#include <errno.h> // errno

//#include <sys/socket.h> // socket(), bind, sendto(),recvfrom(), setsockopt()
//#include <netinet/in.h>
#include <arpa/inet.h> // inet_addr(), htons(), htonl()
//#include <netdb.h>

#include "calendar.c"
#include "vdif.c"
#include "vdifcp.h"

// Standard VDIF Data Frame (packet length 1312) : Header(32) + Data(1280)
struct VDIF_DF1312 {
   VDIF_FRAMEHEADER vdif_hdr;  // Header
   unsigned char vdif_dat[1280];      // Data
};

struct mytm {
  time_t t; // 0~   , number of seconds since the epoch
  int year; // 2000~, year A.D.
  int doy;  // 1~366, day of year
  int hh;   // 0~23,  hours
  int mm;   // 0~59,  minutes
  int ss;   // 0~59,  seconds
};

// call this routine whenever one or more members was changed.
void normalize_time(struct mytm *mytm)
{
  struct tm tm;
  tm.tm_year = mytm->year - 1900;
  tm.tm_mon  = 0;
  tm.tm_mday = mytm->doy;
  tm.tm_hour = mytm->hh;
  tm.tm_min  = mytm->mm;
  tm.tm_sec  = mytm->ss;
  mytm->t = timegm(&tm);  // get the number of seconds since the epoch
  gmtime_r(&mytm->t,&tm); // convert it to the broken-down time representation.
  mytm->year = tm.tm_year + 1900;
  mytm->doy  = tm.tm_yday + 1;
  mytm->hh   = tm.tm_hour;
  mytm->mm   = tm.tm_min;
  mytm->ss   = tm.tm_sec;
}

char *timestamp(time_t t,int type)
{
  static char s[80];
  struct tm *p;
  switch(type) {
  case 1:
    p = localtime(&t);
    break;
  case 0:
  default:
    p = gmtime(&t);
    break;
  }
  sprintf(s,"%4d/%02d/%02d %02d:%02d:%02d",
    p->tm_year+1900,p->tm_mon+1,p->tm_mday,
    p->tm_hour,p->tm_min,p->tm_sec);
  return(s);
}

char *QandA(char *Q)
{
  static char A[80];
  fprintf(stderr,"%s",Q?Q:"Press Enter ");
  fgets(A,sizeof(A),stdin);
  return(A);
}

int main(int argc, char *argv[])
{
  int i,k;
  char *p,*q;

  char *fn = {"out.vdif"};
  FILE *fo;
  struct stat st;
  time_t t_begin,t_end; // wall-clock

  int sock_cntl,sock_data, len_sockaddr = sizeof(struct sockaddr_in);
  struct sockaddr_in addr_clnt, addr_cntl;
  char *srvr_addr = NULL;      // no default 
  char *srvr_port = {"60000"}; // default port number
  struct timeval timeout;      // setting timeout to socket

  struct VDIF_DF1312 vf_headdf;
  struct VDIFCP_VR   vfcp_vr;

  struct timeval tv_ctlstart,tv_ctlend; // control-time
  struct mytm start,finish, rqst,save;  // data-time
  int n_again;

  int iSec,nSec,*nPkt_saved=NULL;
  int nFrames, vr_seqno;

  //----------------------------------------------------------------------
  // Pointers for converting from "LE" to "BE" for vfcp_vr
  unsigned int          *px   = (unsigned int          *)(&vfcp_vr);

  // Pointers for converting from "octa-vdif" data frame to "standard-vdif" data frame
//struct OCTA_VDIF_HEAD *octa = (struct OCTA_VDIF_HEAD *)(&vf_headdf.vdif_hdr);
  VDIF_FRAMEHEADER      *std  = (VDIF_FRAMEHEADER      *)(&vf_headdf.vdif_hdr);
  unsigned int          *pu   = (unsigned int          *)(&vf_headdf.vdif_hdr);
  unsigned char         *pc   = (unsigned char         *)(&vf_headdf.vdif_dat);

  // Variables needed to convert VDIF data frame header
  int RecRate     = 1024;    // normal 1Gbps
  int PlayRate    = 2;       // half speed
  int Log2Nchans  = 4;       // 0,1,2,3,4 for Nchans=1,2,4,8,16
  int NbitsM1     = 1;       // force to "2 bits/sample"
  int InvalidFlag = 1;       // as same as original
  int ThreadID    = -1;      // 0 ~ 1023
  int StationID   = -1;      // to hold two char. station ID
  int SecsOffset  = -86400;  // for direct access to OCTADDB/OCTADISC
  //----------------------------------------------------------------------

  // Local function ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  void REQUEST(void) // send a request of time "rqst"
  {
    //fprintf(stderr,
    //  "#1234,%04d%03d%02d%02d%02d,%d,32,2,ACQ,PDATAVP1,PDATAVP2;\n", 
    //  rqst.year,rqst.doy,rqst.hh,rqst.mm,rqst.ss, RecRate/2); 
    fprintf(stderr,"Rqst %04d/%03d %02d:%02d:%02d\n",
      rqst.year,rqst.doy,rqst.hh,rqst.mm,rqst.ss);

    // Build VDIFCP_VR
    memset(&vfcp_vr,0,sizeof(vfcp_vr));
    vfcp_vr.version = 1;           // Verion 1
    vfcp_vr.class   = 3;           // 3=VR
    vfcp_vr.VSIport = 1;           // VSI port(1 - 4)
    vfcp_vr.plen    = 28;          // packet length, 28 bytes
    vfcp_vr.seqno   = vr_seqno++;  // packet sequence number
    vfcp_vr.mode    = 2;           // VDIF Mode
    vfcp_vr.y1000   = (rqst.year/1000)%10; // 4bit-BCD for YYYYDDDhhmmss
    vfcp_vr.y100    = (rqst.year/100 )%10;
    vfcp_vr.y10     = (rqst.year/10  )%10;
    vfcp_vr.y1      = (rqst.year     )%10;
    vfcp_vr.d100    = (rqst.doy/100)%10;
    vfcp_vr.d10     = (rqst.doy/10 )%10;
    vfcp_vr.d1      = (rqst.doy    )%10;
    vfcp_vr.h10     = (rqst.hh/10 )%10;
    vfcp_vr.h1      = (rqst.hh    )%10;
    vfcp_vr.m10     = (rqst.mm/10 )%10;
    vfcp_vr.m1      = (rqst.mm    )%10;
    vfcp_vr.s10     = (rqst.ss/10 )%10;
    vfcp_vr.s1      = (rqst.ss    )%10;
    // LE-to-BE
    for(k=0;k<7;k++) px[k] = htonl( px[k] );

    // send VDIFCP/VR using control port(60001), UTC
    if(sendto(sock_cntl, &vfcp_vr, 28, 0, (struct sockaddr *)&addr_cntl, len_sockaddr) < 0) {
      fprintf(stderr,"Fail to send VDIFCP/VR\n");
    }
    return;
  }
  int RECEIVE(void) // receive the data packets of time "save"
  {
    int rPkt=0,nPkt=0;
    int RefEpoch,Secs,kSec = save.t-start.t;
    off_t offset;

    RefEpoch = (save.year-2000)*2;
    k = DOY(save.year,7,1);
    if(save.doy < k) {
      Secs = (save.doy - 1)*86400;
    }else{
      RefEpoch += 1;
      Secs = (save.doy - k)*86400;
    }
    Secs += (save.hh*60 + save.mm)*60 + save.ss;
    fprintf(stderr,"Recv %4d/%03d %02d:%02d:%02d -- ",save.year,save.doy,save.hh,save.mm,save.ss);

    for(i=0; i<nFrames; i++){
      // Receive a VDIF data frame
      if(recvfrom(sock_data, &vf_headdf, 1312, MSG_NOSIGNAL, NULL, 0) < 0) {
        fprintf(stderr,"TIMEOUT at packet %d\n",i);
        if(1 < kSec) nPkt = -1; // End of partition
        return(nPkt);
      }
      rPkt++;

      // Need to convert from "octa-vdif" data frame to "standard-vdif" data frame
      //----------------------------------------------------------------------
      // (1) PACKET HEADER : 32 bytes = 8 unsigned int
      //     Convert "OCTA vdif Header" to "Standard vdif Header".
      //     (1.1) "BigEndian" to "LittleEndian".
      for(k=0;k<8;k++) pu[k] = ntohl( pu[k] );
      //     (1.2) Adjust some fields
      //Word 0 : octa = invalid:1, reserved:1, secs:30
      //         std  = invalid:1, legacy:1, secs:30
      if(InvalidFlag==0) std->invalid = 0; // trun off if requested
      std->secs += SecsOffset;
      // octa->reserved:1=0 == std->legacy:1=0 for 32bytes VDIF DF Header
      //Word 1 : octa = reserved:2, refepoch:6, framenum:24
      //         std  = reserved:2, refepoch:6, framenum:24
      //Word 2 : octa = reserved:8, framelen:24
      //         std  = version:3, log2nchans:5, framelen:24
      // octa->reserved:(3/8)=0 == std->version:3=0 for VDIF version number 0
      // octa->reserved:(5/8)=0 != std->log2nchans:5
      std->log2nchans = Log2Nchans; // forced
      //Word 3 : octa = reserved:1, nbitsm1:5, threadid:10, reserved:16
      //         std  = datatype:1, nbitsm1:5, threadid:10, stationid:16
      // octa->reserved:1=0  == std->datatype:1=0 for real
      // octa->reserved:16=0 != std->stationid:16
      std->nbitsm1 = NbitsM1; // forced
      if(0<=ThreadID) std->threadid = ThreadID;
      if(0<=StationID) std->stationid = StationID;
      //Word 4 : octa = reserved:8,reserved:4, effbitn:4, sampling:4, split:4, timevalid:2, refepochSRC:6
      //         std  = EDV:8, EUD1:24
      // Ignore all of the "EXtended User Data" of "OCTA vdif Header"
      std->EDV = 0;
      std->EUD1 = 0;
      //Word 5 : octa = reserved:1, secsSRC:31
      //         std  = EUD2:32
      std->EUD2 = 0;
      //Word 6 : octa = count:4, reserved:28
      //         std  = EUD3:32
      std->EUD3 = 0;
      //Word 7 : octa = reserved:32
      //         std  = EUD4:32
      // octa->reserved:32=0 == std->EUD4:32=0
      //----------------------------------------------------------------------
      // (2) Check time
      if(i != std->framenum) { // framenum misalign
        // This may occur at the beginning of transfer (2 sec margin),
        // if the requested start time is the middle part of existing data partition.
        // (I didn't meet this case if I set the start time as before of the existing.)
        // It seems that the UDP/IP modules of OCTADDB/OCTADISC does not syncronized yet,
        // and there is some loss of packets.
        //
        // While testing with various conditions, 
        // I found that one or more (several, but not much) packets were disappeared 
        // occasionally from the OS's UDP buffer. I presume that this might be related
        // to the NIC' buffer size and the not-yet-served successive interrupts.
        //  
        // Force my index 'i' to follow the framenum of received packet.
        if(i < std->framenum) {
          //if(std->framenum - i == 1) fprintf(stderr,"lost %d, ",i);
          //else fprintf(stderr,"lost %d~%d, ",i,std->framenum-1);
          i = std->framenum; // jump index 'i'
          if(i == nFrames-1) {
            //fprintf(stderr,"edge! "); 
            break; // Stop reading here. Expecting next RECEIVE() gets from framenum=0.
          }else{
            continue; // No need to write. Continue to read next packet.
          }
        }else{
          //fprintf(stderr,"found %d at %d, ",std->framenum,i);
          i = std->framenum; // Rewind index 'i', and continue to read
        }
      } 
      if(kSec<0 || nSec<=kSec) { // Outside of requested time range.
        //if(i%10000==0) fprintf(stderr,"o");
        continue; // No need to write. Just read and throw away
      }
      //----------------------------------------------------------------------
      // (3) PACKET DATA : 1280 bytes
      //     Do bitswap for each byte, using the pre-defined table.
      for(k=0;k<1280;k++) pc[k] = bit_swap_tbl[ pc[k] ];
      //----------------------------------------------------------------------

      // Write a VDIF data frame
      //   only when the time is valid for "save"
      if(RefEpoch == std->refepoch && Secs == std->secs) {
        offset = (kSec*nFrames + std->framenum)*1312L;
        fseek(fo,offset,SEEK_SET);
        fwrite(&vf_headdf,1312L,1L,fo);
        nPkt++;
      }
    }//for(i)

    // mark the status
    if(0<=kSec && kSec<nSec) nPkt_saved[kSec] = nPkt;
    fprintf(stderr,"r,w=%d,%d packets, fpos=%lu\n",rPkt,nPkt,ftell(fo));
    return(nPkt);
  }
  // Local function ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

  fprintf(stderr,"Receive OCTADDB/OCTADISC vdif packets, save into VDIF file\n");

  // Clear buffers
  memset(&vf_headdf.vdif_hdr, 0, sizeof(vf_headdf.vdif_hdr));
  memset(&vf_headdf.vdif_dat, 0, sizeof(vf_headdf.vdif_dat));
  memset(&vfcp_vr, 0, sizeof(vfcp_vr));
  memset(&addr_clnt, 0, sizeof(addr_clnt));
  memset(&addr_cntl, 0, sizeof(addr_cntl));

  //============================================================================

  // Command Line Parameters
  for(i=1; i<argc; i++) { // pick-up the set options first
    char *eqs;
    if((eqs = index(argv[i],'=')) != NULL) {
      if(strncasecmp(argv[i],"RecRate",3)==0) {
        k = atoi(eqs+1);
        if(k==1024 || k==2048) RecRate = k;
        else fprintf(stderr,"Ignored the invalid set option, %s\n",argv[i]);
      }else if(strncasecmp(argv[i],"PlayRate",3)==0) {
        k = atoi(eqs+1);
        if(k==1 || k==2 || k==4 || k==8 || k==16) PlayRate = k;
        else fprintf(stderr,"Ignored the invalid set option, %s\n",argv[i]);
      }else if(strncasecmp(argv[i],"Nchans",3)==0) {
        k = ilog2( atoi(eqs+1) );
        if(0<=k && k<=4) Log2Nchans = k;
        else fprintf(stderr,"Ignored the invalid set option, %s\n",argv[i]);
      }else if(strncasecmp(argv[i],"Nbits",3)==0) {
        k = atoi(eqs+1);
        if(1<=k && k<=32) NbitsM1 = k - 1;
        else fprintf(stderr,"Ignored the invalid set option, %s\n",argv[i]);
      }else if(strncasecmp(argv[i],"InvalidFlag",3)==0) {
        k = atoi(eqs+1);
        if(k==0 || k==1) InvalidFlag = k;
        else fprintf(stderr,"Ignored the invalid set option, %s\n",argv[i]);
      }else if(strncasecmp(argv[i],"ThreadID",3)==0) {
        k = atoi(eqs+1);
        if(0<=k && k<=1023) ThreadID = k;
        else fprintf(stderr,"Ignored the invalid set option, %s\n",argv[i]);
      }else if(strncasecmp(argv[i],"StationID",3)==0) {
        if(strlen(eqs+1)>=2) { StationID = 0; memcpy((void *)&StationID,(void *)(eqs+1),2); }
        else fprintf(stderr,"Ignored the invalid set option, %s\n",argv[i]);
      }else if(strncasecmp(argv[i],"SecsOffset",3)==0) {
        SecsOffset = atoi(eqs+1);
      }else{
        fprintf(stderr,"Ignored the unknown set option, %s\n",argv[i]);
      }
      argv[i] = NULL;
    }
  }
  for(i=1; i<argc; i++) { // clean up
    if(argv[i]==NULL) {
      for(k=i+1; k<argc; k++) {
        if(argv[k]!=NULL) { argv[i] = argv[k]; argv[k] = NULL; break; }
      }
      if(k==argc) { argc = i; break; }
    }
  }
  if(argc < 5) {
    fprintf(stderr,"Usage: $ %s Addr[:Port] ObsDate StartTime FinishTime [out.vdif] [set options ...]\n",argv[0]);
    fprintf(stderr,
      "  where Addr[:Port] = IP address and port number(default=60000) of OCTADDB/OCTADISC\n"
      "        ObsDate     = yyyy/mm/dd, yyyy/doy, or yydoy\n"
      "        StartTime   = hh:mm:ss\n"
      "        FinishTime  = hh:mm:ss\n"
      "        [out.vdif]  = output, 'standard' vdif file\n"
      "  set options:\n"
      "    RecRate=#      1024 or 2048 Mbps, default=1024\n"
      "    PlayRate=#     1[normal], 2[half], 4[quad], 8[1/8], or 16[1/16], default=2\n"
      "    Nchans=#       Number of Channels, default=16 for C5 mode\n"
      "    Nbits=#        Number of bits per sample, default=2 for normal\n"
      "    InvalidFlag=#  0=turn off, 1=as same as original, default=1\n"
      "    ThreadID=#     Thread ID, 0~1023, no default.\n"
      "    StaionID=XX    Two char. Station ID, no default. Use KY for KVN-Yonsei\n"
      "    SecsOffset=#   Secs Offset (will be added to the received packet header)\n"
      );
    exit(1);
  }

  //============================================================================
  // Receive OCTADDB/OCTADISC vdif packets ---> writing "Standard VDIF file"

  // Command Line Parameters
  // argv[1] = Addr:Port
  if((p = index(argv[1],':')) == NULL) {
    // Addr only
    srvr_addr = argv[1];  // IP address
  }else{
    // Addr:Port
    *p = 0;
    srvr_addr = argv[1];  // IP address
    srvr_port = p+1;      // Port number
  }
  // address checking
  if(inet_addr(srvr_addr) == INADDR_NONE) {
    fprintf(stderr,"Bad address '%s'\n",argv[1]);
    exit(1);
  }

  // argv[2~4] = ObsDate StartTime FinishTime
  if((p = index(argv[2],'/')) == NULL) {
    // yydoy
    i = atoi(argv[2]);
    k = i/1000;
    start.year = (0<=k && k<=99) ? 2000+k : k;
    start.doy  = i%1000;
  }else{
    if((q = index(p+1,'/')) == NULL) {
      // yyyy/doy
      start.year = atoi(argv[2]);
      start.doy  = atoi(p+1);
    }else{
      // yyyy/mm/dd
      start.year = atoi(argv[2]);
      i = atoi(p+1);
      k = atoi(q+1);
      start.doy = DOY(start.year,i,k);
    }
  }
  p = index(argv[3],':');
  q = p ? index(p+1,':') : NULL;
  if(p && q) {
    // hh:mm:ss
    start.hh = atoi(argv[3]);
    start.mm = atoi(p+1);
    start.ss = atoi(q+1); normalize_time(&start);
  }else{
    fprintf(stderr,"Bad StartTime '%s'\n",argv[3]);
    exit(1);
  }
  p = index(argv[4],':');
  q = p ? index(p+1,':') : NULL;
  if(p && q) {
    // hh:mm:ss
    finish.year = start.year;
    finish.doy  = start.doy;
    finish.hh = atoi(argv[4]);
    finish.mm = atoi(p+1);
    finish.ss = atoi(q+1); normalize_time(&finish);
    if(finish.t < start.t) {
      finish.doy++; normalize_time(&finish);
    }
  }else{
    fprintf(stderr,"Bad FinishTime '%s'\n",argv[4]);
    exit(1);
  }

  // optional argv[5] = filename
  if(argc>5) fn = argv[5];

  //===== Show the value of set options : forced or user-supplied only are effective.
  fprintf(stderr,"set options: RecRate     = %d Mbps\n",RecRate);
  fprintf(stderr,"set options: PlayRate    = %d\n",PlayRate);
  fprintf(stderr,"set options: Nchans      = 2^%d\n",Log2Nchans);
  fprintf(stderr,"set options: Nbits       = %d bits/sample\n",NbitsM1+1);
  if(InvalidFlag==0) {
    fprintf(stderr,"set options: InvalidFlag = %d (Turn off)\n",InvalidFlag);
  }
  if(0<=ThreadID) {
    fprintf(stderr,"set options: ThreadID    = %d\n",ThreadID);
  }
  if(0<=StationID) {
    pc = (unsigned char *)&StationID;
    fprintf(stderr,"set options: StationID   = %c%c (%d)\n",
      isprint(*pc)?*pc:'?', isprint(*(pc+1))?*(pc+1):'?', StationID);
  }
  if(SecsOffset) {
    fprintf(stderr,"set options: SecsOffset  = %d seconds\n",SecsOffset);
  }
  fprintf(stderr,"\n");

  //============================================================================
  // output file with "w+"
  if(stat(fn,&st)==0) { // file exist
    fprintf(stderr,"Output file already exist: '%s' = %lu bytes.\n",fn,st.st_size);
    p = QandA("Overwrite ? (yes/NO) ");
    if(strncasecmp(p,"yes",3)!=0) exit(1);
    // file will be truncated.
  }
  if((fo = fopen(fn,"w+")) == NULL) {
    fprintf(stderr,"Fail to open '%s' with 'w+'\n",fn);
    exit(1);
  }

  // Set # of packets per second
  nFrames = (RecRate/1024) * 100000;

  // Set # of seconds to be received
  nSec = finish.t - start.t;
  nPkt_saved = calloc(nSec,sizeof(int)); // to check the staus of receive
  if(nPkt_saved == NULL) {
    fprintf(stderr,"Fail to calloc() for status buffer\n");
    exit(1);
  }

  fprintf(stderr,"Data length requested = %u sec\n",nSec);
  if(nSec<=0) exit(1);
  fprintf(stderr,"Expected running time = %u sec or more ^.^\n",(nSec+3)*PlayRate);

  //-------------------------------------------------------------------- sockets
  // Make a UDP socket for CNTL
  if((sock_cntl = socket(PF_INET, SOCK_DGRAM, 0)) == -1) {
    fprintf(stderr,"Fail to get a socket for control.");
    exit(1);
  }

  // only transmit --- for sending VR packet
  addr_cntl.sin_family = AF_INET;
  addr_cntl.sin_addr.s_addr = inet_addr(srvr_addr);
  addr_cntl.sin_port = htons(atoi(srvr_port) + 1);  // port 60001

  //-------------------------------------------------------------------- sockets
  // Make a UDP socket for DATA
  if((sock_data = socket(PF_INET, SOCK_DGRAM, 0)) == -1) {
    fprintf(stderr,"Fail to get a socket for data.");
    exit(1);
  }

  // my (client's) addr:port for both receive and transmit
  addr_clnt.sin_family = AF_INET;
  addr_clnt.sin_addr.s_addr = htonl(INADDR_ANY);
  addr_clnt.sin_port = htons(atoi(srvr_port));  // WHY atoi(srvr_port) ?
//addr_clnt.sin_port = htons(0);                // 0 for any available port

  // socket options for 'sock_data' socket
  timeout.tv_sec = 1; timeout.tv_usec = 0;
  if(setsockopt(sock_data, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout)) == -1) {
    fprintf(stderr,"Fail to setsocketopt() : Recv Timeout");
    exit(1);
  }
  k = 1;
  if(setsockopt(sock_data, SOL_SOCKET, SO_REUSEADDR, &k, sizeof(int)) == -1) {
    fprintf(stderr,"Fail to setsocketopt() : Reuse Addr");
    exit(1);
  }
  k = 2684354560;
  if(setsockopt(sock_data, SOL_SOCKET, SO_RCVBUF,    &k, sizeof(int)) == -1) {
    fprintf(stderr,"Fail to setsocketopt() : Recv Buffer Size");
    exit(1);
  }
  k = 2684354560;
  if(setsockopt(sock_data, SOL_SOCKET, SO_SNDBUF,    &k, sizeof(int)) == -1) {
    fprintf(stderr,"Fail to setsocketopt() : Send Buffer Size");
    exit(1);
  }

  // Need to bind in case of both receive and transmit
  if(bind(sock_data, (struct sockaddr *)&addr_clnt, len_sockaddr) == -1) {
    fprintf(stderr,"Fail to bind()");
    exit(1);
  }

  //-------------------------------------------------------------------- sockets

  // Ready to START
  QandA("\nPress ENTER to start. ");

  // wait until the control-time (measured by wall-clock) passes the second-boubdary exactly 
  gettimeofday(&tv_ctlend, NULL);
  do{
    gettimeofday(&tv_ctlstart, NULL);
  } while(tv_ctlstart.tv_usec > tv_ctlend.tv_usec);


  // OCTADDB/OCTADISK needs two seconds at least for sync. 
  // This is the reason why we start 2 sec before to the actual time range.
  //
  // Because 'writing data into disc file' is time consuming work, 
  // and also there could be another tasks what require the disc IO,
  // it could not be returned within the 'PlayRate' seconds occasionally. 
  // For such a case, we must check the status of 'previous writing', and
  // send a re-transmit request to recover the failure. 
  // At the end of loop, program reports the # of re-transmit requests.
  time(&t_begin); // time begin
  vr_seqno = 0;
  save.t = rqst.t = 0;
  n_again = 0;
  for(iSec=-2;;) {
    if(save.t) { 
      // There are two previous requests : save & rqst
      // If the previous "save" status is OK, send a request of next time,
      // else send a re-transmit request of that time ("save").
      // And then, try to receive the response of previous request ("rqst").
      int jSec = save.t - start.t;
      if(0<=jSec && jSec<=nSec) { 
        // "save" is inside of actual time range
        if(nPkt_saved[jSec] < nFrames) {
          // insufficent packets received for "save" : Request again
          struct mytm again = save;
          fprintf(stderr,"\t\t\tiSec=%d: jSec=%d -- Request again!\n",iSec,jSec);
          save = rqst;
          rqst = again; REQUEST(); n_again++;
        }else{
          // all packets for "save" are OK. Go ahead normally
          save = rqst;
          rqst = start; rqst.ss += iSec++; normalize_time(&rqst);
          REQUEST();
        }
      }else{
        // "save" is outside of actual time range : no care of received packets
        save = rqst;
        rqst = start; rqst.ss += iSec++; normalize_time(&rqst); 
        REQUEST();
      }
      if(RECEIVE() < 0) {
        // RECEIVE() returns -1, when the request time is over the existing data range.
        //   Because OCTADDB/OCTADISC will send no packet at all, 
        //   RECEIVE() meets "TIMEOUT" 
        // Reduce finish time for reporting.
        finish = rqst; finish.ss--; normalize_time(&finish);
        break;
      }

    }else if(rqst.t) {
      // There is one previous request : rqst
      // Send the 2nd request, and try to receive the response of 1st request.
      // Because we started 2 seconds before, no data will comes.
      save = rqst;
      rqst = start; rqst.ss += iSec++; normalize_time(&rqst); 
      REQUEST();
      RECEIVE();

    }else{        
      // At first time, there is no prev request. Now, send the first request.
      rqst = start; rqst.ss += iSec++; normalize_time(&rqst); 
      REQUEST();
      // Nothing to receiver yet
    }

    // wait until the control-time (measured by wall-clock) passes 'PlayRate' seconds exactly 
    do{
      gettimeofday(&tv_ctlend, NULL);
    } while((int)(tv_ctlend.tv_sec - tv_ctlstart.tv_sec) < PlayRate);
    tv_ctlstart.tv_sec = tv_ctlend.tv_sec;

    // Quit when iSec become to bigger than nSec.
    // It is needed to get the data packets for the last second of acutal time range.
    // (Because OCTADDB/OCTADISC sends the previously requested data packets 
    // if it receives a new VR. The request of last VR is ignored.)
    if(iSec>nSec) break;
  }//for(iSec)

  // ALL done
  time(&t_end); // time end
  close(sock_cntl);
  close(sock_data);
  fclose(fo);

  // Report some ...
  fprintf(stderr,"\n");
  fprintf(stderr,"Data length : %ld seconds from %s", finish.t-start.t,timestamp(start.t,2));
  fprintf(stderr," to %s in UT\n",timestamp(finish.t,2));
  fprintf(stderr,"Re-transmit : %d times.\n",n_again);
  fprintf(stderr,"Run time    : %ld seconds from %s", t_end-t_begin,timestamp(t_begin,1));
  fprintf(stderr," to %s in KST\n",timestamp(t_end,1));

  //===== Show info about the created vdif file
  vdif_info(">>>",fn);
  return(0);
}

