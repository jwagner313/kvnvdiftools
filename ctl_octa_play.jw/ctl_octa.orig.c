
#define _LARGEFILE64_SOURCE // open-> O_LARGEFILE
#define _GNU_SOURCE // open -> O_DIRECT
#define _XOPEN_SOURCE 600


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include <fcntl.h>
#include <malloc.h>

#include "vdif_io_orig.h"

static int strt_flag = 0;
uint64_t last_fpos=0;

int main(int argc, char *argv[])
{
   int rc;
   int m, n;

   tharg *arg;
   tharg *darg;
//   FILE *fo;
   int fo;

   char *srvr_port = "60000";
   struct sockaddr_in adr_srvr;
   struct sockaddr_in adr_ctl;
   struct sockaddr_in adr_clnt;
   struct VDIF_HEAD_DF vf_headdf;
//   struct VDIF_HEAD_DF vf_headdf[10];
   struct VDIFCP_SR vfcp_sr;
   struct VDIFCP_VR vfcp_vr;
   int len_inet;
   int s;
   uint16_t year = 2013;
   uint16_t day =343;
   uint8_t hh = 6;
   uint8_t mm =46; 
   uint8_t ss = 30;
   uint16_t strt_year;
   uint16_t strt_day;
   uint8_t strt_hh;
   uint8_t strt_mm; 
   uint8_t strt_ss;
   uint16_t fin_year;
   uint16_t fin_day;
   uint8_t fin_hh;
   uint8_t fin_mm; 
   uint8_t fin_ss;
   int mepoch, mtime;
//   char src_time[13] = "2014001000000";
   uint32_t seqno_swap;

   unsigned int frame_no;
   unsigned int frame_ch;
   unsigned int sec_refepoch;
   unsigned char sec_count;
   unsigned int sec_refepoch_swap;
   unsigned int frame_no_swap;
   uint8_t* p_vf_dat;
   uint8_t* p_bit_swap;
   unsigned int sec_wait=0;
   unsigned int recrate;
   unsigned int playrate;
   unsigned int frame_length;
   unsigned int rate_count;

   //char tofname[256];
   len_inet = sizeof(adr_srvr);
   memset(&adr_srvr, 0, sizeof(adr_srvr));
   memset(&adr_ctl, 0, sizeof(adr_ctl));
   memset(&adr_clnt, 0, sizeof(adr_clnt));
   memset(&vf_headdf.vdif_hdr, 0, sizeof(vf_headdf.vdif_hdr));
   memset(&vf_headdf.vdif_dat, 0, sizeof(vf_headdf.vdif_dat));
//   memset(vf_headdf, 0, 13120);
   memset(&vfcp_sr, 0, sizeof(vfcp_sr));
   memset(&vfcp_vr, 0, sizeof(vfcp_vr));

   // Command Line Parameters
   if(argc != 16) {
     fprintf(stderr,"Usage: $ %s IPaddr Port# Outfile[VDIF format] rec.rate# play.rate# start time:(yyyy ddd hh mm ss) finish time:(yyyy ddd hh mm ss)\r\n     ---> rec.rate# : 2048 or 1024\r\n     ---> play.rate# : 1[normale], 2[half], 4[quad], 8[1/8], 16[1/16]\r\n", argv[0]); 
     exit(1);
   }

   /* Getting THIS server IP */
   if (argc > 1) {
      printf("Sender IP address : %s\r\n", argv[1]);
      adr_srvr.sin_addr.s_addr = inet_addr(argv[1]);
      if (adr_srvr.sin_addr.s_addr == INADDR_NONE) bail("Bad address.");

      adr_ctl.sin_addr.s_addr = inet_addr(argv[1]);
      if (adr_ctl.sin_addr.s_addr == INADDR_NONE) bail("Bad address.");
   }
   else
   {
      adr_srvr.sin_addr.s_addr = inet_addr("127.0.0.1");
      adr_ctl.sin_addr.s_addr = inet_addr("127.0.0.1");
   }

   /* Getting THIS server port number */
   if (argc > 2){
      srvr_port = argv[2];
   }
//   printf("Sender Port : %s\r\n", srvr_port);

//   if((fo = fopen(argv[3],"wb")) == NULL) {
//   if((fo = open(argv[3], O_CREAT | O_TRUNC | O_WRONLY | O_LARGEFILE | O_SYNC, 0644)) < 0) {
   if((fo = open(argv[3], O_CREAT | O_TRUNC | O_WRONLY | O_LARGEFILE , 0644)) < 0) {
     fprintf(stderr,"Fail to open '%s'\n",argv[3]);
     exit(1);
   }

   recrate = (unsigned int)atoi(argv[4]);
   playrate = (unsigned int)atoi(argv[5]);
   strt_year = (uint16_t)atoi(argv[6]);
   strt_day  = (uint16_t)atoi(argv[7]);
   strt_hh   = (uint8_t)atoi(argv[8]);
   strt_mm   = (uint8_t)atoi(argv[9]);
   strt_ss   = (uint8_t)atoi(argv[10]);
   fin_year = (uint16_t)atoi(argv[11]);
   fin_day  = (uint16_t)atoi(argv[12]);
   fin_hh   = (uint8_t)atoi(argv[13]);
   fin_mm   = (uint8_t)atoi(argv[14]);
   fin_ss   = (uint8_t)atoi(argv[15]);

   /* Creating thread argument */
   arg = (tharg *)malloc(sizeof(tharg));
   if (arg == NULL) bail("malloc(2)");

   /* Creating thread argument */
   darg = (tharg *)malloc(sizeof(tharg));
   if (darg == NULL) bail("malloc(2)");

   /* Making a UDP socket on server */
   arg->sd = socket(PF_INET, SOCK_DGRAM, 0);
   if (arg->sd == -1) bail("socket()");

   /* Making a UDP socket on server */
   darg->sd = socket(PF_INET, SOCK_DGRAM, 0);
   if (darg->sd == -1) bail("socket()");

   // only transmit
   adr_srvr.sin_family = AF_INET;
   adr_srvr.sin_port = htons(atoi(srvr_port));
   len_inet = sizeof(adr_srvr);

   // only transmit
   adr_ctl.sin_family = AF_INET;
   adr_ctl.sin_port = htons(atoi(srvr_port) + 1);

   // both receive and transmit
   adr_clnt.sin_addr.s_addr = htonl(INADDR_ANY);
   adr_clnt.sin_family = AF_INET;
   adr_clnt.sin_port = htons(atoi(srvr_port));

   timeout.tv_sec = 1;
   timeout.tv_usec = 0;
   z = setsockopt(darg->sd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));
   if (z == -1) bail("setsocketopt");

   s = 1;
   setsockopt(darg->sd, SOL_SOCKET, SO_REUSEADDR, &s, sizeof(s));
//   s = 268435456;
//   s = 536870912;
   s = 2684354560;
//   opt = atoi( read_proc_value("/proc/sys/net/core/rmem_max"));
   setsockopt(darg->sd, SOL_SOCKET, SO_RCVBUF, &s, sizeof(s));
//   s = 268435456;
//   s = 536870912;
   s = 2684354560;
//   opt = atoi( read_proc_value("/proc/sys/net/core/wmem_max"));
   setsockopt(darg->sd, SOL_SOCKET, SO_SNDBUF, &s, sizeof(s));


   // need bind in case of both receive and transmit
   z = bind(darg->sd, (struct sockaddr *)&adr_clnt, len_inet);
   if (z == -1) bail("bind(2)");

   /* START timestamp */
   time(&etd);
   etm = *localtime(&etd);
   strftime(etstr, sizeof(etstr), tfmt, &etm);
   printf("Ready to SEND(UDP) START at %s.\r\n", etstr);

   if(strt_ss < 10){
      ss = (uint8_t)(50 + strt_ss);
      if(strt_mm == 0){
         mm = (uint8_t)59;
         if(strt_hh == 0){
            hh = (uint8_t)23;
            day = (uint16_t)(strt_day - 1);
            year = (uint16_t)strt_year;
         }
         else{
            year = (uint16_t)strt_year;
            day = (uint16_t)strt_day;
            hh = (uint8_t)(strt_hh - 1);
         }
      }
      else{
         year = (uint16_t)strt_year;
         day = (uint16_t)strt_day;
         hh = (uint8_t)strt_hh;
         mm = (uint8_t)(strt_mm - 1);
      }
   }
   else{
      year = (uint16_t)strt_year;
      day = (uint16_t)strt_day;
      hh = (uint8_t)strt_hh;
      mm = (uint8_t)strt_mm;
      ss = (uint8_t)(strt_ss - 10);
   }

   getchar();

   vfcp_vr.ver_kind = 0x13; // VDIFCP version(0x1), VR(0x3)
   vfcp_vr.port = 0x10; // VSI port(1 - 4), reserved
   vfcp_vr.plen = 0x1c00; // VR packet length(default 28byte), need swap
//   vfcp_vr.plen = 0x5000; // VR packet length(default 80byte)
   vfcp_vr.seqno = 0; // packet sequence number
   seqno_swap = 0;

   vfcp_vr.mode_timeupper = 0x20 | ((year / 1000) & 0x0f); // VSI mode 1, year(2013=>2)
   vfcp_vr.time[0] = ((((year / 100) % 10) << 4) & 0xf0) | (((year / 10) % 10) & 0x0f);
   vfcp_vr.time[1] = (((year % 10) << 4) & 0xf0) | ((day / 100) & 0x000f);
   vfcp_vr.time[2] = ((((day / 10) % 10) << 4) & 0xf0) | ((day % 10) & 0x0f);
   vfcp_vr.time[3] = (((hh / 10) << 4) & 0xf0) | ((hh % 10) & 0x0f);
   vfcp_vr.time[4] = (((mm / 10) << 4) & 0xf0) | ((mm % 10) & 0x0f);
   vfcp_vr.time[5] = (((ss / 10) << 4) & 0xf0) | ((ss % 10) & 0x0f);

   if(recrate == 2048){
      frame_length = 200000;
      rate_count = 5;

      fprintf(stderr,
            "#1234,%04d%03d%02d%02d%02d,1024,32,2,ACQ,PDATAVP1,PDATAVP2;\r\n",
            year, day, hh, mm, ss); 
   }
   else if(recrate == 1024){
      frame_length = 100000;
      rate_count = 10;

      fprintf(stderr,
            "#1234,%04d%03d%02d%02d%02d,512,32,2,ACQ,PDATAVP1,PDATAVP2;.\r\n", 
            year, day, hh, mm, ss); 
   }

   if(playrate == 1){ // normal
      rate_count = 1;
   }
   else if(playrate == 2){ // half
      rate_count = 2;
   }
   else if(playrate == 4){ // quad
      rate_count = 4;
   }
   else if(playrate == 8){ // 1/8
      rate_count = 8;
   }
   else if(playrate == 16){ // 1/16
      rate_count = 16;
   }

   frame_no = 0;
   sec_refepoch = 86400;
   sec_count = 0;

   scl = clock();

   do{
      ecl = clock();
      runtime = (ecl - scl)/CLOCKS_PER_SEC;
   }while( runtime < 1.0 );

   gettimeofday(&t_start, NULL);
   gettimeofday(&t_ctlstart, NULL);

   while(1) {
      if(year == fin_year && day == fin_day && hh == fin_hh && mm == fin_mm && ss == fin_ss)
         break;

      // send VDICP/VR using control port(60001), UTC
      z = sendto(arg->sd, &vfcp_vr, 28, 0,
            (struct sockaddr *)&adr_ctl, len_inet);

      if(strt_flag == 1){
         last_fpos = lseek64(fo, 0, SEEK_CUR);
         fprintf(stderr, "file offset: %lld\r\n", last_fpos);
//         last_fpos = fseek(fo, 0, SEEK_CUR);
//         fprintf(stderr, "file offset: %d\r\n", last_fpos);

         for(m=0; m<frame_length; m++){
//            for(n=0; n<10; n++){ 
//               z = recvfrom(darg->sd, &vf_headdf[n], 1312, MSG_NOSIGNAL,
               z = recvfrom(darg->sd, &vf_headdf, 1312, MSG_NOSIGNAL,
                           NULL, 0);
//                           (struct sockaddr *)&adr_srvr, &len_inet); // only need sendto function
                                                               // receive client information
//            }
            if(z < 0){
               fprintf(stderr, "UDP receive error!\r\n");

               if(sec_wait == 0){
                  sec_wait = 1;
                  last_fpos = lseek64(fo, last_fpos, SEEK_SET);
                  fprintf(stderr, "file pos: exec m: %d, %lld\r\n", m, last_fpos);
//                  last_fpos = fseek(fo, last_fpos, SEEK_SET);
//                  fprintf(stderr, "file pos: %d\r\n", last_fpos);

                  if(ss < 2){
                     ss = 58 + ss;
                     if(mm == 0){
                        mm = 59;
                        if(hh == 0){
                           hh = 23;
                           day--;
                        }
                        else hh--;
                     }
                     else mm--;
                  }
                  else ss -= 2;
               }
               else{
                  sec_wait = 0;
               }

               break;
            }

            if(sec_wait == 1){
               if(m == (frame_length - 1)){ 
                  sec_wait = 0;
               }
            }
            else{
//               fwrite(vf_headdf, 1, 13120, fo);
//               fwrite(&vf_headdf, 1, 1312, fo);
               write(fo, &vf_headdf, 1312);
//               write(fo, vf_headdf, 13120);
            }
         }
      }
      else{ // strt_flag == 0
         if(year == strt_year && day == strt_day && hh == strt_hh && mm == strt_mm && ss == strt_ss) strt_flag = 1;
      }

      do{
         gettimeofday(&t_ctlend, NULL);
         t_ctldiff.tv_sec = t_ctlend.tv_sec - t_ctlstart.tv_sec;
//         t_ctldiff.tv_usec = t_ctlend.tv_usec - t_ctlstart.tv_usec;

//         if(t_ctldiff.tv_usec < 0){
//            t_ctldiff.tv_usec = t_ctldiff.tv_usec + 1000000;
//         }
      } while (t_ctldiff.tv_sec < playrate); // normal
      t_ctlstart.tv_sec = t_ctlend.tv_sec;

      /* End timestamp */
//      time(&etd);
//      etm = *localtime(&etd);
//      strftime(etstr, sizeof(etstr), tfmt, &etm);

//      printf("SEND(UDP) at %s.\n", etstr);

      sec_count++;
      sec_refepoch++;

       ss++;
       if( ss == 60 ){
          ss = 0;
          mm++;
          if( mm == 60){
             mm = 0;
             hh++;
             if( hh == 24){
                hh = 0;
                day++;
                if(day == 366){
                   day = 1;
                   year++;
                }
             }
          }
       }

//      vfcp_vr.mode_timeupper = 0x10 | ((year / 1000) & 0x0f); // VSI mode 1, year(2013=>2)
//      vfcp_vr.time[0] = ((((year / 100) % 10) << 4) & 0xf0) | (((year / 10) % 10) & 0x0f);
      vfcp_vr.time[1] = (((year % 10) << 4) & 0xf0) | ((day / 100) & 0x000f);
      vfcp_vr.time[2] = ((((day / 10) % 10) << 4) & 0xf0) | ((day % 10) & 0x0f);
      vfcp_vr.time[3] = (((hh / 10) << 4) & 0xf0) | ((hh % 10) & 0x0f);
      vfcp_vr.time[4] = (((mm / 10) << 4) & 0xf0) | ((mm % 10) & 0x0f);
      vfcp_vr.time[5] = (((ss / 10) << 4) & 0xf0) | ((ss % 10) & 0x0f);

      if(recrate == 2048){
         fprintf(stderr,
               "#1234,%04d%03d%02d%02d%02d,1024,32,2,ACQ,PDATAVP1,PDATAVP2;\r\n",
               year, day, hh, mm, ss); 
      }
      else if(recrate == 1024){
         fprintf(stderr,
               "#1234,%04d%03d%02d%02d%02d,512,32,2,ACQ,PDATAVP1,PDATAVP2;.\r\n", 
               year, day, hh, mm, ss); 
      }

      seqno_swap++;
      vfcp_vr.seqno = ((seqno_swap >> 24) & 0x000000ff) |
                      ((seqno_swap >> 8) & 0x0000ff00) |
                      ((seqno_swap << 8) & 0x00ff0000) |
                      ((seqno_swap << 24) & 0xff000000); // 2014. 01

   }

   /* START timestamp */
   time(&etd);
   etm = *localtime(&etd);
   strftime(etstr, sizeof(etstr), tfmt, &etm);
   printf("SEND(UDP) END at %s.\n", etstr);

   close(arg->fd);
   close(darg->fd);
//   fclose(fo);
   close(fo);

   free(arg);
   free(darg);
   exit(0);
}

static void bail(const char *on_what)
{
        if (errno != 0) {
                fputs(strerror(errno), stderr);
                fputs(": ", stderr);
        }
        fputs(on_what, stderr);
        fputc('\n', stderr);
        exit(1);
}


