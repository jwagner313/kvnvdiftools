
#define _LARGEFILE64_SOURCE // open-> O_LARGEFILE
#define _GNU_SOURCE // open -> O_DIRECT
#define _XOPEN_SOURCE 600


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include <fcntl.h>
#include <malloc.h>

#include "vdif_io_orig.h"

static int strt_flag = 0;
uint64_t last_fpos=0;

//// JANW  ////////////////////////////////////////////////////////////////////////////////////////////
#define ADJUST_SAMPLER_LEVELS      1  // 1 to change 2-bit sample voltages from Mark5B encoding into VDIF encoding (if desired...)
#define VDIF_HEADER_SIZE          32
#define KVN_VDIF_PAYLOAD_SIZE   1280  // bytes of payload data without header
#define KVN_VDIF_FRAME_SIZE     1312  // bytes total with 32-byte header

static unsigned char m_lut_signmagswap[256];
static unsigned char m_lut_signmagswap_hardcoded[256] =
{
  0x00,  0x02,  0x01,  0x03,  0x08,  0x0A,  0x09,  0x0B,  0x04,  0x06,  0x05,  0x07,  0x0C,  0x0E,  0x0D,  0x0F,  
  0x20,  0x22,  0x21,  0x23,  0x28,  0x2A,  0x29,  0x2B,  0x24,  0x26,  0x25,  0x27,  0x2C,  0x2E,  0x2D,  0x2F,  
  0x10,  0x12,  0x11,  0x13,  0x18,  0x1A,  0x19,  0x1B,  0x14,  0x16,  0x15,  0x17,  0x1C,  0x1E,  0x1D,  0x1F,  
  0x30,  0x32,  0x31,  0x33,  0x38,  0x3A,  0x39,  0x3B,  0x34,  0x36,  0x35,  0x37,  0x3C,  0x3E,  0x3D,  0x3F,  
  0x80,  0x82,  0x81,  0x83,  0x88,  0x8A,  0x89,  0x8B,  0x84,  0x86,  0x85,  0x87,  0x8C,  0x8E,  0x8D,  0x8F,  
  0xA0,  0xA2,  0xA1,  0xA3,  0xA8,  0xAA,  0xA9,  0xAB,  0xA4,  0xA6,  0xA5,  0xA7,  0xAC,  0xAE,  0xAD,  0xAF,  
  0x90,  0x92,  0x91,  0x93,  0x98,  0x9A,  0x99,  0x9B,  0x94,  0x96,  0x95,  0x97,  0x9C,  0x9E,  0x9D,  0x9F,  
  0xB0,  0xB2,  0xB1,  0xB3,  0xB8,  0xBA,  0xB9,  0xBB,  0xB4,  0xB6,  0xB5,  0xB7,  0xBC,  0xBE,  0xBD,  0xBF,  
  0x40,  0x42,  0x41,  0x43,  0x48,  0x4A,  0x49,  0x4B,  0x44,  0x46,  0x45,  0x47,  0x4C,  0x4E,  0x4D,  0x4F,  
  0x60,  0x62,  0x61,  0x63,  0x68,  0x6A,  0x69,  0x6B,  0x64,  0x66,  0x65,  0x67,  0x6C,  0x6E,  0x6D,  0x6F,  
  0x50,  0x52,  0x51,  0x53,  0x58,  0x5A,  0x59,  0x5B,  0x54,  0x56,  0x55,  0x57,  0x5C,  0x5E,  0x5D,  0x5F,  
  0x70,  0x72,  0x71,  0x73,  0x78,  0x7A,  0x79,  0x7B,  0x74,  0x76,  0x75,  0x77,  0x7C,  0x7E,  0x7D,  0x7F,  
  0xC0,  0xC2,  0xC1,  0xC3,  0xC8,  0xCA,  0xC9,  0xCB,  0xC4,  0xC6,  0xC5,  0xC7,  0xCC,  0xCE,  0xCD,  0xCF,  
  0xE0,  0xE2,  0xE1,  0xE3,  0xE8,  0xEA,  0xE9,  0xEB,  0xE4,  0xE6,  0xE5,  0xE7,  0xEC,  0xEE,  0xED,  0xEF,  
  0xD0,  0xD2,  0xD1,  0xD3,  0xD8,  0xDA,  0xD9,  0xDB,  0xD4,  0xD6,  0xD5,  0xD7,  0xDC,  0xDE,  0xDD,  0xDF,  
  0xF0,  0xF2,  0xF1,  0xF3,  0xF8,  0xFA,  0xF9,  0xFB,  0xF4,  0xF6,  0xF5,  0xF7,  0xFC,  0xFE,  0xFD,  0xFF
};

/** For one VDIF frame reverse the 32-bit bit order and correct some VDIF header information */
static void fix_kvn_vdif_frame(unsigned char* buf)
{
    int i;

    /* Step 1: fix header endianness */
    for (i=0; i<VDIF_HEADER_SIZE; i+=4)
    {
        // swap outer 2 bytes
        unsigned char c1 = buf[i+0];
        buf[i+0] = buf[i+3]; buf[i+3] = c1;

        // swap inner 2 bytes
        unsigned char c2 = buf[i+1];
        buf[i+1] = buf[i+2]; buf[i+2] = c2;
    }

    /* Step 2: fix the 'Invalid' bit */
    buf[3] = buf[3] & ~0x80;

    /* Step 5: correct the bit order of payload data */
    /* Step 6: reverse the sign and magnitude bits */
    for (i=VDIF_HEADER_SIZE; i<KVN_VDIF_FRAME_SIZE; i+=4)
    {
        unsigned char b0 = buf[i+0];
        unsigned char b1 = buf[i+1];
        unsigned char b2 = buf[i+2];
        unsigned char b3 = buf[i+3];

        // Note: the VSI-H TVG recording in KVN VDIF format at
        //   /home/oper/vdif/vdiftvg_play_test_1.dat
        // suggests that for KVN VDIF, a full 32-bit flip is needed to be Standard VDIF.
        //
        // On the other hand, the r13340b spectral line scan No0004 in OCTA VDIF and Mark5B
        // recording formats suggests that 8-bit flip, then 2-bit flip, and no change in Endianness
        // is enough for the correction.

        // Flip the bits in all 8-bit bytes
        b0 = byte_bit_swap[b0];
        b1 = byte_bit_swap[b1];
        b2 = byte_bit_swap[b2];
        b3 = byte_bit_swap[b3];

#if ADJUST_SAMPLER_LEVELS
        // Flip 2-bit sample sign/magitude bits
        // VDIF 1.1 page 11: 10. Sample representation
        //    00, 01, 10, 11 = -Mag, -1.0, +1.0, +Mag
        // Mark5B in mark5 memo #019:
        //    00, 01, 10, 11 = same as VDIF, follows Mark4/VLBA encoding
        // But: Mark5access library decoder, mark5_format_mark5b.c definition of lut4level[4]:
        //    00, 01, 10, 11 = -Mag, +1.0, -1.0, +Mag
        b0 = m_lut_signmagswap_hardcoded[b0];
        b1 = m_lut_signmagswap_hardcoded[b1];
        b2 = m_lut_signmagswap_hardcoded[b2];
        b3 = m_lut_signmagswap_hardcoded[b3];
#endif

        // Store
        buf[i+0] = b0;
        buf[i+1] = b1;
        buf[i+2] = b2;
        buf[i+3] = b3;
    }

    return;
}

/** A lookup table for flipping 2-bit sample sign and magnitude in 8-bit bytes */
static void init_lut_signmagswap()
{
    int i;
    unsigned char b;
    for (i=0; i<256; i++)
    {
        // test case
        //   b = 0x1B; // 0b00 01 10 11  so expecting  swp = 0b00 10 01 11 = 0x27 as output
        b = (unsigned char)i;
        unsigned char s0 = b & 0xAA;  // 0xAA = 0b101010
        unsigned char m0 = b & 0x55;  // 0x55 = 0b010101
        unsigned char swp = (s0 >> 1) | (m0 << 1);
        m_lut_signmagswap[b] = swp;
    }
    // for (i=0; i<256; i++)
    //   fprintf(stderr, " 0x%02X, ", m_lut_signmagswap[i]);
}
//// JANW  ////////////////////////////////////////////////////////////////////////////////////////////



int main(int argc, char *argv[])
{
   int rc;
   int m, n;

   tharg *arg;
   tharg *darg;
//   FILE *fo;
   int fo;

   char *srvr_port = "60000";
   struct sockaddr_in adr_srvr;
   struct sockaddr_in adr_ctl;
   struct sockaddr_in adr_clnt;
   struct VDIF_HEAD_DF vf_headdf;
//   struct VDIF_HEAD_DF vf_headdf[10];
   struct VDIFCP_SR vfcp_sr;
   struct VDIFCP_VR vfcp_vr;
   int len_inet;
   int s;
   uint16_t year = 2013;
   uint16_t day =343;
   uint8_t hh = 6;
   uint8_t mm =46; 
   uint8_t ss = 30;
   uint16_t strt_year;
   uint16_t strt_day;
   uint8_t strt_hh;
   uint8_t strt_mm; 
   uint8_t strt_ss;
   uint16_t fin_year;
   uint16_t fin_day;
   uint8_t fin_hh;
   uint8_t fin_mm; 
   uint8_t fin_ss;
   int mepoch, mtime;
//   char src_time[13] = "2014001000000";
   uint32_t seqno_swap;

   unsigned int frame_no;
   unsigned int frame_ch;
   unsigned int sec_refepoch;
   unsigned char sec_count;
   unsigned int sec_refepoch_swap;
   unsigned int frame_no_swap;
   uint8_t* p_vf_dat;
   uint8_t* p_bit_swap;
   unsigned int sec_wait=0;
   unsigned int recrate;
   unsigned int playrate;
   unsigned int frame_length;
   unsigned int rate_count;

   //char tofname[256];
   len_inet = sizeof(adr_srvr);
   memset(&adr_srvr, 0, sizeof(adr_srvr));
   memset(&adr_ctl, 0, sizeof(adr_ctl));
   memset(&adr_clnt, 0, sizeof(adr_clnt));
   memset(&vf_headdf.vdif_hdr, 0, sizeof(vf_headdf.vdif_hdr));
   memset(&vf_headdf.vdif_dat, 0, sizeof(vf_headdf.vdif_dat));
//   memset(vf_headdf, 0, 13120);
   memset(&vfcp_sr, 0, sizeof(vfcp_sr));
   memset(&vfcp_vr, 0, sizeof(vfcp_vr));
   init_lut_signmagswap();

   // Command Line Parameters
   if(argc != 16) {
     fprintf(stderr,"Usage: $ %s IPaddr Port# Outfile[VDIF format] rec.rate# play.rate# start time:(yyyy ddd hh mm ss) finish time:(yyyy ddd hh mm ss)\r\n     ---> rec.rate# : 2048 or 1024\r\n     ---> play.rate# : 1[normale], 2[half], 4[quad], 8[1/8], 16[1/16]\r\n", argv[0]); 
     exit(1);
   }

   /* Getting THIS server IP */
   if (argc > 1) {
      printf("Sender IP address : %s\r\n", argv[1]);
      adr_srvr.sin_addr.s_addr = inet_addr(argv[1]);
      if (adr_srvr.sin_addr.s_addr == INADDR_NONE) bail("Bad address.");

      adr_ctl.sin_addr.s_addr = inet_addr(argv[1]);
      if (adr_ctl.sin_addr.s_addr == INADDR_NONE) bail("Bad address.");
   }
   else
   {
      adr_srvr.sin_addr.s_addr = inet_addr("127.0.0.1");
      adr_ctl.sin_addr.s_addr = inet_addr("127.0.0.1");
   }

   /* Getting THIS server port number */
   if (argc > 2){
      srvr_port = argv[2];
   }
//   printf("Sender Port : %s\r\n", srvr_port);

//   if((fo = fopen(argv[3],"wb")) == NULL) {
//   if((fo = open(argv[3], O_CREAT | O_TRUNC | O_WRONLY | O_LARGEFILE | O_SYNC, 0644)) < 0) {
   if((fo = open(argv[3], O_CREAT | O_TRUNC | O_WRONLY | O_LARGEFILE , 0644)) < 0) {
     fprintf(stderr,"Fail to open '%s'\n",argv[3]);
     exit(1);
   }

   recrate = (unsigned int)atoi(argv[4]);
   playrate = (unsigned int)atoi(argv[5]);
   strt_year = (uint16_t)atoi(argv[6]);
   strt_day  = (uint16_t)atoi(argv[7]);
   strt_hh   = (uint8_t)atoi(argv[8]);
   strt_mm   = (uint8_t)atoi(argv[9]);
   strt_ss   = (uint8_t)atoi(argv[10]);
   fin_year = (uint16_t)atoi(argv[11]);
   fin_day  = (uint16_t)atoi(argv[12]);
   fin_hh   = (uint8_t)atoi(argv[13]);
   fin_mm   = (uint8_t)atoi(argv[14]);
   fin_ss   = (uint8_t)atoi(argv[15]);

   /* Creating thread argument */
   arg = (tharg *)malloc(sizeof(tharg));
   if (arg == NULL) bail("malloc(2)");

   /* Creating thread argument */
   darg = (tharg *)malloc(sizeof(tharg));
   if (darg == NULL) bail("malloc(2)");

   /* Making a UDP socket on server */
   arg->sd = socket(PF_INET, SOCK_DGRAM, 0);
   if (arg->sd == -1) bail("socket()");

   /* Making a UDP socket on server */
   darg->sd = socket(PF_INET, SOCK_DGRAM, 0);
   if (darg->sd == -1) bail("socket()");

   // only transmit
   adr_srvr.sin_family = AF_INET;
   adr_srvr.sin_port = htons(atoi(srvr_port));
   len_inet = sizeof(adr_srvr);

   // only transmit
   adr_ctl.sin_family = AF_INET;
   adr_ctl.sin_port = htons(atoi(srvr_port) + 1);

   // both receive and transmit
   adr_clnt.sin_addr.s_addr = htonl(INADDR_ANY);
   adr_clnt.sin_family = AF_INET;
   adr_clnt.sin_port = htons(atoi(srvr_port));

   timeout.tv_sec = 1;
   timeout.tv_usec = 0;
   z = setsockopt(darg->sd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));
   if (z == -1) bail("setsocketopt");

   s = 1;
   setsockopt(darg->sd, SOL_SOCKET, SO_REUSEADDR, &s, sizeof(s));
//   s = 268435456;
//   s = 536870912;
   s = 2684354560;
//   opt = atoi( read_proc_value("/proc/sys/net/core/rmem_max"));
   setsockopt(darg->sd, SOL_SOCKET, SO_RCVBUF, &s, sizeof(s));
//   s = 268435456;
//   s = 536870912;
   s = 2684354560;
//   opt = atoi( read_proc_value("/proc/sys/net/core/wmem_max"));
   setsockopt(darg->sd, SOL_SOCKET, SO_SNDBUF, &s, sizeof(s));


   // need bind in case of both receive and transmit
   z = bind(darg->sd, (struct sockaddr *)&adr_clnt, len_inet);
   if (z == -1) bail("bind(2)");

   /* START timestamp */
   time(&etd);
   etm = *localtime(&etd);
   strftime(etstr, sizeof(etstr), tfmt, &etm);
   printf("Ready to SEND(UDP) START at %s.\r\n", etstr);

   if(strt_ss < 10){
      ss = (uint8_t)(50 + strt_ss);
      if(strt_mm == 0){
         mm = (uint8_t)59;
         if(strt_hh == 0){
            hh = (uint8_t)23;
            day = (uint16_t)(strt_day - 1);
            year = (uint16_t)strt_year;
         }
         else{
            year = (uint16_t)strt_year;
            day = (uint16_t)strt_day;
            hh = (uint8_t)(strt_hh - 1);
         }
      }
      else{
         year = (uint16_t)strt_year;
         day = (uint16_t)strt_day;
         hh = (uint8_t)strt_hh;
         mm = (uint8_t)(strt_mm - 1);
      }
   }
   else{
      year = (uint16_t)strt_year;
      day = (uint16_t)strt_day;
      hh = (uint8_t)strt_hh;
      mm = (uint8_t)strt_mm;
      ss = (uint8_t)(strt_ss - 10);
   }

   getchar();

   vfcp_vr.ver_kind = 0x13; // VDIFCP version(0x1), VR(0x3)
   vfcp_vr.port = 0x10; // VSI port(1 - 4), reserved
   vfcp_vr.plen = 0x1c00; // VR packet length(default 28byte), need swap
//   vfcp_vr.plen = 0x5000; // VR packet length(default 80byte)
   vfcp_vr.seqno = 0; // packet sequence number
   seqno_swap = 0;

   vfcp_vr.mode_timeupper = 0x20 | ((year / 1000) & 0x0f); // VSI mode 1, year(2013=>2)
   vfcp_vr.time[0] = ((((year / 100) % 10) << 4) & 0xf0) | (((year / 10) % 10) & 0x0f);
   vfcp_vr.time[1] = (((year % 10) << 4) & 0xf0) | ((day / 100) & 0x000f);
   vfcp_vr.time[2] = ((((day / 10) % 10) << 4) & 0xf0) | ((day % 10) & 0x0f);
   vfcp_vr.time[3] = (((hh / 10) << 4) & 0xf0) | ((hh % 10) & 0x0f);
   vfcp_vr.time[4] = (((mm / 10) << 4) & 0xf0) | ((mm % 10) & 0x0f);
   vfcp_vr.time[5] = (((ss / 10) << 4) & 0xf0) | ((ss % 10) & 0x0f);

   if(recrate == 2048){
      frame_length = 200000;
      rate_count = 5;

      fprintf(stderr,
            "#1234,%04d%03d%02d%02d%02d,1024,32,2,ACQ,PDATAVP1,PDATAVP2;\r\n",
            year, day, hh, mm, ss); 
   }
   else if(recrate == 1024){
      frame_length = 100000;
      rate_count = 10;

      fprintf(stderr,
            "#1234,%04d%03d%02d%02d%02d,512,32,2,ACQ,PDATAVP1,PDATAVP2;.\r\n", 
            year, day, hh, mm, ss); 
   }

   if(playrate == 1){ // normal
      rate_count = 1;
   }
   else if(playrate == 2){ // half
      rate_count = 2;
   }
   else if(playrate == 4){ // quad
      rate_count = 4;
   }
   else if(playrate == 8){ // 1/8
      rate_count = 8;
   }
   else if(playrate == 16){ // 1/16
      rate_count = 16;
   }

   frame_no = 0;
   sec_refepoch = 86400;
   sec_count = 0;

   scl = clock();

   do{
      ecl = clock();
      runtime = (ecl - scl)/CLOCKS_PER_SEC;
   }while( runtime < 1.0 );

   gettimeofday(&t_start, NULL);
   gettimeofday(&t_ctlstart, NULL);

   while(1) {
      if(year == fin_year && day == fin_day && hh == fin_hh && mm == fin_mm && ss == fin_ss)
         break;

      // send VDICP/VR using control port(60001), UTC
      z = sendto(arg->sd, &vfcp_vr, 28, 0,
            (struct sockaddr *)&adr_ctl, len_inet);

      if(strt_flag == 1){
         last_fpos = lseek64(fo, 0, SEEK_CUR);
         fprintf(stderr, "file offset: %lld\r\n", last_fpos);
//         last_fpos = fseek(fo, 0, SEEK_CUR);
//         fprintf(stderr, "file offset: %d\r\n", last_fpos);

         for(m=0; m<frame_length; m++){
//            for(n=0; n<10; n++){ 
//               z = recvfrom(darg->sd, &vf_headdf[n], 1312, MSG_NOSIGNAL,
               z = recvfrom(darg->sd, &vf_headdf, 1312, MSG_NOSIGNAL,
                           NULL, 0);
//                           (struct sockaddr *)&adr_srvr, &len_inet); // only need sendto function
                                                               // receive client information
//            }
            if(z < 0){
               fprintf(stderr, "UDP receive error!\r\n");

               if(sec_wait == 0){
                  sec_wait = 1;
                  last_fpos = lseek64(fo, last_fpos, SEEK_SET);
                  fprintf(stderr, "file pos: exec m: %d, %lld\r\n", m, last_fpos);
//                  last_fpos = fseek(fo, last_fpos, SEEK_SET);
//                  fprintf(stderr, "file pos: %d\r\n", last_fpos);

                  if(ss < 2){
                     ss = 58 + ss;
                     if(mm == 0){
                        mm = 59;
                        if(hh == 0){
                           hh = 23;
                           day--;
                        }
                        else hh--;
                     }
                     else mm--;
                  }
                  else ss -= 2;
               }
               else{
                  sec_wait = 0;
               }

               break;
            }

            if(sec_wait == 1){
               if(m == (frame_length - 1)){ 
                  sec_wait = 0;
               }
            }
            else{
                 fix_kvn_vdif_frame((unsigned char*)&vf_headdf); // JanW
//               fwrite(vf_headdf, 1, 13120, fo);
//               fwrite(&vf_headdf, 1, 1312, fo);
               write(fo, &vf_headdf, 1312);
//               write(fo, vf_headdf, 13120);
            }
         }
      }
      else{ // strt_flag == 0
         if(year == strt_year && day == strt_day && hh == strt_hh && mm == strt_mm && ss == strt_ss) strt_flag = 1;
      }

      do{
         gettimeofday(&t_ctlend, NULL);
         t_ctldiff.tv_sec = t_ctlend.tv_sec - t_ctlstart.tv_sec;
//         t_ctldiff.tv_usec = t_ctlend.tv_usec - t_ctlstart.tv_usec;

//         if(t_ctldiff.tv_usec < 0){
//            t_ctldiff.tv_usec = t_ctldiff.tv_usec + 1000000;
//         }
      } while (t_ctldiff.tv_sec < playrate); // normal
      t_ctlstart.tv_sec = t_ctlend.tv_sec;

      /* End timestamp */
//      time(&etd);
//      etm = *localtime(&etd);
//      strftime(etstr, sizeof(etstr), tfmt, &etm);

//      printf("SEND(UDP) at %s.\n", etstr);

      sec_count++;
      sec_refepoch++;

       ss++;
       if( ss == 60 ){
          ss = 0;
          mm++;
          if( mm == 60){
             mm = 0;
             hh++;
             if( hh == 24){
                hh = 0;
                day++;
                if(day == 366){
                   day = 1;
                   year++;
                }
             }
          }
       }

//      vfcp_vr.mode_timeupper = 0x10 | ((year / 1000) & 0x0f); // VSI mode 1, year(2013=>2)
//      vfcp_vr.time[0] = ((((year / 100) % 10) << 4) & 0xf0) | (((year / 10) % 10) & 0x0f);
      vfcp_vr.time[1] = (((year % 10) << 4) & 0xf0) | ((day / 100) & 0x000f);
      vfcp_vr.time[2] = ((((day / 10) % 10) << 4) & 0xf0) | ((day % 10) & 0x0f);
      vfcp_vr.time[3] = (((hh / 10) << 4) & 0xf0) | ((hh % 10) & 0x0f);
      vfcp_vr.time[4] = (((mm / 10) << 4) & 0xf0) | ((mm % 10) & 0x0f);
      vfcp_vr.time[5] = (((ss / 10) << 4) & 0xf0) | ((ss % 10) & 0x0f);

      if(recrate == 2048){
         fprintf(stderr,
               "#1234,%04d%03d%02d%02d%02d,1024,32,2,ACQ,PDATAVP1,PDATAVP2;\r\n",
               year, day, hh, mm, ss); 
      }
      else if(recrate == 1024){
         fprintf(stderr,
               "#1234,%04d%03d%02d%02d%02d,512,32,2,ACQ,PDATAVP1,PDATAVP2;.\r\n", 
               year, day, hh, mm, ss); 
      }

      seqno_swap++;
      vfcp_vr.seqno = ((seqno_swap >> 24) & 0x000000ff) |
                      ((seqno_swap >> 8) & 0x0000ff00) |
                      ((seqno_swap << 8) & 0x00ff0000) |
                      ((seqno_swap << 24) & 0xff000000); // 2014. 01

   }

   /* START timestamp */
   time(&etd);
   etm = *localtime(&etd);
   strftime(etstr, sizeof(etstr), tfmt, &etm);
   printf("SEND(UDP) END at %s.\n", etstr);

   close(arg->fd);
   close(darg->fd);
//   fclose(fo);
   close(fo);

   free(arg);
   free(darg);
   exit(0);
}

static void bail(const char *on_what)
{
        if (errno != 0) {
                fputs(strerror(errno), stderr);
                fputs(": ", stderr);
        }
        fputs(on_what, stderr);
        fputc('\n', stderr);
        exit(1);
}


