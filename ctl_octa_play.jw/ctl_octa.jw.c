/****************************************************************
 **
 ** OCTA Data Stream Request and VDIF UDP/IP Data Stream Capture
 **
 ** Version 1.50
 **
 ** Written by: ???
 ** Change log:
 **   06Aug2014 Janw -- heavily modified the code, added
 **                     real-time correction of KVN "VDIF"
 **                     into more standard-compliant VDIF
 **
 ** Note that the program is specific to the OCTA system.
 **
 ** Because the OCTA system needs to receive regular requests for
 ** more data, using a normal fast 1G/10G capture program like
 ** GULP (http://staff.washington.edu/corey/gulp/) is maybe not
 ** easily possible...
 **
 *****************************************************************/

#define _LARGEFILE64_SOURCE
#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE
#define _XOPEN_SOURCE 600

#include "vdif_io.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <malloc.h>
#include <memory.h>

#define PROGRAM_NAME  "ctl_octa_play.jw"
#define VERSION_STR   "1.5"


///////////////////////////////////////////////////////////////////////
// Switches
///////////////////////////////////////////////////////////////////////

#define ADJUST_SAMPLER_LEVELS       1  // 1 to change 2-bit sample voltages from Mark5B encoding into VDIF encoding (if desired...)
#define FIX_KVN_VDIF_ISSUES         1  // 1 to change 32-bit data in "KVN VDIF" stream to the correct standard VDIF endianness


///////////////////////////////////////////////////////////////////////
// Defines
///////////////////////////////////////////////////////////////////////

#define FRAMEBUF_SIZE 10  // not really used; original code had comments for 10 buffered VDIF frames


///////////////////////////////////////////////////////////////////////
// Helpers
///////////////////////////////////////////////////////////////////////

static int start_flag = 0;
static uint64_t last_fpos=0;

/** Helper: show error message and exit */
static void bail(const char *on_what)
{
   if (errno != 0)
      fprintf(stderr, "%s : ", strerror(errno));
   fprintf(stderr, "%s\n", on_what);
   exit(1);
}


/** Realtime config */
// TODO: can add CPU affinity if necessary for more stable 10GbE capture
#if 0
static void init_realtime(int cpu)
{
     // Stick to single CPU core, do not "hop" to different cores
     cpu_set_t mask;
     CPU_ZERO(&mask);
     CPU_SET(cpu,&mask); // stick to CPU number (e.g. any between 0-63)
     sched_setaffinity(0, sizeof(mask), &mask);

     // Keep all data in RAM (no swapping)
     mlockall(MCL_CURRENT | MCL_FUTURE);
}
#endif


/** A lookup table for flipping 2-bit sample sign and magnitude in 8-bit bytes */
static unsigned char m_lut_signmagswap[256];
static void init_lut_signmagswap()
{
    int i;
    unsigned char b;
    for (i=0; i<256; i++)
    {
        // test case
        //   b = 0x1B; // 0b00 01 10 11  so expecting  swp = 0b00 10 01 11 = 0x27 as output
        b = (unsigned char)i;
        unsigned char s0 = b & 0xAA;  // 0xAA = 0b101010
        unsigned char m0 = b & 0x55;  // 0x55 = 0b010101
        unsigned char swp = (s0 >> 1) | (m0 << 1);
        m_lut_signmagswap[b] = swp;
    }
}

unsigned char m_lut_signmagswap_hardcoded[256] =
{
  0x00,  0x02,  0x01,  0x03,  0x08,  0x0A,  0x09,  0x0B,  0x04,  0x06,  0x05,  0x07,  0x0C,  0x0E,  0x0D,  0x0F,
  0x20,  0x22,  0x21,  0x23,  0x28,  0x2A,  0x29,  0x2B,  0x24,  0x26,  0x25,  0x27,  0x2C,  0x2E,  0x2D,  0x2F,
  0x10,  0x12,  0x11,  0x13,  0x18,  0x1A,  0x19,  0x1B,  0x14,  0x16,  0x15,  0x17,  0x1C,  0x1E,  0x1D,  0x1F,
  0x30,  0x32,  0x31,  0x33,  0x38,  0x3A,  0x39,  0x3B,  0x34,  0x36,  0x35,  0x37,  0x3C,  0x3E,  0x3D,  0x3F,
  0x80,  0x82,  0x81,  0x83,  0x88,  0x8A,  0x89,  0x8B,  0x84,  0x86,  0x85,  0x87,  0x8C,  0x8E,  0x8D,  0x8F,
  0xA0,  0xA2,  0xA1,  0xA3,  0xA8,  0xAA,  0xA9,  0xAB,  0xA4,  0xA6,  0xA5,  0xA7,  0xAC,  0xAE,  0xAD,  0xAF,
  0x90,  0x92,  0x91,  0x93,  0x98,  0x9A,  0x99,  0x9B,  0x94,  0x96,  0x95,  0x97,  0x9C,  0x9E,  0x9D,  0x9F,
  0xB0,  0xB2,  0xB1,  0xB3,  0xB8,  0xBA,  0xB9,  0xBB,  0xB4,  0xB6,  0xB5,  0xB7,  0xBC,  0xBE,  0xBD,  0xBF,
  0x40,  0x42,  0x41,  0x43,  0x48,  0x4A,  0x49,  0x4B,  0x44,  0x46,  0x45,  0x47,  0x4C,  0x4E,  0x4D,  0x4F,
  0x60,  0x62,  0x61,  0x63,  0x68,  0x6A,  0x69,  0x6B,  0x64,  0x66,  0x65,  0x67,  0x6C,  0x6E,  0x6D,  0x6F,
  0x50,  0x52,  0x51,  0x53,  0x58,  0x5A,  0x59,  0x5B,  0x54,  0x56,  0x55,  0x57,  0x5C,  0x5E,  0x5D,  0x5F,
  0x70,  0x72,  0x71,  0x73,  0x78,  0x7A,  0x79,  0x7B,  0x74,  0x76,  0x75,  0x77,  0x7C,  0x7E,  0x7D,  0x7F,
  0xC0,  0xC2,  0xC1,  0xC3,  0xC8,  0xCA,  0xC9,  0xCB,  0xC4,  0xC6,  0xC5,  0xC7,  0xCC,  0xCE,  0xCD,  0xCF,
  0xE0,  0xE2,  0xE1,  0xE3,  0xE8,  0xEA,  0xE9,  0xEB,  0xE4,  0xE6,  0xE5,  0xE7,  0xEC,  0xEE,  0xED,  0xEF,
  0xD0,  0xD2,  0xD1,  0xD3,  0xD8,  0xDA,  0xD9,  0xDB,  0xD4,  0xD6,  0xD5,  0xD7,  0xDC,  0xDE,  0xDD,  0xDF,
  0xF0,  0xF2,  0xF1,  0xF3,  0xF8,  0xFA,  0xF9,  0xFB,  0xF4,  0xF6,  0xF5,  0xF7,  0xFC,  0xFE,  0xFD,  0xFF
};


/** For one VDIF frame reverse the 32-bit bit order and correct some VDIF header information */
static void fix_kvn_vdif_frame(unsigned char* buf, const char* ID, const char log2_nchan)
{
    int i;

    /* Step 1: fix header endianness */
    for (i=0; i<VDIF_HEADER_SIZE; i+=4)
    {
        // swap outer 2 bytes
        unsigned char c1 = buf[i+0];
        buf[i+0] = buf[i+3]; buf[i+3] = c1;

        // swap inner 2 bytes
        unsigned char c2 = buf[i+1];
        buf[i+1] = buf[i+2]; buf[i+2] = c2;
    }

    /* Step 2: fix the 'Invalid' bit */
    buf[3] = buf[3] & ~0x80;

    /* Step 3: fix 'Number of channels' in word2? */
#if 0
    // Apparently not necessary, KVN can indeed produce 1-channel data!
    buf[3+2*4] = (buf[3+2*4] & ~0x1F) | (log2_nchan & 0x1F);
#endif

    /* Step 4: insert Station ID in word3 */
    if (ID != NULL)
    {
        buf[0+3*4] = ID[1];
        buf[1+3*4] = ID[0];
    }

    /* Step 5: correct the bit order of payload data */
    /* Step 6: reverse the sign and magnitude bits */
    for (i=VDIF_HEADER_SIZE; i<KVN_VDIF_FRAME_SIZE; i+=4)
    {
        unsigned char b0 = buf[i+0];
        unsigned char b1 = buf[i+1];
        unsigned char b2 = buf[i+2];
        unsigned char b3 = buf[i+3];

        // Note: the VSI-H TVG recording in KVN VDIF format at
        //   /home/oper/vdif/vdiftvg_play_test_1.dat
        // suggests that for KVN VDIF, a full 32-bit flip is needed to be Standard VDIF.
        //
        // On the other hand, the r13340b spectral line scan No0004 in OCTA VDIF and Mark5B
        // recording formats suggests that 8-bit flip, then 2-bit flip, and no change in Endianness
        // is enough for the correction.

        // Flip the bits in all 8-bit bytes
        b0 = byte_bit_swap[b0];
        b1 = byte_bit_swap[b1];
        b2 = byte_bit_swap[b2];
        b3 = byte_bit_swap[b3];

#if ADJUST_SAMPLER_LEVELS
        // Flip 2-bit sample sign/magitude bits
        // VDIF 1.1 page 11: 10. Sample representation
        //    00, 01, 10, 11 = -Mag, -1.0, +1.0, +Mag
        // Mark5B in mark5 memo #019:
        //    00, 01, 10, 11 = same as VDIF, follows Mark4/VLBA encoding
        // But: Mark5access library decoder, mark5_format_mark5b.c definition of lut4level[4]:
        //    00, 01, 10, 11 = -Mag, +1.0, -1.0, +Mag
        b0 = m_lut_signmagswap_hardcoded[b0];
        b1 = m_lut_signmagswap_hardcoded[b1];
        b2 = m_lut_signmagswap_hardcoded[b2];
        b3 = m_lut_signmagswap_hardcoded[b3];
#endif

        // Store
        buf[i+0] = b0;
        buf[i+1] = b1;
        buf[i+2] = b2;
        buf[i+3] = b3;
    }

    return;
}


/** Date-time into seconds */
uint64_t datetime2secs(uint64_t year, uint64_t day, uint64_t hh, uint64_t mm, uint64_t ss)
{
   uint64_t t0 = (((365UL*year + day)*24UL + hh)*60UL + mm)*60UL + ss;
   return t0;
}

/** Seconds into date-time */
void secs2datetime(uint64_t t, uint16_t* year, uint16_t* day, uint8_t* hh, uint8_t* mm, uint8_t* ss)
{
   uint64_t r;

   r = t % 60UL;
   t = t - r;
   *ss = (uint8_t)r;

   r = t % (60UL*60UL);
   t = t - r;
   *mm = (uint8_t)(r / 60UL);

   r = t % (24UL*60UL*60UL);
   t = t - r;
   *hh = (uint8_t)(r / (60UL*60UL));

   r = t % (365UL*24UL*60UL*60UL);
   t = t - r;
   *day = (uint16_t)(r / (24UL*60UL*60UL));
   
   *year = (uint16_t)(t / (365UL*24UL*60UL*60UL));   

   return;
}


///////////////////////////////////////////////////////////////////////
// MAIN
///////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
   int m, n;

   threadarg_t *arg_tx;
   threadarg_t *arg_rx;
   int fo;

   char *srvr_port = "60000";
   struct sockaddr_in adr_srvr;
   struct sockaddr_in adr_ctl;
   struct sockaddr_in adr_clnt;

   vdif_cp_vr_t vfcp_vr;
   vdif_frame_t frame                 __attribute__ ((aligned (16)));
   vdif_frame_t frames[FRAMEBUF_SIZE] __attribute__ ((aligned (16)));

   int s;
   uint64_t t64 = 0;
   uint16_t year = 2013;
   uint16_t day =343;
   uint8_t hh = 6;
   uint8_t mm = 46; 
   uint8_t ss = 30;
   uint16_t start_year;
   uint16_t start_day;
   uint8_t start_hh;
   uint8_t start_mm; 
   uint8_t start_ss;
   uint16_t stop_year;
   uint16_t stop_day;
   uint8_t stop_hh;
   uint8_t stop_mm; 
   uint8_t stop_ss;
   uint32_t PSN;

   unsigned char sec_count;
   unsigned int  sec_wait=0;
   unsigned int  recrate;
   unsigned int  playrate;
   unsigned int  frames_per_second;


   /* Init */
   memset(&adr_srvr, 0, sizeof(adr_srvr));
   memset(&adr_ctl,  0, sizeof(adr_ctl));
   memset(&adr_clnt, 0, sizeof(adr_clnt));
   memset(&frame,    0, sizeof(vdif_frame_t));
   memset(&vfcp_vr,  0, sizeof(vdif_cp_vr_t));
   init_lut_signmagswap();


   /* Report runtime mode and other infos */
   fprintf(stderr, "\n-- %s version %s -----\n", PROGRAM_NAME, VERSION_STR);
#if ADJUST_SAMPLER_LEVELS
   fprintf(stderr, "Compiled with 2-bit sample encoding conversion enabled (ADJUST_SAMPLER_LEVELS=1)\n");
#endif
#if FIX_KVN_VDIF_ISSUES
   fprintf(stderr, "Compiled with correction of 'KVN VDIF' into proper VDIF format (FIX_KVN_VDIF_ISSUES=1)\n");
#endif


   /* Command Line Parameters */
   if(argc != 16) {
     fprintf(stderr, "\n"
             "Usage: $ %s <IPaddr> <Port#> <Outfile[VDIF format]> <rec.rate#> <play.rate#>\n"
             "                   <start time:(yyyy ddd hh mm ss)> <finish time:(yyyy ddd hh mm ss)>\n"
             "     ---> rec.rate#  : 2048 or 1024\n"
             "     ---> play.rate# : 1[normal]], 2[half], 4[quad], 8[1/8], 16[1/16]\n\n",
             argv[0]
     ); 
     exit(1);
   }

   // Numerical command line parameters
   recrate = (unsigned int)atoi(argv[4]);
   playrate = (unsigned int)atoi(argv[5]);
   start_year = (uint16_t)atoi(argv[6]);
   start_day  = (uint16_t)atoi(argv[7]);
   start_hh   = (uint8_t)atoi(argv[8]);
   start_mm   = (uint8_t)atoi(argv[9]);
   start_ss   = (uint8_t)atoi(argv[10]);
   stop_year = (uint16_t)atoi(argv[11]);
   stop_day  = (uint16_t)atoi(argv[12]);
   stop_hh   = (uint8_t)atoi(argv[13]);
   stop_mm   = (uint8_t)atoi(argv[14]);
   stop_ss   = (uint8_t)atoi(argv[15]);

   // Remote IP and port
   printf("Data source IP address and port : %s:%s\n", argv[1],argv[2]);
   srvr_port = argv[2];


   /* Open the "block file" */
   int f_opts = O_CREAT | O_TRUNC | O_WRONLY;
   f_opts    |= O_LARGEFILE;
   // f_opts |= O_DIRECT;
   // f_opts |= O_SYNC;
   fprintf(stderr, "Opening '%s' for data capture...\n", argv[3]);
   fo = open(argv[3], f_opts, 0644);
   if (fo < 0) {
      bail("Error opening output file");
   }


   /* Create "thread" arguments (JanW: original code says "thread" but uses no multithreading) */
   arg_tx = (threadarg_t *)malloc(sizeof(threadarg_t));
   if (arg_tx == NULL) { bail("malloc(2)"); }
   arg_rx = (threadarg_t *)malloc(sizeof(threadarg_t));
   if (arg_rx == NULL) { bail("malloc(2)"); }

   // Network config
   arg_tx->sd = socket(PF_INET, SOCK_DGRAM, 0);
   if (arg_tx->sd == -1) bail("socket()");
   arg_rx->sd = socket(PF_INET, SOCK_DGRAM, 0);
   if (arg_rx->sd == -1) bail("socket()");

   // "Server" at IP:port+0
   // adr_srvr.sin_addr.s_addr = inet_addr("127.0.0.1");
   adr_srvr.sin_addr.s_addr = inet_addr(argv[1]);
   if (adr_srvr.sin_addr.s_addr == INADDR_NONE) { 
      bail("Bad server IP address."); 
   }
   adr_srvr.sin_family = AF_INET;
   adr_srvr.sin_port = htons(atoi(srvr_port));

   // "Control" (TX) at IP:port+1
   // adr_ctl.sin_addr.s_addr  = inet_addr("127.0.0.1");
   adr_ctl.sin_addr.s_addr = inet_addr(argv[1]);
   if (adr_ctl.sin_addr.s_addr == INADDR_NONE) { 
      bail("Bad controller IP address."); 
   }
   adr_ctl.sin_family = AF_INET;
   adr_ctl.sin_port = htons(atoi(srvr_port) + 1);

   // "Client" (RX) i.e. this program
   adr_clnt.sin_addr.s_addr = htonl(INADDR_ANY);
   adr_clnt.sin_family = AF_INET;
   adr_clnt.sin_port = htons(atoi(srvr_port));

   // "Client" (RX) socket UDP receive timeout
   timeout.tv_sec = 1;
   timeout.tv_usec = 0;
   z = setsockopt(arg_rx->sd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));
   if (z == -1) { bail("setsocketopt(SO_RCVTIMEO)"); }

   s = 1;
   setsockopt(arg_rx->sd, SOL_SOCKET, SO_REUSEADDR, &s, sizeof(s));
   s = 2684354560; // or: s = atoi( read_proc_value("/proc/sys/net/core/rmem_max"));
   setsockopt(arg_rx->sd, SOL_SOCKET, SO_RCVBUF, &s, sizeof(s));
   s = 2684354560; // or: s =  atoi( read_proc_value("/proc/sys/net/core/wmem_max"));
   setsockopt(arg_rx->sd, SOL_SOCKET, SO_SNDBUF, &s, sizeof(s));

   // Start listening for incoming UDP
   z = bind(arg_rx->sd, (struct sockaddr *)&adr_clnt, sizeof(adr_clnt));
   if (z == -1) { bail("bind(2) to incoming UDP port"); }


   /* START timestamp */
   time(&etd);
   etm = *localtime(&etd);
   strftime(etstr, sizeof(etstr), tfmt, &etm);
   printf("Ready to SEND(UDP) START at %s.\n", etstr);

   // JanW: original code requests a stream start with 10 seconds earlier data, 
   // not sure why, especially given that DIO VSI --> 10GbE OCTA time-sync anyway introduces
   // a loss of VLBI data of 4-5 seconds during hardware "sync training".
   t64 = datetime2secs(start_year, start_day, start_hh, start_mm, start_ss);
   secs2datetime(t64 - 10, &year, &day, &hh, &mm, &ss);
   fprintf(stderr, "User-requested start         : %04u-%03u %02u:%02u:%02u\n", start_year, start_day, start_hh, start_mm, start_ss);
   fprintf(stderr, "Actual start, shifted by -10s: %04u-%03u %02u:%02u:%02u\n", year, day, hh, mm, ss);

   // "Press any key to continue" ?
   getchar();


   /* Configure "VSI Time" report (VR) */
   PSN = 0;
   vfcp_vr.ver_kind = 0x13; // VDIFCP version(0x1), VR(0x3)
   vfcp_vr.port = 0x10;     // VSI port(1 - 4), reserved
   vfcp_vr.plen = 0x1c00;   // VR packet length(default 28byte), need swap  
   //vfcp_vr.plen = 0x5000; // VR packet length(default 80byte)
   vfcp_vr.seqno = 0;       // packet sequence number

   // Time code: 'yyyy' 'ddd' 'hh' 'mm' 'ss' in BCD format
   vfcp_vr.mode_timeupper = 0x20 | ((year / 1000) & 0x0f); // VSI mode 1, year(2013=>2)
   vfcp_vr.time[0] = ((((year / 100) % 10) << 4) & 0xf0) | (((year / 10) % 10) & 0x0f);
   vfcp_vr.time[1] = (((year % 10) << 4) & 0xf0) | ((day / 100) & 0x000f);
   vfcp_vr.time[2] = ((((day / 10) % 10) << 4) & 0xf0) | ((day % 10) & 0x0f);
   vfcp_vr.time[3] = (((hh / 10) << 4) & 0xf0) | ((hh % 10) & 0x0f);
   vfcp_vr.time[4] = (((mm / 10) << 4) & 0xf0) | ((mm % 10) & 0x0f);
   vfcp_vr.time[5] = (((ss / 10) << 4) & 0xf0) | ((ss % 10) & 0x0f);

   // Expected frame rate
   // assuming KVN VDIF payload of 1280 bytes, a rate of 200000 frames/s means 1280*200000*8bit = 2048e9 bit/s = 2048 Mbps
   frames_per_second = 12500 * (recrate/128);
   if(recrate == 2048){
      fprintf(stderr, "#1234,%04u%03u%02u%02u%02u,1024,32,2,ACQ,PDATAVP1,PDATAVP2;\n", year, day, hh, mm, ss); 
   } else if(recrate == 1024){
      fprintf(stderr, "#1234,%04u%03u%02u%02u%02u,512,32,2,ACQ,PDATAVP1,PDATAVP2;.\n", year, day, hh, mm, ss); 
   }


   /* Wait for 1.0 seconds (JanW: not sure why! nothing has been sent to OCTA yet!) */
   scl = clock();
   do {
      ecl = clock();
      runtime = (ecl - scl)/CLOCKS_PER_SEC;
   } while (runtime < 1.0);
   gettimeofday(&t_start, NULL);
   gettimeofday(&t_ctlstart, NULL);
   sec_count = 0;


   /* Receive loop -- receive data in 1.0s pieces? */
   while (1) {

      if(year >= stop_year && day >= stop_day && hh >= stop_hh && mm >= stop_mm && ss >= stop_ss)
         break;

      // UTC in VSI Time report, sent to control port (default port: 60001)
      z = sendto(arg_tx->sd, &vfcp_vr, 28, 0, (struct sockaddr *)&adr_ctl, sizeof(adr_ctl));

      // JanW: CRITICAL?
      // Should we wait here until UDP/IP packet reaches the VDICP "server" and has been processed?
      // Should we also wait for a "server" OK or other reply?
      // How to be sure the UDP/IP has not been lost?

      if(start_flag == 1) {

         last_fpos = lseek64(fo, 0, SEEK_CUR);
         // fprintf(stderr, "At file offset: %lu\n", last_fpos);

         for (m=0; m<frames_per_second; m++) {

            // Receive new VDIF frame
            z = recvfrom(arg_rx->sd, (void*)&frame, KVN_VDIF_FRAME_SIZE, MSG_NOSIGNAL, NULL, NULL);
            if (z < 0) {

               fprintf(stderr, "UDP receive error!\n");
               if (sec_wait == 0) {
                  sec_wait = 1;
                  lseek64(fo, last_fpos, SEEK_SET);
                  fprintf(stderr, "Current frame: %d.  Returning back to file pos of previous second: %lu\n", m, last_fpos);

                  // JanW: original code tries to jump back 2 seconds
                  t64 = datetime2secs(year, day, hh, mm, ss);
                  secs2datetime(t64 - 2, &year, &day, &hh, &mm, &ss);
               } else {
                  sec_wait = 0;
               }

               break;
            }

            /* Throw away data? */
            // JanW: original code discards the first second after 2-second jump-back at UDP receive error
            if (sec_wait == 1) {
               if(m == (frames_per_second - 1)) { 
                  sec_wait = 0;
               }
            } else {

            /* Record the received data */

#if FIX_KVN_VDIF_ISSUES
               /* Apply corrections to produce standard VDIF */
               fix_kvn_vdif_frame((unsigned char*)&frame, NULL, 0);
#endif

               /* Debug */
               // TODO: could check if really  m == VDIF frame nr
               if (m == 0) {
                  fprintf(stderr, "Assumed frame#0 : actual frame#=%05u, second=%6u\n", 
                      (frame.hdr.epoch_dframe_no & 0x00FFFFFF), (frame.hdr.invalid_sec_epoch & 0x3FFFFFFF)
                  );
               }

               /* Write VDIF frame to file */
               write(fo, (void*)&frame, KVN_VDIF_FRAME_SIZE);

            }
         }
      } else { // if(start_flag == 1)
         if (year >= start_year && day >= start_day && hh >= start_hh && mm >= start_mm && ss >= start_ss) {
            start_flag = 1;
         } else {
            usleep(1000000);
         }
      }

      // JanW: original code waited here, with "do {} while (t_ctldiff.tv_sec < playrate);"
      //       so it would wait 2s for 2048 Mbps, and 1s for 1024 Mbps.
      // Not sure what the wait is for, so I deleted it. The *sending* side already
      // dictates the data rate, so it is pointless to do a buzy-sleep here?


      // Time increment (original code)
      // Time *should* be read from VDIF headers (future TODO)
      sec_count++;
       ss++;
       if( ss >= 60 ){
          ss = 0;
          mm++;
          if( mm >= 60){
             mm = 0;
             hh++;
             if( hh >= 24){
                hh = 0;
                day++;
                if(day >= 366){
                   day = 1;
                   year++;
                }
             }
          }
       }

      /* Update the VSI Time with next BCD-encoded UTC time */
      PSN++;
      vfcp_vr.seqno = htonl(PSN);
      // vfcp_vr.mode_timeupper = 0x10 | ((year / 1000) & 0x0f); // VSI mode 1, year(2013=>2)
      // vfcp_vr.time[0] = ((((year / 100) % 10) << 4) & 0xf0) | (((year / 10) % 10) & 0x0f);
      vfcp_vr.time[1] = (((year % 10) << 4) & 0xf0) | ((day / 100) & 0x000f);
      vfcp_vr.time[2] = ((((day / 10) % 10) << 4) & 0xf0) | ((day % 10) & 0x0f);
      vfcp_vr.time[3] = (((hh / 10) << 4) & 0xf0) | ((hh % 10) & 0x0f);
      vfcp_vr.time[4] = (((mm / 10) << 4) & 0xf0) | ((mm % 10) & 0x0f);
      vfcp_vr.time[5] = (((ss / 10) << 4) & 0xf0) | ((ss % 10) & 0x0f);

      if (recrate == 2048) {
         fprintf(stderr, "#1234,%04u%03u%02u%02u%02u,1024,32,2,ACQ,PDATAVP1,PDATAVP2;\n", year, day, hh, mm, ss); 
      }
      else if (recrate == 1024) {
         fprintf(stderr, "#1234,%04u%03u%02u%02u%02u,512,32,2,ACQ,PDATAVP1,PDATAVP2;.\n", year, day, hh, mm, ss); 
      }


   }

   /* START timestamp */
   time(&etd);
   etm = *localtime(&etd);
   strftime(etstr, sizeof(etstr), tfmt, &etm);
   printf("SEND(UDP) END at %s.\n", etstr);

   close(arg_tx->fd);
   close(arg_rx->fd);
   close(fo);

   free(arg_tx);
   free(arg_rx);
   return 0;
}
