#!/bin/bash

MK5C_PORT=14242
MK5C_IP_1=203.250.155.72   # attached to VOA
MK5C_IP_2=203.250.155.122  # attached to FILA10G

set -m  # enable 'fg', 'bg'

# unique name for scan, based on current date/time
SCANLABEL=`date -u +%Yy%jd%Hh%Mm%Ss`
CMD_REC_1="record=on:30:15:$SCANLABEL:n14st02c:voa;"
CMD_REC_2="record=on:30:15:$SCANLABEL:n14st02c:fila10;"

# initialize the systems

CMDS_INIT_1=(
   "mod_init?;"
   "group=open:12;"
   "mstat?12;"
   "input_stream=add:voa:vdif:1312:42:0:eth2:192.168.1.1:60000;"
   "input_stream=commit;"
)
CMDS_INIT_2=(
   "mod_init?;"
   "group=open:12;"
   "mstat?12;"
   "input_stream=add:fila:vdif:1312:50:0:eth2:10.10.1.20:46227;"
   "input_stream=commit;"
)

for cmd in ${CMDS_INIT_1[@]}; do
	echo "Mark5C system $MK5C_IP_1:$MK5C_PORT : $cmd"
	echo $cmd | netcat -q 1 $MK5C_IP_1 $MK5C_PORT
done

for cmd in ${CMDS_INIT_2[@]}; do
	echo "Mark5C system $MK5C_IP_2:$MK5C_PORT : $cmd"
	echo $cmd | netcat -q 1 $MK5C_IP_2 $MK5C_PORT
done


# start recording at about the same time

echo "Record cmd to $MK5C_IP_1:$MK5C_PORT and $MK5C_IP_2:$MK5C_PORT  : $CMD_REC"
echo $CMD_REC_1 | netcat -q 1 $MK5C_IP_1 $MK5C_PORT   
echo $CMD_REC_2 | netcat -q 1 $MK5C_IP_2 $MK5C_PORT 

fg

echo "record?" | netcat -q 1 $MK5C_IP_1 $MK5C_PORT
echo "record?" | netcat -q 1 $MK5C_IP_2 $MK5C_PORT
sleep 5

echo "record=off" | netcat -q 1 $MK5C_IP_1 $MK5C_PORT
echo "record=off" | netcat -q 1 $MK5C_IP_2 $MK5C_PORT


