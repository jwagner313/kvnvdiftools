#!/usr/bin/perl
#
# Usage:
#    timeconvVDIF2MJD.pl <refepoch> <second>
#      to convert the given VDIF timestamp information into mdj hh:mm:ss
#    timeconvVDIF2MJD.pl <mjd> <hh> <mm> <ss>
#      to convert the given MJD timestamp information into VDIF refepoch:second

# Note on VDIF reference epochs:
#  0:00 UT 01 Jan 2000 + <ref ep nr>*6 Months
#
#  i.e.  <ref ep nr> of 0x00 = 0:00 UT 01 Jan 2000 = 51544
#                       0x01 = 0:00 UT 01 Jul 2000
#                       0x02 = 0:00 UT 01 Jan 2001
#                       ...
#  and 01/01/2000 is MJD 51544
#  but because of leap years
#    http://www.csgnetwork.com/julianmodifdateconv.html
#  base epoch nr 0x00 = 0:00 UT 01 Jan 2000 = 51544 = base +  0 months (1st January 2000)
#  next epoch nr 0x01 = 0:00 UT 01 Jul 2000 = 51726 = base +  6 months (1st July    2000)
#  next epoch nr 0x02 = 0:00 UT 01 Jan 2001 = 51910 = base + 12 months (1st January 2001)
#  ...
#
#    Matlab code, maybe also Octave, to generate the below table
#    yy = 2000:2050; mm = [1 7];
#    for y=yy, for m=mm, fprintf(1, '%d ', mjuliandate(y,m,1)), end, end;

my @arrVDIFrefMJDs = qw(
    51544 51726 51910 52091 52275 52456 52640 52821 53005 53187 53371 53552 53736
    53917 54101 54282 54466 54648 54832 55013 55197 55378 55562 55743 55927 56109
    56293 56474 56658 56839 57023 57204 57388 57570 57754 57935 58119 58300 58484
    58665 58849 59031 59215 59396 59580 59761 59945 60126 60310 60492 60676 60857
    61041 61222 61406 61587 61771 61953 62137 62318 62502 62683 62867 63048 63232
    63414 63598 63779 63963 64144 64328 64509 64693 64875 65059 65240 65424 65605
    65789 65970 66154 66336 66520 66701 66885 67066 67250 67431 67615 67797 67981
    68162 68346 68527 68711 68892 69076 69258 69442 69623 69807 69988 
);

if ($#ARGV == 1)
{
   $refep = $ARGV[0];
   $sec   = $ARGV[1];
   $day   = int($sec / (24*60*60));
   $rem   = $sec - $day*24*60*60;
   $hour  = int($rem / (60*60));
   $rem   = $rem - $hour*60*60;
   $min   = int($rem / 60);
   $rem   = $rem - $min*60;
   $tsec  = $rem;

   $MJD   = $arrVDIFrefMJDs[$refep] + $day;

   $yearsMJD = $arrVDIFrefMJDs[2*int($refep/2)];  # refep 0,2,4,.. are first day of year (01/01/20xx)
   $doy = $MJD - $yearsMJD + 1;

   printf "VDIF ep %u sec %u  =>  MJD %6u %02u:%02u:%02u  doy %u\n", $refep,$sec,$MJD,$hour,$min,$tsec,$doy;
}
elsif ($#ARGV == 3)
{
   $MJD  = $ARGV[0];
   $hour = $ARGV[1];
   $min  = $ARGV[2];
   $tsec = $ARGV[3];

   $vsec = 0;
   $refep = 0;

   # find largest ref.epoch MJD that is smaller than the given MJD
   for my $ii (0 .. $#arrVDIFrefMJDs) 
   {
      if ($arrVDIFrefMJDs[$ii] < $MJD) {
         $refep = $ii;
      }
   }
   $day = $MJD - $arrVDIFrefMJDs[$refep];
   $vsec = 24*60*60*$day + 60*60*$hour + 60*$min + $tsec;

   printf "MJD %6u %02u:%02u:%02u  =>  VDIF ep %u sec %u\n", $MJD,$hour,$min,$tsec,$refep,$vsec;
}
else
{
   print "Usage:\n";
   print "  timeconvVDIF2MJD.pl <refepoch> <second>\n";
   print "    or\n";
   print "  timeconvVDIF2MJD.pl <mjd> <hh> <mm> <ss>\n";
}
