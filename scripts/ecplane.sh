#!/bin/bash
# ecplane
# Do a proper end and shutdown of c-plane
# Revised:  2012 Jul 26, CAR
#           2014 Dec 08, JW
#
name="cplane"

echo $name
array=( $(ps auxw | grep -E -v 'grep|ecplane|$name:t' | grep $name) )
echo ${array[@]}
if [[  ${#array[*]} -gt 1 &&  ${array[1]} -gt 0 ]]; then
  echo "$0:t sending INTERRUPT to $name ${array[1]} "
  kill -HUP ${array[1]}
else
  echo "${0:t} ERROR:  Can't find program $name "
fi

exit $?

