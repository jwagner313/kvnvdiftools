#!/usr/bin/python
"""
VEX File Cleaner for DiFX correlation   v1.0  Jan Wagner  20151006

Usage: clean_vex_for_difx.py <original.vex>

Produces a copy of the input VEX file that has been cleaned up so that
it can be used directly (hopefully...) to start a correlation in DiFX.

Comments out typically wrong but unimportant lines and VERA dual-beam 
related non-standard VEX lines. Merges "fake" modes such as modes k.fas, 
k.fas.tsys, k.fas-f into a single mode such as k.fas.
"""

import re
import sys

N_copied = 0
N_commented = 0
N_reformatted = 0

if len(sys.argv) != 2:
	print (__doc__)
	sys.exit(-1)

vex  = sys.argv[1]
fin  = open(vex, "rt")
fout = open(vex+".difx", "wt")

print ('Copying and cleaning VEX file %s into new file %s.difx ...' % (vex,vex))

# Make list of lines to delete:
# 1) lines that may have an unpexpected format for DiFX
difxvex_del_keys = ['preob_cal','midob_cal','postob_cal','antenna_diam','antenna_name']
# 2) lines that sometimes lack quotation marks (PI_name = John Doe;   instead of properly PI_name = "John Doe";)
difxvex_del_keys += ['PI_name', 'contact_name', 'scheduler_name', 'exper_description']
# 3) legacy tape related things
difxvex_del_keys += ['parity_check', 'setup_always', 'new_source_command', 'tape_prepass', 'new_tape_setup', 'tape_change', 'tape_length', 'tape_motion']
# 4) VERA dual-beam related things. Special correlation passes are needed for this anyway!
difxvex_del_keys += ['beam1', 'beam2', 'pointing_sector', 'multi_beam_number', 'multi_beam_type', 'beam_separation', 'field_rotation', 'source2=']

# Process the VEX file
for line in fin:

	# Remove (comment out) some special lines
	remove = False
	for key in difxvex_del_keys:
		if key in line:
			remove = True
			break

	# Use same mode=k.fas for mode=k.fas.tsys, mode=k.fas-f, mode=k.fas-l, and similar aliases
	m = re.search("mode=[a-zA-Z]*.fas", line)
	if m != None:
		newmode = m.group(0) + ";"
		line = re.sub("mode=[a-zA-Z]*.fas[\.\-a-zA-Z]*;", newmode, line)
		N_reformatted += 1
		remove = False
	else:
		# Even more generic mode=<string>[-.]<string>[-.]<rest> replacement with just  mode=<string>[-.]<string>
		m = re.search("mode=[a-zA-Z]*[\.\-][a-zA-Z]*", line)
		if m != None:
			newmode = m.group(0) + ";"
			line = re.sub("mode=[a-zA-Z]*[\.\-a-zA-Z0-9]*;", newmode, line)
			N_reformatted += 1
			remove = False

	# Avoid commenting out lines that are *already* commented out
	linetr = line.strip()
	if len(linetr)>0 and linetr[0]=='*':
		remove = False

	# Write output file
	if remove:
		fout.write("* " + line)
		N_commented += 1
	else:
		fout.write(line)
		N_copied += 1

fin.close()
fout.close()
print ('Done (%d lines copied, %d reformatted, %d commented out).' % (N_copied,N_reformatted,N_commented))

