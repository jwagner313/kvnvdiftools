#!/bin/python
"""
Usage: clean_antab.py [input files]

Takes multiple KVN ANTAB files (named, e.g., s16tj02aKTN.ANTAB.1
and similar) and changes them into AIPS format with station names
as used in DiFX correlation.
"""
import numpy, re, sys

cutoff = 30 # filter out values >30*Median.Abs.Dev away from median

def mad(data, axis=None):
    """ Median absolute deviation"""
    M = numpy.median(data, axis)
    dM = numpy.absolute(data-M)
    return numpy.median(dM, axis)

def readANTABfiltered(fnin):
    """Read an ANTAB file and try to comment out outlier Tsys lines.
       Assumes the file contains only one station."""
    fi = open(fnin,'r')
    txt = fi.readlines()
    fi.close()

    Nifs = 0
    data = []
    linenrs = []

    # Check for and parse strings like '099 23:40:06 143.206 176.513 244.663 254.405'
    mre = re.compile(r"[-+]?\d+(\s)?\d+\:?\d+\:?\d+(\s)")
    for i in range(len(txt)):
        l = txt[i]
        m = mre.match(l)
        if m == None:
           continue
        if l[0] == '!':
           print l
        r = mre.sub('', l)
        x = [float(v) for v in r.split()]
        Nifs = len(x)
        data.append(x)
        linenrs.append(i)

    # Filter
    Nfiltered = 0
    data = numpy.array(data)
    M = mad(data,axis=0)
    dd = numpy.divide(numpy.absolute(data - numpy.median(data,axis=0)), M)
    for i in range(len(dd)):
        if any(dd[i] > cutoff):
            txt[linenrs[i]] = '! ' + txt[linenrs[i]]
            Nfiltered += 1
            #print txt[linenrs[i]], dd[i]

    if (Nfiltered > 0):
        print ('Commented out %d lines with values deviating by >%d MAD from median' % (Nfiltered,cutoff))
    return txt

def generateANTAB(fnout, fnin_list):
    """Merge KVN ANTAB files into a single file, correction the station names 
       at the same time. Possibly incorrect frequency labels are not corrected here!"""
    Nlines = 0
    fo = open(fnout,'w')
    for fnin in fnin_list:
        print ('Adding %s' % fnin)
        txt = readANTABfiltered(fnin)
        for line in txt:
            line = line.replace('KVNTAMNA','KT')
            line = line.replace('KVNYONSEI','KY')
            line = line.replace('KVNULSAN','KU')
            fo.write(line)
        Nlines += len(txt)
    fo.close()
    print ('Wrote %d lines into %s' % (Nlines,fnout))

if len(sys.argv)<2:
    print (__doc__)
    sys.exit(1)

fnout = 'new.antab'
generateANTAB(fnout, sys.argv[1:])
