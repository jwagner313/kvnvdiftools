/***************************************************************************
 *   Copyright (C) 2008-2011 by Walter Brisken & Chris Phillips            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
//===========================================================================
// SVN properties (DO NOT CHANGE)
//
// $Id: m5decimfir.c 6687 2015-06-03 02:14:10Z JanWagner $
// $HeadURL: https://svn.atnf.csiro.au/difx/libraries/mark5access/trunk/mark5access/mark5_stream.c $
// $LastChangedRevision: 6687 $
// $Author: JanWagner $
// $LastChangedDate: 2015-06-03 11:14:10 +0900 (Wed, 03 Jun 2015) $
//
//============================================================================

// Test:
// m5decimfir /lustre/jwagner/strippe/st/s14db02c_KVNYS_No0006 Mark5B-2048-1-2 tmp.vdif 1 nofile.coeff

// TODO:
// - ensure continuity accross buffers, i.e., if sample rate reduction R < Ncoeff we should
//   retain some of the trailing samples to put them into the start of the most recent buffer

#include "../mark5access/mark5_stream.h"
#define _FILE_OFFSET_BITS 64
#define OUTPUT_BITS       32   // output VDIF file bit depth: 2 (VDIF enc.), 8 (linear enc.), or 32 (float)
#define MAX_COEFFS      8192   // maximum nr of FIR coeffs in file

#include <assert.h>
#include <fcntl.h>
#include <malloc.h>
#include <math.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>

#define REPORT_INTERVAL 10000

const char program[] = "m5decimfir";
const char author[]  = "Jan Wagner";
const char version[] = "1.1.2";
const char verdate[] = "20160509";

typedef struct VDIFEncapsulator_tt {
	int fd;
	char* fmt;
	uint32_t hdr[8];
	size_t payloadbytes;
	uint32_t framenr;
	uint32_t framesec;
	uint32_t refepoch;
	double fps;
	int writepos;
} VDIFEncapsulator_t;

int vdifencap_open(VDIFEncapsulator_t* t, int rate_Mbps);
int vdifencap_updateheader(VDIFEncapsulator_t* t);
int vdifencap_copyheader(const char* fn, VDIFEncapsulator_t* t);
int vdifencap_write(VDIFEncapsulator_t* t, const char* data, size_t len);

int coefffile_load(const char* fn, float** coeff, int* Ncoeff);

int die = 0;

typedef void (*sighandler_t)(int);

sighandler_t oldsiginthand;

void siginthand(int j)
{
	printf("\nBeing killed.  Partial results will be saved.\n\n");
	die = 1;

	signal(SIGINT, oldsiginthand);
}

static void usage()
{
	printf("\n");

	printf("%s ver. %s   %s  %s\n\n", program, version, author, verdate);
	printf("A decimating and frequency-translating FIR filter for Mark5 time domain data.\n");
	printf("The data from one recorded channel are upsampled by factor 1:2 (to allow the\n");
	printf("frequency xlation to use a real-valued LO), filtered by a lowpass FIR to remove\n");
	printf("the spectral image from upsampling, and filtered by the user filter, then\n");
	printf("decimated. User coefficients should be the convolution of lowpass FIR and\n");
	printf("actual user FIR coefficients.\n");
	printf("Can use VLBA, Mark3/4, and Mark5B formats using the\nmark5access library.\n\n");
	printf("Usage : %s <infile> <dataformat> <outfile> <if_nr> <coeff_file.txt> [<offset>]\n\n", program);
	printf("  <infile> is the name of the input file\n\n");
	printf("  <dataformat> should be of the form: "
		"<FORMAT>-<Mbps>-<nchan>-<nbit>, e.g.:\n");
	printf("    VLBA1_2-256-8-2\n");
	printf("    MKIV1_4-128-2-1\n");
	printf("    Mark5B-512-16-2\n");
	printf("    VDIF_1000-64-1-2 (here 1000 is payload size in bytes)\n\n");
	printf("  <outfile> is the output VDIF file for the extracted subband\n\n");
	printf("  <if_nr> is the IF to process (1 for the first recorded IF)\n\n");
	printf("  <coeff_file> is the text file with FIR coeffients\n\n");
	printf("  <offset> is number of bytes into file to start decoding\n\n");
	printf("\n");
}

float stddev(const float* v, const size_t N)
{
	float sum = 0.0f, sum2 = 0.0f;
	size_t i;
	for (i = 0; i < N; i++)
	{
		sum += v[i];
		sum2 += v[i]*v[i];
	}
	sum /= N;
	sum2 /= N;
	return sqrtf(sum2 - sum*sum);
}

int filterRealData(const char* infile, struct mark5_stream *ms, const int fdout, const int if_nr, const float* coeffs, const int Ncoeff)
{
	double bw_in, bw_out;
	int workbuf_len, Lout;
	int i, j, k, rc;
	int R, m;

	size_t niter = 0;
	struct timeval t1, t2;

	VDIFEncapsulator_t vdif;
	const int nbit_out = OUTPUT_BITS;
	int mjd, sec;
	double nsec;

	float **raw, *data, *lo, *out;
	float sigmainv = 0.0f, sigma = 0.0f;
	signed char *out_integer;

	int fd_dbg = open("trace.bin", O_CREAT|O_TRUNC|O_WRONLY, S_IWUSR|S_IRUSR|S_IRGRP|S_IROTH);

	// Derived
	workbuf_len = ((int)(32768 / Ncoeff)) * Ncoeff;
	bw_in = ms->samprate * 0.5e-6;
	bw_out = bw_in / Ncoeff;
	R = Ncoeff;

	// Allocate
	raw = (float **)malloc(ms->nchan*sizeof(float *));
	for (i = 0; i < ms->nchan; ++i)
	{
		raw[i] = memalign(4096, sizeof(float)*workbuf_len);
	}
	data = memalign(4096, sizeof(float)*(workbuf_len+Ncoeff+1)*2);
	lo = memalign(4096, sizeof(float)*(workbuf_len+Ncoeff+1)*2);
	out = memalign(4096, sizeof(float)*workbuf_len);
	out_integer = memalign(4096, 4*workbuf_len);
	memset(data, 0x00, sizeof(float)*(workbuf_len+Ncoeff+1)*2);

	// Make sure we start at an integer second in the input file
	niter = 0;
	while (1)
	{
		mark5_stream_get_sample_time(ms, &mjd, &sec, &nsec);
		if (nsec == 0)
		{
			break;
		}
		rc = mark5_stream_decode(ms, 1, raw);
		if (rc < 0)
		{
			return -1;
		}
		niter++;
	}
	printf("Input file: first integer second (MJD %d sec %d) found after %zd samples.\n", mjd, sec, niter);

//	int vdif_ep = 29, vdif_sec = sec;
//	convert_MJDsec_to_VDIFepSec(mjd, sec, &vdif_ep, &vdif_sec);

	// Output format
	vdifencap_open(&vdif, 2*bw_out*nbit_out);
	vdif.fd = fdout;
	vdif.framesec = sec; // TODO: MJD -> refepoch + second
	vdifencap_updateheader(&vdif);
	printf("Output format: VDIF_%zu-%.0f-1-%d with %.1f frames/s\n", vdif.payloadbytes, 2*bw_out*nbit_out, nbit_out, vdif.fps);

	// Frequency translation (integer period(s) must fit into work buffer!)
	m = workbuf_len/6; // LO freq is m/L  (0<m<L/2; L=workbuf_len)
	for (i = 0; i < workbuf_len; i++)
	{
		lo[i] = (float) cos(2*M_PI*((double)i*(double)m)/((double)workbuf_len));
	}
	//write(fd_dbg, lo, workbuf_len*sizeof(float));

	// Report
	printf("Input bandwidth %.2f MHz, output bandwidth %.2f MHz, R=%d, Ncoeff=%d, N=%d, m=%d\n",
		bw_in, bw_out, R, Ncoeff, workbuf_len, m);

	// Process slices of input data
	niter = 0;
	gettimeofday(&t1, NULL);
	while (!die)
	{
		// Read a block of data
		rc = mark5_stream_decode(ms, workbuf_len, raw);
		if(rc < 0)
		{
			break;
		}

		// Frequency translate and upsample 1:2
		for (i = 0; i < workbuf_len; i++)
		{
			data[2*i] = lo[i] * raw[if_nr][i];
		}
		//write(fd_dbg, data, 2*workbuf_len*sizeof(float));

		// Filter and decimate the upsampled signal
		for (i = 0, k = 0; i < 2*workbuf_len; i += R, k++)
		{
			float fir = 0.0f;
			for (j = 0; j < Ncoeff; j++)
			{
				fir += data[i+j] * coeffs[j];
			}
			out[k] = fir;
		}
		Lout = k;

		// Calculate standard deviation once, used for scaling data for 8-bit or 2-bit output
		if (sigmainv <= 0.0f)
		{
			sigma = stddev(out, Lout);
			sigmainv = 1.0f / sigma;
			printf("Scaling factor 1/stddev = %f\n", sigmainv);
		}

		// Store 32-bit / 8-bit / 2-bit
#if OUTPUT_BITS==32
		//if (vdifencap_write(&vdif, (const char*)out, Lout*sizeof(float)) < 0) // VDIF format
		if (write(fdout, out, Lout*sizeof(float)) < 0) // headerless format
		{
			perror("write");
		}
#elif OUTPUT_BITS==8
		for (j = 0; j < Lout; j++)
		{
			out_integer[j] = out[j] * 8.0*sigmainv;
		}
		if (vdifencap_write(&vdif, (const char*)out_integer, Lout*sizeof(char)) < 0) // VDIF format
		//if (write(fdout, out_integer, Lout*sizeof(char)) < 0) // headerless format
		{
			perror("write");
		}
#elif OUTPUT_BITS==2
		assert((Lout % 4) == 0);
		for (j=0; j<Lout; j+=4)
		{
			float v1, v2, v3, v4;
			unsigned char enc1, enc2, enc3, enc4;

			v1 = out[j+0];
			v2 = out[j+1];
			v3 = out[j+2];
			v4 = out[j+3];

			// 2-bit : 00 = -VHi, 01=-VLo, 10=+VLo, 11=+VHi
			if (v1 > sigma) { enc1 = 0b11; }
			else if (v1 >= 0) { enc1 = 0b10; }
			else if (v1 >= -sigma) { enc1 = 0b01; }
			else { enc1 = 0b00; }

			if (v2 > sigma) { enc2 = 0b11; }
			else if (v2 >= 0) { enc2 = 0b10; }
			else if (v2 >= -sigma) { enc2 = 0b01; }
			else { enc2 = 0b00; }

			if (v3 > sigma) { enc3 = 0b11; }
			else if (v3 >= 0) { enc3 = 0b10; }
			else if (v3 >= -sigma) { enc3 = 0b01; }
			else { enc3 = 0b00; }

			if (v4 > sigma) { enc4 = 0b11; }
			else if (v4 >= 0) { enc4 = 0b10; }
			else if (v4 >= -sigma) { enc4 = 0b01; }
			else { enc4 = 0b00; }

			out_integer[j/4] = (enc4<<6) | (enc3<<4) | (enc2<<2) | enc1;
		}
		if (vdifencap_write(&vdif, (const char*)out_integer, Lout/4) < 0) // VDIF format
		//if (write(fdout, out, Lout/4) < 0) // headerless format
		{
			perror("write");
		}
#endif


		if ((niter % REPORT_INTERVAL) == 0)
		{
			double dt;
			mark5_stream_get_sample_time(ms, &mjd, &sec, &nsec);
			gettimeofday(&t2, NULL);
			dt = (t2.tv_sec - t1.tv_sec) + 1e-6*(t2.tv_usec - t1.tv_usec);
			printf("niter %zd : data %dd %.4fs : CPU %.2f Ms/s\n", niter, mjd, nsec*1e-9+sec, 1e-6*REPORT_INTERVAL*(workbuf_len/dt));
			t1 = t2;
		}
		niter++;

	}
	close(fdout);

	return 0;
}

int vdifencap_open(VDIFEncapsulator_t* t, int rate_Mbps)
{
	t->framenr = 0;
	t->framesec = 0;
	t->refepoch = 0;
	t->payloadbytes = 8000;
	t->writepos = 0;
	t->fps = (rate_Mbps*1e6/8.0) / t->payloadbytes;
	while ((floor(t->fps) != t->fps) || ((t->payloadbytes % 8) != 0))
	{
		t->payloadbytes++;
		t->fps = (rate_Mbps*1e6/8.0) / t->payloadbytes;
	}
	t->hdr[0] = 0; // word 0: [Invalid(1) | Legacy(1) | Seconds from ref epoch(30)]
	t->hdr[1] = 0; // word 1: [none(2)  | RefEp(6) | Data Frame#(24)]
	t->hdr[2] = (t->payloadbytes + 32)/8;   // word 2: [Version(3) | log2 Nch(5) | Framelen(24) in 8-byte units with 32-byte header included]
	t->hdr[3] = (OUTPUT_BITS-1)<<26;        // word 3: [Complex(1) | bit/sample-1 (5) | Thread ID(10) | Station ID(16)]
	t->hdr[4] = 0; // # words 4 to 8: extended user data
	t->hdr[5] = 0;
	t->hdr[6] = 0;
	t->hdr[7] = 0;
	return 0;
}

int vdifencap_copyheader(const char* fn, VDIFEncapsulator_t* t)
{
	uint32_t hdr[8];
	int fd = open(fn, O_RDONLY);
	if (read(fd, hdr, 8*sizeof(uint32_t)) < 0)
	{
		perror("reading VDIF input file");
		return -1;
	}
	t->hdr[0] = hdr[0] & ((1ULL<<31)-1); // copy Seconds from rec epoch
	t->hdr[1] = hdr[1] & 0x3F000000;     // copy RefEpoch
	close(fd);
	return 0;
}

int vdifencap_updateheader(VDIFEncapsulator_t* t)
{
	t->hdr[0] = t->framesec;
	t->hdr[1] = ((t->refepoch << 24) & 0x3F000000) | (t->framenr & 0x00FFFFFF);
	return 0;
}

int vdifencap_inc_header(VDIFEncapsulator_t* t)
{
	t->framenr++;
	if (t->framenr >= t->fps)
	{
		t->framenr = 0;
		t->framesec++;
	}
	vdifencap_updateheader(t);
	return 0;
}

int vdifencap_write(VDIFEncapsulator_t* t, const char* data, size_t len)
{
	size_t i = 0, nremain, nfit, nwrite;
	ssize_t rc;
	while (i < len)
	{
		if (t->writepos == 0)
		{
			write(t->fd, t->hdr, sizeof(t->hdr));
			vdifencap_inc_header(t);
		}
		nremain = len - i;
		nfit = t->payloadbytes - t->writepos;
		nwrite = (nremain < nfit) ? nremain : nfit;
		rc = write(t->fd, (void*)(&data[i]), nwrite);
		if (rc < 0)
		{
			return rc;
		}
		i += nwrite;
		t->writepos += nwrite;
		if (t->writepos >= t->payloadbytes)
		{
			t->writepos = 0;
		}
	}
	return len;
}

int main(int argc, char **argv)
{
	struct mark5_stream *ms;
	long long offset = 0;
	int fdout, if_nr;
	int retval;
	char* infile = argv[1];
	char* format = argv[2];
	char* outfile = argv[3];
	char* coeffile = argv[5];
	if_nr = atoi(argv[4]) - 1;

	float *coeffs;
	int Ncoeff;

	if (argc < 6)
	{
		usage();
		return EXIT_FAILURE;
	}

	if (argc > 6)
	{
		offset = atoll(argv[6]);
	}

	oldsiginthand = signal(SIGINT, siginthand);

	ms = new_mark5_stream_absorb(
		new_mark5_stream_file(infile, offset),
		new_mark5_format_generic_from_string(format) );
	if (!ms)
	{
		printf("Error: problem opening %s\n", infile);

		return EXIT_FAILURE;
	}
	mark5_stream_print(ms);

	if ((if_nr < 0) || (if_nr > (ms->nchan-1)))
	{
		printf("Error: IF number (%d) must be between %d and %d\n", if_nr+1, 1, ms->nchan);

		return EXIT_FAILURE;
	}

	fdout = open(outfile, O_CREAT|O_TRUNC|O_WRONLY, S_IWUSR|S_IRUSR|S_IRGRP|S_IROTH);

	coefffile_load(coeffile, &coeffs, &Ncoeff);
	assert(Ncoeff != 0);
	assert((Ncoeff % 8) == 0);

	retval = filterRealData(infile, ms, fdout, if_nr, coeffs, Ncoeff);

	return retval;
}

int coefffile_load(const char* fn, float** coeff, int* Ncoeff)
{
	FILE *f;
	char l[80];

	*Ncoeff = 0;
	*coeff = memalign(4096, MAX_COEFFS*sizeof(float));

	f = fopen(fn, "r");
	if (f == NULL)
	{
		perror("fopen");
		return 0;
	}

	while (fgets(l, sizeof(l)-1, f) != NULL)
	{
		(*coeff)[*Ncoeff] = atof(l);
		*Ncoeff = (*Ncoeff) + 1;
	}

	return *Ncoeff;
}

