/********************************************************************************
 **
 ** mark5B2VDIF v1.01
 **
 ** Usage:    mark5B2VDIF [--station=<ID>] [--log2nchan=<n>] [--thread=<n>]
 **                          [--refmjd=<n>] [--no2bitswap] <input.m5b> <output.vdif>
 **
 ** Example:  mark5B2VDIF --thread=0 --station=Eb --log2nchan=0 test.m5b test.vdif
 **
 ** Converts a standard Mark5B file (max rate: 2048 Mbps) into a VDIF file.
 ** Conversion involves replacing Mark5B headers with VDIF headers and converting
 ** the sample encoding from Mark5B to VDIF encoding (for 2-bit samples).
 **
 ** Optionally also sets the Station ID field in the VDIF header.
 **
 ** The output VDIF frame size is not "network-compatible".
 ** The frame size is 10.000 bytes (Mark5B payload) + 32 bytes (header) = 10.032 bytes.
 **
 ** (C) 2015 Jan Wagner
 **
 *******************************************************************************/

// Compile with:
//    gcc -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE -D_LARGEFILE_SOURCE -O3 -msse -mavx -msse2 -msse3 mark5B2VDIF.c -o mark5B2VDIF

#define DO_CHECK_SYNCWORD 1

#define PAYLOAD_SIZE          10000  // in bytes, Mark5B & VDIF
#define HEADER_SIZE_5B           16  // in bytes, Mark5B only
#define HEADER_SIZE_VDIF         32  // in bytes, VDIF only
#define NUM_BUFFERED_FRAMES     512

#define FRAME_SIZE_VDIF  (PAYLOAD_SIZE + HEADER_SIZE_VDIF)
#define FRAME_SIZE_5B    (PAYLOAD_SIZE + HEADER_SIZE_5B)
#define BUFSIZE          (FRAME_SIZE_5B * NUM_BUFFERED_FRAMES)

#include <malloc.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <getopt.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>

static uint32_t m_default_refmjd = 12000;
static uint32_t m_VDIF_refep_MJDs[];
static unsigned char m_lut_signmagswap[256];

static struct option long_options[] =
    {
        {"no2bitswap",      no_argument, 0, '2'},
        {"station",   required_argument, 0, 's'},
        {"thread",    required_argument, 0, 't'},
        {"log2nchan", required_argument, 0, 'c'},
        {"refmjd",    required_argument, 0, 'm'},
        {0, 0, 0, 0}
    };

static void usage(void)
{
    printf("\nMark5B to VDIF converter mark5B2VDIF v1.00\n"
           "Converts a Mark5B recording of at most 2 Gbps into a VDIF file\n"
           "that has a 10032-byte frame size and 10000-byte payload size.\n\n"
           "Usage: mark5B2VDIF [--station=<ID>] [--log2nchan=<n>] [--thread=<n>]\n"
           "                      [--refmjd=<n>] [--no2bitswap] <input.m5b> <output.vdif>\n\n"
           "      input.m5b          input file in Mark5B format, at most 2 Gbps because of Mark5B header limits.\n"
           "      output.vdif        new output file to be written in VDIF format\n\n"
           "    Optional arguments:\n"
           "      --station=<ID>     the 2-byte station ID in ASCII\n"
           "      --log2nchan=<n>    the number of IFs (channels) in log2 base, e.g., 4 for 16 IFs\n"
           "      --thread=<n>       the thread ID number\n"
           "      --refmjd=<n>       the base MJD of the truncated Mark5B timestamp (default: %u, calculated from current year)\n"
           "      --no2bitswap       disable swap of Sign/Magnitude bits of assumed 2-bit sample data\n"
           "\n", m_default_refmjd);
}

int main(int argc, char** argv)
{
    int fdi, fdo;
    ssize_t nrd, nwr;
    int nframes;
    int i, c;

    char vdif_header_template[HEADER_SIZE_VDIF];
    char* buf = NULL;

    time_t t_today      = time(NULL);
    struct tm *tm_today = localtime(&t_today);

    /* Defaults */
    char* station_id = NULL;
    int log2nchan    = 4;
    int option_index = 0;
    int thread_id    = 0;
    int do_sign_mag_swap = 1;

    /* Also default MJD base for Mark5B data (Mark5B stores only 'jjj' part of 'JJjjj' MJD timestamp...) */
    uint32_t refmjd_m5b = 0;
    refmjd_m5b = 15022 + (uint32_t)(365.5*tm_today->tm_year); // tm_year zero point is 01/01/1900=MJD_15022
    refmjd_m5b = refmjd_m5b - (refmjd_m5b % 1000);            // discard lower part of 'JJjjj'
    m_default_refmjd = refmjd_m5b;
    uint32_t refep_vdif = (uint32_t)(-1);                     // is set later once first MJD stamp in Mark5B data is known
    uint32_t refep_vdif_MJD = 51544;                          // -"-

    /* Parse optional command line options */
    while (1)
    {
        c = getopt_long(argc, argv, "s:c:t:m:2", long_options, &option_index);
        if (c == -1) break;

        switch (c)
        {
            case 's':
                station_id = strndup(optarg, 2);
                break;
            case 'c':
                log2nchan = atoi(optarg);
                break;
            case 't':
                thread_id = atoi(optarg);
                break;
            case 'm':
                refmjd_m5b = atoi(optarg);
                break;
            case '2':
                do_sign_mag_swap = 0;
                break;
            default:
                usage();
                return -1;
        }
    }

    /* Check that we have the obligatory 2 file names */
    if ((argc - optind) < 2)
    {
        usage();
        return -1;
    }

    /* Open files */
    fdi = open(argv[optind+0], O_RDONLY|O_LARGEFILE);
    if (fdi < 0)
    {
        printf("Error opening input file '%s': %s\n", argv[optind+0], strerror(errno));
        return -1;
    }

    fdo = open(argv[optind+1],
                O_WRONLY|O_CREAT|O_LARGEFILE,
                S_IRUSR|S_IWUSR | S_IRGRP|S_IWGRP | S_IROTH
               );
    if (fdo < 0)
    {
        printf("Error creating output file '%s': %s\n", argv[optind+1], strerror(errno));
        return -1;
    }


    /* Prepare standard fields of VDIF header */
    memset((void*)vdif_header_template, 0x00, HEADER_SIZE_VDIF);
    vdif_header_template[0+2*4] = 0xE6; // length in 8-byte units = 10032/8 = 1254 = 0x04E6
    vdif_header_template[1+2*4] = 0x04; // length in 8-byte units = 10032/8 = 1254 = 0x04E6
    vdif_header_template[3+2*4] = (vdif_header_template[3+2*4] & ~0x1F) | (log2nchan & 0x1F);
    if (station_id != NULL)
    {
        vdif_header_template[0+3*4] = station_id[1];
        vdif_header_template[1+3*4] = station_id[0];
    }
    vdif_header_template[2+3*4] = thread_id & 0x00FF; // Thread ID field is 10-bit, but for now we just discard the upper 2 bits...
    vdif_header_template[3+3*4] |= 0x04;              // Bits per sample minus 1 in bits 2:7

    /* Prepare bit swapping to convert sign/magnitude bit order from Mark5B to VDIF (necessary in *some* cases) */
    if (do_sign_mag_swap)
    {
        unsigned char b;
        for (i=0; i<256; i++)
        {
            b = (unsigned char)i;
            unsigned char s0 = b & 0xAA;  // 0xAA = 0b101010
            unsigned char m0 = b & 0x55;  // 0x55 = 0b010101
            unsigned char swp = (s0 >> 1) | (m0 << 1);
            m_lut_signmagswap[i] = swp;
            // test case
            //   b = 0x1B; // 0b00 01 10 11  so expecting  swp = 0b00 10 01 11 = 0x27 as output
            if (b == 0x1B) { printf("m_lut_signmagswap[0x1B] = 0x%2X\n", swp); }
        }
        printf("Note: swapping of sign/magnitude bits during conversion is enabled.\n");
    }


    /* Copy input file to output file with 5B->VDIF header replacement */
    buf = memalign(16, BUFSIZE);
    while (1)
    {
        nrd = read(fdi, (void*)buf, BUFSIZE);
        if (nrd <= 0) { break; }

        nframes = nrd / FRAME_SIZE_5B;
        for (i=0; i<nframes; i++)
        {
            unsigned char* m5 = ((unsigned char*)buf) + i*FRAME_SIZE_5B;

#if DO_CHECK_SYNCWORD
            // Check for magic sync word 0xABADDEED = 0xED DE AD AB
            if (!(m5[0] == 0xED && m5[1] == 0xDE && m5[2] == 0xAD && m5[3] == 0xAB))
            {
                printf("Lost Mark5B sync. Can't cope. Please fix input file...\n");
                return -1;
            }
#endif

            // Copy the 15-bit Frame# into VDIF 24-bit Frame#
            vdif_header_template[0+1*4] = m5[0+1*4];
            vdif_header_template[1+1*4] = m5[1+1*4] & ~0x80;

            // Decode Mark5B time stamp
            // Note: Mark5B word2 is BCD code 'JJJSSSSS' = 'JJ' 'JS' 'SS' 'SS'
            //       e.g. on disk 0x99 40 24 63 = 44099s on MJD 632
            uint32_t b0 = m5[0+2*4];
            uint32_t b1 = m5[1+2*4];
            uint32_t b2 = m5[2+2*4];
            uint32_t b3 = m5[3+2*4];
            uint32_t JJJ   = ((b3 & 0xF0) >> 4)*100L   + ((b3 & 0x0F) >> 0)*10L
                           + ((b2 & 0xF0) >> 4);
            uint32_t SSSSS = ((b2 & 0x0F) >> 0)*10000L
                           + ((b1 & 0xF0) >> 4)*1000L  + ((b1 & 0x0F) >> 0)*100L
                           + ((b0 & 0xF0) >> 4)*10L    + ((b0 & 0x0F) >> 0);
            uint32_t MJD = refmjd_m5b + JJJ;
            // printf("Debug Mk5B BCD timestamp conversion : %02X-%02X-%02X-%02X : %3u %5u\n", b0, b1, b2, b3, JJJ, SSSSS);

            // Set VDIF reference epoch *once*
            // Reference = 0:00 UT 01 Jan 2000 + <ref ep nr>*6 Months
            //  i.e.  <ref ep nr> of 0x00 = 0:00 UT 01 Jan 2000 = 51544
            //                       0x01 = 0:00 UT 01 Jul 2000
            //                       0x02 = 0:00 UT 01 Jan 2001
            if (refep_vdif == (uint32_t)(-1))
            {
                refep_vdif = (MJD - 51544) / (366/2);             // number of 6-month periods from 01/01/2000
                refep_vdif_MJD = m_VDIF_refep_MJDs[refep_vdif];   // do table look-up instead of complex calculation
                vdif_header_template[3+1*4] = refep_vdif & 0x3F;  // take lower six bits
                printf("Determined VDIF reference epoch: MJD %u and epoch number %u\n", refep_vdif_MJD, refep_vdif);
            }

            // Set VDIF seconds
            uint32_t vdif_second = (MJD - refep_vdif_MJD) * 86400 + SSSSS;
            memcpy((void*)vdif_header_template, (void*)&vdif_second, sizeof(uint32_t));

            // Write VDIF header
            nwr = write(fdo, (void*)vdif_header_template, HEADER_SIZE_VDIF);

            if (do_sign_mag_swap)
            {
                // Swap sign and magnitude
                // VDIF 1.1 page 11: 10. Sample representation
                //    00, 01, 10, 11 = -Mag, -1.0, +1.0, +Mag
                // Mark5B in mark5 memo #019: 
                //    00, 01, 10, 11 = same as VDIF, follows Mark4/VLBA encoding
                // Mark5access library decoder, mark5_format_mark5b.c definition of lut4level[4]:
                //    00, 01, 10, 11 = -Mag, +1.0, -1.0, +Mag
                for (c = HEADER_SIZE_5B; c < FRAME_SIZE_5B; c+=4)
                {
                    *(m5 + c + 0) = m_lut_signmagswap[*(m5 + c + 0)];
                    *(m5 + c + 1) = m_lut_signmagswap[*(m5 + c + 1)];
                    *(m5 + c + 2) = m_lut_signmagswap[*(m5 + c + 2)];
                    *(m5 + c + 3) = m_lut_signmagswap[*(m5 + c + 3)];
                }
            }

            // Append payload
            nwr = write(fdo, (void*)(m5 + HEADER_SIZE_5B), PAYLOAD_SIZE);
        }

    }

    printf("\nDone.\n");
    close(fdi);
    close(fdo);
    return 0;
}


/* List of several VDIF reference epoch MJD's with leap years already accounted for */
static uint32_t m_VDIF_refep_MJDs[] =
{
    // http://www.csgnetwork.com/julianmodifdateconv.html
    // Base epoch nr 0x00 = 0:00 UT 01 Jan 2000 = 51544 = base +  0 months (1st January 2000)
    // Next epoch nr 0x01 = 0:00 UT 01 Jul 2000 = 51726 = base +  6 months (1st July    2000)
    // Next epoch nr 0x02 = 0:00 UT 01 Jan 2001 = 51910 = base + 12 months (1st January 2001)
    // ...

    // Matlab code, maybe also Octave, to generate this table:
    //    yy = 2000:2050; mm = [1 7];
    //    for y=yy, for m=mm, fprintf(1, '%d, ', mjuliandate(y,m,1)), end, end;

    51544, 51726, 51910, 52091, 52275, 52456, 52640, 52821, 53005, 53187, 53371, 53552, 53736,
    53917, 54101, 54282, 54466, 54648, 54832, 55013, 55197, 55378, 55562, 55743, 55927, 56109,
    56293, 56474, 56658, 56839, 57023, 57204, 57388, 57570, 57754, 57935, 58119, 58300, 58484,
    58665, 58849, 59031, 59215, 59396, 59580, 59761, 59945, 60126, 60310, 60492, 60676, 60857,
    61041, 61222, 61406, 61587, 61771, 61953, 62137, 62318, 62502, 62683, 62867, 63048, 63232,
    63414, 63598, 63779, 63963, 64144, 64328, 64509, 64693, 64875, 65059, 65240, 65424, 65605,
    65789, 65970, 66154, 66336, 66520, 66701, 66885, 67066, 67250, 67431, 67615, 67797, 67981,
    68162, 68346, 68527, 68711, 68892, 69076, 69258, 69442, 69623, 69807, 69988

};
