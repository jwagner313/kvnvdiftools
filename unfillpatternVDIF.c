/********************************************************************************
 **
 ** unfillpatternVDIF
 **
 ** Usage:    unfillpatternVDIF <in_out.vdif> [<framesize>]
 **
 ** Cleans up VDIF coming from faulty Mark5B modules, where the
 ** data file can have large blocks of 0x11223344 fillpattern data.
 ** Locations where frame headers are corrupt/fillpattern are
 ** replaced by an Invalid frame.
 **
 ** (C) 2018 Jan Wagner
 **
 *******************************************************************************/

// Compile with:
//    gcc -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE -D_LARGEFILE_SOURCE -O3 unfillpatternVDIF.c -o unfillpatternVDIF

#define NUM_BUFFERED_FRAMES   2048
#define FILL_PATTERN0         0x11223344
#define FILL_PATTERN1         0x22334411
#define FILL_PATTERN2         0x33441122
#define FILL_PATTERN3         0x44112233
#define DEBUG 1

#include <malloc.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <fcntl.h>
#include <errno.h>
#include <endian.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char** argv)
{
    int fi, fo;

    ssize_t nrd, nwr;
    off64_t rwpos = 0;
    size_t  framesize = 0;
    int     nframes;
    uint32_t prev_valid_header[8];

    unsigned char* buf = NULL;

    /* Arguments */
    if (argc != 2 && argc != 3)
    {
        printf("Program unfillpatternVDIF Version 1.0\n\n"
               "Usage: unfillpatternVDIF <in_out.vdif> [<framesize>]\n"
               "\n"
               "Cleans up VDIF coming from faulty Mark5B modules, where the\n"
               "data file can have large blocks of 0x11223344 fillpattern data.\n"
               "Locations where frame headers are corrupt/fillpattern are\n"
               "replaced by an Invalid frame.\n");
        return -1;
    }

    fi = open(argv[1], O_RDWR|O_LARGEFILE);
    if (fi < 0)
    {
        printf("Error opening input file '%s': %s\n", argv[1], strerror(errno));
        return -1;
    }
    fo = fi;
    /* fo = open(argv[2], O_RDWR|O_LARGEFILE|O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
    if (fo < 0)
    {
        printf("Error opening output file '%s': %s\n", argv[2], strerror(errno));
        return -1;
    }
    */
    if (argc ==3)
    {
        framesize = atoi(argv[2]);
    }

    /* Determine the frame size if not specified */
    if (framesize == 0)
    {
        uint32_t vdif_header_w32[4];
        read(fi, (void*)&vdif_header_w32, sizeof(vdif_header_w32));
        lseek64(fi, 0, SEEK_SET);
        framesize = (vdif_header_w32[2] & 0x00FFFFFF) * 8;
        fprintf(stderr, "The VDIF frame size seems to be %zu bytes\n", framesize);
    }
    if (framesize > 32768)
    {
       fprintf(stderr, "Suspiciously large frame size. Aborting.\n");
       return -1;
    }

    /* Modify the file */
    buf = (unsigned char*)memalign(16, framesize*NUM_BUFFERED_FRAMES);
    setbuf(stdout, NULL) ;
    while (1)
    {
        int i, nfilled = 0;

        lseek64(fi, rwpos, SEEK_SET);
        nrd = read(fi, (void*)buf, framesize*NUM_BUFFERED_FRAMES);
        if (nrd <= 0)
        {
            break;
        }

        nframes = nrd / framesize;
        printf("at %zu GB\r", rwpos/(1024*1024*1024));
        //putchar('.');

        for (i=0; i<nframes; i++)
        {
            uint32_t* tptr = (uint32_t*)(buf + i*framesize);
            int invalid = 0, j;

            // Look for fill pattern in the 4-word/8-word header
            for (j=0; j<4; j++)
            {
                invalid |= (tptr[j] == FILL_PATTERN0);
                invalid |= (tptr[j] == FILL_PATTERN1);
                invalid |= (tptr[j] == FILL_PATTERN2);
                invalid |= (tptr[j] == FILL_PATTERN3);
            }
            if (!invalid)
            {
                // Remember latest valid VDIF header
                memcpy(prev_valid_header, tptr, sizeof(prev_valid_header));
            }
            else
            {
                // Replace a fill-pattern header with latest valid header, but marked Invalid
                memcpy(tptr, prev_valid_header, sizeof(prev_valid_header));
                tptr[0] |= (1<<31);
                nfilled++;
            }
        }

        if (DEBUG && (nfilled > 0))
        {
            printf("block at %zu: %d frames, %d were fill (%.1f%%)\n", rwpos, nframes, nfilled, 100.0*nfilled/nframes);
        }

        lseek64(fo, rwpos, SEEK_SET);
        nwr = write(fo, (void*)buf, nrd);
        rwpos += nwr;
    }

    printf("\nDone.\n");
    return 0;
}
