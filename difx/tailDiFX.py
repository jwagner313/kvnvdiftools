#!/usr/bin/python -u
import math, numpy, signal, sys, time, os
import parseDiFX
from optparse import OptionParser
try:
    # see http://stackoverflow.com/questions/15795720/matplotlib-major-display-issue-with-dense-data-sets
    import matplotlib as mpl
    mpl.rcParams['path.simplify'] = False
except:
    pass
import pylab
pylab.ion()

# Settings
helpstr = "tailDiFX.py [options] <difx file>\n\n"
helpstr += "Shows current DiFX visibility data from an updating difx file."
helpstr += "Similar to plotDiFX.py except this displays one baseline x frequency x polarization only,"
helpstr += "and waits for more data to be produced by DiFX. Does not use the DiFX monitoring facility."
parser = OptionParser(helpstr)
parser.add_option("-f", "--freq", dest="freq", metavar="targetfreq", default="-1",
                  help="Only display visilities from this comma-separated list of frequency indices")
parser.add_option("-b", "--baseline", dest="baseline", metavar="targetbaseline", default="-1",
                  help="Only display visibilities from this comma-separated list of baseline numbers")
parser.add_option("-p", "--pols", dest="pollist", default="RR,RL,LR,LL,YY,YX,XY,XX",
                  help="Only display polarization pairs from this comma-separated list")
parser.add_option("-v", "--verbose", dest="verbose", action="store_true", default=False,
                  help="Turn verbose printing on")
parser.add_option("-i", "--inputfile", dest="inputfile", default="",
                  help="An input file to use as guide for number of channels for each freq")

# Hard-coded configuration
Lhist       = 200
linestyles  = ['b', 'r', 'g', 'k', 'y']
DiFX_header_len = 13  # length in bytes of DiFX header (13 bytes in DiFX 2.4.0)

def mad(arr):
    """ Median absolute deviation """
    return numpy.median(numpy.abs(arr - numpy.median(arr)))

CtrlC = 0
def signal_handler(signal, frame):
    """ Ctrl-C handler, sets a global flag """
    global CtrlC
    CtrlC = 1

def file_wait(f, length=1):
    """ Wait for the requested N bytes to be available in a growing file. """

    gotEOF     = False
    tempdata   = ''
    pos        = f.tell()

    while not CtrlC:

        tempdata = f.read(length)
        f.seek(pos)
        if (len(tempdata) >= length):
            break
        time.sleep(1)
        gotEOF = True
        print ('.'),

    return gotEOF

def process_single_baseline_freq_pol(inputfile,difxfilename,targetbaseline=257,targetfreq=0,targetpol='LL'):
    """ Plots the progress on a single baseline-frequency-polarization tuple"""

    # Analyze the difx .input file
    (numconfigs, inp_configs) = parseDiFX.get_configtable_info(inputfile)
    (numfreqs, inp_freqs) = parseDiFX.get_freqtable_info(inputfile)
    (numtelescopes, inp_telescopes) = parseDiFX.get_telescopetable_info(inputfile)
    (numdatastreams, inp_datastreams) = parseDiFX.get_datastreamtable_info(inputfile)
    (numbaselines, inp_baselines) = parseDiFX.get_baselinetable_info(inputfile)
    if numfreqs == 0 or numtelescopes == 0 or numdatastreams == 0 or numbaselines == 0 or numconfigs == 0:
        parser.error("Couldn't parse input file " + inputfile + " correctly")
        return
 
    # Read first header from DiFX .difx file
    difxfile = open(difxfilename, 'rb')
    file_wait(difxfile, DiFX_header_len)
    difxheader = parseDiFX.parse_output_header(difxfile)
    if len(difxheader) == 0:
        return
    (baseline,mjd,seconds,tmp1,tmp2,freqindex,polpair) = difxheader[0:7]
    nchan = inp_freqs[freqindex].numchan / inp_freqs[freqindex].specavg

    # Initialize the data history
    amp   = [None] * Lhist
    phase = [None] * Lhist
    vis   = [None] * Lhist
    lag   = [None] * Lhist
    lagamp = [None] * Lhist
    xy_tints = [None]
    xy_chans = [None]
    for i in range(Lhist):
        amp[i] = numpy.zeros(nchan)
        phase[i] = numpy.zeros(nchan)
        vis[i] = numpy.zeros(nchan)
        lag[i] = numpy.zeros(nchan)
        lagamp[i] = numpy.zeros(nchan)
    xy_tints,xy_chans = numpy.mgrid[0:Lhist, 0:nchan]

    # Read all data and headers one by one
    Nread = 0
    need_redraw = True
    while (not len(difxheader) == 0) and not CtrlC:

        # Read header
        (baseline,mjd,seconds,tmp1,tmp2,freqindex,polpair) = difxheader[0:7]
        ant1     = baseline / 256 - 1
        ant2     = baseline % 256 - 1
        ant1name = inp_telescopes[ant1].name
        ant2name = inp_telescopes[ant2].name
        nchan    = inp_freqs[freqindex].numchan / inp_freqs[freqindex].specavg
        lowfreq  = inp_freqs[freqindex].freq
        hifreq   = inp_freqs[freqindex].freq + inp_freqs[freqindex].bandwidth
        sbname   = ['USB','LSB'][inp_freqs[freqindex].lsb>0]
        if inp_freqs[freqindex].lsb:
                lowfreq -= inp_freqs[freqindex].bandwidth
                hifreq  -= inp_freqs[freqindex].bandwidth
        hour          = int(seconds/3600)
        minute        = int(seconds/60 - 60*hour)
        second    = int(seconds - (3600*hour + 60*minute))
        mjdstring = "%dd%02dh%02dm%02ds" % (mjd, hour, minute, second)
        visname   = "%s-%s %.2f MHz %s %s at %s" % \
                     (ant1name, ant2name, inp_freqs[freqindex].freq, sbname, polpair, mjdstring)
        figfile   = "%s-%s-%.2fMHz-%s-%s.png" % (ant1name, ant2name, inp_freqs[freqindex].freq, sbname, polpair)

        # Read data inside the DiFX frame
        got_eof = file_wait(difxfile, 8*nchan)
        if got_eof:
            need_redraw = True
        raw   = difxfile.read(8*nchan)
        nchan = inp_freqs[freqindex].numchan / inp_freqs[freqindex].specavg
        cvis  = numpy.fromstring(raw, dtype='complex64')

        # Already get the next header
        got_eof = file_wait(difxfile, DiFX_header_len)
        if got_eof:
            need_redraw = True
        difxheader = parseDiFX.parse_output_header(difxfile)

        # Skip current data if not selected for plotting
        match = (baseline == targetbaseline) and (freqindex == targetfreq) and (polpair == targetpol)
        if not match:
            continue

        # Shift the history
        Nread = Nread + 1
        amp[0:(Lhist-1)]    = amp[1:Lhist]
        phase[0:(Lhist-1)]  = phase[1:Lhist]
        lag[0:(Lhist-1)]    = lag[1:Lhist]
        lagamp[0:(Lhist-1)] = lagamp[1:Lhist]

        # Put current data into history and analyze it
        amp[-1]    = numpy.abs(cvis)
        phase[-1]  = numpy.angle(cvis, deg=True)
        lag[-1]    = numpy.fft.fftshift(numpy.fft.ifft(cvis, nchan))
        lagamp[-1] = numpy.abs(lag[-1])

        # Find fringe
        maxlag  = numpy.argmax(lagamp[-1])
        delayoffsetus = (maxlag - nchan/2) * 1.0/(inp_freqs[freqindex].bandwidth)

        # Estimate fringe SNR
        amax = lagamp[-1][maxlag]
        abase = numpy.copy(lagamp[-1])
        abase[maxlag] = 0.0
        if (maxlag >= 1):
                abase[maxlag-1] = 0.0
        if (maxlag <= (nchan-1)):
                abase[nchan-1] = 0.0
        snr1 = amax / numpy.std(abase)
        snr2 = amax / mad(lagamp[-1])
        snr1corr = (snr1-3)/2.0
        print ('%s : fringe SNR %.2f sigma (%.2f mad), offset %0.3f us' % (visname,snr1,snr2,delayoffsetus))

	# When read didn't hit temporary EOF, just try to read until end of file
        if not need_redraw and not(Nread % Lhist == 0):
           continue
        print ('Making plot')
        need_redraw = False

	# Plot
        pylab.clf()
        pylab.gcf().set_facecolor('white')
        pylab.subplot(411)
        pylab.plot(amp[-1], 'k-')
        pylab.xlim([0, nchan])
        pylab.ylabel("Amplitude")
        pylab.title(visname)

        pylab.subplot(412)
        pylab.plot(phase[-1], 'k+')
        pylab.xlim([0, nchan])
        pylab.ylim([-200, 200])
        pylab.ylabel("Phase (deg)")

        pylab.subplot(413)
        pylab.plot(lagamp[-1], 'k-')
        pylab.xlim([0, nchan])
        pylab.ylabel("Lag")

        ax = pylab.subplot(414)
        M = numpy.vstack(lagamp)
        M = numpy.fft.fftshift(numpy.abs(numpy.fft.fft(M,axis=0)), axes=[0])
	pylab.imshow(M, cmap='binary', aspect='auto')
        pylab.xlabel("Channel")
	pylab.colorbar()

        pylab.figtext(0.5,0.0, "Fringe SNR %0.2f sigma (%0.2f mad) @ offset %0.3f us" % \
                              (snr1, snr2, delayoffsetus), ha='center')

        pylab.draw()
        if figfile != None:
            pylab.savefig(figfile)
            # os.rename('/tmp/' + figfile, './' + figfile)
            print ('Saved %s' % (figfile))

    print ('Done.')
    if not CtrlC:
        pylab.show()

# Main
def main():
    (opts, args) = parser.parse_args()
    numfiles        = len(args)
    targetfreqs     = [int(f) for f in opts.freq.split(",")]
    targetbaselines = [int(b) for b in opts.baseline.split(",")]
    targetpols      = opts.pollist.upper().split(",")
    verbose         = opts.verbose
    inputfile       = opts.inputfile

    if numfiles != 1:
        parser.error("You must supply one DiFX output file!")

    if len(inputfile) < 1:
        parser.error("You must supply a DiFX input file (-i | --inputfile)!")

    if (len(targetfreqs) < 1) or (targetfreqs[0] == -1):
        targetfreqs = [0]
        print ('No frequency (-f <n>) specified. Defaulting to 0.')

    if (len(targetbaselines) < 1) or (targetbaselines[0] == -1):
        targetbaselines = [1*256 + 2]
        print ('No baseline (-b <n>) specified. Defaulting to 258.')

    process_single_baseline_freq_pol(inputfile,args[0],targetbaselines[0],targetfreqs[0],targetpols[0])

signal.signal(signal.SIGINT, signal_handler)
main()
