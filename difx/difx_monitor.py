#!/usr/bin/python
"""
difx_monitor.py ver 1.0    Jan Wagner 20150609

A DiFX monitoring utility loosely based on C++ program 'difx_monitor'.
It polls the progress of an ongoing DiFX job by querying accumulated 
visibility data from the DiFX monitoring server monitor_server.

Usage:

$ difx_monitor.py <inputfile> <host> <baseline nr> <freq nr>

"""
import socket, struct, sys, time
import pylab, numpy
import parseDiFX
pylab.ion()

MONITOR_PORT = 52300
MAXPOL = 4

class MonServer:

	def __init__(self):
		self.s = None

	def connect(self, host, port):

		# Connect
		self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.s.connect((host, int(port)))
		self.s_port = int(port)
		self.s_host = host

		# Receive status code
		status32 = self.read_uint32()
		if not(status32 == 0):
			print ('Error: server returned status %s' % (str(status32)))
		else:
			print ('Connected. Server status %s' % (str(status32)))

	def reconnect(self):
		s.shutdown()
		s.close()
		time.sleep(1)
		self.connect(s.s_host, s.s_port)

	def write_packed(self, fmt, value):
		self.s.send(struct.pack(fmt, value))

	def read_packed(self, fmt, nbytes):
		try:
			d = self.s.recv(nbytes)
			v = struct.unpack(fmt, d)[0]
			return v
		except:
			return None

	def write_sint32(self, value):
		self.write_packed('i', value)
	def read_sint32(self):
		return self.read_packed('i', 4)

	def write_uint32(self, value):
		self.write_packed('I', value)
	def read_uint32(self):
		return self.read_packed('I', 4)

	def request_visibilities(self, offsets,npoints,baselines):
		self.write_sint32(len(offsets))
		for i in xrange(len(offsets)):
			self.write_sint32(offsets[i])
			self.write_sint32(npoints[i])
			self.write_sint32(baselines[i])
		status32 = self.read_uint32()
		if not(status32 == 0):
			print ('Error: could not request monitor products, server returned status %s' % (str(status32)))
			return False
		# print ('Requested visibility data (off=%s, nchan=%s, pair=%s)' % (str(offsets),str(npoints),str(baselines)))
		return True

	def read_visibilities(self):
		v = {}

		# Get header and raw dataset of integration period
		print ('Reading the requested visibility data...')
		v['timestamp'] = self.read_sint32()
		nretvis = self.read_sint32()
		bufsize = self.read_sint32()
		print ('Got time %d, %d visibilities, %d bytes' % (v['timestamp'],nretvis,bufsize))
		raw     = self.s.recv(bufsize)
		if len(raw) != bufsize:
			print ('Short read! Got %d bytes instead of %d' % (len(raw),bufsize))

		# Split raw dataset into baseline visibility data
		v['nchan'] = []
		v['baseline'] = []
		v['visdata'] = []
		i = 0
		for jj in xrange(nretvis):
			vis_hdr         = raw[i:(i+8)]
			(nchan,product) = struct.unpack('ii', vis_hdr)
			vis_data        = raw[(i+8):(i+8+nchan*2*4)]
			cdata32 = numpy.frombuffer(vis_data, dtype=numpy.dtype('complex64'))
			print ('Got product %d nchan %d vis %d' % (product,nchan,jj))
			v['nchan'].append(nchan)
			v['baseline'].append(product)
			v['visdata'].append(cdata32)
			i = i + 8 + nchan*2*4

		return v

def main(args=sys.argv):

	inputfile = args[0]
	host      = args[1]
	bline     = int(args[2])
	freq      = int(args[3])

	(numfreqs, inp_freqs) = parseDiFX.get_freqtable_info(inputfile)
	(numtelescopes, inp_telescopes) = parseDiFX.get_telescopetable_info(inputfile)
	nchan = inp_freqs[freq].numchan / inp_freqs[freq].specavg
	ant1     = bline / 256 - 1
	ant2     = bline % 256 - 1
	ant1name = inp_telescopes[ant1].name
	ant2name = inp_telescopes[ant2].name
        lowfreq  = inp_freqs[freq].freq
        sbname   = ['USB','LSB'][inp_freqs[freq].lsb>0]
	label    = '%s-%s %.2f MHz %s' % (ant1name,ant2name,lowfreq,sbname)

	m = MonServer()
	m.connect(host, MONITOR_PORT)

	while True:
		reqsuccess = m.request_visibilities([2*nchan*freq], [nchan], [bline])
		if not reqsuccess:
			print ('Reconnecting to server')
			m.reconnect()
			continue

		pylab.clf()
		v = m.read_visibilities()
		for ii in range(len(v['visdata'])):
			cvis = v['visdata'][ii]
			phase = numpy.angle(cvis, deg=True)
			lagamp = numpy.abs(numpy.fft.fftshift(numpy.fft.ifft(cvis)))
			pylab.subplot(311)
			pylab.plot(numpy.abs(cvis), 'k-')
			pylab.xlim([0,nchan])
			pylab.subplot(312)
			pylab.plot(phase, 'kx')
			pylab.xlim([0,nchan])
			pylab.subplot(313)
			pylab.plot(lagamp, 'k-')
			pylab.xlim([0,nchan])
		pylab.subplot(311)
		pylab.title(label + ' ' + str(v['timestamp']))
		pylab.draw()


if len(sys.argv) not in [5]:
	# difx_monitor.py <inputfile> <host> <baseline nr> <freq nr>
	print __doc__
	sys.exit(-1)

main(sys.argv[1:])
