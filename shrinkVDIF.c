/********************************************************************************
 **
 ** shrinkVDIF v1.0
 **
 ** Usage:    shrinkVDIF <options> <infile.vdif> <outfile.vdif>
 **
 ** Quick hack to play with special case VDIF data produced by FILA10G
 ** that gets inputs from VSI 1-2-3-4, data on each VSI being single-channel
 ** wideband data, and each VSI being attached to an independent frequency band
 **
 ** The tool copies just one selected "band" from the input file to the output,
 ** and throws away the others, reducing the VDIF frame size to 1/4th in the process.
 **
 ** This is an alternative method to 1) using FILA10G register 'chan_perm' (it
 ** in principle can shuffle 128-bit (4 x 32-bit VSI) data and thereby combine
 ** same-timestamp samples from each band to be next to each other as expected
 ** by standard VDIF), or 2) using FILA10G split mode and corner-turn=off to
 ** separate VSI's into their own VDIF Threads (VSI1 and VSI2 land as own Threads
 ** on 10G eth0, but by FILA10G FPGA constraints VSI3+VSI4 must land in a
 ** single thread on eth1).
 **
 ** (C) 2015 Jan Wagner
 **
 *******************************************************************************/

#define NUM_BUFFERED_FRAMES   512 // should be small to fit in CPU cache
#define VDIF_HEADER_LEN        32 // the non-legacy VDIF header is 32 byte long

#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <malloc.h>
#include <math.h>
#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#ifndef _BSD_SOURCE
    #define _BSD_SOURCE
#endif
#include <endian.h>

static struct option opts[] =
{
    {"vsi",    required_argument, 0, 'x'},
    {"numvsi", required_argument, 0, 'n'},
    {0, 0, 0, 0}
};

void usage(void)
{
    printf("\n"
           "Usage: shrinkVDIF <required options> <infile.vdif> <outfile.vdif>\n"
           "\n"
           "   Required options:\n"
           "     -x|--vsi=[1|2|3|4]   which VSI bus (1,2,3 or 4) to extract into output file\n"
           "     -n|--numvsi=[2|4]    number of VSI buses contained in the input file (default: 4)\n"
           "\n"
    );
    return;
}


int main(int argc, char** argv)
{
    int     fdi, fdo;
    off64_t rdpos = 0;
    ssize_t nrd, nwr;
    size_t  framesize;
    size_t  framesize_new;
    size_t  num_input128b;

    unsigned char *buf8 = NULL;

    uint32_t hdr32[VDIF_HEADER_LEN/4] = {0};
    int      vsi_to_extract = -1;
    int      Nvsi = 4;

    /* Arguments */
    while (1)
    {
        int c, optidx = 0;
        c = getopt_long (argc, argv, "x:n:", opts, &optidx);
        if (c == -1)
        {
            break;
        }
        switch (c)
        {
            case 0:
                printf("option %s", opts[optidx].name);
                if (optarg)
                {
                    printf(" with arg %s", optarg);
                }
                printf("\n");
                break;
            case 'x':
                vsi_to_extract = atoi(optarg);
                break;
            case 'n':
                Nvsi = atoi(optarg);
                break;
            default:
                usage();
                exit(-1);
        }
    }
    if ( ((argc-optind) <= 1) || (Nvsi < 2) || (vsi_to_extract > Nvsi) || (vsi_to_extract < 1) )
    {
        usage();
        exit(-1);
    }

    /* Open files */
    fdi = open(argv[optind], O_RDONLY|O_LARGEFILE);
    if (fdi < 0)
    {
        printf("Error opening file '%s': %s\n", argv[optind], strerror(errno));
        exit(-1);
    }
    fdo = open(argv[optind+1], O_RDWR|O_LARGEFILE|O_CREAT, 0644);
    if (fdo < 0)
    {
        printf("Error opening output file '%s': %s\n", argv[optind+1], strerror(errno));
        exit(-1);
    }

    /* Check frame size */
    nrd = read(fdi, (void*)&hdr32, sizeof(hdr32));
    framesize = (hdr32[2] & 0x00FFFFFF) * 8; // VDIF: size is in 8-byte units
    if ((framesize > 4*1024*1024) || (framesize < 1024))
    {
        printf("Frame size in first header is suspicious (%zu bytes). Quitting!\n", framesize);
        exit(-1);
    }
    buf8 = (unsigned char*)memalign(128, framesize*NUM_BUFFERED_FRAMES);
    num_input128b = (framesize-VDIF_HEADER_LEN)/(Nvsi*sizeof(uint32_t));
    framesize_new = VDIF_HEADER_LEN + (framesize-VDIF_HEADER_LEN)/Nvsi;

    printf("Current frame size : %zu bytes\n", framesize);
    printf("Adjusted frame size: %zu bytes\n", framesize_new);
    printf("Num of %d-bit input words to reduce to 32-bit by choosing VSI#%d: %zu\n",
           32*Nvsi, vsi_to_extract, num_input128b);

    /* Perform copying */
    lseek64(fdi, 0, SEEK_SET);
    setbuf(stdout, NULL);
    while (1)
    {
        size_t nframes, i, j;
        uint32_t *src32 = (uint32_t*)buf8;
        uint32_t *dst32 = (uint32_t*)buf8;

        nrd = read(fdi, (void*)buf8, framesize*NUM_BUFFERED_FRAMES);
        if (nrd <= 0)
        {
            break;
        }
        rdpos += nrd;

        nframes = nrd / framesize;
        for (i=0; i<nframes; i++)
        {
            // Copy header and reduce 'frame size' and 'log2(Nchan)' fields
            memmove((void*)dst32, (void*)src32, VDIF_HEADER_LEN);
            dst32[2] = (dst32[2] & 0x60000000) | (framesize_new/8);

            // Copy data of selected VSI interface
            src32 += VDIF_HEADER_LEN/sizeof(uint32_t);
            dst32 += VDIF_HEADER_LEN/sizeof(uint32_t);
            for (j=0; j<num_input128b; src32+=Nvsi, dst32+=1, j++)
            {
               dst32[0] = src32[Nvsi-vsi_to_extract];
            }

        }

        nwr = write(fdo, (void*)buf8, nframes*framesize_new);

        // putchar('.');
        // printf("\33[2K\r %.0f MByte", ((double)rwpos)/(1048576.0) );
        printf("\33[2K\r %.0f GByte", ((double)rdpos)*9.31322574615479e-10 ); // 1/2^30

    }

    printf("\nDone.\n");
    return 0;
}
