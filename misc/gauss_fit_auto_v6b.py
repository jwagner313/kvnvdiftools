#!/usr/bin/python
import random, sys
try:
        import matplotlib
        matplotlib.rcParams['path.simplify'] = False # http://stackoverflow.com/questions/15795720/matplotlib-major-display-issue-with-dense-data-sets
except:
        pass
import numpy, pylab
#pylab.ion()

g_constant_linewidths = True


def str2floats(s):
    return [float(x) for x in s.split()]

def find_nearest(vec,val):
    vec = numpy.asarray(vec)
    return vec[numpy.abs(vec-val).argmin()]


def read_gbt_spectrum(fn):
    s = type('gaussfit_spectrum',(object,), {})

    with open(fn,'r') as f:
        content = f.readlines()

        # Header
        hdr = content[0].split()
        s.obs_src  = hdr[6]
        s.obs_date = hdr[7]
        s.mjd      = float(content[2])

        # Spectral data
        data = content[3:]
        data = [list(map(float, l.split())) for l in data]

        # Transpose to get columns into rows, then extract data vectors
        data = map(list,map(None,*data))
        s.vel = numpy.asarray(data[0])
        s.amp = numpy.asarray(data[1])
        s.rms = numpy.asarray(data[2])

    return s


def read_inp_file(fn):
    c = type('gaussfit_configuration',(object,), {})

    with open(fn,'r') as f:

        # Read full file, removing single-line and line-tailing comments
        content = f.readlines()
        cmt = '!'
        content = [l.strip() for l in content if len(l)>0 and l.strip()[0]!=cmt]
        content = [(l.split(cmt)[0]).strip() for l in content]

        # Number of files and parameters listed
        N = len(content)
        files  = content[0:(N-8)]
        params = content[(N-8):]

        # Get the parameters
        (c.b_vel,c.b_acc)                   = str2floats(params[0])
        (c.e_vel,c.e_acc)                   = str2floats(params[1])
        (c.num_trials,c.random_seed)        = str2floats(params[2])
        (c.itermax)                         = str2floats(params[3])[0]
        (c.ref_date)                        = str2floats(params[4])[0]
        (c.vel_spacing,c.vel_sigma)         = str2floats(params[5])
        (c.acc_expected,c.acc_sigma)        = str2floats(params[6])
        (c.a_priori_widths,c.a_priori_errs) = str2floats(params[7])
        c.v_mid = (c.b_vel + c.e_vel)/2.0

        # Store file list
        c.files    = files
        c.n_epochs = len(files)

        # Change params if necessary
        if (c.random_seed <= 0):
            c.random_seed = 0.78357
        if (c.itermax < 10):
            c.itermax = 10

    print (' Configuration file params : %s' % (str(c.__dict__)))
    print (' Velocity mid-point at reference epoch = %10.2f' % (c.v_mid))
    return c

def read_spectra(cfg):
    """ Fortran: read_all_files() and read_one_file() """
    s = type('gaussfit_spectra',(object,), {})
    s.velocities = []; s.fluxes = []; s.fluxerr = []; s.mjds = []; s.specidcs = []
    sidx = 0

    for fn in cfg.files:

        spec = read_gbt_spectrum(fn)

        b_vel_ep = cfg.b_vel + cfg.b_acc*(spec.mjd - cfg.ref_date)
        e_vel_ep = cfg.e_vel + cfg.e_acc*(spec.mjd - cfg.ref_date)

        selected = (spec.vel>=b_vel_ep) & (spec.vel<=e_vel_ep)
        spec.vel = spec.vel[selected]
        spec.amp = spec.amp[selected] * 1e-3 # mJy -> Jy
        spec.rms = spec.rms[selected] * 1e-3 # mJy -> Jy

        # Liz/2007: Add 2% error in quarature to account for dynamic range and FX correlator windowing
        spec.rms = numpy.sqrt(spec.rms**2 + (0.02*spec.amp)**2) 

        s.mjds.append(spec.mjd)
        s.velocities.append(spec.vel)
        s.fluxes.append(spec.amp)
        s.fluxerr.append(spec.rms)
        s.specidcs.append(sidx)

        print ('>>> %s' % (fn))
        print (' Julian Date of spectrum:              %.1f' % (spec.mjd))
        print (' Beginning & ending velocities:        %.2f %.2f' % (b_vel_ep,e_vel_ep))
        print (' Beginning from internal array index:  %u' % (s.specidcs[-1]))
        print (' Number of spectral channels read in:  %u' % (len(spec.amp)))
        print ('')
        sidx += len(spec.amp)

    # Populate some extra fields
    s.specidcs.append(sidx)
    s.n_specs = cfg.n_epochs
    cfg.mjds  = s.mjds
    return s


def calc_model(cfg,model,mjd=0.0,vels=numpy.asarray([0])):
    """ Generate model spectrum at given MJD with given velocities """
    flux     = numpy.zeros_like(vels)
    fwhm2sig = 0.18033688011112042 # = 1/(2*numpy.sqrt(2*numpy.log(2))) **2 = 1sigma / FWHM

    closest_mjd = find_nearest(cfg.mjds,mjd)
    print mjd, closest_mjd

    for gg in range(len(model.amp)):

        # Model 1: amplitude and FWHM apply across all epochs
        if g_constant_linewidths:
            amp  = model.amp[gg]
            fwhm = model.fwhm[gg] 
        # Model 2: amplitude and FWHM may change at each epoch; get these from closest epoch?
        else:
            ep_idx = 0  # TODO
            amp  = model.amp[gg + ep_idx]
            fwhm = model.fwhm[gg + ep_idx] 

        # Gaussian : f(v) = A * exp( -(v-B)^2 / 2*C^2 )
        if amp>0 and fwhm>0:
            g_arg = -0.5 * numpy.square( (vels - model.vcenter[gg])/(fwhm*fwhm2sig) )
            flux += amp * numpy.exp(g_arg)

    # Likely unused but present in Fortran code
    baseline = model.base_dc + model.base_slope * (vels - cfg.v_mid)

    return flux + baseline


def random_model(cfg,specs):
    """ Fortran: new_starting_params() """

    print ('Creating random_model()...')

    m = type('gaussfit_configuration',(object,), {})
    m.vcenter = numpy.zeros(0)
    m.acc     = numpy.zeros_like(m.vcenter)
    m.amp     = numpy.zeros_like(m.vcenter)
    m.fwhm    = numpy.zeros_like(m.vcenter)

    # Generate some Gaussian components to fill the velocity range of [b_vel,e_vel]
    vel = cfg.b_vel
    while True:

        dv = abs(random.gauss(mu=cfg.vel_spacing, sigma=cfg.vel_sigma))
        a  = random.gauss(mu=cfg.acc_expected, sigma=cfg.acc_sigma)

        if (dv < 0.5*cfg.vel_spacing):
            # " Enforce minimal separation between component velocities"
            dv += 0.5*cfg.vel_spacing
        vel += dv
        if (vel >= cfg.e_vel):
            break

        m.vcenter = numpy.append(m.vcenter, vel)
        m.acc     = numpy.append(m.acc,     a)
        m.amp     = numpy.append(m.amp,     1.0)
        m.fwhm    = numpy.append(m.fwhm,    cfg.vel_spacing)

    # Allow amplitude and FWHM to "jump" between epochs (e.g., line disappears or narrows)?
    for ee in range(cfg.n_epochs):
        for gg in range(len(m.vcenter)):
            m.amp[gg] = 1.0
        break

    # Special mode: replace accelerations with a slope (a(v) = a0 + v*a1)
    # where the slope [m^2/s^3] is between +-50% of expected magnitude of acceleration [m/s^2]
    if True:
        a_intercept = random.gauss(mu=cfg.acc_expected, sigma=cfg.acc_sigma)    # a at v_mid
        a_slope = random.uniform(-0.5*cfg.acc_expected, +0.5*cfg.acc_expected)
        for gg in range(len(m.acc)):
            da = a_slope * (m.vcenter[gg] - cfg.v_mid)/(0.5*(cfg.e_vel - cfg.b_vel))
            m.acc[gg] = a_intercept + da
        print (' Random initial accelerations replaced with sloped initial accelerations');

    # Extra parameters in the Fortran code that seem unused
    m.base_dc = 0.0
    m.base_slope = 0.0

    return m


def main(argv=sys.argv):
    """ Fit acceleration models to data """

    # Input data
    cfg   = read_inp_file('gauss_fit_auto.inp')
    specs = read_spectra(cfg)

    # Use seed to get same random numbers on each program run
    random.seed(cfg.random_seed)

    # Models
    model = random_model(cfg,specs)

    # Evaluation
    v = numpy.linspace(cfg.b_vel,cfg.e_vel,512)
    T = specs.mjds[0] + 123.0
    f = calc_model(cfg,model,mjd=T,vels=v)

    #print ('Plotting at MJD=%f with %u velocity bins...' % (T,len(v)))
    #pylab.plot(v,f)
    #pylab.show()


if __name__ == "__main__":
	sys.exit(main())

