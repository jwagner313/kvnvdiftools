#!/bin/env python
#
# Quick and dirty KVN VDIF header modifier to
# change the 'Invalid' bit of the VDIF header.
#
# Usage: validflagVDIF.py <inoutstream> <payloadsize>
#        validflagVDIF.py short.vdif 1280
#  where 1280 is the payload size of a 1312-byte frame
#
# (C) 2014 Jan Wagner
#
import sys
import struct

if len(sys.argv) != 3:
        print 'Usage: validflagVDIF.py <inoutstream> <payloadsize>'
        sys.exit(-1)

finout = open(sys.argv[1], "r+b")
payloadsize = int(sys.argv[2])
pos = 0
offset = 3
posMB = 0

while True:
        finout.seek(pos + offset)
        b = finout.read(1)
        finout.seek(pos + offset)
        if len(b)<1:
                break

        if b == '\x80':
                print 'I',
                finout.write('\x00')
        else:
                print '.',
                pass
        pos = pos + payloadsize + 32

        if (posMB != int(pos/1048576.0)):
                posMB = int(pos/1048576.0)
                print '%d MB' % (posMB)

finout.close()
