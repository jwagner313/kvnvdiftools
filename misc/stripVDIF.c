/********************************************************************************
 ** Strip VDIF headers from payload
 **
 ** (C) 2014 Jan Wagner 
 **
 *******************************************************************************/

// gcc -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE -D_LARGEFILE_SOURCE -O3 stripVDIF.c -o stripVDIF

#define BUFSIZE  32768
#define VDIF_HEADER_LEN 32
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <malloc.h>

int main(int argc, char** argv)
{
    int fdi, fdo, psize;
    ssize_t nrd, nwr;
    unsigned char* buf;

    if (argc <= 3)
    {
        printf("Usage: stripVDIF <infile> <outfile> <payloadsize>\n"
               "Strips the VDIF headers from the input file, outputs headerless new file.\n"
               "If frames are missing *no* fill pattern data is inserted.\n");
        return -1;
    }

    buf = memalign(16, BUFSIZE);
    fdi = open(argv[1], O_RDONLY|O_LARGEFILE);
    fdo = open(argv[2],
                O_WRONLY|O_CREAT|O_LARGEFILE,
                S_IRUSR|S_IWUSR | S_IRGRP|S_IWGRP | S_IROTH
               );
    psize = atoi(argv[3]);
    // todo: check fdi, fdo errors...

    while (1)
    {
        nrd = read(fdi, (void*)buf, psize+VDIF_HEADER_LEN);
        if (nrd <= VDIF_HEADER_LEN) { break; }

        nwr = write(fdo, (void*)(buf + VDIF_HEADER_LEN), nrd - VDIF_HEADER_LEN); // todo: check if nwritten == nrd
    }

    close(fdi);
    close(fdo);
    return 0;
}

