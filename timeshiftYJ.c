#define NUM_BUFFERED_FRAMES   512 // should be small to fit in CPU cache

#include <errno.h>
#include <fcntl.h>
#include <malloc.h>
#include <math.h>
#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

void usage(void)
{
    printf("\n"
           "timeshiftYJ v0.01  Jan Wagner 20230522\n"
           "\n"
           "Specific to vo3124. Take 64-channel 1-threaded 31250 frames/sec Complex VDIF.\n"
           "In each 64-channel (256-bit; 32-byte) group replace the high 16 channels (high 8 byte)\n"
           "with data from 1 second earlier in the file.\n"
           "NB: the input VDIF file must have zero lost frames.\n"
           "\n"
           "Usage: timeshiftYJ <infile.vdif> <outfile.vdif>\n"
           "\n"
    );
    return;
}


int main(int argc, char** argv)
{
    int     fdi, fdo, i, j;
    off64_t rdpos = 0, wrpos = 0;
    ssize_t nrd, nwr;

	const size_t fps = 31250;
    const size_t framesize = 32800;
    const int headerlength = 32;
    const int blocklen_64ch = 32; // 2-bit Complex 64-channel = 2 * 2 * 64 / 8 = 32 byte
    const int blocklen_16ch = 8;

    off64_t patching_offset = -(framesize * fps);
    rdpos = framesize * fps;

    unsigned char* buf = NULL;
    unsigned char* offsetbuf = NULL;

    /* Open file to modify. Create different output file. */
    if (argc != 3)
    {
        usage();
        exit(-1);
    }

    fdi = open(argv[1], (O_RDONLY|O_LARGEFILE));
    if (fdi < 0)
    {
        printf("Error opening file '%s': %s\n", argv[1], strerror(errno));
        exit(-1);
    }
    fdo = open(argv[2], O_RDWR|O_LARGEFILE|O_CREAT, 0644);
    if (fdo < 0)
    {
        printf("Error opening output file '%s': %s\n", argv[2], strerror(errno));
        exit(-1);
    }

    buf = (unsigned char*)memalign(16, framesize*NUM_BUFFERED_FRAMES);
    offsetbuf = (unsigned char*)memalign(16, framesize*NUM_BUFFERED_FRAMES);

    /* Make a modification pass over the file */
    setbuf(stdout, NULL);
    while (1)
    {
        int nframes;

        nrd = pread(fdi, (void*)buf, framesize*NUM_BUFFERED_FRAMES, rdpos);
        if (nrd <= 0)
        {
            break;
        }

        (void)pread(fdi, (void*)offsetbuf, framesize*NUM_BUFFERED_FRAMES, rdpos + patching_offset);

        rdpos += nrd;

        nframes = nrd / framesize;

        unsigned char* dst = buf;
        const unsigned char* src = offsetbuf;

        for (i=0; i<nframes; i++)
        {
            uint32_t* h32 = (uint32_t*)dst;
            uint32_t* h32offset = (uint32_t*)src;

            uint32_t tcurr = (h32[0] & 0x3FFFFFFF);
            uint32_t framenr_curr = (h32[1] & 0x00FFFFFF);

            uint32_t toffset = (h32offset[0] & 0x3FFFFFFF);
            uint32_t framenr_offset = (h32offset[1] & 0x00FFFFFF);

            /* Picky about data time continuity */
            if ((tcurr - 1) != toffset || framenr_curr != framenr_offset)
            {
                printf("Error: input VDIF was discontinuous\n");
                printf("Exiting now at sec %u frame %u, since data '-1 sec' earlier contained sec %u frame %u\n", tcurr, framenr_curr, toffset, framenr_offset);
                exit(0);
            }

            /* Patch current payload data with the 1-sec ahead data */
            for (j = headerlength; (j + blocklen_64ch) <= framesize; j += blocklen_64ch)
            {
                //memcpy(dst + j, src + j, blocklen_16ch);
                memcpy(dst + j + (blocklen_64ch-blocklen_16ch), src + j + (blocklen_64ch-blocklen_16ch), blocklen_16ch);
            }

            dst += framesize;
            src += framesize;
        }

        pwrite(fdo, (void*)buf, nrd, wrpos);
        wrpos += nrd;

        // putchar('.');
        // printf("\33[2K\r %.0f MByte", ((double)rwpos)/(1048576.0) );
        printf("\33[2K\r %.0f GByte", ((double)rdpos)*9.31322574615479e-10 ); // 1/2^30

    }

    printf("\nDone.\n");
    return 0;
}
