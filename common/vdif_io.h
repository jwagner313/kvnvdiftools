#ifndef __VDIF_IO_H_
#define __VDIF_IO_H_

#include <time.h>
#include <sys/types.h>
#include <stdint.h>
#include <inttypes.h>

/////////////////////////////////////////////////////
// Definitions
/////////////////////////////////////////////////////

typedef uint8_t   uchar;
typedef uint16_t  ushort;
typedef uint32_t  uint; 
typedef enum boolean {false, true} bool;

#define NBUFF                   1000
#define BUFFSIZE              265264
#define SBUFFSIZE                 64
#define MAXFIFO                NBUFF
#define VDIF_HEADER_SIZE          32
#define KVN_VDIF_PAYLOAD_SIZE   1280  // bytes of payload data without header
#define KVN_VDIF_FRAME_SIZE     1312  // bytes total with 32-byte header
#define KVN_VDIF_LOG_N_CHANNELS    4  // 16 channels (2^N with N=4)
#define KVN_VDIF_BITSPERSAMPLE     2
#define KVN_VDIF_THREAD_ID         0


/////////////////////////////////////////////////////
// Globals 
/////////////////////////////////////////////////////

extern struct timeval timeout; // for setting timeout on socket
extern struct timeval t_start, t_ctlstart;
extern struct timeval t_end,   t_ctlend;
extern struct timeval t_diff,  t_ctldiff;
extern bool timedout;
extern int z;

extern time_t      std, etd;
extern struct tm   stm, etm;
extern clock_t     scl, ecl;
extern char        ststr[80];
extern char        etstr[80];
extern double      runtime;
extern double      mbps;
extern const char *tfmt;

extern uint8_t byte_bit_swap[256];



/////////////////////////////////////////////////////
// User data types
/////////////////////////////////////////////////////

// Standard VDIF header v1.1
// See http://vlbi.org/vdif/docs/VDIF_specification_Release_1.1.1.pdf page 5
typedef struct vdif_header_tt {
   uint32_t invalid_sec_epoch;   // word0: [Invalid data flag:1 | reserved:1 | seconds in reference epoch:30]
   uint32_t epoch_dframe_no;     // word1: [Unassigned:2 | reference epoch:6 | data frame nr:24]
   uint32_t dframe_len;          // word2: [VDIF Version:3 | log2 channels: 5 | payload length in 8-byte units: 24]
   uint32_t bits_tid;            // word3: [Complex data flag:1 | bits/sample minus one:5 | thread ID:10 | station ID:16]
   uint32_t valbit_sample_split; // word4: [Extended Data Version:8 | Extended User Data:24]
                                 //   note for KVN Extended User Data: [reserved:3 | valid:5 | samplig:4 | split:4 | TVG:2 | ref.epoch:6}
   uint32_t src_sec_epoch;       // word5: [Extended User Data:32]
                                 //   note for KVN Extended User Data: [reserved:2 | source's seconds from ref epoch:30]
   uint32_t sec_counts;          // word6: [Extended User Data:32]
                                 //   note for KVN Extended User Data: [seconds counts:8 | reserved:24]	
   uint32_t reserved;            // word7: [Extended User Data:32]
} vdif_header_t;

// Standard VDIF frame: 32-Byte Header + N-Byte Payload
typedef struct vdif_frame_tt {
   vdif_header_t   hdr;
   uint8_t         data[KVN_VDIF_PAYLOAD_SIZE];
} vdif_frame_t;

// VDIFCP Sender Report (SR)
typedef struct vdif_cp_sr_tt {
   uint8_t   ver_kind;           // version and packet-kind
   uint8_t   port;               // vsi-port
   uint16_t  plen;               // packet length
   uint32_t  seqno;              // seq-number
   uint8_t   reserve1;
   uint8_t   mode_timeupper;     // mode and time
   uint8_t   time[6];            // mode and time
   uint32_t  reserve2;
   uint8_t   aux[8];             // AUX (NULL fill)
   uint8_t   pdata[52];          // PDATA (variable length)   JanW: default size 52 leads to overflow in sprintf()!
} vdif_cp_sr_t;

// VDIFCP Receiver Report (RR)
typedef struct vdif_cp_rr_tt {
   uint8_t   ver_kind;           // version and packet-kind
   uint8_t   port;               // vsi-port
   uint16_t  plen;               // packet length
   uint32_t  seqno;              // seq-number
   uint8_t   tv;                 // time valid flag (MSB)
   uint8_t   mode_timeupper;     // mode and time
   uint8_t   time[6];            // mode and time
   uint32_t  h_fcnt;             // H-frame frame count
   uint32_t  l_fcnt;             // L-frame frame count
   uint32_t  reserve;
} vdif_cp_rr_t;

// VDIFCP VSI Time (VR)
typedef struct vdif_cp_vr_tt {
   uint8_t   ver_kind;           // version and packet-kind
   uint8_t   port;               // vsi-port
   uint16_t  plen;               // packet length
   uint32_t  seqno;              // seq-number
   uint8_t   reserve1;
   uint8_t   mode_timeupper;     // mode and time
   uint8_t   time[6];            // mode and time
   uint32_t  reserve2[3];
} vdif_cp_vr_t;


// Time-related (?)
typedef struct datum_tt {
        char          *buf;
        unsigned int  rbytes;
        int           bfid;
} datum_t;
typedef struct node_tt {
        datum_t      data;
        struct node  *next;
} node_t;


// Thread arguments (?)
typedef struct threadarg_tt {
        int sd;
        int fd;
        char sbuf[SBUFFSIZE];
        uint64_t totrbytes;
        uint64_t totwbytes;
        uint64_t filesize;
        char ofname[256];
} threadarg_t;


#endif // __VDIF_IO_H_

