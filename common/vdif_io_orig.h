
#ifndef __VDIF_IO_H_
#define __VDIF_IO_H_ 0x55ff55ff

#include <time.h>
#include <sys/types.h>
#include <stdint.h>


typedef uint8_t   uchar;
typedef uint16_t  ushort;
typedef uint32_t  uint; 
typedef enum boolean {false, true} bool;

#define NBUFF 1000
#define BUFFSIZE 265264
#define SBUFFSIZE 64
#define MAXFIFO NBUFF

struct timeval timeout; //setting timeout to socket
struct timeval t_start, t_ctlstart;
struct timeval t_end, t_ctlend;
struct timeval t_diff, t_ctldiff;
bool timedout;

int z;

time_t std, etd;
struct tm stm, etm;
clock_t scl, ecl;
char ststr[80], etstr[80];
double runtime, mbps;
char *tfmt = "%Y.%m.%d %H:%M:%S";


// VDIF header
struct VDIF_HEAD {
   uint32_t invalid_sec_epoch;   // Invalid data(1bit), reserved(1bit)
                                 // seconds from reference epoch(30bit)
   uint32_t epoch_dframe_no;     // unassign(2), ref epoch(6)
                                 // data frame # within seconds(24)
   uint32_t dframe_len;          // rsv(8), data frame length-1(24)
   uint32_t bits_tid;            // rsv(1), bits(5), thread ID(10), rsv(16)
   uint32_t valbit_sample_split; // rsv(11), valid bits(5), sampling(4)
                                 // split(4), TV(2), ref. epoch(6)
   uint32_t src_sec_epoch;       // rsv(2), source's seconds from ref epoch(30)
   uint32_t sec_counts;          // seconds counts(8), rsv(24)
   uint32_t reserve;             // rsv(32)
};

// VDIF Data Frame
struct VDIF_DF {
   uint8_t  vdif_dat[1280];      // Data
};

// VDIF Head + Data Frame
struct VDIF_HEAD_DF {
   struct VDIF_HEAD vdif_hdr;         // Header
   uint8_t  vdif_dat[1280];      // Data

};

// VDIFCP SR (Sender Report)
struct VDIFCP_SR {
   uint8_t   ver_kind;           // version and packet-kind
   uint8_t   port;               // vsi-port
   uint16_t  plen;               // packet length
   uint32_t  seqno;              // seq-number
   uint8_t   reserve1;
   uint8_t   mode_timeupper;       // mode and time
   uint8_t   time[6];       // mode and time
   uint32_t  reserve2;
   uint8_t   aux[8];             // AUX (NULL fill)
   uint8_t   pdata[52];           // PDATA (variable length)
} ;

// VDIFCP RR (Receiver Report)
struct VDIFCP_RR {
   uint8_t   ver_kind;           // version and packet-kind
   uint8_t   port;               // vsi-port
   uint16_t  plen;               // packet length
   uint32_t  seqno;              // seq-number
   uint8_t   tv;                 // time valid flag (MSB)
   uint8_t   mode_timeupper;     // mode and time
   uint8_t   time[6];       // mode and time
   uint32_t  h_fcnt;             // H-frame frame count
   uint32_t  l_fcnt;             // L-frame frame count
   uint32_t  reserve;
};

// VDIFCP VR (Vsi Time)
struct VDIFCP_VR {
   uint8_t   ver_kind;           // version and packet-kind
   uint8_t   port;               // vsi-port
   uint16_t  plen;               // packet length
   uint32_t  seqno;              // seq-number
   uint8_t   reserve1;
   uint8_t   mode_timeupper;     // mode and time
   uint8_t   time[6];            // mode and time
   uint32_t  reserve2[3];
};


typedef struct {
        char *buf;
        unsigned int rbytes;
        int bfid;
} datum;

typedef struct node {
        datum data;
        struct node *next;
} node;

typedef struct {
        int sd;
        int fd;
        char sbuf[SBUFFSIZE];
        uint64_t totrbytes;
        uint64_t totwbytes;
        uint64_t filesize;
        char ofname[256];
} tharg;

uint8_t byte_bit_swap[256] = {
            0x00, 0x80, 0x40, 0xc0, 0x20, 0xa0, 0x60, 0xe0,
            0x10, 0x90, 0x50, 0xd0, 0x30, 0xb0, 0x70, 0xf0,
            0x08, 0x88, 0x48, 0xc8, 0x28, 0xa8, 0x68, 0xe8,
            0x18, 0x98, 0x58, 0xd8, 0x38, 0xb8, 0x78, 0xf8,
            0x04, 0x84, 0x44, 0xc4, 0x24, 0xa4, 0x64, 0xe4,
            0x14, 0x94, 0x54, 0xd4, 0x34, 0xb4, 0x74, 0xf4,
            0x0c, 0x8c, 0x4c, 0xcc, 0x2c, 0xac, 0x6c, 0xec,
            0x1c, 0x9c, 0x5c, 0xdc, 0x3c, 0xbc, 0x7c, 0xfc,
            0x02, 0x82, 0x42, 0xc2, 0x22, 0xa2, 0x62, 0xe2,
            0x12, 0x92, 0x52, 0xd2, 0x32, 0xb2, 0x72, 0xf2,
            0x0a, 0x8a, 0x4a, 0xca, 0x2a, 0xaa, 0x6a, 0xea,
            0x1a, 0x9a, 0x5a, 0xda, 0x3a, 0xba, 0x7a, 0xfa,
            0x06, 0x86, 0x46, 0xc6, 0x26, 0xa6, 0x66, 0xe6,
            0x16, 0x96, 0x56, 0xd6, 0x36, 0xb6, 0x76, 0xf6,
            0x0e, 0x8e, 0x4e, 0xce, 0x2e, 0xae, 0x6e, 0xee,
            0x1e, 0x9e, 0x5e, 0xde, 0x3e, 0xbe, 0x7e, 0xfe,
            0x01, 0x81, 0x41, 0xc1, 0x21, 0xa1, 0x61, 0xe1,
            0x11, 0x91, 0x51, 0xd1, 0x31, 0xb1, 0x71, 0xf1,
            0x09, 0x89, 0x49, 0xc9, 0x29, 0xa9, 0x69, 0xe9,
            0x19, 0x99, 0x59, 0xd9, 0x39, 0xb9, 0x79, 0xf9,
            0x05, 0x85, 0x45, 0xc5, 0x25, 0xa5, 0x65, 0xe5,
            0x15, 0x95, 0x55, 0xd5, 0x35, 0xb5, 0x75, 0xf5,
            0x0d, 0x8d, 0x4d, 0xcd, 0x2d, 0xad, 0x6d, 0xed,
            0x1d, 0x9d, 0x5d, 0xdd, 0x3d, 0xbd, 0x7d, 0xfd,
            0x03, 0x83, 0x43, 0xc3, 0x23, 0xa3, 0x63, 0xe3,
            0x13, 0x93, 0x53, 0xd3, 0x33, 0xb3, 0x73, 0xf3,
            0x0b, 0x8b, 0x4b, 0xcb, 0x2b, 0xab, 0x6b, 0xeb,
            0x1b, 0x9b, 0x5b, 0xdb, 0x3b, 0xbb, 0x7b, 0xfb,
            0x07, 0x87, 0x47, 0xc7, 0x27, 0xa7, 0x67, 0xe7,
            0x17, 0x97, 0x57, 0xd7, 0x37, 0xb7, 0x77, 0xf7,
            0x0f, 0x8f, 0x4f, 0xcf, 0x2f, 0xaf, 0x6f, 0xef,
            0x1f, 0x9f, 0x5f, 0xdf, 0x3f, 0xbf, 0x7f, 0xff
};


static void bail(const char *on_what);

#endif


