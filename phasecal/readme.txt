INFO

The PCal extraction in DiFX 'mpifxcorr' supports an
offset from 0 Hz of the first tone in the band
of say 10 kHz.

On the other hand, the original 'm5pcal' in DiFX
supports only integer-MHz tone offsets and tone spacings.

This new copy updates m5pcal.c to work in the
same (or at least similar) way as 'mpifxcorr'.

As with the older 'm5pcal' the output is basically
a text file in a plain format. It can be loaded
into Matlab/Octave/Python but not directly into AIPS.


TODO

 - add an output format ready for AIPS PCLOD?
   http://www.aips.nrao.edu/cgi-bin/ZXHLP2.PL?PCLOD

 - add a 'WVR' output format?


EXAMPLES

# Tones from first out of 16 IFs in a VDIF recording that
# actually does not contain PCal tones (need real test file later!)

jwagner@polaris:~/code/phasecal$ ./m5pcal  /scratch0/oper/jw-eht/YS/s15jw01a_KVNYS_No0004 KVN5B-1024-16-2 0.01 tmp.out
freq_kHz[0] = 10
Determined optimal DFT length to be 3200 points over 16.00 MHz.
Edge band not provided; taking 1/16 of the bandwidth = 1.000000 MHz
3200000 / 3200000 samples unpacked
Tone  0 with f=   10 in chan 1/1600=0.0100 MHz
Tone  1 with f= 1010 in chan 101/1600=1.0100 MHz
Tone  2 with f= 2010 in chan 201/1600=2.0100 MHz
Tone  3 with f= 3010 in chan 301/1600=3.0100 MHz
Tone  4 with f= 4010 in chan 401/1600=4.0100 MHz
Tone  5 with f= 5010 in chan 501/1600=5.0100 MHz
Tone  6 with f= 6010 in chan 601/1600=6.0100 MHz
Tone  7 with f= 7010 in chan 701/1600=7.0100 MHz
Tone  8 with f= 8010 in chan 801/1600=8.0100 MHz
Tone  9 with f= 9010 in chan 901/1600=9.0100 MHz
Tone 10 with f=10010 in chan 1001/1600=10.0100 MHz
Tone 11 with f=11010 in chan 1101/1600=11.0100 MHz
Tone 12 with f=12010 in chan 1201/1600=12.0100 MHz
Tone 13 with f=13010 in chan 1301/1600=13.0100 MHz
Tone 14 with f=14010 in chan 1401/1600=14.0100 MHz
Tone 15 with f=15010 in chan 1501/1600=15.0100 MHz
Sub-band 0 = 0.010000-16.010000 MHz

  Sample   0  Tone  0  Freq=0.010 MHz  Amp=0.0120  Phase= 80.72 deg
  Sample   0  Tone  1  Freq=1.010 MHz  Amp=0.0406  Phase=  2.60 deg
  Sample   0  Tone  2  Freq=2.010 MHz  Amp=0.0085  Phase=177.94 deg
  Sample   0  Tone  3  Freq=3.010 MHz  Amp=0.0130  Phase= 10.22 deg
  Sample   0  Tone  4  Freq=4.010 MHz  Amp=0.0330  Phase=-41.34 deg
  Sample   0  Tone  5  Freq=5.010 MHz  Amp=0.0236  Phase=115.57 deg
  Sample   0  Tone  6  Freq=6.010 MHz  Amp=0.0214  Phase=-159.19 deg
  Sample   0  Tone  7  Freq=7.010 MHz  Amp=0.0101  Phase=-128.18 deg
  Sample   0  Tone  8  Freq=8.010 MHz  Amp=0.0333  Phase=-37.78 deg
  Sample   0  Tone  9  Freq=9.010 MHz  Amp=0.0335  Phase=-42.97 deg
  Sample   0  Tone 10  Freq=10.010 MHz  Amp=0.0270  Phase=-74.91 deg
  Sample   0  Tone 11  Freq=11.010 MHz  Amp=0.0189  Phase=-72.86 deg
  Sample   0  Tone 12  Freq=12.010 MHz  Amp=0.0421  Phase=112.00 deg
  Sample   0  Tone 13  Freq=13.010 MHz  Amp=0.0111  Phase=-66.22 deg
  Sample   0  Tone 14  Freq=14.010 MHz  Amp=0.0193  Phase=161.90 deg
  Sample   0  Tone 15  Freq=15.010 MHz  Amp=0.0314  Phase= 83.16 deg
  t1=1080.00000 s  t2=1080.10000 s  Freq=8.010 MHz  Delay=-97.176213 ns


Old version:  uses fixed DFT length of 6400 samples

$ ./m5pcal.orig.bin  /scratch0/oper/jw-eht/YS/s15jw01a_KVNYS_No0004 KVN5B-1024-16-2 0 tmp.out
  Sample   0  Tone  0  Freq=-1 MHz  Amp=0.0126  Phase=-151.16 deg
  Sample   0  Tone  1  Freq=-2 MHz  Amp=0.0436  Phase= 23.25 deg
  Sample   0  Tone  2  Freq=-3 MHz  Amp=0.0158  Phase=-76.14 deg
  ...
  Sample   0  Tone 13  Freq=-14 MHz  Amp=0.0117  Phase=-119.36 deg
  Sample   0  Tone 14  Freq=-15 MHz  Amp=0.0097  Phase=-123.04 deg
  t1=1080.00000 s  t2=1080.20000 s  Freq=8.000 MHz  Delay=0.000000 ns
   
   (delay is 0.0ns because calcDelay() thinks tones are not in valid band)

$ ./m5pcal.orig.bin  /scratch0/oper/jw-eht/YS/s15jw01a_KVNYS_No0004 KVN5B-1024-16-2 1 tmp.out
  Sample   0  Tone  0  Freq=-1 MHz  Amp=0.0126  Phase=-151.16 deg
  Sample   0  Tone  1  Freq=-2 MHz  Amp=0.0436  Phase= 23.25 deg
  Sample   0  Tone  2  Freq=-3 MHz  Amp=0.0158  Phase=-76.14 deg
  ...
  Sample   0  Tone 13  Freq=-14 MHz  Amp=0.0117  Phase=-119.36 deg
  Sample   0  Tone 14  Freq=-15 MHz  Amp=0.0097  Phase=-123.04 deg
  t1=1080.00000 s  t2=1080.20000 s  Freq=8.000 MHz  Delay=434.151637 ns
   
   (changed offset of 1st tone to 1 MHz, results in same phase information as
   before, but now the tones are recognized by calcDelay() as being in the valid band)

New version:  determines DFT length of 32 for the above case, 
              but after hard-coding back to 6400 just for comparison the output is

$ ./m5pcal.new.bin  /scratch0/oper/jw-eht/YS/s15jw01a_KVNYS_No0004 KVN5B-1024-16-2 0 tmp.out
  Sample   0  Tone  0  Freq=1.000 MHz  Amp=0.0126  Phase=-151.16 deg
  Sample   0  Tone  1  Freq=2.000 MHz  Amp=0.0436  Phase= 23.25 deg
  Sample   0  Tone  2  Freq=3.000 MHz  Amp=0.0158  Phase=-76.14 deg
  ...
  Sample   0  Tone 13  Freq=14.000 MHz  Amp=0.0117  Phase=-119.36 deg
  Sample   0  Tone 14  Freq=15.000 MHz  Amp=0.0097  Phase=-123.04 deg
  t1=1080.00000 s  t2=1080.20000 s  Freq=8.000 MHz  Delay=434.151637 ns

