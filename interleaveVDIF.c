/********************************************************************************
 **
 ** interleaveVDIF
 **
 ** Usage:    interleaveVDIF infile1.vdif infile2.vdif [... infileN.vdif] out.vdif
 **
 ** Interleaves several VDIF files into one output file. Does not
 ** change Thread IDs in the input files during interleaving.
 **
 ** (C) 2018 Jan Wagner
 **
 *******************************************************************************/

// Compile with:
//    gcc -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE -D_LARGEFILE_SOURCE -O3 interleaveVDIF.c -o interleaveVDIF

#include <malloc.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <fcntl.h>
#include <errno.h>
#include <endian.h>
#include <sys/types.h>
#include <sys/stat.h>

void usage()
{
    printf("Program interleaveVDIF Version 1.0.1\n\n"
           "Usage: interleaveVDIF infile1.vdif infile2.vdif [... infileN.vdif] out.vdif\n"
           "\n"
           "Interlaves several VDIF files into one output file. Does not\n"
           "change Thread IDs in the input files during interleaving.\n"
           "An output filename of '-' writes to stdout instead of a file.\n");
}

int main(int argc, char** argv)
{
    int fi[128], fo, i, Nin, Nopen;

    ssize_t nrd;
    size_t  framesize, bufsize;

    unsigned char* buf = NULL;
    uint32_t vdif_header_w32[4];

    /* Arguments */
    if (argc < 4)
    {
        usage();
        return -1;
    }

    Nin = argc - 2;
    Nopen = Nin;
    for (i = 0; i < Nin; i++)
    {
        fi[i] = open(argv[1+i], O_RDONLY|O_LARGEFILE);
        if (fi < 0)
        {
            printf("Error opening file '%s': %s\n", argv[1+i], strerror(errno));
            return -1;
        }
    }
    if (strcmp(argv[argc-1], "-") == 0)
    {
        fo = STDOUT_FILENO;
    }
    else
    {
        if (open(argv[argc-1], O_RDONLY) >= 0)
        {
            printf("Error: output file '%s' already exists and would be overwritten. Quitting.\n", argv[1+i]);
            return -1;
        }
        fo = open(argv[argc-1], O_WRONLY|O_LARGEFILE|O_CREAT, S_IWGRP|S_IRGRP|S_IRUSR|S_IWUSR|S_IROTH);
    }
    if (fo < 0)
    {
        printf("Error creating output file '%s': %s\n", argv[1+i], strerror(errno));
        return -1;
    }

    /* Interleave */
    while (Nopen > 0)
    {
        for (i = 0; i < Nin; i++)
        {
            if (fi[i] < 0) { continue; }

            nrd = read(fi[i], (void*)&vdif_header_w32, sizeof(vdif_header_w32));
            if (nrd < 0 || nrd < (ssize_t)sizeof(vdif_header_w32))
            {
                printf("EOF file %d\n", i+1);
                close(fi[i]);
                fi[i] = -1;
                Nopen--;
                continue;
            }

            framesize = (vdif_header_w32[2] & 0x00FFFFFF) * 8;
            if (buf == NULL)
            {
                printf("From file %d got frame size %zu bytes\n", i+1, framesize);
                buf = (unsigned char*)memalign(4096, framesize);
                bufsize = framesize;
            }
            else if (framesize != bufsize)
            {
                printf("Stopping due to discrepancy in frame size %zu of file %d with earlier buf size %zu\n", framesize, i+1, bufsize);
                Nopen = 0;
                break;
            }

            nrd = read(fi[i], (void*)buf, framesize - sizeof(vdif_header_w32));
            if (nrd < (framesize - sizeof(vdif_header_w32)))
            {
                printf("EOF file %d\n", i+1);
                close(fi[i]);
                fi[i] = -1;
                Nopen--;
                continue;
            }

            write(fo, &vdif_header_w32, sizeof(vdif_header_w32));
            write(fo, buf, framesize - sizeof(vdif_header_w32));
        }
    }

    printf("\nDone.\n");
    return 0;
}
