/********************************************************************************
 **
 ** renumberVDIF
 **
 ** Usage:    renumberVDIF [-s|-n] <in.vdif> <out.vdif> <frames per sec>
 **
 ** Adjusts either VDIF frame timestamps or frame numbers, such that
 ** the seconds field increments when the frame number is zero (-s),
 ** or the frame number resets when the seconds change (-n).
 **
 ** (C) 2018 Jan Wagner
 **
 *******************************************************************************/

// Compile with:
//    gcc -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE -D_LARGEFILE_SOURCE -O3 renumberVDIF.c -o renumberVDIF

// Number of VDIF frames in memory. Keep it small (512*8032byte = ~4 MB) to fit into CPU cache.
#define NUM_BUFFERED_FRAMES   512
#define DEBUG 0

#include <malloc.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <fcntl.h>
#include <errno.h>
#include <endian.h>
#include <sys/types.h>
#include <sys/stat.h>

#define MAX_THREADS 16

int main(int argc, char** argv)
{
    int fi, fo;
    uint32_t fps;
    int believe_framenrs;

    ssize_t nrd, nwr;
    off64_t rwpos = 0;
    size_t  framesize;
    int     nframes;

    unsigned char* buf = NULL;
    uint32_t vdif_header_w32[4];
    uint32_t thread_sec[MAX_THREADS] = {0};
    uint32_t thread_framenr[MAX_THREADS] = {0};

    /* Arguments */
    if (argc != 5)
    {
        printf("Program renumberVDIF Version 1.0\n\n"
               "Usage: renumberVDIF [-s|-n] <in.vdif> <out.vdif> <frames per sec>\n"
               "\n"
               "Adjusts either VDIF frame timestamps or frame numbers, such that\n"
               "the seconds field increments when the frame number is zero (-s),\n"
               "or the frame number resets when the seconds change (-n).\n");
        return -1;
    }

    believe_framenrs = (argv[1][1] == 's');
    fi = open(argv[2], O_RDONLY|O_LARGEFILE);
    if (fi < 0)
    {
        printf("Error opening input file '%s': %s\n", argv[2], strerror(errno));
        return -1;
    }
    fo = open(argv[3], O_RDWR|O_LARGEFILE|O_CREAT, S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH);
    if (fo < 0)
    {
        printf("Error opening output file '%s': %s\n", argv[3], strerror(errno));
        return -1;
    }
    fps = atoi(argv[4]);

    /* Determine the frame size */
    read(fi, (void*)&vdif_header_w32, sizeof(vdif_header_w32));
    lseek64(fi, 0, SEEK_SET);
    // VDIF Header word0: [Invalid data flag:1 | reserved:1 | seconds in reference epoch:30]
    // VDIF Header word1: [Unassigned:2 | reference epoch:6 | data frame nr:24]
    // VDIF Header word2: [VDIF Version:3bit | log2 channels: 5bit | frame size in 8-byte units: 24bit]
    framesize = (vdif_header_w32[2] & 0x00FFFFFF) * 8;
    fprintf(stderr, "The VDIF frame size seems to be %zu bytes\n", framesize);
    if (framesize > 32768)
    {
       fprintf(stderr, "Suspiciously large frame size. Aborting.\n");
       return -1;
    }

    buf = (unsigned char*)memalign(16, framesize*NUM_BUFFERED_FRAMES);

    /* Modify the file */
    printf("Adjusting %s\n", (believe_framenrs ? "seconds field" : "frame# field"));
    setbuf(stdout, NULL) ;
    while (1)
    {
        int i;

        lseek64(fi, rwpos, SEEK_SET);
        nrd = read(fi, (void*)buf, framesize*NUM_BUFFERED_FRAMES);
        if (nrd <= 0)
        {
            break;
        }

        nframes = nrd / framesize;
        putchar('.');

        for (i=0; i<nframes; i++)
        {
            uint32_t* tptr = (uint32_t*)(buf + i*framesize);

            // Get thread ID
            int tid = (tptr[3] >> 16) & 0x03FF;
            if  (tid >= MAX_THREADS)
            {
                fprintf(stderr, "Thread ID %d exceeded internal maximum of %d. Aborting.\n", tid, MAX_THREADS-1);
                return -1;
            }

            uint32_t seconds = tptr[0] & 0x3FFFFFFF;
            uint32_t framenr = tptr[1] & 0x00FFFFFF;
            uint32_t invalid = tptr[0] & (1<<31);

            if (invalid)
            {
                if (DEBUG) printf("invalid frame\n");
                continue;
            }

            // First time to see this thread?
            if (thread_sec[tid] == 0)
            {
                if (DEBUG) printf("thread %d first frame has sec %u frame %u\n", tid, seconds, framenr);
                thread_sec[tid] = seconds;
                thread_framenr[tid] = believe_framenrs ? framenr : 0;
                framenr = believe_framenrs ? framenr : 0;
            }
            else
            {
                if (believe_framenrs)
                {
                    // Believe frame# is correct; increment seconds at each frame# roll-over
                    if (framenr == 0)
                    {
                        if (DEBUG) printf("inc thread %d seconds %d by +1\n", tid, thread_sec[tid]);
                        thread_sec[tid]++;
                    }
                    seconds = thread_sec[tid];
                }
                if (!believe_framenrs)
                {
                    // Believe seconds# are correct; reset frame# at each roll-over, otherwise increment it
                    if (seconds != thread_sec[tid])
                    {
                        if (DEBUG) printf("jump in thread %d seconds %u, resetting framenr to 0\n", tid, thread_sec[tid]);
                        thread_sec[tid] = seconds;
                        thread_framenr[tid] = 0;
                        framenr = 0;
                    }
                    else
                    {
                        thread_framenr[tid] = (thread_framenr[tid] + 1) % fps;
                        framenr = thread_framenr[tid];
                    }
                }
            }

            // Update the header
            tptr[0] = (tptr[0] & ~0x3FFFFFFF) | seconds;
            tptr[1] = (tptr[1] & ~0x00FFFFFF) | framenr;
        }

        lseek64(fo, rwpos, SEEK_SET);
        nwr = write(fo, (void*)buf, nrd);
        rwpos += nwr;
    }

    printf("\nDone.\n");
    return 0;
}
