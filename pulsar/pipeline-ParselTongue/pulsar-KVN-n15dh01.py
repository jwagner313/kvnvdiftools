#
# Start with:
# $ ParselTongue pulsar-KVN-<exptname>.py
#
# KVN N15DH01A/B/C/D/E
# Assumes a phase-referenced observation (magentar + QSO), 
# with a second source (Sgr A*) in the primary beam that
# was processed in DiFX using multiple phase centers and
# thus producing two FITS files; one FITS with all sources
# except for Sgr A*, and another FITS file with just Sgr A*.
#

from AIPS import AIPS
from AIPSTask import AIPSTask, AIPSList
from AIPSData import AIPSUVData, AIPSImage
from pulsarPipelineKVN import *
import os

AIPS.userno = 514
EXPT        = 'n15dh01b'
REF_ANT     = 1
FITS_ROOT   = '/home/jwagner/FITS/pulsar/n15dh01/'

#   FITS_ROOT/n15dh01a_DiFX_normal.fits               has correlator output from non-pulsar DiFX processing
#   FITS_ROOT/n15dh01a.fixed.antab                    is the KVN ANTAB file with corrected R1,L1 pols
#   FITS_ROOT/binning-3.76s/n15dh01a.bin_<nr>.fits    are the DiFX pulsar binning mode FITS files, one per phase bin
#   FITS_ROOT/n15dh01a-normal-2phasecenter.fits       has correlator output from non-pulsar DiFX processing, 3.76s tInt
#   FITS_ROOT/n15dh01a-normal-2phasecenter-SgrA.fits  -"- with data on the additional phase center (Sgr A*)

T_INT = 3.76 # integration time used in correlator
SOURCE_FF   = 'NRAO530'
SOURCE_PREF = 'J1744-3116'	# ra = 17h44m23.5782350s; dec = -31d16'36.294330" in VEX file
SOURCE_PSR  = 'PSRJ1745-290'	# ra = 17h45m40.1929339s; dec = -29d00'30.277473" in VEX file
KVN_BEAM    = 6.0e-3            # 6 mas at 22 GHz

################################################################################################
# Table   Input   Applies Task    Description
# SN#1                    ACCOR   normalization with auto-corrs
# SN#2                    ACCOR   calibration with Tsys and gain curves
# SN#3                    FRING   delay on 1st NRAO530 scan
# SN#4                    FRING   delay and rate on NRAO530
# SN#5                    FRING   fring on  J1744-3116
# Table   Input   Applies Task    Description
# CL#2    CL#1    SN#1    CLCAL   normalization with auto-corrs
# CL#3    CL#2    SN#2    CLCAL   calibration with Tsys and gain curves
# CL#4    CL#3            CLCOR   parallactic angle correction
# CL#5    CL#4            TECOR   TEC correction
# CL#6    CL#5    SN#3    CLCAL   delay on 1st NRAO530 scan (interpol=2pt)
# CL#7    CL#6    SN#4    CLCAL   delay and rate on NRAO530 (interpol=ambg)
# CL#8    CL#7    SN#5    CLCAL   fring on J1744-3116 (interpol=2pt)
################################################################################################

################################################################################################

def process_normal(reload=False):
	"""Load normal (non-pulsar) DiFX output data FITS file and go through
	   basic calibration steps. Also do phase calibration using the phase
	   reference source, and for comparison, also do phase self-calibration
	   on only Sgr A* as well.
	"""

	## Process the NRAO 530 + J1744-3116 + PSRJ1745-290 data
	uv = AIPSUVData(EXPT.upper()+'N','UVDATA',AIPS_DISK,1)
	if reload or not uv.exists():
		fin = FITS_ROOT + EXPT + '-normal-2phasecenter.fits'
		uv = loadFITS(fin, EXPT.upper()+'N', 1, overwrite=True, clint=T_INT/60.0)
	if True:
		# Calibrate the data
		process_normal_basic(uv)
		process_normal_phaseref(uv)
		process_normal_imagr(uv)
		process_normal_export(uv)

		# Summary of tables
		printTableLog(getSNTableLog())
		printTableLog(getCLTableLog())
		setSNTableLog({})
		setCLTableLog({})

	return

	## Process the Sgr A* -only data of the 2nd phase center
	uv2 = AIPSUVData(EXPT.upper()+'N2','UVDATA',AIPS_DISK,1)
	if reload or not uv2.exists():
		fin = FITS_ROOT + EXPT + '-normal-2phasecenter-SgrA.fits'
		uv2 = loadFITS(fin, EXPT.upper()+'N2', 1, overwrite=True, clint=T_INT/60.0)
		indxr = makeTask('INDXR', ['indata',uv2])
		indxr.cparm[3] = T_INT/60.0
		execTask(indxr)
	if True:
		# Calibrate the data
		# First: basic calibration
		process_normal_basic(uv2)
		# Next : copy the existing delay and phase ref solutions, apply to Sgr A*
		# Out     In      Solns
		# CL#6    CL#5    SN#3    CLCAL   delay on 1st NRAO530 scan (interpol=2pt)
		# CL#7    CL#6    SN#4    CLCAL   delay and rate on NRAO530 (interpol=ambg)
		# CL#8    CL#7    SN#5    CLCAL   fring on J1744-3116 (interpol=2pt)
		for ii in range(uv.table_highver('BP')+1):
			# Note: copyTable(uvsrc,uvdst,inext,invers)
			copyTable(uv, uv2, 'BP', ii, doLog=True)
		for ii in range(3, uv.table_highver('SN')+1):
			copyTable(uv, uv2, 'SN', ii, doLog=True)
		doClCal(uv2, snver=3, clin=5, interpol='2pt',  refant=REF_ANT, msg='delay on 1st %s scan' % (SOURCE_FF))
		doClCal(uv2, snver=4, clin=6, interpol='ambg', refant=REF_ANT, msg='delay and rate on %s' % (SOURCE_FF))
		doClCal(uv2, snver=5, clin=7, interpol='2pt',  refant=REF_ANT, msg='fring on %s' % (SOURCE_PREF))

		# Summary of tables
		printTableLog(getSNTableLog())
		printTableLog(getCLTableLog())

	return uv

def process_normal_basic(uv):

	# Delete any earlier SN, TY, GC, and CL (except CL#1) tables
	purgeTables(uv)

	# ACCOR
	doAccor(uv)
	
	# Load Tsys, gain curve
	fantab = FITS_ROOT + EXPT + '.fixed.antab'
	antab = makeTask('ANTAB', ['indata',uv, 'calin',fantab, 'tyver',1, 'gcver',1, 'blver',-1])
	execTask(antab)

	# APCAL
	doApcal(uv, solint=4.0*T_INT/60.0)

	# CLCOR 'PANG' for all antennas (any VERA antennas should be excluded here!)
	doPAng(uv, antennas=[0])

	# TECOR
	calibTEC_VLBA(uv)

def process_normal_phaseref(uv):

	# FRING delays only using 1 scan
	N = 4 # select one scan N>=1 as any out of 6 pointings
	if EXPT=='n15dh01e':
		N = 2
	nx    = getSourceNX(uv, SOURCE_FF, N=N) 
	clver = uv.table_highver('CL')
	snver = uv.table_highver('SN')+1
	fring = makeTask('FRING', ['indata',uv, 'calsour',AIPSList([SOURCE_FF]), 'docalib',1, 'gainuse',clver])
	fring = updateTask(fring, ['refant',REF_ANT, 'snver',snver, 'doapply',-1, 'solint',0])
	fring.aparm    = AIPSList([2,0,0,0,0,0,0,0,0])
	# fring.aparm[5] = 1 # average LL+RR
	fring.aparm[6] = 2 # verbose output
	fring.dparm    = AIPSList([0,300,300,0])
	fring.dparm[8] = 5 # zero rates and phases, keep delays
	fring.timerang = getTimeRange(nx['time'], nx['time'] + nx['time_interval'])
	execTask(fring)
	logTableEdit('fring', 'sn', 'delay on 1st %s scan' % (SOURCE_FF), outver=snver)

	# CLCAL of FRING solutions
	doClCal(uv, snver=snver, clin=clver, interpol='2pt', refant=REF_ANT, msg='delay on 1st %s scan' % (SOURCE_FF))

	# FRING for delays and rates over the course of the experiment
	clver = uv.table_highver('CL')
	snver = uv.table_highver('SN')+1
	fring = makeTask('FRING', ['indata',uv, 'calsour',AIPSList([SOURCE_FF]), 'docalib',1, 'gainuse',clver])
	fring = updateTask(fring, ['refant',REF_ANT, 'snver',snver, 'doapply',-1, 'solint',0])
	# fring.aparm[1] = 0 # average LL,RR
	fring.aparm[6] = 2 # verbose output
	fring.dparm    = AIPSList([0,300,300,0])
	fring.dparm[8] = 4 # zero phases, keep rates and delays
	execTask(fring)
	logTableEdit('fring', 'sn', 'delay and rate on %s' % (SOURCE_FF), outver=snver)

	# CLCAL of FRING solutions
	if True:
		doClCal(uv, snver=snver, clin=clver, interpol='ambg', refant=REF_ANT, msg='delay and rate on %s' % (SOURCE_FF))

	# BPASS
	bpver = calibBandpass(uv, bpsource=SOURCE_FF, doPhase=False)
	bpver = 2 # bandpass over full timerange (#2), or individual scans (#1)

	# FRING or CALIB on phase reference
        clver = uv.table_highver('CL')    # use same CL# in this CALIB and the CALIB further below
	snver = uv.table_highver('SN')+1
	if True:
		fring = makeTask('FRING', ['indata',uv, 'calsour',AIPSList([SOURCE_PREF]), 'docalib',1, 'gainuse',clver])
		fring = updateTask(fring, ['doband',1, 'bpver',bpver, 'solint',0, 'snver',snver, 'refant',REF_ANT])
		fring.aparm[6] = 3   # verbosity
		fring.dparm[2] = 100 # delay search window of +-100 nanosec
		execTask(fring)
		logTableEdit('fring', 'sn', 'fring on  %s' % (SOURCE_PREF), outver=fring.snver)
		doClCal(uv, snver=fring.snver, clin=clver, interpol='2pt', refant=REF_ANT, msg='fring on %s' % (SOURCE_PREF))
	else:
		calib = makeTask('CALIB', ['indata',uv, 'calsour',AIPSList([SOURCE_PREF]), 'docalib',1, 'gainuse',clver])
		calib = updateTask(calib, ['doband',1, 'bpver',bpver, 'solint',0, 'snver',snver, 'refant',REF_ANT])
		calib.smodel = AIPSList([0.6,0,0])
		calib.solmode = 'P'
		calib.soltype = 'L1R'
		calib.aparm[6] = 3 # verbosity
		execTask(calib)
		logTableEdit('calib', 'sn', 'calib on  %s' % (SOURCE_PREF), outver=fring.snver)
		doClCal(uv, snver=fring.snver, clin=clver, interpol='2pt', refant=REF_ANT, msg='calib on %s' % (SOURCE_PREF))

def process_normal_export(uv):

	# Export our calibration tables
	fnout = FITS_ROOT + EXPT + '-normal-2phasecenter.tasav.fits'
	try:
		os.remove(fnout)
	except:
		pass
	uv_save = AIPSUVData(uv.name,'TASAV',AIPS_DISK,1)
	if uv_save.exists():
		uv_save.zap()
	tasav = makeTask('TASAV', ['indata',uv, 'outdata',uv_save])
	execTask(tasav)
	fittp = makeTask('FITTP', ['indata',uv_save, 'dataout',fnout])
	execTask(fittp)

	# Make some plots
	snplt = makeTask('SNPLT', ['indata',uv, 'dotv',-1, 'nplots',6, 'doblank',1])
	snplt.inext = 'CL'
	snplt.invers = uv.table_highver('CL')
	snplt.optype = 'PHAS'
	snplt.pixrange = AIPSList([-180,180])
	execTask(snplt)
	snplt.inext = 'SN'
	snplt.invers = uv.table_highver('SN')
	execTask(snplt)
	snplt.optype = 'SNR'
	snplt.pixrange = AIPSList([0,0])
	execTask(snplt)
	snplt.optype = 'TSYS'
	snplt.inext = 'TY'
	snplt.invers = 1
	execTask(snplt)

	vplot = makeTask('VPLOT', ['indata',uv, 'dotv',-1, 'nplots',6])
	vplot.bparm = AIPSList([12,19,0]) # Time (IAT hours) x Elevation
	execTask(vplot)

	possm = makeTask('POSSM', ['indata',uv, 'dotv',-1, 'nplots',6, 'bpver',2])
	possm.aparm[7] = 1 # Hz for horizontal axis
	possm.aparm[8] = 2 # plot BP table
	possm.aparm[9] = 1
	execTask(possm)

	# Save plots to PostScript file
	ps_file = FITS_ROOT + EXPT + '_QSO.ps'
	try:
		os.remove(ps_file)
	except:
		pass
	lwpla = makeTask('LWPLA', ['indata',uv, 'plver',1, 'invers',uv.table_highver('PL')])
	lwpla.dparm[5] = 2 # paper orientation: landscape
	lwpla.outfile = ps_file
	execTask(lwpla)

def process_normal_imagr(uv):
	"""Image the sources in a uv dataset calibrated by process_normal()."""

	Nchmargin = 8
	clver = uv.table_highver('CL')
        bpver = uv.table_highver('BP')

	# Preparations for imaging
	Nch   = get_header(uv, 'FREQ')
	imagr = makeTask('IMAGR', ['indata',uv, 'docalib',1, 'gainuse',clver, 'doband',1, 'bpver',bpver])
	imagr = updateTask(imagr, ['doband',1, 'bpver',bpver, 'bchan',Nchmargin, 'echan',Nch-Nchmargin])
	imagr = updateTask(imagr, ['nchav',Nch-2*Nchmargin, 'chinc',Nch-2*Nchmargin, 'niter',400, 'stokes','I'])
	imagr.cellsize = AIPSList([KVN_BEAM/6.0,KVN_BEAM/6.0])
	imagr.imsize   = AIPSList([256,256])
	imagr.flux     = 10e-3  # stop cleaning when abs(residual)<imagr.flux[Jy]

	# Preparations for converting Image --> PL
	kontr = makeTask('KNTR',  ['docont',1, 'dogrey',1, 'dovect',-1, 'label',2, 'dotv',-1, 'cbplot',1])
	tacop = makeTask('TACOP', ['outdata',uv, 'inext','PL','invers',1, 'ncount',1, 'outvers',0])

	# Image the fringe finder
	src = SOURCE_FF
	im  = AIPSImage(src, 'ICL001', AIPS_DISK, 1)
	ib  = AIPSImage(src, 'IBM001', AIPS_DISK, 1)
	imagr.sources = AIPSList([src])
	imagr.outname = src; imagr.outseq = im.seq
	kontr.indata  = im
	tacop.indata  = im
	if im.exists():
		im.zap()
	if ib.exists():
		ib.zap()
	execTask(imagr)
	execTask(kontr)
	execTask(tacop)

	# Image the phase reference source
	src = SOURCE_PREF
	im  = AIPSImage(src, 'ICL001', AIPS_DISK, 1)
	ib  = AIPSImage(src, 'IBM001', AIPS_DISK, 1)
	imagr.sources = AIPSList([src])
	imagr.outname = src; imagr.outseq = im.seq
	kontr.indata  = im
	tacop.indata  = im
	if im.exists():
		im.zap()
	if ib.exists():
		ib.zap()
	execTask(imagr)
	execTask(kontr)
	execTask(tacop)

	# Image pulsar position (unlikely to detect anything! too much noise folded over pulses)
	src = SOURCE_PSR
	im  = AIPSImage(src, 'ICL001', AIPS_DISK, 1)
	ib  = AIPSImage(src, 'IBM001', AIPS_DISK, 1)
	imagr.sources = AIPSList([src])
	imagr.outname = src; imagr.outseq = im.seq
	kontr.indata  = im
	tacop.indata  = im
	if im.exists():
		im.zap()
	if ib.exists():
		ib.zap()
	execTask(imagr)
	execTask(kontr)
	execTask(tacop)

################################################################################################

def process_pulsarbinning(startbin=1,stopbin=64,uvcalib=None):
	"""Load all FITS files from DiFX pulsar binning mode correlation (one FITS per pulse phase bin)
	   and apply calibration tables from the normal (non-pulsar) DiFX correlated data set.
	   Then subtract a Sgr A* model/image, and image each pulsar phase bin.
	   Two sets of data/images are made: the first set is based on phase calibrations on the
	   phase reference source, the second set is based on phase calibrations on the
	   in-beam source Sgr A*.
	"""

	if uvcalib==None:
		uvcalib = AIPSUVData(EXPT.upper()+'NRM', 'UVDATA', AIPS_DISK, 1)
	if not uvcalib.exists():
		print ('Error: calibration input %s does not exist' % (uvcalib))
		return

	for bin in range(startbin,stopbin+1):

		uv = AIPSUVData('%sBIN'%(EXPT.upper()), 'UVDATA', AIPS_DISK, bin)
		if not uv.exists():
			fin = '%s/%s/%s.bin_%d.fits' % (FITS_ROOT,'binning-3.76s',EXPT,bin-1)
			uv = loadFITS(fin, EXPT.upper()+'BIN', bin, overwrite=True)

		# Delete any earlier SN, TY, GC, and CL (except CL#1) tables
		purgeTables(uv)

		# Copy calibrations from main uv dataset
		copyTable(uvcalib, uv, 'BP', 1)
		copyTable(uvcalib, uv, 'BP', 2)
		copyTable(uvcalib, uv, 'CL', 8) # phase calibrated on J1744-3116
		copyTable(uvcalib, uv, 'CL', 9) # phase calibrated on Sgr A*
		bpver = 2  # bandpass BP over full timerange (#2), or individual scans (#1)

		# Use SPLAT/SPLIT to apply calibrations, and make single-source uv set needed for UVSUB
		uv1 = AIPSUVData('%sB-P'%(EXPT.upper()), 'SPLIT', AIPS_DISK, bin)
		uv2 = AIPSUVData('%sB-S'%(EXPT.upper()), 'SPLIT', AIPS_DISK, bin)
		if uv1.exists():
			uv1.zap()
		if uv2.exists():
			uv2.zap()
		splat = makeTask('SPLAT', ['indata',uv, 'sources',AIPSList([SOURCE_PSR])])
		splat = updateTask(splat, ['stokes','FULL', 'doband',1, 'bpver',bpver, 'docalib',1])
		splat.aparm[5] = 1  # pass auto and cross-power data
		splat.aparm[6] = 1  # include name of source in header
		splat.aparm[7] = 1  # make single-source file
		splat.outdata = uv1
		splat.gainuse = 8
		execTask(splat)
		splat.outdata = uv2
		splat.gainuse = 9
		execTask(splat)

		# Use UVSUB to remove Sgr A*
		img1    = AIPSImage('SGRA-INBM-P', 'ICL001', AIPS_DISK, 1)
		img2    = AIPSImage('SGRA-INBM-S', 'ICL001', AIPS_DISK, 1)
                uv_pref = AIPSUVData(uv1.name, 'UVDATA', AIPS_DISK, bin)
                uv_self = AIPSUVData(uv2.name, 'UVDATA', AIPS_DISK, bin)
		if not (img1.exists() and img2.exists()):
			print ('Could not find CC of Sgr A*. Run process_normal_imagr() first.')
			return
		if uv_pref.exists():
			uv_pref.zap()
		if uv_self.exists():
			uv_self.zap()
		uvsub1 = makeTask('UVSUB', ['indata',uv1, 'in2data',img1, 'outdata',uv_pref])
		uvsub2 = makeTask('UVSUB', ['indata',uv2, 'in2data',img2, 'outdata',uv_self])
		execTask(uvsub1)
		execTask(uvsub2)

		# Imaging setup
		Nchmargin = 8
		Nch   = get_header(uv_pref, 'FREQ')
		imagr = makeTask('IMAGR', ['sources',AIPSList([SOURCE_PSR]), 'docalib',-1])
		imagr = updateTask(imagr, ['doband',-1, 'bchan',Nchmargin, 'echan',Nch-Nchmargin])
		imagr = updateTask(imagr, ['nchav',Nch-2*Nchmargin, 'chinc',Nch-2*Nchmargin, 'niter',100])
		imagr.cellsize = AIPSList([KVN_BEAM/4.0,KVN_BEAM/4.0])
		imagr.imsize = AIPSList([512,512])
		imagr.stokes = 'I'
		imagr.flux = 100e-3  # stop cleaning when abs(residual)<imagr.flux[Jy]

		# Imaging : phase-referenced
		imagr.indata  = uv_pref
		imagr.outname = SOURCE_PSR[0:8]+'-P'
		imagr.outseq  = bin
		deleteImage(imagr.outname, imagr.stokes, imagr.outseq)
		execTask(imagr)

		# Imaging : self-cal
		imagr.indata  = uv_self
		imagr.outname = SOURCE_PSR[0:8]+'-S'
		imagr.outseq = bin
		deleteImage(imagr.outname, imagr.stokes, imagr.outseq)
		execTask(imagr)

def process_pulsarbinning_combine(startbin=1,stopbin=64):
	"""Combine individual images into an image cube"""
	stokes = 'I'
	im_out = AIPSImage(SOURCE_PSR[0:8]+'PCB', stokes.upper()+'MCUBE', AIPS_DISK, 1)
	if im_out.exists():
		im_out.zap()
	im     = AIPSImage(SOURCE_PSR[0:8]+'-P', stokes.upper()+'CL001', AIPS_DISK, startbin)
	mcube  = makeTask('MCUBE', ['indata',im, 'in2seq',stopbin, 'in3seq',1, 'axref',startbin])
	mcube  = updateTask(mcube, ['ax2ref',stopbin, 'npoints',(stopbin-startbin+1), 'outdata',im_out])
	execTask(mcube)

	stokes = 'I'
	im_out = AIPSImage(SOURCE_PSR[0:8]+'SCB', stokes.upper()+'MCUBE', AIPS_DISK, 1)
	if im_out.exists():
		im_out.zap()
	im     = AIPSImage(SOURCE_PSR[0:8]+'-S', stokes.upper()+'CL001', AIPS_DISK, startbin)
	mcube  = makeTask('MCUBE', ['indata',im, 'in2seq',stopbin, 'in3seq',1, 'axref',startbin])
	mcube  = updateTask(mcube, ['ax2ref',stopbin, 'npoints',(stopbin-startbin+1), 'outdata',im_out])
	execTask(mcube)

def run():

	init()
	Nbins = 64

	# Processing
	uvcalib = AIPSUVData(EXPT.upper()+'N', 'UVDATA', AIPS_DISK, 1)

	process_normal()
	# process_normal_imagr(uvcalib)
	# process_pulsarbinning(stopbin=Nbins,uvcalib=uvcalib)
	# process_pulsarbinning_combine(stopbin=Nbins)


run()

