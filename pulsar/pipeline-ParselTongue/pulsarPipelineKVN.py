#!/bin/python
#
# Pipeline utilities for DiFX correlator pulsar FITS file processing (KVN in particular)
# (C) 2015 Jan Wagner
#
# ParselTongue examples:
# http://www.jive.nl/~huib/Archive/adassxv_pt.pdf
# http://www.jive.nl/dokuwiki/doku.php?id=parseltongue:grimoire

from AIPS import AIPS
from AIPSTask import AIPSTask, AIPSList
from AIPSData import AIPSUVData, AIPSImage
from Wizardry.AIPSData import AIPSUVData as WizAIPSUVData
from Wizardry.AIPSData import AIPSImage as WizAIPSImage
import datetime, math, os, re, subprocess, sys
import urllib

######################################################################################################
## LIBRARY INIT
######################################################################################################

_pulsarlog = None
_aipslogfile = None
_tasklogfile = None
AIPS_DISK = 1

# Initialize the pulsar pipeline
def init(pulsarlogfile='pulsartables.log',aipslogfile='aips.log',tasklogfile='taskparm.log'):
	global _pulsarlog
	global _aipslogfile
	global _tasklogfile
	buffering    = 0 # Python: 0=unbuffered, 1=line buffered, >1=bufsize, <0=OS default
	_pulsarlog   = open(pulsarlogfile,  'a', buffering)
	_aipslogfile = open(aipslogfile, 'a', buffering)
	_tasklogfile = open(tasklogfile, 'a', buffering)
	AIPS.log = _aipslogfile

######################################################################################################
## LOGGING HELPERS
######################################################################################################

def mprint(s):
	try:
		_aipslogfile.seek(0, 2)  #whence=2=end
		_aipslogfile.write(str(s) + '\n')
	except:
		pass
	print str(s)

######################################################################################################
## MISC HELPERS
######################################################################################################

# Returns copy of input 'v' ensuring it is always in Python list format
def always_list(v):
	if (type(v)==type([])):
		return v
	return [v];

# Returns value from UV dataset header
def get_header(uv, name):
	if not(name in uv.header.ctype):
		return None
	i = uv.header.ctype.index(name)
	return uv.header.naxis[i]

# Returns table numbers with defaults replaced, deletes existing output
def apply_tableversion_defaults(uv, tabname, inver=0, outver=0, dodel=True):
	if (inver==0):
		inver = uv.table_highver(tabname.upper())
	if (outver==0):
		outver = uv.table_highver(tabname.upper()) + 1
	if dodel:
		deleteTable(uv, tabname, outver)
	return (inver,outver)

######################################################################################################
## EASY AIPS TASK WRAPPERS
######################################################################################################

# Create new task with list of argument pairs
def makeTask(taskname, arglist=[]):
	t = AIPSTask(taskname)
	N = len(arglist)/2
	for ii in range(N):
		key = arglist[2*ii+0]
		val = arglist[2*ii+1]
		setattr(t, key, val)
	return t

# Update task arguments
def updateTask(t, arglist=[]):
	N = len(arglist)/2
	for ii in range(N):
		key = arglist[2*ii+0]
		val = arglist[2*ii+1]
		setattr(t, key, val)
	return t

# Execute task with arguments logging
def execTask(t):
	s = str(t._name).upper() + ' on ' + str(datetime.datetime.utcnow()) + ' UT'
	s = s.center(80, '-')
	_tasklogfile.write(s + '\n')
	for key in t._input_list:
		v = t.__dict__[key]
		if (type(v)==type('')):
			if (v == ''):
				s = "%-10s = ''" % (str(key))
			else:
				s = "%-10s = '%s'" % (str(key),v)
		else:
			# try to parse and shorten arrays to AIPS-style compact printout without tailing duplicates,
			# a bit tricky since type(task.someArray)==type(Task.List) which is neither [] nor AIPSList
			# could use: if (str(type(v))=="<class 'Task.List'>")
			try:
				if (len(v)>1) and (v[0] == None):
					# got an AIPSList
					v = v[1:]   # trim 1st entry of None
					nkeep = len(v)
					for ii in range((nkeep-1),-1,-1):
						if (v[ii] == v[-1]):
							nkeep = nkeep -1
						else:
							break
					v = v[:(nkeep+1)]
			except:
				pass
			s = "%-10s = %s" % (str(key), str(v))
		_tasklogfile.write(s + '\n')
	_tasklogfile.flush()
	try:
		t()
		_tasklogfile.write(' -- completed\n')
	except:
		_tasklogfile.write(' -- FAILED!\n')
		sys.exit(-1)

# Remove all tables except CL#1 and PC#1
def purgeTables(uv):
	tasknames = ['sn','cl','bp','gc','ty','pc','pl']
	for tname in tasknames:
		tname = ('aips '+tname).upper()
		tlow  = 1
		thigh = uv.table_highver(tname)
		if (tname[-2:] in ['CL', 'PC']):
			tlow = 2
		for ver in range(tlow,thigh+1):
			if tableExists(uv, tname, ver):
				mprint ('Removing %s#%d' % (tname,ver))
				uv.zap_table(tname, ver)
			else:
				mprint ('Non-existent %s#%d' % (tname,ver))

# Check if given table exists in given version
# Uses only the last 2 characters of table names, thus "AIPS CL" is the same as "CL"
# Returns: True if exists
def tableExists(uv, tablename, ver):
	if (ver==0):
		return True
	tablename = str(tablename).upper()
	tablename = tablename[-2:]
	ver = int(ver)
	for t in uv.tables:
		currver = int(t[0])
		currname = str(t[1])
		currname = currname[-2:]
		if (currname==tablename) and (currver==ver):
			return True
	return False

# Delete a table
def deleteTable(uv, tablename, ver):
	tname = ('aips '+tablename[-2:]).upper()
	if tableExists(uv, tablename, ver):
		mprint ('Removing %s#%d' % (tname,ver))
		uv.zap_table(tname, ver)
	else:
		mprint ('Removing of %s#%d not necessary, did not exist' % (tname,ver))
	if (tname=='SN') and (str(ver) in _tablelog_sn):
		del _tablelog_sn[str(ver)]
	if (tname=='CL') and (str(ver) in _tablelog_cl):
		del _tablelog_cl[str(ver)]

# Write a table to disk
def saveTable(uv, tablename, inver, filename):
	try:
		os.remove(filename)
	except:
		pass
	t = makeTask('tbout', ['indata',uv, 'inext',tablename.upper(), 'invers',inver, 'outtext',filename])
	execTask(t)

# Read a table from disk
def loadTable(uv, filename):
	#tname = ('aips '+tablename[-2:]).upper()
	t = makeTask('tbin', ['outdata',uv, 'intext',filename])
	execTask(t)

######################################################################################################
## COMMON AIPS TASKS
######################################################################################################

def loadFITS(filename,oname,oseq,overwrite=False,clint=5/60.0):

        uvout = AIPSUVData(oname,'UVDATA',AIPS_DISK,oseq)
        if uvout.exists() and overwrite:
                uvout.zap()

        fitld = makeTask('FITLD', ['datain',filename, 'outdata',uvout, 'digicor',1])
        fitld = updateTask(fitld, ['digicor',1, 'clint',clint])
        execTask(fitld)

        return uvout

def copyTable(uvsrc,uvdst,inext,invers,doLog=False):

        if tableExists(uvdst, inext, invers):
                deleteTable(uvdst, inext, invers)

        tacop = makeTask('TACOP', ['indata',uvsrc, 'outdata',uvdst, 'inext',inext, 'invers',invers])
        tacop = updateTask(tacop, ['outvers',invers, 'ncount',1, 'doflag',1])
        execTask(tacop)
	if doLog:
		ssrc = '%s.%s.%d.%d' % (uvsrc.name, uvsrc.klass, uvsrc.disk, uvsrc.seq)
		sdst = '%s.%s.%d.%d' % (uvdst.name, uvdst.klass, uvdst.disk, uvdst.seq)
		logTableEdit('tacop', inext, 'copied table %s --> %s' % (ssrc,sdst), outver=invers)

def deleteImage(name,pol,seq):
	disk = 1
	ai = AIPSImage(name, pol.upper()+'CL001', disk, seq)
	if ai.exists():
		ai.zap()
	ai = AIPSImage(name, pol.upper()+'BM001', disk, seq)
	if ai.exists():
		ai.zap()

def doAccor(uv):

	snver = uv.table_highver('SN') + 1
	accor = makeTask('ACCOR', ['indata',uv, 'solint',1.0])
	execTask(accor)
	logTableEdit('accor', 'sn', 'normalization with auto-corrs', outver=snver)

	(clin,clout) = apply_tableversion_defaults(uv, 'CL', 0, 0)
	clcal = makeTask('clcal', ['indata',uv, 'gainver',clin, 'gainuse',clout, 'snver',snver, 'invers',0])
	clcal = updateTask(clcal, ['interpol','self', 'opcode','CALP', 'refant',1])
	execTask(clcal)
	logTableEdit('clcal', 'cl', 'normalization with auto-corrs', inver=clin, outver=clout, auxtable='sn', auxver=snver)

def doClCal(uv, snver=0, clin=0, clout=0, msg='apply solutions',interpol='self',refant=1,calsour=AIPSList(['']),sources=AIPSList(['']), dobtween=1, doblank=1):
	(clin,clout) = apply_tableversion_defaults(uv, 'CL', clin, clout)
	if snver==0:
		snver = uv.table_highver('SN')
	clcal = makeTask('clcal', ['indata',uv, 'gainver',clin, 'gainuse',clout, 'snver',snver, 'invers',0])
	clcal = updateTask(clcal, ['interpol',interpol, 'opcode','CALI', 'refant',refant])
	clcal.calsour = calsour
	clcal.sources = sources
	clcal.doblank = doblank
	clcal.dobtween = dobtween
	execTask(clcal)
	logTableEdit('clcal', 'cl', '%s (interpol=%s)'%(msg,interpol), inver=clin, outver=clout, auxtable='sn', auxver=snver)

def doApcal(uv,solint=10/60.0):

        snver = uv.table_highver('SN') + 1
        apcal = makeTask('APCAL', ['indata',uv, 'tyver',1, 'gcver',1, 'snver',snver, 'solint',solint])
        apcal = updateTask(apcal, ['invers',-1, 'dofit',AIPSList([-1]), 'calin','', 'dotv',-1])
        execTask(apcal)
	logTableEdit('accor', 'sn', 'calibration with Tsys and gain curves', outver=snver)

	(clin,clout) = apply_tableversion_defaults(uv, 'CL', 0, 0)
	clcal = makeTask('clcal', ['indata',uv, 'gainver',clin, 'gainuse',clout, 'snver',snver, 'invers',0])
	clcal = updateTask(clcal, ['interpol','2pt', 'opcode','CALP', 'refant',1])
	execTask(clcal)
	logTableEdit('clcal', 'cl', 'calibration with Tsys and gain curves', inver=clin, outver=clout, auxtable='sn', auxver=snver)

# Applies parallactic angle correction.
# Be careful to exclude any VERA stations (they rotate their feeds and do not need PANG correction).
def doPAng(uv, clin=0, clout=0, antennas=[0]):

	Nstokes = get_header(uv, 'STOKES')
	if (Nstokes==1):
		mprint ('Multiple single-Stokes IFs instead of dual-Stokes IFs is currently not supported!')
		raise NameError('single-Stokes IFs are unsupported')

	(clin,clout) = apply_tableversion_defaults(uv, 'CL', clin, clout)

	# copy current CL table for self-updating
	tacop = makeTask('tacop', ['indata',uv, 'inext','CL', 'invers',clin, 'outvers',clout, 'doflag',1])
	execTask(tacop)

	# prepare parallactic angle correction (overwrites input CL!)
	clcor = makeTask('clcor', ['indata',uv, 'opcode','PANG', 'gainver',clout, 'gainuse',clout, 'bif',0, 'eif',0])
	clcor.clcorprm = AIPSList([1,0])  # 1,0 to add, 0,0 to subtract
	clcor.antennas = AIPSList(antennas)
	execTask(clcor)

	# remember edit
	logTableEdit('clcor', 'cl', 'parallactic angle correction', inver=clin, outver=clout)
	return clout

# Derives bandpass solution using auto correlations only. Writes only amplitude, no phase.
# Auto-correlations on the bandpass source are used for baseline amplitude removal.
# Cross-correlations on the maser source _could_ be used for phase, however so far the
# result has been mostly a corruption of the amplitudes incl stokes 'i' (=> BPASS bug?)
#
# Thus a complex bandpass solutions is _not_ derived here.
# Complex bandpass: http://veraserver.mtk.nao.ac.jp/VERA/kurayama/WinterSchool/aips6.htm
#
# Returns: new BP number
def calibBandpass(uv, clin=0, clout=0, bpout=0, refant=0, bpsource=[''], doPhase=True):

	(clin,clout) = apply_tableversion_defaults(uv, 'CL', clin, clout)
	if bpout==0:
		bpout = uv.table_highver('BP')
	if bpout==0:
		bpout = 1
	deleteTable(uv, 'BP', bpout)
	deleteTable(uv, 'BP', bpout + 1)

	# ensure bandpass source is list
	if not(type(bpsource)==type([])):
		bpsource = [bpsource]

	# first step: amplitude from auto-correlations

	# band pass over individual scans: user to verify BP vs time looks smooth
	bpass = makeTask('bpass', ['indata',uv, 'docalib',2, 'gainuse',clin, 'refant',refant, 'outvers',bpout])
	bpass = updateTask(bpass, ['soltype','L1R', 'calsour',AIPSList(bpsource), 'doband',-1, 'flagver',0])
	bpass.solint = 0        # 0 means per-scan, <0 means integrated
	bpass.bpassprm[1] = 1   # 1 = use only auto-corrs
	bpass.bpassprm[3] = 1   # do not divide by source model
	bpass.bpassprm[4] = -1  # write only amplitude terms, no phase terms
	bpass.bpassprm[5] = 1   # no division by channel 0 
	bpass.bpassprm[9] = 1   # interpolate over flagged channels if possible
	bpass.bpassprm[10] = 3  # normalize amp, zero average phase using ichansel channels
	bpass.ichansel[1] = AIPSList([0])   # default: range set to inner 75% of observing band
	bpass.smooth      = AIPSList([0])   # if smooting, would need later same settings for spectral line data
	execTask(bpass)

	# band pass over full time: to be applied to data later
	bpass = updateTask(bpass, ['solint',-1, 'outvers',(bpout+1)])
	execTask(bpass)

	if doPhase:
		# second step: phase on band pass over individual scans, needs user verification of results!

		bpass = updateTask(bpass, ['solint',0, 'outvers',bpout])
		bpass.bpassprm[1] = 0   # use not only auto-corrs
		bpass.bpassprm[4] = 1   # store phases only, not amplitudes
		execTask(bpass)

		bpass = updateTask(bpass, ['solint',-1, 'outvers',(bpout+1)])
		execTask(bpass)

	# remember table edits
	descr = 'two-step BP with amp=%s phas=%s' % (bpsource[0],bpsource[0])
	logTableEdit('bpass', 'bp', descr+' scan boundaries', outver=bpout)
	logTableEdit('bpass', 'bp', descr+' full time range', outver=(bpout+1))

	return (bpout+1)

######################################################################################################
## AIPS Procedures
######################################################################################################

# Applies TEC correction: VLBATECR taken from 31DEC12/RUN/VLBAUTIL.001
# Returns: new CL number
def calibTEC_VLBA(uv, clin=0, clout=0):
	vba_cr = int(uv.header['date_map'][:4])   # UV data creation date
	vba_yr = int(uv.header['date_obs'][:4])   # observation date
	vba_doy = -1
	if (vba_yr>1997) and (vba_yr<=vba_cr):
		vba_doy = getDOY(uv.header['date_obs'])
	else:
		return -3
	if (vba_doy<1) or (vba_doy>366):
		return -4
	if (vba_yr==1998) and (vba_dnum>151):
		return -5

	# start time, duration from NX#1 to get days spanned by experiment
	nxtab = uv.table('NX', 1)
	nxlast = nxtab[len(nxtab)-1]
	vba_days = float(nxlast['time'])
	vba_dur = 0.5*float(nxlast['time_interval'])
	vba_nfil = int(math.ceil(vba_days+vba_dur))

	# undocumented old data fix of the DOY
	if (vba_yr<2002) or (vba_yr==2002 and vba_dnum<305):
		if (((vba_days+vba_dur)-float(vba_nfil)+1.0)*24.0 > 23.0):
			vba_nfil = vba_nfil + 1
		nxfirst = nxtab[0]
		vba_days = float(nxfirst['time'])
		vba_dur = 0.5*float(nxfirst['time_interval'])
		if ((vba_days-vba_dur)*24.0 < 1.0):
			vba_nfil = vba_nfil + 1
			vba_doy = vba_doy - 1

	# downloading from URL of format ftp://cddis.gsfc.nasa.gov/gps/products/ionex/2006/063/jplg0630.06i.Z (063=doy)
	# and storing decompressed as "/tmp/CCCCdddC.yyC" where ddd=doy, yy=year, C=anything
	vba_yr_str = uv.header['date_obs'][:4]
	filelist = []
	for doy in range(vba_doy,vba_doy+int(vba_nfil)):

		# fetch
		url = 'ftp://cddis.gsfc.nasa.gov/gps/products/ionex/'
		url = url + ('%u/%03u/jplg%03u0.%si.Z' % (vba_yr, doy, doy, vba_yr_str[2:4]))
		outfile = '/tmp/CCCC%03dC.%2sC' % (doy, vba_yr_str[2:4])
		tmpfile = outfile + '.gz'

		mprint ('Downloading %s to %s ...' % (url,tmpfile))
		urllib.urlretrieve(url, tmpfile)
		if not (os.path.isfile(tmpfile)):
			mprint ('Download of ' + url + ' failed!')
			return -1

		# decompress /tmp/xxx.gz into /tmp/xxx using gzip
		sp = subprocess.Popen(['gzip', '-fd', tmpfile])
		sp.wait()
		if not (os.path.isfile(outfile)):
			mprint ('Decompression into ' + outfile + ' failed!')
			return -2
		filelist.append(outfile)

	(clin,clout) = apply_tableversion_defaults(uv, 'CL', clin, clout)

	# finally, apply downloaded data
	t = makeTask('tecor', ['indata',uv, 'nfiles',vba_nfil, 'infile',filelist[0], 'gainver',clin, 'gainuse',clout, 'subarr',1])
	t.aparm = AIPSList([1,0]) # dispersive delay correction on, ionosphere earth co-rotation on (solar co-rotation correction off)
	execTask(t)

	# remove temporary files
	for fn in filelist:
		os.remove(fn)

	logTableEdit('tecor', 'cl', 'TEC correction', inver=clin, outver=clout)
	return clout

######################################################################################################
## TABLE EDIT BOOKKEEPING
######################################################################################################

_tablelog_cl = {}
_tablelog_sn = {}

def getCLTableLog():
	global _tablelog_cl
	return _tablelog_cl

def setCLTableLog(cllog):
	global _tablelog_cl
	_tablelog_cl = cllog

def getSNTableLog():
	global _tablelog_sn
	return _tablelog_sn

def setSNTableLog(snlog):
	global _tablelog_sn
	_tablelog_sn = snlog

# natural sort key aka alphanumeric sort
tokenize_regexp = re.compile(r'(\d+)|(\D+)').findall
def natural_sortkey(string):
	return tuple(int(num) if num else alpha for num, alpha in tokenize_regexp(string))

# Pretty-print a sorted list of calibration or SN table entries with descriptions
def printTableLog(t):
	if (t==None) or (len(t)<=0):
		mprint ('(empty table history)')
		return None
	mprint ('Table\tInput\tApplies\tTask\tDescription')
	for key in sorted(t.iterkeys(), key=natural_sortkey):
		e = t[key]
		mprint ('%s\t%s\t%s\t%s\t%s' % (e['out'], e['in'], e['use'], e['task'], e['desc']))

# Store a description for a new SN or CL or other table edit
def logTableEdit(task='none', table='na', desc='', inver=-1, outver=0, auxtable='na', auxver=-1):
	global _tablelog_cl
	global _tablelog_sn

	task = str(task).upper()
	table = (str(table)[-2:]).upper()
	auxtable = (str(auxtable)[-2:]).upper()

	# text version of log entry
	if (inver >= 0):
		if (auxver < 0):
			ostr = '%s#%d : %s using %s#%d : %s' % (table,outver,task,table,inver,desc)
		else:
			ostr = '%s#%d : %s from %s#%d using %s#%d : %s' % (table,outver,task,table,inver,auxtable,auxver,desc)
	else:
		if (auxver < 0):
			ostr = '%s#%d : %s : %s' % (table,outver,task,desc)
		else:
			ostr = '%s#%d : %s using %s#%d : %s' % (table,outver,task,auxtable,auxver,desc)

	# show and write to file
	mprint (ostr + '\n')
	if not(_pulsarlog==None):
		_pulsarlog.write(ostr + '\n')

	# also make SN/CL dictionary entry
	entry = {}
	entry['task'] = task
	entry['desc'] = desc
	if (inver>=0):
		entry['in'] = ('%s#%d') % (table,inver)
	else:
		entry['in'] = ''
	if (outver>=0):
		entry['out'] = ('%s#%d') % (table,outver)
	else:
		entry['out'] = ''
	if (auxver>=0):
		entry['use'] = ('%s#%d') % (auxtable,auxver)
	else:
		entry['use'] = ''

	if (table=='SN'):
		_tablelog_sn[str(outver)] = entry
	if (table=='CL'):
		_tablelog_cl[str(outver)] = entry


######################################################################################################
## DATE-TIME CONVERSION
######################################################################################################

# Return Day-of-Year from given date
# Based on DAYNUM helper from 31DEC12/RUN/VLBAUTIL.001
def getDOY(vba_date):
	# parse vba_date format '2012-03-01'
	vba_yr = int(vba_date[:4])
	vba_mo = int(vba_date[5:7])
	vba_day = int(vba_date[8:10])
	if (vba_mo<1) or (vba_mo>12) or (vba_day<1) or (vba_day>31):
		return -1
	# {month, day} => {doy} with leap year correction
	vba_mnum = [0,31,59,90, 120,151,181,212, 243,273,304,334]
	vba_leap = 0
	if ((vba_yr % 4)==0) and (vba_mo>2):  # doesnt check the mod(400)-and-mod(100) condition
		vba_leap = 1
	vba_dnum = vba_mnum[vba_mo-1] + vba_day + vba_leap
	return vba_dnum

# Convert day fraction into hour, minute, seconds
# AIPS f=0 for 00:00:00s and f=1.0 for 24:00:00s
# Returns: dictionary with hour, minute and seconds entries
def getHMS(f):
	d = int(f)			# TODO: does f>1.0 really mean the following day...?
	f = f - float(d)
	h = math.floor(f*24.0)
	r = f*24.0 - h;
	m = math.floor(r*60.0);
	r = 60.0*r - m;
	s = math.floor(r*60.0 + 0.5);   # +0.5 due to some stupid AIPS thing
	hms = {}
	hms['day']  = d
	hms['hour'] = h
	hms['min']  = m
	hms['sec']  = s
	return hms

# Convert day fraction into hour, minute, seconds
# Returns: string in hh:mm:s.c format
def getHMSString(f):
	t = getHMS(f)
	tstr = '%u/%02u:%02u:%04.1f' % (t['day'],t['hour'],t['min'],t['sec'])
	return tstr

# Convert two day fractions into an AIPSList for the task.timerange parameter
def getTimeRange(f1, f2):
	if (f1==0) and (f2==0):
		return AIPSList([0])
	t1 = getHMS(f1)
	t2 = getHMS(f2)
	(d1,h1,m1,s1) = (t1['day'], t1['hour'], t1['min'], t1['sec'])
	(d2,h2,m2,s2) = (t2['day'], t2['hour'], t2['min'], t2['sec'])
	timerange = AIPSList([d1,h1,m1,s1, d2,h2,m2,s2])
	return timerange


######################################################################################################
## SINGLE SCAN AND SCAN BLOCK ACCESSORS
######################################################################################################
# Return the name of the source of a given NX scan number
def getNXSource(uv, nxindex):
	nxtab = uv.table('NX',0)
	sutab = uv.table('SU',0)
	try:
		sourceid = int(nxtab[nxindex-1]['source_id'])
		return str(sutab[sourceid-1]['source']).strip()
	except:
		return ''
# Find and return the N:th (1...inf) NX scan entry for the given source name
def getSourceNX(uv, srcname, N=1):
	# find source ID
	sutab = uv.table('SU',0)
	suexists = False
	su_id = -1
	sulist = []
	mprint ('Searching SU, NX tables for source %s to get source ID and source scan #%u' % (srcname,N))
	for su in sutab:
		tabname = su['source'].strip()
		sulist.append(tabname)
		if tabname == srcname:
			suexists = True
			su_id = su['id__no']
			break
	if not suexists:
		mprint ('Could not find source %s in SU table.' % (srcname))
		mprint ('Valid sources are: %s' % (str(sulist)))
		return None
	# find the N:th NX entry for this source ID
	nxtab = uv.table('NX',0)
	nxscan = 0
	nxsrc = None
	nxidx = 1
	for nx in nxtab:
		if nx['source_id']==su_id:
			nxscan = nxscan + 1
			if (nxscan==N):
				nxsrc = nx
				break
		nxidx = nxidx + 1
	# final result
	if (nxscan<=0):
		mprint ('Could not find any scan on source %s in NX table!' % (srcname))
	if (nxsrc==None):
		mprint ('Could not find scan %u out of %u scans on source %s in NX table!' % (N,nxscan,srcname))
	else:
		mprint ('Source %s scan %u selected, nx index %u' % (srcname,nxscan,nxidx))
	return nxsrc
# Detect geodetic blocks, interpreted as scan blocks separated by 1 hour or more. Returns: list of 
# [Tstart,Tduration,Nstart,Nstop] pairs for each block Returned times are 0.0-1.0 day time fractions and scan numbers N are 
# 1...inf
def getGeodeticBlocks(uv):
	geoblocks = []
	Tthreshold = 1.0/24 # scan gaps of 1h interpreted as start of new geo block
	nxtab = uv.table('NX',0)
	if (len(nxtab)<=0):
		return None
	# get first few blocks
	newblock = True
	for ii in range(0,len(nxtab)-1):
		if newblock:
			Nblockstart = ii
			Tblockstart = nxtab[ii]['time']
			Tblockdur = nxtab[ii]['time_interval']
			newblock = False
		Tnextscan = nxtab[ii+1]['time']
		if (Tnextscan >= (Tblockstart + Tblockdur + Tthreshold)):
			block = [Tblockstart,Tblockdur,Nblockstart+1,ii+1]
			geoblocks.append(block)
			newblock = True
		else:
			Tblockdur = Tblockdur + nxtab[ii]['time_interval']
	# complete with last block
	block = [Tblockstart,Tblockdur,Nblockstart+1,len(nxtab)]
	geoblocks.append(block)
	# print list of blocks
	if True:
		for b in geoblocks:
			t1 = getHMSString(b[0])
			t2 = getHMSString(b[0]+b[1])
			mprint ('Found geo block: scan %2d to %2d: time %s to %s' % (b[2],b[3],t1,t2))
	return geoblocks

