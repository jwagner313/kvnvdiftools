mkdir -p bulletinb
cd ./bulletinb/
for ii in `seq 252 333`; do
	if [[ -e bulletinb.$ii ]]; then
		echo "Skipping existing bulletinb.$ii"
	else
		wget "http://hpiers.obspm.fr/iers/bul/bulb_new/bulletinb.$ii" -Obulletinb.$ii
	fi
done
cd ..

