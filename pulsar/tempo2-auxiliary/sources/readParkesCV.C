
#include "ClockCorrections/readParkesCV.h"
#include <cstdio>
#include <cmath>
#include <string>

namespace ClockCorrections
{

  ClockConversion
  readParkesCV(const std::string &fname, double clipval)
  {
    FILE *f = std::fopen(fname.c_str(), "r");
    if (!f)
      throw "Unable to open file";

    ClockConversion function("UTC(PKS)", "UTC(AUS)");
    function.add_dated_comment(std::string("Imported from file ")+fname);

    double mjd, offset_ns;
    char line[1024], *c;
    int iline=0;
    while (std::fgets(line, 1024, f))
    { 
      iline++;
      // check for comments
      for (c=line; *c && *c!='#'; c++)
	;
      if (*c=='\0') // not a comment
      {
	if (sscanf(line, "%*s %*s %lf %lf", &mjd, &offset_ns)!=2)
	{
	  std::fprintf(stderr, "Error parsing file %s, line %d\n",
		       fname.c_str(), iline);
	  std::exit(1);
	}
	if (std::fabs(offset_ns*1.0e-9) < clipval)
	  function.insert_value(std::pair<double,double>
				(mjd, offset_ns*-1.0e-9));
      }
    }

    std::fclose(f);

    return function;
  }
}
