#include <cstdio>
#include <cstdlib>
#include <cstring>

#include "ClockCorrections/readParkesGPS.h"
#include "ClockCorrections/readParkesCV.h"
#include "ClockCorrections/readCircularT.h"

using namespace ClockCorrections;

void
usage()
{
  std::fprintf(stderr, "Usage:\n");
  std::fprintf(stderr, "   update_clkcorr -t <transfer code> <input filenames>\n");
  std::fprintf(stderr, " Updates tempo2 clock correction files.\n");
  std::fprintf(stderr, " Specify the code for the transfer you wish to update:\n");
  std::fprintf(stderr, " Code         Input                        Output\n");
  std::fprintf(stderr, " ------------------------------------------------------------------\n");
  std::fprintf(stderr, " pksgps4      Parkes Mark IV clock logs    pks2gps.clk\n");
  std::fprintf(stderr, " pksgps6      Parkes Mark VI clock logs    pks2gps.clk\n");
  std::fprintf(stderr, " pkscv        Parkes->NML CommonView logs  pks2aus.clk\n");
  std::fprintf(stderr, " ausutc       BIPM Circular T files        aus2utc.clk\n");
  std::fprintf(stderr, " nistutc      BIPM Circular T files        nist2utc.clk\n");
  std::fprintf(stderr, " gpsutc       BIPM Circular T files        gps2utc.clk\n");

  std::fprintf(stderr, "\n Updated file appears in $TEMPO2/clock/<see above>.clk.new");
  std::fprintf(stderr, " Check the output for sanity, then copy it to $TEMPO2/clock/<see above>.clk\n");
  std::exit(1);
}

int 
main(int argc, char *argv[])
{
  // get -t argument
  if (argc < 4 || strcmp(argv[1], "-t"))
    usage();
  std::string transfer = argv[2];

  // get associated name of clock file
  char *clkname;
  if (transfer == "pksgps4" || transfer == "pksgps6")
    clkname = "pks2gps";
  else if (transfer == "pkscv")
    clkname = "pks2aus";
  else if (transfer == "ausutc")
    clkname = "aus2utc";
  else if (transfer == "nistutc")
    clkname = "nist2utc";
  else if (transfer == "gpsutc")
    clkname = "gps2utc";
  else
    usage();

  // Read in original set of corrections
  char ifname[1024];
  std::sprintf(ifname, "%s/clock/%s.clk", std::getenv("TEMPO2"), clkname);
  ClockConversion conversion(ifname);

  // Read in updates and merge them
  int i;
  for (i=3; i < argc; i++)
  {
    ClockConversion update;
    if (transfer == "pksgps4")
    {
      update = readParkesGPS4(argv[i]);
      update.scrunch(1.0);
    }
    else if (transfer == "pksgps6")
    {
      update = readParkesGPS6(argv[i]);
      update.scrunch(1.0);
    }
    else if (transfer == "pkscv")
    {
      update = readParkesCV(argv[i]);
      update.resample(1.0);
    } 
    else if (transfer == "ausutc")
      update = readCircularT_AUS(argv[i]);
    else if (transfer == "nistutc")
      update = readCircularT_NIST(argv[i]);
    else if (transfer == "gpsutc")
      update = readCircularT_GPS(argv[i]);

    conversion.merge(update, 0.1); // throw away samples < 0.1 d apart 
  }

  char ofname[1024];
  std::sprintf(ofname, "%s.new", ifname);
  conversion.save(ofname);
    
  return 0;
}
