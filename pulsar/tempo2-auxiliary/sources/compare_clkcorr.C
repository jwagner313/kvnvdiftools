// redwards program to compare clock corrections using different
// sequences of conversion

#include <cstdio>
#include <unistd.h>
#include <string>

#include "ClockCorrections/ClockConversionChain.h"

using namespace ClockCorrections;

void
usage()
{
  std::fprintf(stderr, "Usage:\n");
  std::fprintf(stderr, "compare_clkcorr [options]\n");
  std::fprintf(stderr, "     -m \"mjd1 mjd2 [mjd_step]\"                       Range/spacing of data\n");
  std::fprintf(stderr, "     -c \"clock_from clkfile1 ... clkfileN clock_to\"\n");
  std::fprintf(stderr, "         --- Specify a conversion chain\n");
  std::fprintf(stderr, "Example (silly but illustrative):\n");
  std::fprintf(stderr, "compare_clkcorr -m \"51000 51100\" -c \"UtC(PKs) pks2gps gps2utc utc\" -c \"utc nist2utc nist\"\n");
  std::exit(1);
}


void
print_comparision(const std::vector<ClockConversionChain> &chains,
		  double mjd1, double mjd2, double mjd_step)
{
  double mjd;
  int ic, nc=chains.size();

  for (mjd=mjd1; mjd<=mjd2; mjd+=mjd_step)
  {
    printf("%11.5lf", mjd);
    for (ic=0; ic<nc; ic++)
      printf(" %12.5le", chains[ic].get_correction(mjd));
    printf("\n");    
  }
}

int
main(int argc, char *argv[])
{
  double mjd1=-1.0, mjd2, mjd_step=1.0;
  int c;
  std::vector<ClockConversionChain> chains;

  while ((c=getopt(argc, argv, "m:c:"))!=-1)
  {
    switch (c)
    {
      case 'm':
      {
	int narg = sscanf(optarg, "%lf %lf %lf", &mjd1, &mjd2, &mjd_step);
	if (narg < 2)
	  usage();
	if (narg == 2)
	  mjd_step = 1.0;
	break;
      }
      case 'c':
      {
	// break arg up into strings
	std::vector<std::string> strings;
	std::string arg(optarg);
	std::string::size_type p1, p2;
	std::string white(" \n\r\t");
	for (p1=arg.find_first_not_of(white);
	     p1!=std::string::npos;
	     p1=arg.find_first_not_of(white, p2))
	{
	  p2 =  arg.find_first_of(white,p1);
	  if (p2 == std::string::npos)
	  {
	    strings.push_back(arg.substr(p1, arg.length()-p1));
	    break;
	  }
	  else
	    strings.push_back(arg.substr(p1, p2-p1));
	}
	// save it
	chains.push_back(ClockConversionChain
			 (strings[0], strings[strings.size()-1],
			  std::vector<std::string>(strings.begin()+1,
						    strings.end()-1)));
	break;
      }
    default:
      usage();
      break;
     }

  }

  if (chains.size()==0)
    usage();

  if (mjd1 < 0.0)
  {
    std::vector<ClockConversionChain>::const_iterator ic;
    mjd1 = 0.0;
    mjd2 = 1e6;
    for (ic=chains.begin(); ic!=chains.end(); ic++)
    {
      mjd1 = std::max(mjd1, ic->get_mjd_start());
      mjd2 = std::min(mjd2, ic->get_mjd_end());
    }
  }

  print_comparision(chains, mjd1, mjd2, mjd_step);

  return 0;
}
