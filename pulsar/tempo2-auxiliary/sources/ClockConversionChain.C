
#include "ClockCorrections/ClockConversionChain.h"
#include <stdlib.h>

namespace ClockCorrections
{

  int cmp_nocase(const std::string& s1, const std::string& s2);

  ClockConversionChain::ClockConversionChain
  (const std::string &clockFrom_, 
   const std::string &clockTo_,
   const std::vector<std::string> &fnames)
    : clockFrom(clockFrom_), clockTo(clockTo_)
  {
    std::vector<std::string>::const_iterator i;

    for (i=fnames.begin(); i!=fnames.end(); i++)
      chain.push_back(ClockConversion
		      (std::string(getenv("TEMPO2"))+"/clock/"+*i+".clk"));
  }

  double
  ClockConversionChain::get_correction(double mjd) const
  {
    std::string clock = clockFrom;
    double value=0.0;
    std::vector<ClockConversion>::const_iterator ic;

    for (ic=chain.begin(); ic!=chain.end(); ic++)
    {
      bool backwards = cmp_nocase(clock, ic->clockFrom);
//       if (backwards) printf(" Backwards ");
      if (backwards && cmp_nocase(clock, ic->clockTo))
      {
	std::fprintf(stderr, "Error, the specified clock conversion chain (%s->%s) is not linked!\n", clockFrom.c_str(), clockTo.c_str());
	std::exit(1);
      }
      value += ic->evaluate(mjd, ClockConversion::IS_Linear, true)
	* (backwards ? -1.0 : 1.0);
      clock = (backwards ? ic->clockFrom : ic->clockTo);
    }
    if (cmp_nocase(clock, clockTo))
    {
	std::fprintf(stderr, "Error, the specified clock conversion chain (%s->%s) is not linked!\n", clockFrom.c_str(), clockTo.c_str());
      std::exit(1);
    }
    return value;
  }

  double
  ClockConversionChain::get_mjd_start() const
  {
    double mjd = 0.0;
    std::vector<ClockConversion>::const_iterator ic;

    for (ic=chain.begin(); ic!=chain.end(); ic++)
      mjd = std::max(mjd, ic->get_mjd_start());
    
    return mjd;
  }

  double
  ClockConversionChain::get_mjd_end() const
  {
    double mjd = 1e6;
    std::vector<ClockConversion>::const_iterator ic;

    for (ic=chain.begin(); ic!=chain.end(); ic++)
      mjd = std::min(mjd, ic->get_mjd_end());
    
    return mjd;
  }
}
