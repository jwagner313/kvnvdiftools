//-*-C++-*-
#ifndef CLOCKCONVERSION_H
#define CLOCKCONVERSION_H

#include "ClockCorrections/NUSFunction.h"

#include <string>
#include <vector>

namespace ClockCorrections
{
  // class to embody a clock correction function
  // In addition to NUSFunction, it supplies recording of:
  //    * names of clocks involved
  //    * comments / processing history
  // and does I/O in a sensible format

  class ClockConversion : public NUSFunction<double,double>
  {
  public:
    ClockConversion() : NUSFunction<double,double>() {}
    ClockConversion(const std::string &clockFrom,
		    const std::string &clockTo);
    ClockConversion(const std::string &fname);
    
    void load(const std::string &fname);
    void save(const std::string &fname) const;
    
    void merge(const ClockConversion &in,
	       double min_distance=0.0);

    void add_dated_comment(const std::string &comment);

    double get_mjd_start() const;
    double get_mjd_end() const;



    std::string clockFrom, clockTo, badness;
    std::vector<std::string> comments;
  };
}

#endif
