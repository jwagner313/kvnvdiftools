//-*-C++-*-
#ifndef readParkesCV_h
#define readParkesCV_h

#include <string>
#include "ClockCorrections/ClockConversion.h"

namespace ClockCorrections
{

  ClockConversion readParkesCV(const std::string &fname,
                                double clipval=1.0e-4);
}



#endif
