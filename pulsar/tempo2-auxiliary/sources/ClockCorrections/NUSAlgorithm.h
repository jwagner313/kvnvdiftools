//-*-C++-*-
#ifndef NUSALGORITHM_H
#define NUSALGORITHM_H

#include "ClockCorrections/NUSFunction.h"

namespace ClockCorrections
{

  // Calculates the  Lomb-Scargle periodogram of a NUS Function
  // Directly borrowed from Numerical::LombScargle (utils/nr++)
  template <class XT, class YT>
  void LombScargle(const NUSFunction<XT,YT> &input,
		   NUSFunction<XT,YT> &output,
		   float oversampling=4.0, float overnyquist=4.0)
  {
    const std::vector<std::pair<XT, YT> > &in = input.getValues();

    unsigned npt = in.size();
    unsigned ipt = 0;
      
    if (npt < 2)
      return;
      
    unsigned nvals = unsigned (0.5 *oversampling *overnyquist *(float)npt);
    output.clear();
    
    XT min = 
      std::min_element(in.begin(), in.end(), LessThanPX<XT,YT>())->first;
    XT max = 
      std::max_element(in.begin(), in.end(), LessThanPX<XT,YT>())->first;
    YT avg = in[0].second;
      
    for (ipt=1; ipt < npt; ipt++)
      avg += in[ipt].second;
      
    avg /= double(npt);
      
    XT range = max - min;
    XT mid = 0.5 * (max + min);
      
    double interval = 1.0/((double)range*oversampling);
      
    double* wpr = new double [npt];
    double* wpi = new double [npt];
    double* wr = new double [npt];
    double* wi = new double [npt];
      
    for (ipt=0; ipt < npt; ipt++) {
      double arg = 2.0 * M_PI * ((double)(in[ipt].first - mid) * interval);
      double sinarg = sin (0.5*arg);
      wpr[ipt] = -2.0 * sinarg * sinarg;
      wpi[ipt] = sin(arg);
      wr[ipt]  = cos(arg);
      wi[ipt]  = wpi[ipt];
    }
      
    double freq = interval;
      
    for (unsigned ival = 0; ival < nvals; ival++) {
	
      double sumsh = 0.0;
      double sumc = 0.0;
	
      for (ipt=0; ipt < npt; ipt++) {
	double c = wr[ipt];
	double s = wi[ipt];
	sumsh += s*c;
	sumc += (c-s)*(c+s);
      }
	
      double wtau = 0.5 * atan2 (2.0*sumsh, sumc);
      double swtau = sin(wtau);
      double cwtau = cos(wtau);
	
      sumc = 0.0;
      double sums = 0.0;
      double sumsy= 0.0;
      double sumcy= 0.0;
	
      for (ipt=0; ipt < npt; ipt++) {
	double c = wr[ipt];
	double s = wi[ipt];
	  
	double cc = c*cwtau + s*swtau;
	double ss = s*cwtau - c*swtau;
	  
	sums += ss*ss;
	sumc += cc*cc;
	  
	YT yoff = in[ipt].second - avg;
	  
	sumsy += yoff*ss;
	sumcy += yoff*cc;
	  
	double wtemp = wr[ipt];
	wr[ipt] += (wtemp*wpr[ipt] - wi[ipt]*wpi[ipt]);
	wi[ipt] += (wi[ipt]*wpr[ipt] + wtemp*wpi[ipt]);
      }
	
      output.insert_value(std::make_pair
			  ((XT)freq, 
			   0.5 * (sumcy*sumcy/sumc + sumsy*sumsy/sums)));
	
      freq += interval;
    }
    delete [] wpr;
    delete [] wpi;
    delete [] wr;
    delete [] wi;
  }
  
}



#endif
