//-*-C++-*-
#ifndef READCIRCULART_H
#define READCIRCULART_H
#include <string>
#include "ClockCorrections/ClockConversion.h"


// redwards functions to read info from Circular T

namespace ClockCorrections
{

  // function to get UTC-NIST from Circular T
  ClockConversion readCircularT_NIST(const std::string &fname);

  // function to get UTC-AUS from Circular T
  ClockConversion readCircularT_AUS(const std::string &fname);

  // function to get UTC-GPS from Circular T
  ClockConversion readCircularT_GPS(const std::string &fname);

}

#endif
