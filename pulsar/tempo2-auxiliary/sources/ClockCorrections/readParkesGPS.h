//-*-C++-*-
#ifndef PARKESGPS_H
#define PARKESGPS_H

#include <string>
#include "ClockCorrections/ClockConversion.h"


// redwards function to read in PKS-GPS data as produced by the observatory
// in gps.log (Mark IV) and pkclk00.log (Mark VI). These are negated
// to produce GPS-PKS.
//
// NOTE (1): It seems clear that the old scheme of producing clock
//   corrections for tempo with time.dat (pkstime.c) mistakenly took
//   the log values as GPS-PKS, and mistook the twice column of time.dat
//   to contain PKS-NIST. This double error gave the correct sign for
//   the PKS->GPS correction, but the wrong sign for GPS->NIST. Be aware
//   then that data produced by the present module using update_clkcorr
//   (--->pks2gps.clk) is GPS-PKS, while the second column of time.dat
//   is (GPS-PKS)+(GPS-NIST) (contrary to pkstime.c which claims it is
//   NIST-PKS). So they should have roughly the same form (since they
//   are dominated by GPS-PKS).
//    
// NOTE (2): Automatically applies corrections to compensate for changes
// in the configuration of the monitoring system. These consist of 
// discrete jumps at the following times:
//
//   -92 ns   @ MJD > 51835  (18 Oct 2000)
//   +40 ns   @ MJD > 52241  (28 Nov 2001)
//
//  These are compensated by adding the offset to all earlier data
//  (since the reconfigurations are believed to bring the system closer
//  to the true value of GPS-PKS).
//  Note that while this should have been compensated for in the old
//  scheme (see time_fix.c), the second fix was apparently NOT present
//  in time.dat as of August 2004. It is suggested that the corrected
//  time.dat was accidentally overwritten by an uncorrected one.

namespace ClockCorrections
{

  ClockConversion readParkesGPS4(const std::string &fname,
				double clipval=1.0e-4);
  ClockConversion readParkesGPS6(const std::string &fname,
				double clipval=1.0e-4);
}

#endif
