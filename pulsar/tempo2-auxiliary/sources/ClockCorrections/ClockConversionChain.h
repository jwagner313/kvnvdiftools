//-*-C++-*-
#ifndef CLOCKCONVERSIONCHAIN_H
#define CLOCKCONVERSIONCHAIN_H

#include <string>
#include <vector>
#include <ClockCorrections/ClockConversion.h>

namespace ClockCorrections
{
  class ClockConversionChain
  {
  public:
    ClockConversionChain() {}
    ClockConversionChain(const std::string &clockFrom, 
			 const std::string &clockTo,
			 const std::vector<std::string> &fnames);
    double get_correction(double mjd) const;

    double get_mjd_start() const;
    double get_mjd_end() const;

  protected:
    std::string clockFrom;
    std::string clockTo;
    std::vector<ClockConversion> chain;
  };
    
}
#endif
