//-*-C++-*-
#ifndef NUSFUNCTION_H
#define NUSFUNCTION_H

#include <vector>
#include <utility>
#include <algorithm>
#include <cstdio>
#include <cmath>

namespace ClockCorrections
{

// helper functors
template <class XT, class YT>
class LessThanX
{
public:
  bool operator()(const std::pair<XT,YT> &p, const XT &x)
  {
    return p.first < x;
  }
};

template <class XT, class YT>
class LessThanPX
{
public:
  bool operator()(const std::pair<XT,YT> &p1, const std::pair<XT,YT> &p2)
  {
    return p1.first < p2.first;
  }
};

// template class for non-uniformly sampled 1D function

template <class XT, class YT>
class NUSFunction
{
public:
  NUSFunction() {}
  NUSFunction(FILE *f) { read(f); }
  NUSFunction(const std::vector<std::pair<XT, YT> > &values_)
    : values(values_) 
  {
    if (!is_sorted())
      std::sort(values.begin(), values.end(), LessThanPX<XT,YT>());
  }
  
  enum InterpolationStyle
    {  IS_None, IS_Linear} ;


  YT evaluate(const XT &x, InterpolationStyle interp=IS_Linear,
	      bool enforce_bounds=false) const;

  // function to sum adjacent points up to a given width.
  // DT is the type returned by operator-(XT,XT)
  template <class DT>
  void scrunch(const DT &maxspan);

  // function to re-sample the function by interpolation.
  // NOTE, if the requested sample interval is greater than the present
  // one, this may LOSE INFORMATION / ALIAS unless the provided points are
  // noiseless and/or correlated over that timescale, or the interpolation
  // kernel is that wide
  template <class DT>
  void resample(const DT &interval);


  const std::vector<std::pair<XT, YT> > & getValues() const
  { return values; }

  void insert_value(const std::pair<XT, YT> &value);

  void clear() { values.clear(); }

  // merges the values from the non-overlapping parts of the passed function.
  // points to merge are chosen on the basis of whether they are beyond
  // a certain distance from values already present (including those
  // already inserted)
  void merge(const NUSFunction<XT,YT> &in, XT min_distance=XT());

  void read(FILE *);
  void write(FILE *) const;

protected:
  std::vector<std::pair<XT, YT> > values;
  bool is_sorted()
  {
    typename std::vector<std::pair<XT, YT> >::size_type i, n=values.size();
    if (n=0)
      return true;
    XT xprev=values[0];
    for (i=1; i<n; i++)
    {
      if (values[i].first < xprev)
	return false;
      xprev = values[i].first;
    }
    return true;
  }
};

template <class XT, class YT>
YT
NUSFunction<XT,YT>::evaluate(const XT &x, InterpolationStyle interp,
			     bool enforce_bounds) const
{
  // handle out of bounds first
  if (x < values[0].first)
  {
    if (enforce_bounds)
      throw "NUSFunction error: out of bounds!";
    else
      return values[0].second;
  }
   typename std::vector<std::pair<XT, YT> >::size_type  n = values.size();
  if (x > values[n-1].first)
  {
    if (enforce_bounds)
      throw "NUSFunction error: out of bounds!";
    else
      return values[n-1].second;
  }

  // find the surrounding values
  typename std::vector<std::pair<XT,YT> >::const_iterator right
    = std::lower_bound(values.begin(), values.end(), x, LessThanX<XT,YT>());

  if (right==values.begin()) 
    // already checked for x < first elem, only prob is ==
    return values[0].second;

  typename std::vector<std::pair<XT,YT> >::const_iterator left = right-1;

  // and fractional position between them
  double pos = (x-left->first) / (right->first - left->first);

  // now do the required type of interpolation
  switch (interp)
  {
  case IS_None:
    return (pos < 0.5 ? left->second : right->second);
  case IS_Linear:
    return left->second + pos * (right->second-left->second);
  default:
    std::fprintf(stderr, "Error (%s:%d): Unknown interpolation code!\n",
		 __FILE__, __LINE__);
    std::exit(1);
  }
}

template <class XT, class YT>
void
NUSFunction<XT,YT>::insert_value(const std::pair<XT, YT> &value)
{
  // find place to insert it
  typename std::vector<std::pair<XT,YT> >::iterator right
    = std::lower_bound(values.begin(), values.end(), value.first, LessThanX<XT,YT>());
  // insert it
  values.insert(right, value);
}

template <class XT, class YT>
void
NUSFunction<XT,YT>::merge(const NUSFunction<XT,YT> &in, XT min_distance)
{
  typename std::vector<std::pair<XT,YT> >::const_iterator i, right;
  using std::abs;

  for (i=in.values.begin(); i!=in.values.end(); i++)
  {
    // find the value that comes after it
    right = std::lower_bound(values.begin(), values.end(), i->first, 
			     LessThanX<XT,YT>());
    // check whether this value is too close to existing ones. continue if so
    if (right != values.end()
	&& abs(right->first - i->first) < min_distance)
      continue;
    if (right != values.begin() 
	&& abs((--right)->first - i->first) < min_distance)
      continue;
    // merge value
    insert_value(*i);
  }
}

template <class XT, class YT>
template <class DT>
void
NUSFunction<XT,YT>::scrunch(const DT &maxspan)
{
   typename std::vector<std::pair<XT, YT> >::size_type i_out, i_in, n=values.size(), i_in_start;
  XT start_x;
  YT sum;

  i_in = i_out =0;
  while (i_in < n) // loop over groups of samples to add
  {
    // add together samples up to the maximum span
    start_x = values[i_in].first;
    sum = values[i_in].second;
    i_in_start=i_in;
    i_in++;

    while (i_in < n && values[i_in].first - start_x <= maxspan)
    {
      sum += values[i_in].second;
      i_in++;
    }
    // save it
    values[i_out] = std::pair<XT,YT>((start_x+values[i_in-1].first)/2,
				     sum / (i_in-i_in_start));
    i_out++;
  }
  // just keep the output samples
  values.resize(i_out);
}

template <class XT, class YT>
template <class DT>
void
NUSFunction<XT,YT>::resample(const DT &interval)
{
  if (!values.empty())
  {
    XT x, x0 = values[0].first, x1 = values[values.size()-1].first;
    std::vector<std::pair<XT, YT> > new_values;
    
    for (x=x0; x <= x1; x += interval)
      new_values.push_back(std::pair<XT,YT>(x, evaluate(x)));
    
    values = new_values;
  }
}
    



}

#endif
