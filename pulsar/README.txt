
To support KVN stations in single dish processing:

  see code-to-edit.c

To support DiFX correlation:

  Need tempo2 (easiest from psrchive), but, its data files
  are horribly outdated. Need fresh 'ut1.dat' ($TEMPO2/clock/ut1.dat)
  and $TEMPO2/earth/ several files...
  
  The files can be updated using steps outlined at
  https://sites.google.com/site/maurobarbieriuda/research/updating-tempo2
  and the scripts & software there. A copy of those scripts
  and source code is now also found here (./tempo2-auxiliary/)

  $ cd ./tempo2-auxiliary/sources/ && make

  $ cd ./tempo2-auxiliary/ 
  $ ./get_bulletinb.sh
  $ ./get_cirt.sh
  $ sources/update_clkcorr -t gpsutc ./cirt/cirt.*
  $ mv $TEMPO2/clock/gps2utc.clk.new  $TEMPO2/clock/gps2utc.clk

  $ ./do.iers.ut1.new ./bulletinb/bulletinb.* > ut1.dat
  $ cp ut1.dat $TEMPO2/clock/ut1.dat

  $ wget ftp://maia.usno.navy.mil/ser7/tai-utc.dat -Otai-utc.dat
      "Then manually edit $TEMPO2/clock/utc2tai.clk to insert the new leap seconds"

  $ wget http://hpiers.obspm.fr/iers/eop/eopc04/eopc04_IAU2000.62-now -Oeopc04_IAU2000.62-now
  $ mv eopc04_IAU2000.62-now $TEMPO2/earth
