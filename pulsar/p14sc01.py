# Start with:
# $ ParselTongue p14sc01.py <0=d|1=k|2=o|3=r|4=t>

from AIPS import AIPS
from AIPSTask import AIPSTask, AIPSList
from AIPSData import AIPSUVData, AIPSImage
from pulsarPipelineKVN import *
import os, sys

AIPS.userno = 514

EXPTS_ALL   = ['p14sc01d', 'p14sc01k', 'p14sc01o', 'p14sc01r', 'p14sc01t']
EXPT        = EXPTS_ALL[0]  # choose one of the above
if len(sys.argv)>1:
	EXPT = EXPTS_ALL[int(sys.argv[1])]
print EXPT

FITS_ROOT   = '/VLBI/DiFX/2014B/' + EXPT + '/all/'  # pluto.kasi.re.kr mounted
ANTAB_ROOT  = FITS_ROOT + '../antab/'

FITS_ROOT   = '/home/yjyun/data/kvn/' + EXPT + '/'  # a copy
ANTAB_ROOT  = '/home/jwagner/FITS/calibsurvey/antabs-orig/'

AIPS_DISK   = 2  # NOTE: also edit pulsarPipelineKVN.py accordingly!!

OUT_ROOT    = '/home/jwagner/FITS/calibsurvey/'
T_INT       = 0.819200 # integration time used in correlator
KVN_BEAM    = 6.0e-3  # 6 mas at 22 GHz
REF_ANT     = 3    # Yoon had Yonsei(3), I had Tamna(1)
SOL_INT     = 1.0  # Yoon had 1.0 minutes; tried 0.5 with no improvement in Q band detections; tried 3.0 minutes but not yet checked

################################################################################################

if EXPT=='p14sc01d':
	SOURCE_FF = '0555+394'
	SOURCE_BP = '3C84'
if EXPT=='p14sc01k':
	SOURCE_FF = '3C345'   # has E-W jet elongating core at ~10mas scale
	SOURCE_BP = '3C454.3' # multiple jet components
if EXPT=='p14sc01o':
	# /VLBI/DiFX/2014B/p14sc01o/all/p14sc01o.vex
	SOURCE_FF = '3C84'    # no others in this observation
	SOURCE_BP = '3C84'    # has significant N-S structure, 3 components (core, jet, counterjet) at 22/43 GHz at 20mas scale
if EXPT=='p14sc01r':
	# /VLBI/DiFX/2014B/p14sc01r/all/p14sc01r.vex
	SOURCE_FF = '3C84'    # no others in this observation
	SOURCE_BP = '3C84'
if EXPT=='p14sc01t':
	# /VLBI/DiFX/2014B/p14sc01t/all/p14sc01t.vex
	SOURCE_FF = '4C39.25' # some E-W structure, core elongated at 4..10 mas scale
	SOURCE_BP = '4C39.25'

################################################################################################

def process_normal(reload=False):

	# redo_basic = True; redo_QSOs = False
	# redo_basic = False; redo_QSOs = True
	redo_basic = True; redo_QSOs = True
	redo_fpt = False # FPT won't work since non integer ratio of K, Q frequencies on p14sc01 obs...

	LG = './logs/' + EXPT + '_KQ'
	init(pulsarlogfile=LG+'_tables.log', aipslogfile=LG+'_aips.log', tasklogfile=LG+'_taskparm.log')

	fantab = OUT_ROOT + '/antabs/' + EXPT + '.fixed.antab'
	fin = []
	fin.append(ANTAB_ROOT + EXPT + 'KTN.ANTAB.1')
	fin.append(ANTAB_ROOT + EXPT + 'KUS.ANTAB.1')
	fin.append(ANTAB_ROOT + EXPT + 'KYS.ANTAB.1')
	generateANTAB(fantab, fin)	

	uv = AIPSUVData(EXPT.upper(),'UVDATA',AIPS_DISK,1)
	if not uv.exists():
		fin = FITS_ROOT + EXPT.lower() + '.fits'
		print ('Loading %s' % (fin))
		uv = loadFITS(fin, uv.name, 1, overwrite=True, clint=T_INT/60.0)

	(uv_K,uv_Q) = process_normal_split2bands(uv)

	LG = './logs/' + EXPT + '_K'
	init(pulsarlogfile=LG+'_tables.log', aipslogfile=LG+'_aips.log', tasklogfile=LG+'_taskparm.log')

	if redo_basic:
		process_normal_basic(uv_K)
		printTableLog(getSNTableLog())
		printTableLog(getCLTableLog())

	if redo_QSOs:
		process_normal_QSOs(uv_K)
		printTableLog(getSNTableLog())
		printTableLog(getCLTableLog())

	LG = './logs/' + EXPT + '_Q'
	init(pulsarlogfile=LG+'_tables.log', aipslogfile=LG+'_aips.log', tasklogfile=LG+'_taskparm.log')

	if redo_basic:
		process_normal_basic(uv_Q)
		printTableLog(getSNTableLog())
		printTableLog(getCLTableLog())

	if redo_QSOs:
		process_normal_QSOs(uv_Q)
		printTableLog(getSNTableLog())
		printTableLog(getCLTableLog())

	if redo_fpt:
		process_fpt_KtoQ(uv_K, uv_Q)

	return uv

def process_fpt_KtoQ(uv_K, uv_Q):
	### NOTE: wont work because of multiple IFs, where freq IF[n;Q] != freq 2*IF[n;K]
	# Merge all K-band SN tables
	sn_first = 4
	sn_last = uv_K.table_highver('SN')
	sn_merged = uv_K.table_highver('SN')+1
	clcal = makeTask('clcal', ['indata',uv_K, 'opcode','MERG', 'snver',sn_first, 'invers',sn_last, 'interpol',''])
	clcal = updateTask('clcal', ['gainver',sn_merged, 'gainuse',-1, 'dobtween',-1, 'doblank',-1])
	execTask(clcal)
	# Apply phase xfer
	cl_fpt = 8
	while (uv_Q.table_highver('SN')>=sn_merged): uv_Q.zap_table('SN', 0)
	while (uv_Q.table_highver('CL')>=cl_fpt): uv_Q.zap_table('CL', 0)
	tacop = makeTask('tacop', ['indata',uv_K, 'inext','SN', 'invers',sn_merged,'outvers',sn_merged,'ncount',1,'doflag',1])
	tacop.outdata = uv_Q
	sncor = makeTask('sncor', ['opcode','XFER', 'snver',sn_merged])
	#sncor.phasprm[30] = # xfer from
	#sncor.phasprm[29] = # xfer to


def process_normal_split2bands(uv):

	uv1 = AIPSUVData(EXPT.upper(),'K',AIPS_DISK,1)
	uv2 = AIPSUVData(EXPT.upper(),'Q',AIPS_DISK,1)

	if uv1.exists() and uv2.exists():
		return (uv1,uv2)

	# Delete any earlier tables
	purgeTables(uv,doTY=True,doGC=True)

	# Load Tsys, gain curve
	fantab = OUT_ROOT + '/antabs/' + EXPT + '.fixed.antab'
	antab = makeTask('ANTAB', ['indata',uv, 'calin',fantab, 'tyver',1, 'gcver',1, 'blver',-1])
	execTask(antab)

	# Split into K-band and Q-band data sets
	uvcop = makeTask('UVCOP', ['indata',uv])
	uvcop.uvcopprm = AIPSList([1,0,0,1,0]) # copy ac&xc, keep flagged, report progress
	uvcop.bif = 1; uvcop.eif = 8; uvcop.outdata = uv1
	execTask(uvcop)
	uvcop.bif = 9; uvcop.eif = 16; uvcop.outdata = uv2
	execTask(uvcop)

	return (uv1,uv2)

def process_normal_basic(uv):

	# Delete any earlier SN and CL (except CL#1) tables
	purgeTables(uv,doTY=False,doGC=False)

	# ACCOR
	doAccor(uv)
	
	# APCAL
	doApcal(uv, solint=4.0*T_INT/60.0, corrFactor=1.1) #  # Apply amplitude correction factor. (Lee et al. 2015)

	# CLCOR 'PANG' for all antennas (any VERA antennas should be excluded here!)
	doPAng(uv, antennas=[0])

	# TECOR
	calibTEC_VLBA(uv)

	# FRING delays only using 1 scan
	nx    = getSourceNX(uv, SOURCE_FF, N=1)
	clver = uv.table_highver('CL')
	snver = uv.table_highver('SN')+1
	fring = makeTask('FRING', ['indata',uv, 'calsour',AIPSList([SOURCE_FF]), 'docalib',1, 'gainuse',clver])
	fring = updateTask(fring, ['refant',REF_ANT, 'snver',snver, 'doapply',-1, 'solint',0])
	fring.aparm    = AIPSList([2,0,0,0,0,0,0,0,0])
	fring.aparm[5] = 0 # separate IFs
	fring.aparm[6] = 2 # verbose output
	# fring.dparm    = AIPSList([1,400,400,0])
	fring.dparm    = AIPSList([3,0,0,0])  # dparm[2]=0: search full Nyquist dly range, dparm[3]=0: search full Nyquist rate range
	fring.timerang = getTimeRange(nx['time'], nx['time'] + nx['time_interval'])
	execTask(fring)
	logTableEdit('fring', 'sn', 'delay on 1st %s scan' % (SOURCE_FF), outver=snver)

	# CLCAL of FRING solutions
	doClCal(uv, snver=snver, clin=clver, interpol='2PT', refant=REF_ANT, msg='delay and rate on 1st %s scan' % (SOURCE_FF))

	# BPASS
	bpver = calibBandpass(uv, bpsource=SOURCE_BP, doPhase=False)

def process_normal_QSOs(uv):

	for sn in range(4, uv.table_highver('SN')+1):
		deleteTable(uv, 'SN', sn)
	for cl in range(7, uv.table_highver('CL')+1):
		deleteTable(uv, 'CL', cl)

	clver = uv.table_highver('CL')
	bpver = uv.table_highver('BP')

	fring = makeTask('FRING', ['indata',uv, 'docalib',1, 'gainuse',clver, 'doband',2, 'bpver',bpver])
	fring = updateTask(fring, ['refant',REF_ANT, 'doapply',-1, 'solint',SOL_INT])
	fring.aparm[1] = 2	# min. 2 antennas
	fring.aparm[5] = 1 	# 1 to combine all IFs
	fring.aparm[6] = 2 	# verbose output
	fring.aparm[7] = 3.5    # min. SNR
	fring.dparm = AIPSList([1,400,400,0])
	#fring.dparm = AIPSList([3,0,0,0])  # dparm[2]=0: search full Nyquist dly range, dparm[3]=0: search full Nyquist rate range
	fring.dparm[4] = T_INT	# if AIPS SPLAT was used, it discarded integration time info
	fring.chinc    = 1      # 4: 512-->128 ch for faster fringe fitting but lower SNR in some cases

	# enable exhaustive FRING search
	fring.aparm[9] = 1
	fring.search   = AIPSList([1,2,3])

	sn_start = uv.table_highver('SN')+1

	sutab = uv.table('SU',0)
	for su in sutab:
		srcname = su['source'].strip()
		if srcname in [SOURCE_BP,SOURCE_FF]:
			continue
		print ('Running FRING on %s...' % (srcname))

		snver = uv.table_highver('SN')+1
		fring.snver = snver
		fring.calsour = AIPSList([srcname])

		try:
			execTask(fring)
			logTableEdit('fring', 'sn', 'fring on %s'%(srcname), outver=snver)
		except:
			print ('Failed on %s' % (srcname))

	sn_stop = uv.table_highver('SN')

	clcal = makeTask('CLCAL', ['indata',uv, 'gainver',clver, 'gainuse',clver+1, 'snver',sn_start, 'invers',sn_stop])
        clcal = updateTask(clcal, ['interpol','SELF', 'opcode','CALI', 'refant',0, 'cutoff',1.0])
        clcal.doblank = -1
        clcal.dobtween = -1
        execTask(clcal)
	logTableEdit('clcal', 'cl', 'fring on all QSOs', inver=clver, outver=clver+1, auxtable='sn', auxver=sn_start)


def process_normal_imagr(uv, srclist, pixpbeam=5.0, pixels=512):
	"""Image some select sources in an already calibrated uv dataset. Images are stored into PL tables."""

	Nchmargin = 8
	clver = uv.table_highver('CL')
        bpver = uv.table_highver('BP')

	# Preparations for imaging
	Nch   = get_header(uv, 'FREQ')
	imagr = makeTask('IMAGR', ['indata',uv, 'docalib',1, 'gainuse',clver, 'doband',2, 'bpver',bpver])
	imagr = updateTask(imagr, ['bchan',Nchmargin, 'echan',Nch-Nchmargin])
	imagr = updateTask(imagr, ['nchav',Nch-2*Nchmargin, 'chinc',Nch-2*Nchmargin, 'niter',400, 'stokes','I'])
	imagr.cellsize = AIPSList([KVN_BEAM/pixpbeam,KVN_BEAM/pixpbeam])
	imagr.imsize   = AIPSList([int(pixels/2),int(pixels/2)])
	imagr.flux     = 10e-3  # stop cleaning when abs(residual)<imagr.flux[Jy]

	# Preparations for converting Image --> PL
	kontr = makeTask('KNTR',  ['docont',1, 'dogrey',1, 'dovect',-1, 'label',2, 'dotv',-1, 'cbplot',1])
	tacop = makeTask('TACOP', ['outdata',uv, 'inext','PL','invers',1, 'ncount',1, 'outvers',0])

	# Image, convert CLEAN components into map, store plot in original uv-dataset
	for src in srclist:
		im  = AIPSImage(src, 'ICL001', AIPS_DISK, 1)
		ib  = AIPSImage(src, 'IBM001', AIPS_DISK, 1)
		if im.exists():
			im.zap()
		if ib.exists():
			ib.zap()
		imagr.sources = AIPSList([src])
		imagr.outname = im.name
		imagr.outseq  = im.seq
		kontr.indata  = im
		tacop.indata  = im
		execTask(imagr)
		execTask(kontr)
		execTask(tacop)

def run():

	process_normal()

run()
