# Start with:
# $ ParselTongue p14sc01FullTrack.py <nr>

# Reduction of some of the actual H2O/SiO maser observations,
# here for purpose of imaging the phase reference source

from AIPS import AIPS
from AIPSTask import AIPSTask, AIPSList
from AIPSData import AIPSUVData, AIPSImage
from pulsarPipelineKVN import *
import os, sys

AIPS.userno = 514

EXPT_NR = 0  # choose out of 0--8
if len(sys.argv)>1:
	EXPT_NR = int(sys.argv[1])

################################################################################################

# Notes:
#   p14sc01j is ok on 3C345 but FRING on J1339-2401 fails...

# EXPTS_ALL   = ['p14sc01s', 'p14sc01m', 'p14sc01j']
#  <nr>           0              1             2             3             4            5             6              7             8
EXPTS_ALL   = ['p14sc01s',   'p14sc01m',   'p14sc01j',   'p14sc01h',   'p14sc01q',   'p14sc01y',   'p14sc02c',   'p14sc02d',   'p15sc01a']
EXPTS_QSOs  = ['J0731-2341', 'J1048-1909', 'J1339-2401', 'J0121+1149', 'J2015-0137', 'J2322+5057', 'J1007+1356', 'J2015+3710', 'J2231+5922']
EXPTS_FFs   = ['4C39.25',    '4C39.25',    '3C345',      'J0136+4751', 'BL-LAC',     '3C84',       '4C39.25',    '3C454.3',    '3C84']
SCAN_NR_FFs = [2,             1,            1,            1,            4,            1,            2,            1,            1]
EXPTS_BPs   = ['4C39.25',    '4C39.25',    '3C345',      'J0136+4751', 'BL-LAC',     '3C84',       '4C39.25',    '3C454.3',    '3C84']

AIPS_DISK   = 2
OUT_ROOT    = '/home/jwagner/FITS/calibsurvey/'
T_INT       = 0.819200 # integration time used in correlator
KVN_BEAM    = 6.0e-3  # 6 mas at 22 GHz
REF_ANT     = 3
SOL_INT     = 1.0  # minutes; phase ref scans are ~2 minutes, make 1 soln per scan; "If SOLINT > Scan/2 (in Multisource) SOLINT = Scan"
EXPT        = EXPTS_ALL[EXPT_NR]
SOURCE_QSO  = EXPTS_QSOs[EXPT_NR]
SOURCE_FF   = EXPTS_FFs[EXPT_NR]
SOURCE_BP   = EXPTS_FFs[EXPT_NR]
SCAN_NR_FF  = SCAN_NR_FFs[EXPT_NR]

UVNAME      = 'I'+EXPT[1:] # have uv dataset name different from SNR/fringe search dataset
# FITS_ROOT   = '/VLBI/DiFX/2014B/' + EXPT + '/all/'  # provided that pluto.kasi.re.kr mounted
FITS_ROOT   = '/home/yjyun/data/kvn/'+ EXPT + '/'
ANTAB_ROOT  = '/home/jwagner/FITS/calibsurvey/antabs-orig/'

################################################################################################

# Table   Input   Applies Task    Description
# SN#1                    ACCOR   normalization with auto-corrs
# SN#2                    ACCOR   calibration with Tsys and gain curves
# SN#3                    FRING   delay on <FF>
# SN#4                    FRING   fring on <QSO>
# Table   Input   Applies Task    Description
# CL#2    CL#1    SN#1    CLCAL   normalization with auto-corrs
# CL#3    CL#2    SN#2    CLCAL   calibration with Tsys and gain curves
# CL#4    CL#3            CLCOR   parallactic angle correction
# CL#5    CL#4            TECOR   TEC correction
# CL#6    CL#5    SN#3    CLCAL   delay and rate on <FF> (interpol=2PT, dobtween=1)
# CL#7    CL#6    SN#4    CLCAL   fring on <QSO>

################################################################################################

def process_normal(reload=False):

	print ('Processing %s' % (EXPT))
	redo_basic = True
	redo_QSO = True
	redo_img = True
	redo_plots = True

	LG = './logs/imaging_' + EXPT + '_KQ'
	init(pulsarlogfile=LG+'_tables.log', aipslogfile=LG+'_aips.log', tasklogfile=LG+'_taskparm.log')

	fantab = OUT_ROOT + '/antabs_imaging/' + EXPT + '.fixed.antab'
	fin = []
	fin.append(ANTAB_ROOT + EXPT + 'KTN.ANTAB.1')
	fin.append(ANTAB_ROOT + EXPT + 'KUS.ANTAB.1')
	fin.append(ANTAB_ROOT + EXPT + 'KYS.ANTAB.1')
	generateANTAB(fantab, fin)	

	uv = AIPSUVData(UVNAME.upper(),'UVDATA',AIPS_DISK,1)
	if not uv.exists():
		fin = FITS_ROOT + EXPT.lower() + '.fits'
		print ('Loading %s' % (fin))
		uv = loadFITS(fin, uv.name, 1, overwrite=True, clint=T_INT/60.0)

	(uv_K,uv_Q) = process_normal_split2bands(uv)

	LG = './logs/imaging_' + EXPT + '_K'
	init(pulsarlogfile=LG+'_tables.log', aipslogfile=LG+'_aips.log', tasklogfile=LG+'_taskparm.log')
	if redo_basic:
		# recalibrate
		process_normal_basic(uv_K)
	if redo_QSO:
		process_normal_QSOs(uv_K)
	if redo_img:
		process_normal_imagr(uv_K, [SOURCE_QSO], 'K', pixpbeam=20.0, pixels=256)
	if redo_plots:
		process_normal_extraplots(uv_K, SOURCE_QSO, 'K')

	printTableLog(getSNTableLog())
	printTableLog(getCLTableLog())

	## stop early, just K
	#return uv
	#redo_basic = False
	#redo_QSO = False
	#redo_img = True
	#redo_plots = True

	LG = './logs/imaging_' + EXPT + '_Q'
	init(pulsarlogfile=LG+'_tables.log', aipslogfile=LG+'_aips.log', tasklogfile=LG+'_taskparm.log')
	if redo_basic:
		process_normal_basic(uv_Q)
	if redo_QSO:
		process_normal_QSOs(uv_Q)
	if redo_img:
		process_normal_imagr(uv_Q, [SOURCE_QSO], 'Q', pixpbeam=40.0, pixels=256)
	if redo_plots:
		process_normal_extraplots(uv_Q, SOURCE_QSO, 'Q')

	printTableLog(getSNTableLog())
	printTableLog(getCLTableLog())

	print ('Done processing %s' % (EXPT))

	return uv

def process_normal_split2bands(uv):

	uv1 = AIPSUVData('I'+EXPT.upper(),'K',AIPS_DISK,1)
	uv2 = AIPSUVData('I'+EXPT.upper(),'Q',AIPS_DISK,1)

	if uv1.exists() and uv2.exists():
		return (uv1,uv2)

	# Delete any earlier tables
	purgeTables(uv,doTY=True,doGC=True)

	# Load Tsys, gain curve
	fantab = OUT_ROOT + '/antabs_imaging/' + EXPT + '.fixed.antab'
	antab = makeTask('ANTAB', ['indata',uv, 'calin',fantab, 'tyver',1, 'gcver',1, 'blver',-1])
	execTask(antab)

	# Split into K-band and Q-band data sets
	uvcop = makeTask('UVCOP', ['indata',uv, 'outname',uv.name])
	uvcop.uvcopprm = AIPSList([1,0,0,1,0]) # copy ac&xc, keep flagged, report progress
	uvcop.bif = 1; uvcop.eif = 8; uvcop.outdata = uv1
	execTask(uvcop)
	uvcop.bif = 9; uvcop.eif = 16; uvcop.outdata = uv2
	execTask(uvcop)

	return (uv1,uv2)

def process_normal_basic(uv):

	# Delete any earlier SN and CL (except CL#1) tables
	purgeTables(uv,doTY=False,doGC=False)

	# ACCOR
	doAccor(uv)
	
	# APCAL
	#doApcal(uv, solint=0, corrFactor=1.1) #  # Apply amplitude correction factor. (Lee et al. 2015)
	doApcal(uv, solint=0)

	# CLCOR 'PANG' for all antennas (any VERA antennas should be excluded here!)
	doPAng(uv, antennas=[0])

	# TECOR
	calibTEC_VLBA(uv)

	# FRING delays
	nx = getSourceNX(uv, SOURCE_FF, N=SCAN_NR_FF)
	clver = uv.table_highver('CL')
	snver = uv.table_highver('SN')+1
	fring = makeTask('FRING', ['indata',uv, 'calsour',AIPSList([SOURCE_FF]), 'docalib',1, 'gainuse',clver])
	fring = updateTask(fring, ['refant',REF_ANT, 'snver',snver, 'doapply',-1, 'solint',0])
	fring.aparm    = AIPSList([2,0,0,0,0,0,0,0,0])
	fring.aparm[5] = 0 # 0=separate IFs, 1=combine
	fring.aparm[6] = 2 # verbose output
	# fring.dparm    = AIPSList([1,400,400,0])
	fring.dparm    = AIPSList([3,0,0,0])  # dparm[2]=0: search full Nyquist dly range, dparm[3]=0: search full Nyquist rate range
	# fring.dparm[8] = 1  # zero out the rates
	fring.timerang = getTimeRange(nx['time'], nx['time'] + nx['time_interval'])  # use one scan only
	execTask(fring)
	logTableEdit('fring', 'sn', 'delay on %s scans' % (SOURCE_FF), outver=snver)

	# CLCAL of FRING solutions
	doClCal(uv, snver=snver, clin=clver, interpol='2PT', refant=REF_ANT, msg='delay and rate on %s scan %d' % (SOURCE_FF,SCAN_NR_FF))

	# BPASS
	bpver = calibBandpass(uv, bpsource=SOURCE_BP, doPhase=False)

def process_normal_QSOs(uv):

	for sn in range(4, uv.table_highver('SN')+1):
		deleteTable(uv, 'SN', sn)
	for cl in range(7, uv.table_highver('CL')+1):
		deleteTable(uv, 'CL', cl)

	clver = uv.table_highver('CL')
	bpver = uv.table_highver('BP')
	snver = uv.table_highver('SN')+1

	fring = makeTask('FRING', ['indata',uv, 'docalib',1, 'gainuse',clver, 'doband',2, 'bpver',bpver])
	fring = updateTask(fring, ['refant',REF_ANT, 'doapply',-1, 'solint',SOL_INT])
	fring.aparm[1] = 2	# min. 2 antennas
	fring.aparm[5] = 1 	# 0=separate IFs, 1=combine IFs
	fring.aparm[6] = 2 	# verbose output
	fring.dparm    = AIPSList([3,400,400,0])
	fring.dparm[4] = T_INT	# if AIPS SPLAT was used, it discarded integration time info
	fring.chinc    = 4      # 4: 512-->128 ch for faster fringe fitting but lower SNR in some cases
	fring.snver    = snver
	fring.calsour  = AIPSList([SOURCE_QSO])
	try:
		execTask(fring)
		logTableEdit('fring', 'sn', 'fring on %s'%(SOURCE_QSO), outver=snver)
	except:
		print ('Failed on %s' % (SOURCE_QSO))
		return

	clcal = makeTask('CLCAL', ['indata',uv, 'gainver',clver, 'gainuse',clver+1, 'snver',snver, 'invers',0])
        clcal = updateTask(clcal, ['interpol','AMBG', 'opcode','CALI', 'refant',REF_ANT]) # 2PT
        clcal.doblank = -1
        clcal.dobtween = 1
        execTask(clcal)
	logTableEdit('clcal', 'cl', 'fring on QSO', inver=clver, outver=clver+1, auxtable='sn', auxver=snver)


def process_normal_imagr(uv, srclist, bandname, pixpbeam=40.0, pixels=512):
	"""Image some select sources in an already calibrated uv dataset. Images are stored into PL tables."""

	Nchmargin = 8
	clver = uv.table_highver('CL')
        bpver = uv.table_highver('BP')

	# Preparations for imaging
	Nch   = get_header(uv, 'FREQ')
	imagr = makeTask('IMAGR', ['indata',uv, 'docalib',1, 'gainuse',clver, 'doband',2, 'bpver',bpver, 'ltype',-3])
	imagr = updateTask(imagr, ['bchan',Nchmargin, 'echan',Nch-Nchmargin+1])
	imagr = updateTask(imagr, ['nchav',Nch-2*Nchmargin, 'chinc',Nch-2*Nchmargin, 'niter',200, 'stokes','I'])
	imagr.cellsize = AIPSList([KVN_BEAM/pixpbeam,KVN_BEAM/pixpbeam])
	imagr.imsize   = AIPSList([int(pixels/2),int(pixels/2)])
	imagr.flux     = 10e-3  # stop cleaning when abs(residual)<imagr.flux[Jy]

	# Preparations for converting Image --> PL
	imean = makeTask('IMEAN', ['userid',AIPS.userno, 'domodel',-1, 'docat',1])
	kontr = makeTask('KNTR',  ['docont',1, 'dogrey',-1, 'dovect',-1, 'label',2, 'dotv',-1, 'cbplot',11, 'plev',1, 'ltype',4])
	tacop = makeTask('TACOP', ['outdata',uv, 'inext','PL','invers',1, 'ncount',1, 'outvers',0])
	lwpla = makeTask('LWPLA', ['plver',1, 'dodark',1])

	# Image, convert CLEAN components into map, store plot in original uv-dataset
	for src in srclist:
		name = bandname + src[1:] + 'F'
		im  = AIPSImage(name, 'ICL001', AIPS_DISK, 1)
		ib  = AIPSImage(name, 'IBM001', AIPS_DISK, 1)
		if True:
			if im.exists():
				im.zap()
			if ib.exists():
				ib.zap()
		while (uv.table_highver('PL')>0): uv.zap_table('PL', 0)
		while (im.exists() and im.table_highver('PL')>0): im.zap_table('PL', 0)

		imagr.sources = AIPSList([src])
		imagr.outname = im.name
		imagr.outseq  = im.seq
		imagr.outdisk = im.disk
		imean.indata  = im
		kontr.indata  = im
		tacop.indata  = im

		execTask(imagr)
		execTask(imean)
		firstlev = 100.0*abs(3*imean.pixstd / im.header.datamax) # 3sigma in % of data.max.
		kontr.levs = AIPSList([0]*30)
		kontr.levs[1] = -firstlev
		kontr.levs[2] = firstlev
		for i in range(3,31):
			if (2 * kontr.levs[i-1] >= 100.0):
				break
			kontr.levs[i] = 2 * kontr.levs[i - 1]
		print kontr.levs
		execTask(kontr)

		lwpla.indata  = im
		lwpla.outfile = 'PWD:ftrack_' + src + '_' + bandname + '_map.ps'
		execTask(lwpla)

		execTask(tacop)

def process_normal_extraplots(uv, src, band):

	Nch = get_header(uv, 'FREQ')
	Nchmargin = 8

	clver = uv.table_highver('CL')
        bpver = uv.table_highver('BP')

	keep_upto_ver = 2
	while (uv.table_highver('PL')>keep_upto_ver): uv.zap_table('PL', 0)

	# Closure phase
	clplt = makeTask('CLPLT', ['indata',uv, 'bchan',Nchmargin, 'echan',Nch-Nchmargin+1, 'ltype',-3])
	clplt = updateTask(clplt, ['docalib',1, 'gainuse',clver, 'doband',2, 'bpver',bpver])
	clplt.triangle = AIPSList([1,2,3])
	clplt.solint = 1.0  # can be negative to do vector avg'ing "to avoid difficulties with 2 pi ambiguities"
	clplt.bparm = AIPSList([0,0,1,0,0,-180,180,0])
	clplt.sources = AIPSList([src])
	if True:
		execTask(clplt)
		lwpla = makeTask('LWPLA', ['indata',uv, 'plver',0, 'dodark',1])
		lwpla.outfile = 'PWD:ftrack_' + src + '_' + band + '_clplot.ps'
		execTask(lwpla)

	# UV coverage
	uvplt = makeTask('UVPLT', ['indata',uv, 'docalib',-1, 'dotv',-1, 'bchan',1, 'echan',1])
	uvplt = updateTask(uvplt, ['xinc',10, 'bif',1, 'eif',1, 'ltype',-3, 'sources',AIPSList([src])])
	uvplt.bparm = AIPSList([6,7,2,0])
	if True:
		execTask(uvplt)
		lwpla = makeTask('LWPLA', ['indata',uv, 'plver',0, 'dodark',1])
		lwpla.outfile = 'PWD:ftrack_' + src + '_' + band + '_uvcov.ps'
		execTask(lwpla)

	# UV Ampl/Phase vs Time
	vplot = makeTask('VPLOT', ['indata',uv, 'avgif',1, 'bif',1, 'eif',8, 'sources',AIPSList([src])])
	vplot = updateTask(vplot, ['docalib',1, 'gainuse',clver, 'doband',2, 'bpver',bpver])
	vplot = updateTask(vplot, ['bchan',1, 'echan',Nch, 'ltype',-3, 'nplots',3])
	vplot.solint = 1.0
	vplot.aparm = AIPSList([0])
	# vplot.bparm = AIPSList([12,-1,1,0,0, 0,1.0,-180.0,180.0,0]) # bug: ampl scaled nicely, but phase plotted -180.0..0.0 rather than -180..+180!?
	# vplot.bparm = AIPSList([12,-1,1,0,0, 0,1.0, 0.0,0.0, 0]) # issue: ampl max is unknown a priori and no AIPS task can find it...
	vplot.bparm = AIPSList([12,-1,1,0]) # just auto-scale all
	if True:
		execTask(vplot)
		lwpla = makeTask('LWPLA', ['indata',uv, 'plver',0, 'dodark',1])
		lwpla.outfile = 'PWD:ftrack_' + src + '_' + band + '_uvamp.ps'
		execTask(lwpla)


def run():

	process_normal()

run()
