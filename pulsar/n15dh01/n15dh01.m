%
% Helper to look at phase for polycos from n15dh01
%

clear; close all; fclose('all');

% n15dh01a : start MJD 57284  2015y262d07h00m00s--2015y262d11h59m20s
% n15dh01b : start MJD 57330  2015y308d04h00m00s--2015y308d08h59m20s
% n15dh01c : start MJD 57331  2015y309d04h00m00s--2015y309d08h59m20s
% n15dh01d : start MJD 57332  2015y310d04h00m00s--2015y310d08h59m20s
% n15dh01e : start MJD 57333  2015y311d04h00m00s--2015y311d08h59m20s
    
fT1 = 'J1745-2900-tempo1-polyco.n15dh01.dat';
fT2 = 'J1745-2900-tempo2-polyco.n15dh01.dat';
% fT2 = 'J1745-2900-tempo2-polyco.n15dh01.eph.dat'; % makes freq,phase nearly identical with those by J1745-2900-tempo2-polyco.n15dh01.dat

t0 = 57331.0;      % start MJD for polyco evaluation
tt = 0:(1/1440):1; % full 24h time span (1440min, one point per minute)

[fn1,rp1,tn1] = solve_polyco(fT1,t0+tt);
[fn2,rp2,tn2] = solve_polyco(fT2,t0+tt);
mfn = mean([fn1(~isnan(fn1)); fn2(~isnan(fn2))]);
tn1_h = (tn1-t0)*24;
tn2_h = (tn2-t0)*24;

figure(1),
subplot(2,1,1),
    plot(tn1_h,fn1-mfn, 'rx:'); hold on;
    plot(tn2_h,fn2-mfn, 'gx:'); hold on;
    legend('TEMPO', 'TEMPO2');
    title('Predicted pulse period at station COE/0');
    ylabel(sprintf('Offset from mean of %.6f Hz', mfn));
    xlabel(sprintf('Hours from MJD %d', t0));
subplot(2,1,2),
    plot(tn1_h,2*pi*rp1, 'rx:'); hold on;
    plot(tn2_h,2*pi*rp2, 'gx:'); hold on;
    legend('TEMPO', 'TEMPO2');
    title('Predicted pulse phase at station COE/0');
    ylabel('Phase (rad)');
    xlabel(sprintf('Hours from MJD %d', t0));

figure(2),
subplot(2,1,1),
    plot(tn1_h,fn2-fn1, 'kx:'); hold on;
    legend('Difference (TEMPO2-TEMPO)');
    title('Predicted pulse period at station COE/0');
    ylabel('Period (Hz)');
    xlabel(sprintf('Hours from MJD %d', t0));
subplot(2,1,2),
    plot(tn1_h,2*pi*(rp2-rp1), 'kx:'); hold on;
    legend('Difference (TEMPO2-TEMPO)');
    title('Predicted pulse phase at station COE/0');
    ylabel('Phase (rad)');
    xlabel(sprintf('Hours from MJD %d', t0));

