

After adding new station numbers (0-255) in m5fb_makeheader.c one must update:

TEMPO    obsys.dat     to contain the station's ECEF X Y Z coordinates,
         a new 1-letter code, and a new 2-letter code

TEMPO2   $TEMPO2/observatory/observatories.dat and $TEMPO2/observatory/aliases

SIGPROC  aliases.c     to map from number (0-255) to station name

PRESTO   misc_utils.c  function telescope_to_tempocode(), update the
         map "telescope name" --> ("Pretty name", "ID"), where ID is
         the 2-letter code in TEMPO

         sigproc_fb.c  similar to aliases.c above, update to map the
         new station number (0-255) to a station name, and use the new
         station name previously added to telescope_to_tempocode()

         polycos.c     function make_polycos() needs to map the new
         station name to a 1-letter station character and a "track 
         length" used as one of the TEMPO program calling parameters

Note that TEMPO forces station 1-letter IDs to lower case!
If obsys.dat contains for example 'A' and 'a', TEMPO complains
about duplicate entries.


In detail:

<<$TEMPO/obsys.dat>>
-3287268.6186   4023450.1799    3687380.0198   1  KVNUS               k  KU
-3042280.9137   4045902.7164    3867374.3544   1  KVNYS               l  KY
-3171731.558    4292678.4878    3481038.7252   1  KVNTN               m  KT

<<$TEMPO2/observatory/aliases>>
## KVN
KU k
KU l
KT m

<<$TEMPO2/observatory/observatories.dat>>
### KVN
  -3042278.256  4045902.809     3867376.143      KVNYS               KY
  -3287268.157  4023449.121     3687380.180      KVNUS               KU
  -3171731.481  4292677.373     3481040.458      KVNTN               KT

<<./sigproc/src/aliases.c>>
  ////////// KVN
  case 20:
    return('k'); /*KVNUS*/
    break;
  case 21:
    return('l'); /*KVNYS*/
    break;
  case 22:
    return('m'); /*KVNTN*/
    break;

<<./presto/src/misc_utils.c>>
    // KVN
    } else if (strcasecmp(scope, "KVNUS") == 0) {
        strcpy(obscode, "KU");
        strcpy(outname, "KVNUS");
    } else if (strcasecmp(scope, "KVNYS") == 0) {
        strcpy(obscode, "KY");
        strcpy(outname, "KVNYS");
    } else if (strcasecmp(scope, "KVNTN") == 0) {
        strcpy(obscode, "KT");
        strcpy(outname, "KVNTN");

<<./presto/src/sigproc_fb.c>>
    ////////// KVN
    case 20:
        strcpy(s->telescope, "KVNUS");
        break;
    case 21:
        strcpy(s->telescope, "KVNYS");
        break;
    case 22:
        strcpy(s->telescope, "KVNTN");
        break;

<<./presto/src/polycos.c>>
   // KVN
   } else if (strcmp(idata->telescope, "KVNUS") == 0) {
      scopechar = 'k';
      tracklen = 12;
   } else if (strcmp(idata->telescope, "KVNYS") == 0) {
      scopechar = 'l';
      tracklen = 12;
   } else if (strcmp(idata->telescope, "KVNKT") == 0) {
      scopechar = 'm';
      tracklen = 12;


