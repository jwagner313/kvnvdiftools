/***************************************************************************
 *   Copyright (C) 2008-2011 by Walter Brisken & Chris Phillips            *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 3 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/
//===========================================================================
// SVN properties (DO NOT CHANGE)
//
// $Id: m5fb.c 0001 2012-05-25 01:15:33Z RichardDOdson $
// $HeadURL: https://svn.atnf.csiro.au/difx/libraries/mark5access/trunk/mark5access/mark5_stream.c $
// $LastChangedRevision: 0001 $
// $Author: RichardDodson $
// $LastChangedDate: 2012-05-25 09:15:33 +0800 (Fri, 25 May 2012) $
//
//============================================================================

// Change this to configure detection, if possible
#define USEGETOPT 1

#include <stdio.h>
#include <complex.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <fftw3.h>
#include <math.h>
#include <signal.h>
#include "../mark5access/mark5_stream.h"

#if USEGETOPT
#include <getopt.h>
#endif

#ifndef MIN
	#define MIN(a,b) (((a)<(b))?(a):(b))
	#define MAX(a,b) (((a)>(b))?(a):(b))
#endif

const char program[] = "m5fb";
const char author[]  = "Richard Dodson";
//  Copied extensively from m5spec by Walter Brisken & Chris Phillips
const char version[] = "1.2";
const char verdate[] = "20151013";

int die = 0;

typedef enum {VLBA=1, DBBC, NOPOL} polmodetype;

struct hd_info
{
	int nchan;
	int nint;
	float freq;
	float max;
	float min;
	float mean;
	int nbit;
	char polid[16];
	char *source;
};

struct filterbank_cfg
{
	int do_avg_IF;
	int do_intensity_FFT;
	int do_binary_out;
	int nchan;
	int nif;
	int npol;
	long tint_usec;
	long nint;
	int  nbit_out;
	char *ifid;
	char *polid;
	polmodetype polmode;
};

typedef void (*sighandler_t)(int);

sighandler_t oldsiginthand;

void siginthand(int j)
{
	printf("\nBeing killed.  Partial results will be saved.\n\n");
	die = 1;

	signal(SIGINT, oldsiginthand);
}

static void usage(const char *pgm)
{
	printf("\n");

	printf("%s ver. %s   %s  %s\n\n", program, version, author, verdate);
	printf("A Mark5 filterbank.  Can use VLBA, Mark3/4, and Mark5B formats using the\nmark5access library.\n\n");
	printf("Usage : %s <infile> <dataformat> <nchan> <dint> <outfile> [<offset>]\n\n", program);
	printf("  <infile> is the name of the input file\n\n");
	printf("  <dataformat> should be of the form: "
		"<FORMAT>-<Mbps>-<nchan>-<nbit>, e.g.:\n");
	printf("    VLBA1_2-256-8-2\n");
	printf("    MKIV1_4-128-2-1\n");
	printf("    Mark5B-512-16-2\n");
	printf("    VDIF_1000-64-1-2 (here 1000 is payload size in bytes)\n\n");
	printf("  <nchan> is the number of channels to make per IF\n\n");
	printf("  <dint> is the integration time in microseconds\n\n");
	printf("  <outfile> is the name of the output file\n\n");
	printf("  <offset> is number of bytes into file to start decoding\n\n");
	printf("The following options are supported\n\n");
	printf("    --dbbc     Assume dBBC polarisation order (all Rcp then all Lcp)\n\n");
	printf("    --nopol    Do not compute cross pol terms\n\n");
	printf("    --avg      Average all IFs to output a single IF\n\n");
	printf("    --ascii    Write ascii output\n\n");
	printf("    -I         Compute power spectrum of intensity (|FFT{|complex(t)|^2}|^2)\n");
	printf("               instead of power spectrum (|FFT{real(t)}|^2)\n\n");
	printf("    -p         String for polzn terms (-p RLRL)\n\n");
	printf("    -i         String for IF sideband terms, x discards IF (-i ULULxxUL)\n\n");
	printf("    -b <nbits> Precision of output data (8=uchar (default), 32=float, 64=double)\n\n");
	printf("    --help     This list\n\n");
}

int harvestComplexData(struct mark5_stream *ms, double **spec, fftw_complex **zdata, fftw_complex **zx, const struct filterbank_cfg cfg, int fftlen, long long *total, long long *unpacked)
{
	fftw_plan *plan;
	double complex **cdata;
	int j,status=-1;

	plan = (fftw_plan *)malloc(ms->nchan*sizeof(fftw_plan));
	cdata = (double complex **)malloc(ms->nchan*sizeof(double complex *));
	for(j = 0; j < ms->nchan; ++j)
	{
		cdata[j] = (double complex*)malloc(cfg.nchan*sizeof(double complex));
		plan[j] = fftw_plan_dft_1d(cfg.nchan, cdata[j], zdata[j], FFTW_FORWARD, FFTW_MEASURE);
	}

	for(j = 0; j < cfg.nint; ++j)
	{
		int i;

		if(die)
		{
   			status=-1;
			break;
		}

		status = mark5_stream_decode_double_complex(ms, fftlen, (double complex**)cdata);
		if(status < 0)
		{
			break;
		}
		else
		{
			*total += fftlen;
			*unpacked += status;
		}

		if(ms->consecutivefails > 5)
		{
			break;
		}

		for(i = 0; i < ms->nchan; ++i)
		{
			int fi;
			if (cfg.do_intensity_FFT)
			{
				for(fi=0; fi<cfg.nchan;++fi)
				{
					cdata[i][fi]  = cabs(cdata[i][fi]);
					cdata[i][fi] *= cdata[i][fi];
				}
			}
			/* FFT */
			fftw_execute(plan[i]);
		}

		for(i = 0; i < ms->nchan; ++i)
		{
			int c;

			for(c = 0; c < cfg.nchan; ++c)
			{
				double re, im;

				re = creal(zdata[i][c]);
				im = cimag(zdata[i][c]);
				spec[i][c] += re*re + im*im;
			}
		}

		for(i = 0; i < ms->nchan/2; ++i)
		{
			int c;

			for(c = 0; c < cfg.nchan; ++c)
			{
				zx[i][c] += zdata[2*i][c]*~zdata[2*i+1][c];
			}
		}
	}

	for(j = 0; j < ms->nchan; ++j)
	{
		fftw_destroy_plan(plan[j]);
		free(cdata[j]);
	}
	free(plan);
	free(cdata);

	return status;
}

int harvestRealData(struct mark5_stream *ms, double **spec, fftw_complex **zdata, fftw_complex **zx, const struct filterbank_cfg cfg, int fftlen, long long *total, long long *unpacked)
{
	static fftw_plan *plan=NULL;
	static double **data=NULL;
	int j, status=-1;

	if(!data)
	{
		plan = (fftw_plan *)malloc(ms->nchan*sizeof(fftw_plan));
		data = (double **)malloc(ms->nchan*sizeof(double *));
		for(j = 0; j < ms->nchan; ++j)
		{
			data[j] = (double *)malloc((fftlen+2)*sizeof(double));
			plan[j] = fftw_plan_dft_r2c_1d(2*cfg.nchan, data[j], zdata[j], FFTW_MEASURE);
		}
	}

	for(j = 0; j < cfg.nint; ++j)
	{
		int i;

		if(die)
		{
   			status=-1;
			break;
		}

		status = mark5_stream_decode_double(ms, fftlen, data);
		if(status < 0)
		{
			printf("<EOF> status=%d\n", status);
			break;
		}
		else
		{
			*total += fftlen;
			*unpacked += status;
		}

		if(ms->consecutivefails > 5)
		{
			break;
		}

		for(i = 0; i < ms->nchan; ++i)
		{
			int fi;
			if (cfg.do_intensity_FFT)
			{
				for(fi=0; fi<cfg.nchan;++fi)
				{
					data[i][2*fi] = data[i][2*fi]*data[i][2*fi] + data[i][2*fi+1]*data[i][2*fi+1];
					data[i][2*fi+1] = 0;
				}
			}
			/* FFT */
			fftw_execute(plan[i]);
		}

		for(i = 0; i < ms->nchan; ++i)
		{
			int c;

			for(c = 0; c < cfg.nchan; ++c)
			{
				double re, im;

				re = creal(zdata[i][c]);
				im = cimag(zdata[i][c]);
				spec[i][c] += re*re + im*im;
			}
		}

		if (cfg.polmode==VLBA)
		{
			for(i = 0; i < ms->nchan/2; ++i)
			{
				int c;

				for(c = 0; c < cfg.nchan; ++c)
				{
					zx[i][c] += zdata[2*i][c]*~zdata[2*i+1][c];
				}
			}
		}
		else if (cfg.polmode==DBBC)
		{
			for(i = 0; i < ms->nchan/2; ++i)
			{
				int c;

				for(c = 0; c < cfg.nchan; ++c)
				{
					zx[i][c] += zdata[i][c]*~zdata[i+ms->nchan/2][c];
				}
			}
		}
	}

	if (status<0) {
		for(j = 0; j < ms->nchan; ++j)
		{
			fftw_destroy_plan(plan[j]);
			free(data[j]);
		}
		free(plan);
		free(data);
	}

	printf("\t\t\tTime Processed Appx: %.4e s\r",ms->framens*1e-9*ms->framenum);
	return status;
}

int print_header(struct mark5_stream *ms, struct hd_info hi,FILE *fo)
{
	char tmp[32];
	int h, m, s, i;

	h = ms->sec;
	h = h/3600;
	i = ms->sec-h*3600;
	m = i/60;
	s = i-m*60;

	strncpy(tmp,"KVNTN",5);
	//i=(int) strlen(ms->streamname);
	//strncpy(tmp,(char *)strchr((char *)(ms->streamname+i+3),'_')+1,32);
	//(strchr(tmp,'_'))[0]='\0';
/*
  fprintf(fo,"Site            : %s\n",tmp);
  fprintf(fo,"Observer        : %s\n","Nemo");
  fprintf(fo,"Proposal        : %s\n","XX");
  fprintf(fo,"Array Mode      : %s\n","SD");
  fprintf(fo,"Observing Mode  : \n");
  fprintf(fo,"Date            : %s\n","0/0/0");//mjd2date(ms->mjd+56000));
  fprintf(fo,"Num Antennas    : \n");
  fprintf(fo,"Antennas List   : \n");
  fprintf(fo,"Num Channels    : %d\n",hi.nchan);
  fprintf(fo,"Channel width   : %f\n",ms->samprate/2.0E6/hi.nchan);
  fprintf(fo,"Frequency Ch.1  : %f\n",hi.freq);
  fprintf(fo,"Sampling Time   : %d\n",hi.nint);
  fprintf(fo,"Num bits/sample : 8\n");
  fprintf(fo,"Data Format     : integer binary, little endian\n");
  fprintf(fo,"Polarizations   : LL\n");
  fprintf(fo,"MJD             : %d\n",ms->mjd+56000);
  fprintf(fo,"UTC             : %02d:%02d:%02d\n",h,m,s);
  fprintf(fo,"Source          : J1745-2900\n");
  fprintf(fo,"Coordinates     : 17:45:40.0383, -29:00:28.06\n");
  fprintf(fo,"Coordinate Sys  : J2000\n");
  fprintf(fo,"Drift Rate      : 0.0, 0.0\n");
  fprintf(fo,"Obs. Length     : \n");
  fprintf(fo,"Bad Channels    : \n");
  fprintf(fo,"Bit shift value : \n");
*/
	printf("Default Header\n");
	printf("Site            : %s\n",tmp);
	printf("Observer        : %s\n","Nemo");
	printf("Proposal        : %s\n","XX");
	printf("Array Mode      : %s\n","SD");
	printf("Observing Mode  : \n");
	printf("Date            : %s\n","0/0/0");//mjd2date(ms->mjd+56000));
	printf("Num Antennas    : \n");
	printf("Antennas List   : \n");
	printf("Num Channels    : %d\n",hi.nchan);
	printf("Channel width   : %f\n",ms->nchan*ms->samprate/2.0E6/hi.nchan);
	printf("Frequency Ch.1  : %f\n",hi.freq);
	printf("Sampling Time   : %d\n",hi.nint);
	printf("Num bits/sample : %d\n",hi.nbit);
	printf("Data Format     : integer binary, little endian\n");
	printf("Polarizations   : %s\n",hi.polid);
	printf("MJD             : %d\n",ms->mjd+56000);
	printf("UTC             : %02d:%02d:%02d\n",h,m,s);
	printf("Source          : J1745-2900\n");
	printf("Coordinates     : 17:45:40.0383, -29:00:28.06\n");
	printf("Coordinate Sys  : J2000\n");
	printf("Drift Rate      : 0.0, 0.0\n");
	printf("Obs. Length     : \n");
	printf("Bad Channels    : \n");
	printf("Bit shift value : \n");

	i = ftell(fo);
	memset(tmp,0,32);

	return 0;
}

int spec(const char *filename, const char *formatname, const char *outfile, struct filterbank_cfg cfg, long long offset)
{
	struct mark5_stream *ms;
	double **spec;
	float* uplow;
	double *fbout64;
	float  *fbout32;
	char   *fbout8;
	void   *fbout_src;
	size_t fbout_nbytes;
	fftw_complex **zdata, **zx;
	int i, c, first=1, oi;
	int status=0;
	int fftlen, count;
	int nif_used;
	long long total, unpacked;
	FILE *out;
	double f = 1.0, sum, max = -1.0e32, min = 1.0e32;
	double x, y;
	int docomplex;
	struct hd_info hinfo;
	long long nvalidatefail = 0;

	count = 0;
	total = unpacked = 0;

	/* Input stream */
	ms = new_mark5_stream_absorb(
		new_mark5_stream_file(filename, offset),
		new_mark5_format_generic_from_string(formatname) );

	if(!ms)
	{
		printf("Error: problem opening %s\n", filename);

		return EXIT_FAILURE;
	}

	mark5_stream_print(ms);

	if(ms->complex_decode)
	{
		printf("Complex decode\n");
		docomplex = 1;
		fftlen = cfg.nchan;
	}
	else
	{
		docomplex = 0;
		fftlen = 2*cfg.nchan;
	}
	printf("Using %ld u-seconds integration, ", cfg.tint_usec);
	cfg.nint = ((float)cfg.tint_usec) * (ms->samprate*1E-6/fftlen);


	printf("Which is %ld samples (%ld samples per FFT bin)\n", cfg.nint*((long)cfg.nchan), cfg.nint);

	/* Output file */
	out = fopen(outfile, "w");
	if(!out)
	{
		fprintf(stderr, "Error: cannot open %s for write\n", outfile);
		delete_mark5_stream(ms);

		return EXIT_FAILURE;
	}

	/* Allocations */
	spec = (double **)malloc(ms->nchan*sizeof(double *));
	zdata = (fftw_complex **)malloc(ms->nchan*sizeof(fftw_complex *));
	zx = (fftw_complex **)malloc((ms->nchan/2)*sizeof(fftw_complex *));
	uplow = (float *)malloc(sizeof(float)*MAX(strlen(cfg.ifid),ms->nchan));
	bzero(uplow, sizeof(float)*MAX(strlen(cfg.ifid),ms->nchan));

	for(i = 0; i < ms->nchan; ++i)
	{
		spec[i] = (double *)calloc(cfg.nchan, sizeof(double));
		zdata[i] = (fftw_complex *)malloc((cfg.nchan+2)*sizeof(fftw_complex));
	}
	for(i = 0; i < ms->nchan/2; ++i)
	{
		zx[i] = (fftw_complex *)calloc(cfg.nchan, sizeof(fftw_complex));
	}

	nif_used = MIN(strlen(cfg.ifid), ms->nchan);
	for(i = 0; i < strlen(cfg.ifid); i++)
	{
		switch (cfg.ifid[i])
		{
			case 'U': uplow[i] = +1.0; break;
			case 'L': uplow[i] = -1.0; break;
			case 'X': // fall through
			default:  uplow[i] =  0.0; nif_used--; break;
		}
		printf("IF %d is %cSB, uplow[%d]=%f\n", i,cfg.ifid[i],i,uplow[i]);
	}
	if(nif_used != ms->nchan)
	{
		printf("Changing number of output IFs to %d\n", nif_used);
	}

	fbout8 = calloc(cfg.nchan*ms->nchan, sizeof(char));
	fbout32 = calloc(cfg.nchan*ms->nchan, sizeof(float));
	fbout64 = calloc(cfg.nchan*ms->nchan, sizeof(double));
	fbout_nbytes = (cfg.nchan*cfg.nbit_out/8) * (cfg.do_avg_IF ? 1 : nif_used);
	switch (cfg.nbit_out)
	{
		case 8:
			fbout_src = (void*)fbout8;
			break;
		case 32:
			fbout_src = (void*)fbout32;
			break;
		case 64:
			fbout_src = (void*)fbout64;
			break;
		default:
			printf("Not yet implemented: %d-bit output data!\n", cfg.nbit_out);
			exit(-1);
	}

	/* Produce "filterbank" spectrogram output */
	while(status>=0)
	{

		if(ms->complex_decode)
		{
			status=harvestComplexData(ms, spec, zdata, zx, cfg, fftlen, &total, &unpacked);
		}
		else
		{
			status=harvestRealData(ms, spec, zdata, zx, cfg, fftlen, &total, &unpacked);
		}
		nvalidatefail += ms->nvalidatefail;
		//fprintf(stderr, "Pass %d: %Ld / %Ld samples unpacked\n", ++count, unpacked, total);

		if(!cfg.do_binary_out || first)
		{ // normalize across all ifs/channels
			max = sum = 0.0;
			min = 1e32;
			for(c = 0; c < cfg.nchan; ++c)
			{
				for(i = 0; i < ms->nchan; ++i)
				{
					sum += spec[i][c];
					if (spec[i][c]>max) max=spec[i][c];
					if (spec[i][c]<min) min=spec[i][c];
				}
			}
			f = ms->nchan*cfg.nchan/sum;
			// Add 10% to max and min
			if (max>0) {max *=1.1;}
			else {max *=0.9;}
			if (min<0) {min *=1.1;}
			else {min *=0.9;}
		}

		if (first) { printf("Scale factor used is %E\n",f); }

		if (cfg.do_binary_out)
		{
			if (first)
			{
				hinfo.nchan = cfg.nchan;
				if (!cfg.do_avg_IF)
				{
					hinfo.nchan *= nif_used;
				}
				hinfo.nint=cfg.nint/(ms->samprate*1E-6/fftlen);
				hinfo.freq=(uplow[ms->nchan-1]<0)
					? -uplow[ms->nchan-1] /* Lower Side */
					: uplow[ms->nchan-1]+ms->samprate/2.0E6; /* Upper */
				hinfo.max  = max;
				hinfo.min  = min;
				hinfo.mean = 1/f;
				hinfo.nbit = cfg.nbit_out;
				strncpy(hinfo.polid, cfg.polid, nif_used);

				first = print_header(ms, hinfo, out);
			}


			/* Reorder data to decreasing frequency and discard flagged IFs */
			oi = nif_used * cfg.nchan - 1;
			bzero(fbout8, cfg.nchan*ms->nchan*sizeof(char));
			bzero(fbout32, cfg.nchan*ms->nchan*sizeof(float));
			bzero(fbout64, cfg.nchan*ms->nchan*sizeof(double));
			for(i = 0; i<ms->nchan; ++i)
			{
				if (uplow[i] == 0) continue;
				for(c = 0; c < cfg.nchan; ++c)
				{
					// Flip USB into LSB
					double v = (uplow[i]>0) ? spec[i][cfg.nchan-c-1] : spec[i][c];
					// Insert from tail to head direction into output buffer
					fbout64[oi] = v;
					fbout32[oi] = (float)(fbout64[oi]);
					oi--;
				}
			}

			/* Average the retained IFs together? */
			if (cfg.do_avg_IF)
			{
				for(i = 1; i < nif_used; ++i)
				{
					for(c = 0; c < cfg.nchan; ++c)
					{
						fbout64[c] += fbout64[c + i*cfg.nchan];
						fbout32[c] += fbout32[c + i*cfg.nchan];
					}
				}
				for(c = 0; c < cfg.nchan; ++c)
				{
					fbout64[c] /= nif_used;
					fbout32[c] /= nif_used;
				}
			}

			/* Quantize to n-bit integers? */
			if (cfg.nbit_out == 8)
			{
				for(i = 0; i < nif_used; ++i)
				{
					for(c = 0; c < cfg.nchan; ++c)
					{
						int j = c + i*cfg.nchan;
						if (fbout64[j]>max) fbout64[j]=max;
						if (fbout64[j]<min) fbout64[j]=min;
						fbout8[j] = 255*((fbout64[j]-min)/(max-min));
					}
				}
			}

			/* Write filterbank output */
			ssize_t nwr = fwrite(fbout_src, sizeof(char), fbout_nbytes, out);
			printf("%zd/%d samples: n_invalid=%Ld\r", nwr/(cfg.nbit_out/8), count++, nvalidatefail);

		}
		else
		{
			for(c = 0; c < cfg.nchan; ++c)
			{
				fprintf(out, "%f ", (double)c*ms->samprate/(2.0e6*cfg.nchan));
				for(i = 0; i < ms->nchan; ++i)
				{
					if (uplow[i] == 0) continue;
					fprintf(out, " %f", f*spec[i][c]);
				}

				if(cfg.polmode != NOPOL)
				{
					for(i = 0; i < ms->nchan/2; ++i)
					{
						x = creal(zx[i][c])*f;
						y = cimag(zx[i][c])*f;
						fprintf(out, "  %f %f", sqrt(x*x+y*y), atan2(y, x));
					}
				}
				fprintf(out, "\n");
			}
		}

		// Zero the integrations
		for(i = 0; i < ms->nchan; ++i)
		{
			bzero(spec[i], cfg.nchan*sizeof(double));
			bzero(zdata[i], (cfg.nchan+2)*sizeof(fftw_complex));
		}
		if(cfg.polmode != NOPOL)
		{
			for(i = 0; i < ms->nchan/2; ++i)
			{
				bzero(zx[i], cfg.nchan*sizeof(fftw_complex));
			}
		}
	} // end of file

	fclose(out);

	for(i = 0; i < ms->nchan; ++i)
	{
		free(zdata[i]);
		free(spec[i]);
	}
	for(i = 0; i < ms->nchan/2; ++i)
	{
		free(zx[i]);
	}
	free(zx);
	free(zdata);
	free(spec);
	delete_mark5_stream(ms);

	return EXIT_SUCCESS;
}

int main(int argc, char **argv)
{
	struct filterbank_cfg cfg;
	long long offset = 0;
	char* tmp;
	int retval, i;
	/* Defaults */
	cfg.do_avg_IF = 0;
	cfg.do_binary_out = 1;
	cfg.do_intensity_FFT = 0;
	cfg.ifid  = "ULULULULULULULUL"; //  16 IFs swapping Upper Lower, All LHC
	cfg.polid = "LLLLLLLLLLLLLLLL";
	cfg.polmode = VLBA;
	cfg.nbit_out = 8;

#if USEGETOPT
	int opt;
	struct option options[] = {
		{"dbbc", 0, 0, 'B'},
		{"nopol", 0, 0, 'P'},
		{"intensity", 0, 0, 'I'},
		{"avg", 0, 0, 'A'},
		{"ascii", 0, 0, 'a'},
		{"help", 0, 0, 'h'},
		{"polid", 1, 0, 'p'},
		{"ifid", 1, 0, 'i'},
		{"nbits", 1, 0, 'b'},
		{0, 0, 0, 0}
	};

	while ((opt = getopt_long_only(argc, argv, "BPIAahp:i:b:", options, NULL)) != EOF)
	{
		switch (opt)
		{
			case 'A': // Average all IFs together (must have low DM, or very high obs. frequency)
				cfg.do_avg_IF = 1;
				printf("Averaging together all IFs\n");
				break;

			case 'B': // DBBC Pol mode (all Rcp then all LCP)
				cfg.polmode = DBBC;
				printf("Assuming DBBC polarisation order\n");
				break;

			case 'P': // Don't compute cross pols
				cfg.polmode = NOPOL;
				printf("Not computing cross pol terms\n");
				break;

			case 'I': // compute Intensity FFT
				cfg.do_intensity_FFT = 1;
				printf("Computing Intensity FFT\n");
				break;

			case 'a': // Ascii output
				cfg.do_binary_out = 0;
				printf("Outputing ASCII file\n");
				break;

			case 'p': // polstring
				cfg.polid = strdup(optarg);
				printf("Pol ID string: %s\n", cfg.polid);
				break;

			case 'i': // polstring
				cfg.ifid = strdup(optarg);
				printf("IF ID string: %s\n", cfg.ifid);
				break;

			case 'b': // nbits
				cfg.nbit_out = atoi(optarg);
				printf("Quantizing filterbank output to %d bits\n", cfg.nbit_out);
				break;

			case 'h': // help
				usage(argv[0]);
				return EXIT_SUCCESS;
				break;
		}
	}

#else
	int optind=1;
#endif

	oldsiginthand = signal(SIGINT, siginthand);


	if((argc-optind) == 1)
	{
		struct mark5_format *mf;
		int bufferlen = 1<<11;
		char *buffer;
		FILE *in;
		int r;

		if(strcmp(argv[1], "-h") == 0 ||
		   strcmp(argv[1], "--help") == 0)
		{
			usage(argv[0]);

			return EXIT_SUCCESS;
		}

		in = fopen(argv[1], "r");
		if(!in)
		{
			fprintf(stderr, "Error: cannot open file '%s'\n", argv[1]);

			return EXIT_FAILURE;
		}

		buffer = malloc(bufferlen);

		r = fread(buffer, bufferlen, 1, in);
		if(r < 1)
		{
			fprintf(stderr, "Error, buffer read failed.\n");

			fclose(in);
			free(buffer);

			return EXIT_FAILURE;
		}
		else
		{
			mf = new_mark5_format_from_stream(new_mark5_stream_memory(buffer, bufferlen/2));

			print_mark5_format(mf);
			delete_mark5_format(mf);

			mf = new_mark5_format_from_stream(
					new_mark5_stream_memory(buffer,
					bufferlen/2)
				);

			print_mark5_format(mf);
			delete_mark5_format(mf);
		}

		fclose(in);
		free(buffer);

		return EXIT_SUCCESS;
	}

	else if(argc-optind < 5)
	{
		usage(argv[0]);

		return EXIT_FAILURE;
	}

	cfg.nchan     = atoi(argv[optind+2]);
	cfg.tint_usec = atol(argv[optind+3]);
	cfg.npol      = strlen(cfg.polid);
	cfg.nif       = strlen(cfg.ifid);
	printf("Number of IFs labelled: %d\nNumber of POLs labelled: %d\n", cfg.nif, cfg.npol);
	if(cfg.npol != cfg.nif)
	{
		printf("These don't match assuming missing are same as last");
		if(cfg.npol < cfg.nif)
		{
			tmp = strdup(cfg.polid);
			cfg.polid = malloc(cfg.nif*sizeof(char));
			strcpy(cfg.polid, tmp);
			for(i = cfg.npol; i < cfg.nif; i++)
			{
				cfg.polid[i] = cfg.polid[cfg.npol-1];
			}
			cfg.npol = cfg.nif;
		}
		if(cfg.npol > cfg.nif)
		{
			tmp = strdup(cfg.ifid);
			cfg.ifid = malloc(cfg.npol*sizeof(char));
			strcpy(cfg.ifid, tmp);
			for(i = cfg.nif; i < cfg.npol; i++)
			{
				cfg.ifid[i] = cfg.ifid[cfg.npol-1];
			}
			cfg.nif = cfg.npol;
		}
	}

	if(cfg.tint_usec <= 0)
	{
		cfg.tint_usec = 2000000000L;
	}

	if((argc-optind) > 5)
	{
		offset = atoll(argv[optind+5]);
	}

	retval = spec(argv[optind], argv[optind+1], argv[optind+4], cfg, offset);

	return retval;
}
