%
% function [fn,rp,tn]=solve_polyco(fname,tn)
%
% Reads a TEMPO/TEMPO2 polyco file.
% Reads forward until it is close or EOF.
% Returns period and profile phase at a given time:
%    solve_polyco(filename=polyco.dat,tn=MJD of start)
%
% The time can also be specified as multiple values in a vector,
% containing all fractional MJDs for which to solve the phase for.
% To get solutions over 24h:
%    solve_polyco(filename=polyco.dat,tn=MJD of start+[0:1/1440:1])
%
% Written for octave, but should still be matlab compatable.
% (C) Richard Dodson
%
function [fn,rp,tn]=solve_polyco(fname,tn)

fpoly=fopen(fname,'r');
fseek(fpoly,0,1);
eof=ftell(fpoly);
fseek(fpoly,0,-1);
dt=1e4;

% Find out if the polynomial terms are in fortran or C-style doubles
tipe='E';
str=fgets(fpoly); % line 1 : src name | date dd-mm-YY | time hhmmss.ss | MJD | DM? | ? | ?
str=fgets(fpoly); % line 2 : ? | ? | station ID | poly time len? | Ncoeff | freq GHz
str=fgets(fpoly); % line 3 : first three poly coeffs
if (findstr(str,'D')),
    tipe='D'; 
end
fseek(fpoly,0,-1);

fn = nan(numel(tn),1);
rp = nan(numel(tn),1);

while (ftell(fpoly)<eof),
    
     s=fgetl(fpoly); % line 1: src name etc, with MJD
     tm=sscanf(s(35:55),'%f',1);
     
     s=fgetl(fpoly); % line 2
     r0=sscanf(s(1:20),'%f',1);  % ? (some very large number)
     f0=sscanf(s(21:40),'%f',1); % pulsar 1/period
     nc=sscanf(s(50:54),'%d',1); % num poly coeffs
     tp=sscanf(s(45:49),'%f',1); % length of poly validity in minutes, centered around poly's MJD
     
     if (tipe == 'E'),
       p=fscanf(fpoly,'%f',nc);
     else
       tmp=fscanf(fpoly,'%fD%d',nc*2);
       for n=1:nc,
            p(n)=tmp(2*n-1)*10^(tmp(2*n));
       end
     end
     fgetl(fpoly); % remove linefeed
     
     if (tn(1)==0),
         tn = tn+tm; 
         fprintf(1, 'Changed unspecified start time (0) to %f\n', tm(1));
     end
     
     dt=1440.0*(tn-tm); % 1440=minutes per day

     % Valid time range (indices) of current poly out of requested time range
     idx = find((dt>=-tp/2) & (dt < tp/2));
     if numel(idx)<0,
         continue;
     end

     % http://tempo.sourceforge.net/ref_man_sections/tz-polyco.txt
     % "The pulse phase and frequency at time T are then calculated as:
     %  DT = (T-TMID)*1440
     %  PHASE = RPHASE + DT*60*F0 + COEFF(1) + DT*COEFF(2) + DT^2*COEFF(3) + ....
     %  FREQ(Hz) = F0 + (1/60)*(COEFF(2) + 2*DT*COEFF(3) + 3*DT^2*COEFF(4) + ....)
     % "

     % Pulse period
     tmp=0;
     for n=1:(nc-1),
         tmp = tmp + n.*p(n+1).*(dt.^(n-1));
     end
     fn(idx) = f0 + 1/60.0*tmp(idx);
     
     % Pulse phase
     tmp = r0 + dt*60*f0;
     for n=1:nc, 
         tmp = tmp + p(n).*(dt.^(n-1));
     end
     rp(idx) = tmp(idx);
     % rp(idx) = tmp(idx) - floor(tmp(idx));
end
fclose(fpoly);

% To find the phase jump from one run to the next
%
% run as
% [fn,rp1,tn]=solve_polyco('polyco.dat.1',START_TIME_1)
% then
% [fn,rp2,tn]=solve_polyco('polyco.dat.2',START_TIME_2)
% Then rp1-rp2 is the jump in phase from one run to the next.

