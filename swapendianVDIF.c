/********************************************************************************
 **
 ** swapendianVDIF 1.0
 **
 ** Usage:    modifyVDIF <file.vdif> [<outputfile.vdif>]
 **
 ** Changes the Endianness of VDIF payload data in 32-bit chunks.
 ** Created for NOEMA December 2020 test, where samples in 8-bit blocks (4 channels)
 ** are ordered correctly, but the time series of those 8-bit blocks has wrong
 ** order i.e. is Big Endian instead of VDIF Little Endian.
 **
 ** (C) 2020 Jan Wagner
 **
 *******************************************************************************/

#define NUM_BUFFERED_FRAMES   512 // should be small to fit in CPU cache

#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <malloc.h>
#include <math.h>
#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#ifndef _BSD_SOURCE
    #define _BSD_SOURCE
#endif
#include <endian.h>

void usage()
{
    printf("\n"
           "ModifyVDIF v1.00  Jan Wagner 20201204\n\n"
           "Changes the Endianness of VDIF payload data in 32-bit chunks.\n"
           "Created for NOEMA December 2020 test, where samples in 8-bit blocks (4 channels)\n"
           "are ordered correctly, but the time series of those 8-bit blocks has wrong\n"
           "order i.e. is Big Endian instead of VDIF Little Endian.\n\n"
           "Usage: swapendianVDIF <file.vdif> [<different output file.vdif>]\n"
           "\n"
    );
    return;
}


int main(int argc, char** argv)
{
    const size_t headersize = 32;

    int     fdi, fdo, i, w;
    off64_t rdpos = 0, wrpos = 0;
    ssize_t nrd, nwr;
    size_t  framesize, payloadwords;
    uint32_t t0_orig;

    uint32_t hdr32[4]      = {0,0,0,0};
    unsigned char* buf = NULL;

    /* Open file to modify. Create different output file, if specified. */
    if (argc != 2 && argc != 3)
    {
        usage();
        exit(-1);
    }
    fdi = open(argv[1], (argc == 2) ? (O_RDWR|O_LARGEFILE) : (O_RDONLY|O_LARGEFILE));
    if (fdi < 0)
    {
        printf("Error opening file '%s': %s\n", argv[optind], strerror(errno));
        exit(-1);
    }
    if (argc == 3)
    {
        fdo = open(argv[2], O_RDWR|O_LARGEFILE|O_CREAT, 0644);
        if (fdo < 0)
        {
            printf("Error opening output file '%s': %s\n", argv[2], strerror(errno));
            exit(-1);
        }
        printf("Mode        : input file will be copied to output file with modifications\n");
    }
    else
    {
        printf("Mode        : input file will be modified on the fly\n");
        fdo = fdi;
    }

    /* Check the first frame */
    nrd = read(fdi, (void*)&hdr32, sizeof(hdr32));
    lseek64(fdi, 0, SEEK_SET);

    /* Get some infos from the first frame */
    t0_orig   = (hdr32[0] & 0x3FFFFFFF);
    framesize = (hdr32[2] & 0x00FFFFFF) * 8;
    if ((framesize > 4*1024*1024) || (framesize < 1024))
    {
        printf("Frame size in first header is suspicious (%zu bytes). Quitting!\n", framesize);
        exit(-1);
    }
    buf = (unsigned char*)memalign(16, framesize*NUM_BUFFERED_FRAMES);
    payloadwords = (framesize - headersize) / sizeof(uint32_t);

    /* Make a modification pass over the file */
    printf("Start second: %u\n", t0_orig);
    printf("Frame size  : %zu\n", framesize);
    printf("Payload size: %zu 32-bit words\n", payloadwords);
    setbuf(stdout, NULL);
    while (1)
    {
        int nframes;

        lseek64(fdi, rdpos, SEEK_SET);
        nrd = read(fdi, (void*)buf, framesize*NUM_BUFFERED_FRAMES);
        if (nrd <= 0)
        {
            break;
        }
        rdpos += nrd;

        nframes = nrd / framesize;
        for (i=0; i<nframes; i++)
        {
            uint32_t* h32 = (uint32_t*)(buf + i*framesize);
            uint32_t* p32 = h32 + 8; // payload after 8-word

            for (w=0; w<payloadwords; w++)
            {
                p32[w] = be32toh(p32[w]);
            }
        }

        lseek64(fdo, wrpos, SEEK_SET);
        nwr = write(fdo, (void*)buf, nrd);
        wrpos += nwr;

        // putchar('.');
        // printf("\33[2K\r %.0f MByte", ((double)rwpos)/(1048576.0) );
        printf("\33[2K\r %.0f GByte", ((double)rdpos)*9.31322574615479e-10 ); // 1/2^30

    }

    printf("\nDone.\n");
    return 0;
}
