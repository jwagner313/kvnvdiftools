/********************************************************************************
 **
 ** m5tvg v1.0
 **
 ** Usage: m5tvg <options> <file.vdif>
 ** --header=n remove n-byte headers
 **
 ** Looks for VSI-H TVG pattern in a file.
 **
 ** (C) 2014 Jan Wagner
 **
 *******************************************************************************/

#define TVG_PERIOD (32768-1)      // native period of pseudorandom VSI-H TVG generator
#define NBITS 32                  // standard number of VSI-H TVG bits
#define OUTFILE_XC "m5tvg.xcorr"  // file in which to store results
#define OUTFILE_S  "m5tvg.samp"   // file in which to store samples

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <malloc.h>
#include <math.h>
#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

#ifndef _BSD_SOURCE
    #define _BSD_SOURCE
#endif
#include <endian.h>

void calculate_tvg(uint32_t* tvg, size_t len);
void xc_float(float* v1, float* v2, double* xc_accu, size_t len);
void split32(uint32_t* in, float** out, size_t len);

void usage(void) {
    printf("\n"
           "Usage: m5tvg <--vdif|--m5b> <filename>\n"
           "\n"
           "   Looks for a VSI-H Test Vector Generator (TVG) pattern in the file.\n\n"
           "   Any headers specific to the file format are ignored and only\n"
           "   the actual payload is inspected. The program shows the normalized\n"
           "   cross-correlation between each expected pseudorandom TVG bitstream and\n"
           "   the respective actual bitstream (BS[00] to BS[31]) in the file.\n"
           "   A cross-correlation of +1.0 means perfect correlation, while -1.0 means\n"
           "   perfect anti-correlation.\n"
           "\n"
           "   The program assumes that the full 32 bits of the VSI bus\n"
           "   were captured during recording time (VSI mask 0xffffffff).\n"
           "\n"
    );
    return;
}


int main(int argc, char** argv) {
    int fdi;
    ssize_t nrd;
    size_t  nrd_total = 0;

    FILE* fo;

    size_t framesize;
    size_t header_len;
    size_t payload_len;
    size_t frames_per_period;

    uint32_t* buf;
    uint32_t* tvg;
    float*    buf_float[NBITS];
    float*    tvg_float[NBITS];

    double*   xc[NBITS];
    double    ac0_tvg[NBITS], ac0_file[NBITS];

    int i, j, k;
    size_t n32read;

    typedef enum {NONE, VDIF, Mark5B} data_format;
    data_format df;

    if (argc != 3)
    {
        usage();
        exit(-1);
    }
    fdi = open(argv[2], O_RDONLY|O_LARGEFILE);
    if (fdi < 0)
    {
        printf("Error opening file '%s': %s\n", argv[2], strerror(errno));
        exit(-1);
    }

    if (strcmp(argv[1], "--vdif") == 0)
    {
        uint32_t hdr[4];
        read(fdi, &hdr[0], sizeof(hdr));
        lseek64(fdi, 0, SEEK_SET);

        framesize = 8 * (hdr[2] & 0x00FFFFFF);
        header_len = (hdr[0] & 0x40000000)!=0 ? 16 : 32;
        payload_len = framesize - header_len;
        df = VDIF;

        printf("VDIF header info : %zu-byte header, %zu-byte payload\n", header_len, payload_len);
    }
    else if (strcmp(argv[1], "--m5b") == 0)
    {
        header_len = 16;
        payload_len = 10000;
        framesize = 10016;
        df = Mark5B;
    }
    else
    {
        usage();
        exit(-1);
    }
    frames_per_period = 1 + ((size_t)(sizeof(uint32_t) * TVG_PERIOD)) / payload_len;

    /* Allocs */
    buf = malloc(sizeof(uint32_t) * TVG_PERIOD + payload_len);
    tvg = malloc(sizeof(uint32_t) * (TVG_PERIOD + 1));
    for (i=0; i<NBITS; i++)
    {
       buf_float[i] = malloc(sizeof(float) * TVG_PERIOD * 2);
       tvg_float[i] = malloc(sizeof(float) * TVG_PERIOD);
       xc[i] = malloc(sizeof(double) * TVG_PERIOD);
       memset(xc[i], 0, sizeof(double) * TVG_PERIOD);
    }

    /* Precalculate TVG */
    calculate_tvg(tvg, TVG_PERIOD+1);
    assert(tvg[0] == tvg[TVG_PERIOD]);
    split32(tvg, tvg_float, TVG_PERIOD);

    /* Load at least one TVG period worth of input file samples */
    while (nrd_total < frames_per_period*payload_len)
    {
        char* p = ((char*)buf) + nrd_total;
        nrd = read(fdi, (void*)p, header_len);  // header is ignored
        nrd = read(fdi, (void*)p, payload_len);
        if (nrd <= 0)
        {
            printf("short read after %zu bytes : %s\n", nrd_total, strerror(errno));
            break;
        }

        nrd_total += nrd;
    }
    n32read = nrd_total / sizeof(uint32_t);
    n32read = (n32read > TVG_PERIOD) ? TVG_PERIOD :  n32read;
    printf("Wanted %zu 32-bit words, got %zu.\n", frames_per_period*payload_len/sizeof(uint32_t), n32read);

    /* Decode file 32x1-bit data into float arrays, duplicate to avoid modulo in cross-corr */
    split32(buf, buf_float, TVG_PERIOD);
    for (i=0; i<NBITS; i++)
    {
         memcpy(buf_float[i]+TVG_PERIOD, buf_float[i], TVG_PERIOD*sizeof(float));
    }

    /* Autocorrelations of TVG pattern and file data */
    for (i=0; i<NBITS; i++)
    {
        ac0_tvg[i] = 0.0;
        ac0_file[i] = 0.0;

        xc_float(tvg_float[i], tvg_float[i], &ac0_tvg[i],  TVG_PERIOD);
        xc_float(buf_float[i], buf_float[i], &ac0_file[i], TVG_PERIOD);

        ac0_tvg[i]  = ac0_tvg[i]  / ((double)n32read);
        ac0_file[i] = ac0_file[i] / ((double)n32read);

        printf("Autocorr of BS[%02u]: TVG %f, file %f\n", i, ac0_tvg[i], ac0_file[i]);
    }

    /* Cross-corr by naive approach */
    for (i=0; i<NBITS; i++)
    {
        float* a = tvg_float[i];
        float* b = buf_float[i];
        double xc_max = 0.0;
        int    xc_imax = 0, nerrors = 0;

        printf("TVG BS[%02u] x File BS[%02u] : ", i,i);
        for (j=0; j<TVG_PERIOD; j++)
        {
            for (k=0; k<TVG_PERIOD; k++)
            {
                xc[i][k] += a[j]*b[j+k];
            }
        }
        for (k=0; k<TVG_PERIOD; k++)
        {
            xc[i][k] /= ((double)n32read);
            xc[i][k] /= sqrt(ac0_tvg[i] * ac0_file[i]);
        }

        for (k=0; k<TVG_PERIOD; k++)
        {
            if (abs(xc[i][k]) > abs(xc_max))
            {
                xc_max = xc[i][k];
                xc_imax = k;
            }
        }
        for (k=0; k<TVG_PERIOD; k++)
        {
            if (a[k] != b[k + xc_imax]) nerrors++;
        }
        printf("corr %+.5f : offset %5d : %5d bit errors : BER=%.2f%%\n", xc_max, xc_imax, nerrors, 100.0*((double)nerrors)/TVG_PERIOD);
    }

    /* Write results into text files */
    fo = fopen(OUTFILE_XC, "w");
    if (NULL != fo)
    {
        for (i=0; i<TVG_PERIOD; i++)
        {
            fprintf(fo, "%6d ", i);
            for (j=0; j<NBITS; j++)
            {
                fprintf(fo, " %.4e ", xc[j][i]);
            }
            fprintf(fo, "\n");
        }
        fclose(fo);
        printf("Done. Cross-correlations written to '%s'.\n", OUTFILE_XC);
    }
    fo = fopen(OUTFILE_S, "w");
    if (NULL != fo)
    {
        for (i=0; i<TVG_PERIOD; i++)
        {
            fprintf(fo, "%6u ", i);
            fprintf(fo, "%u ", tvg[i]);
            for (j=0; j<NBITS; j++)
            {
                fprintf(fo, " %+.0f ", tvg_float[j][i]);
            }
            fprintf(fo, "%u ", buf[i]);
            for (j=0; j<NBITS; j++)
            {
                fprintf(fo, " %+.0f ", buf_float[j][i]);
            }
            fprintf(fo, "\n");
        }
        fclose(fo);
        printf("Done. Sample data written to '%s'.\n", OUTFILE_S);
    }

    return 0;
}


/**
 * Calculate the full VSI-H TVG pattern.
 * The source code is based on tvg32.c from CRESTech, Dec 2001
 */
void calculate_tvg(uint32_t* tvg, size_t len)
{
    size_t i;
    if ((tvg == NULL) || (len < 1)) return;

    tvg[0] = 0xBFFF4000;

    for (i=1; i<len; i++)
    {
        uint16_t tvg16;
        uint16_t tvg_register;

        tvg16 = tvg[i-1] & 0x0000ffff;
        tvg_register = tvg16 >> 1;
        tvg16 = (tvg_register ^ (tvg_register >> 1));
        tvg16 &= 0x3fff;
        tvg16 |= ((tvg16 ^ (tvg16 >> 1)) << 15 );
        tvg16 |= ( ((tvg_register >> 14) ^ tvg16) & 1) << 14;
        tvg[i] = ((uint32_t)tvg16) | ((~(uint32_t)tvg16) << 16);
   }
}

/**
 * Split 32 x 1-bit data into 32 separate arrays of type 'float'.
 */
void split32(uint32_t* in, float** out, size_t len)
{
    size_t i;
    int j;
    for (i=0; i<len; i++)
    {
        for (j=0; j<NBITS; j++)
        {
            out[NBITS-j-1][i] = (in[i] & ((uint32_t)1 << j)) ? +1.0 : -1.0;
        }
    }
}

/**
 * Calculate the cross-correlation of two 'float' vectors,
 * without performing any scaling.
 */
void xc_float(float* v1, float* v2, double* xc_accu, size_t len)
{
   size_t i;
   for (i=0; i<len; i++)
   {
      *xc_accu += (v1[i] * v2[i]);
   }
}
