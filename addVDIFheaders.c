/********************************************************************************
 ** Takes "raw" sample data and adds VDIF headers.
 ** The raw input data are assumed to have started at an integer second.
 **
 ** Version 1.1
 **
 ** (C) 2014 Jan Wagner
 **
 *******************************************************************************/

// gcc -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE -D_LARGEFILE_SOURCE -O3 addVDIFheaders.c -o addVDIFheaders -lm

#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <malloc.h>
#include <math.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#define VDIF_HEADER_LEN   32         // length of standard VDIF header in bytes

static unsigned char m_vlba2vdif[256] __attribute__ ((aligned (16)));

static struct option opts[] =
{
    {"nbits",     required_argument, 0, 'b'},
    {"complex",   no_argument, 0, 'C'},
    {"nchannels", required_argument, 0, 'c'},
    {"refepoch",  required_argument, 0, 'r'},
    {"framelen",  required_argument, 0, 'l'},
    {"station",   required_argument, 0, 's'},
    {"starttime", required_argument, 0, 't'},
    {"vlba",      no_argument, 0, 'v'},
    {0, 0, 0, 0}
};

void usage(void)
{
    printf("\n"
           "Usage: addVDIFheaders [--nbits|-b <n>] [--nchannels|-c <n>] [--refepoch|-r <n>] [--vlba|-v]\n"
           "                      [--complex|-C] [--framelen|-l <n byte>] [--station|-s ID] [--starttime|-t seconds]\n"
           "                      <infile.raw> <outfile.vdif> <rate in Mbit/s> [<offset>]\n\n"
           "Encapsulates data from infile.raw into VDIF frames written to outfile.vdif.\n"
           "The VDIF headers use the provided information. The VDIF header seconds and frame number\n"
           "are incremented automatically in the output file.\n\n"
           "Flags:\n"
           "   --vlba          Convert 2-bit samples into VDIF encoding\n"
           "   --complex       Set the Complex data type indicator bit\n\n"
           "Parameters:\n"
           "   --nbits=n       Bits per sample\n"
           "   --nchannels=n   Number of channels\n"
           "   --refepoch=n    VDIF reference epoch\n"
           "   --station=ID    Station name\n"
           "   --framelen=n    Length of frame (with 32-byte header) in bytes\n\n"
    );
}

int main(int argc, char** argv)
{
    int fdi, fdo;
    ssize_t nrd, nwr, n;

    uint32_t payloadsize   = 8192;
    uint32_t datarate_Mbps = 2048;
    uint32_t datarate_fps  = 0;
    uint32_t refepoch      = 0x29;
    uint32_t datasecond    = 0;
    uint32_t framenumber   = 0;
    uint32_t framesize32   = 64;
    uint32_t numchannels   = 1;
    uint32_t numbits       = 2;
    char station[2] = "NA";
    int do_2bit_conversion = 0;
    int is_complex = 0;

    unsigned char* frame;
    unsigned char* payload;
    uint32_t*      frame32;

    uint64_t total_frame_count64 = 0;

    /* Prepare lookup */
    for (n = 0; n < 256; n++)
    {
        // test case
        //   b = 0x1B; // 0b00 01 10 11  so expecting  swp = 0b00 10 01 11 = 0x27 as output
        unsigned char b = (unsigned char)n;
        unsigned char s0 = b & 0xAA;  // 0xAA = 0b101010
        unsigned char m0 = b & 0x55;  // 0x55 = 0b010101
        unsigned char swp = (s0 >> 1) | (m0 << 1);
        m_vlba2vdif[b] = swp;
    }


    /* Arguments */
    while (1)
    {
        int c, optidx = 0;
        uint32_t val = 0;

        c = getopt_long (argc, argv, "b:c:r:l:s:t:vC", opts, &optidx);
        if (c == -1)
        {
            break;
        }

        if (optarg)
        {
            val = atol(optarg);
        }

        switch (c)
        {
            case 0:
                printf("option %s", opts[optidx].name);
                if (optarg)
                {
                    printf(" with arg %s", optarg);
                }
                printf("\n");
                break;
            case 'b':
                numbits = val;
                break;
            case 'c':
                numchannels = val;
                break;
            case 'r':
                refepoch = val;
                break;
            case 'l':
                payloadsize = val - VDIF_HEADER_LEN;
                break;
            case 's':
                if (strlen(optarg) != 2)
                {
                    printf("Warning: station ID must be 2 characters long (got '%s')!\n", optarg);
                    continue;
                }
                station[0] = optarg[0];
                station[1] = optarg[1];
                break;
            case 't':
                datasecond = val;
                break;
            case 'v':
                do_2bit_conversion = 1;
                break;
            case 'C':
                is_complex = 1;
                break;
            default:
                usage();
                exit(-1);
        }
    }

    if ((argc-optind) != 3 && (argc-optind) != 4)
    {
        usage();
        exit(-1);
    }

    fdi = open(argv[optind++], O_RDONLY|O_LARGEFILE);
    if (fdi == -1)
    {
        perror("open() of input file");
        return -1;
    }

    fdo = open(argv[optind++],
                O_WRONLY|O_CREAT|O_LARGEFILE,
                S_IRUSR|S_IWUSR | S_IRGRP|S_IWGRP | S_IROTH
          );
    if (fdo == -1)
    {
        perror("open() of output file");
        return -1;
    }

    datarate_Mbps = atoi(argv[optind++]);
    if (optind != argc)
    {
        printf("seeking to %zu\n", (size_t)atoll(argv[optind++]));
        lseek(fdi, atoll(argv[optind++]), SEEK_SET);
    }

    /* Default frame */
    frame = memalign(4096, VDIF_HEADER_LEN + payloadsize);
    framesize32 = (payloadsize + VDIF_HEADER_LEN)/8;
    payload = frame + VDIF_HEADER_LEN;

    datarate_fps = ((double)datarate_Mbps) * 1e6 / (8.0 * (double)payloadsize);
    framenumber  = 0;
    datasecond  &= 0x3FFFFFFF;
    numbits     -= 1;
    numchannels  = ((uint32_t)log2((double)numchannels)) & 0x1F;
    refepoch     = refepoch & 0x3F;

    memset((void*)frame, 0x00, VDIF_HEADER_LEN);
    frame32    = (uint32_t*)frame;
    frame32[0] = datasecond;
    frame32[1] = (refepoch<<24) | framenumber;
    frame32[2] = (numchannels<<24) | framesize32;
    frame32[3] = ((is_complex&1)<<31) | (numbits<<26) | ((uint32_t)station[0]) | (((uint32_t)station[1]) << 8);
    frame32[4] = 0; // Extend Data Version of 0x00: no extended data

    /* Summarize header */
    printf("Payload data rate %d Mbit/s, frame size %d byte, frame rate %d fps.\n", datarate_Mbps, framesize32*8, datarate_fps);
    printf("Raw samples are %s valued %d-bit from %d channels.\n", is_complex ? "complex":"real", numbits+1, (int)pow(2,numchannels));
    printf("Sample encoding conversion is %s.\n", do_2bit_conversion ? "enabled" : "disabled");

    /* Construct output file */
    while (1)
    {

        // Read payload, write header and payload
        nrd = read(fdi, (void*)payload, payloadsize);
        if (nrd < payloadsize)
        {
            break;
        }

        // Convert from VLBA 2-bit encoding into VDIF 2-bit encoding
        if (do_2bit_conversion)
        {
            for (n = 0; n < payloadsize; n += 4)
            {
                payload[n+0] = m_vlba2vdif[payload[n+0]];
                payload[n+1] = m_vlba2vdif[payload[n+1]];
                payload[n+2] = m_vlba2vdif[payload[n+2]];
                payload[n+3] = m_vlba2vdif[payload[n+3]];
            }
        }
        nwr = write(fdo, (void*)frame, VDIF_HEADER_LEN + payloadsize);
        if (nwr != VDIF_HEADER_LEN + payloadsize) { printf("short write!\n"); }

        total_frame_count64++;

        // Update header before next VDIF frame
        if (++framenumber >= datarate_fps)
        {
            framenumber = 0;
            datasecond++;
            frame32[0] = datasecond;
            putchar('.');
        }
        frame32[1] = (refepoch<<24) | framenumber;

    }

    printf("\nDone. %lu frames processed.\n", total_frame_count64++);
    close(fdi);
    close(fdo);

    return 0;
}
