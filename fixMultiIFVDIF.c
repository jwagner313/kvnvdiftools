/********************************************************************************
 **
 ** fixMultiIFVDIF v1.0
 **
 ** Usage:    fixMultiIFVDIF <options> <infile.vdif> <outfile.vdif>
 **
 ** Quick hack to correct a special case of VDIF data produced by FILA10G
 ** that gets inputs from VSI 1-2-3-4 or from VSI 1-2, with the data from
 ** each VSI being single-channel wideband data. Correct glue'ing of multiple
 ** single-channel VSI can be accomplished with the 'chan_perm' register
 ** on the FILA10G, to change from the default grouping by VSI (32+32(+32+32) -bit
 ** concatenation) and instead use correct grouping of same-time samples (via
 ** bit shuffling together 2-bit samples of the same time instant from each VSI).
 **
 ** (C) 2015 Jan Wagner
 **
 *******************************************************************************/

// Original 2-VSI concatenated input
//   00000020  ba 3a f5 fe 25 4e 4a 66  8c 1d c7 f7 74 39 9e e2
//   word0   ba 3a f5 fe  0xfef53aba = 0b11111110111101010011 10 10 10 11 10 10
//   word1   25 4e 4a 66  0x664a4e25 = 0b01100110010010100100 11 10 00 10 01 01
// Should be merged into
//   word0   1110 1010 0010 1011 0110 0110
//   word1   ...                      0011
// Output 2-VSI interleaved data
//   00000020  66 2b ea 43 99 73 7a 7b  70 6c b5 0d db b4 1b fb
//   word0   66 2b ea 43  0x43ea2b66 = 0b0100 0011 1110 1010 0010 1011 0110 0110
//   word1   99 73 7a 7b  0x7b7a7399 = 0b0111 1011 0111 1010 0111 0011 1001 1001

#define NUM_BUFFERED_FRAMES   512 // should be small to fit in CPU cache
#define VDIF_HEADER_LEN        32 // the non-legacy VDIF header is 32 byte long
#define VDIF_HEADER_LEN32       8 // same in units of 32-bit words

#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <malloc.h>
#include <math.h>
#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

void usage(void)
{
    printf("\n"
           "Usage: fixMultiIFVDIF.c <2|4 (number of VSI buses)> <infile.vdif> <outfile.vdif>]\n"
           "\n"
    );
    return;
}


int main(int argc, char** argv)
{
    int     fdi, fdo;
    off64_t rdpos = 0;
    ssize_t nrd, nwr;
    size_t  framesize;
    size_t  groups_per_frame;

    unsigned char *buf_in = NULL;
    unsigned char *buf_out = NULL;

    uint32_t hdr32[VDIF_HEADER_LEN32] = {0};
    int      Nvsi = 4;

    /* Arguments */
    if (argc != 4)
    {
        usage();
        exit(-1);
    }
    Nvsi = atoi(argv[1]);
    if ((Nvsi != 2) && (Nvsi != 4))
    {
        printf("Error: specified number of VSI buses (%s) must be either 2 or 4!\n", argv[1]);
        exit(-1);
    }

    /* Open files */
    fdi = open(argv[2], O_RDONLY|O_LARGEFILE);
    if (fdi < 0)
    {
        printf("Error opening file '%s': %s\n", argv[2], strerror(errno));
        exit(-1);
    }
    fdo = open(argv[3], O_RDWR|O_LARGEFILE|O_CREAT, 0644);
    if (fdo < 0)
    {
        printf("Error opening output file '%s': %s\n", argv[3], strerror(errno));
        exit(-1);
    }

    /* Check frame size */
    nrd = read(fdi, (void*)&hdr32, sizeof(hdr32));
    framesize = (hdr32[2] & 0x00FFFFFF) * 8; // VDIF: size is in 8-byte units
    if ((framesize > 4*1024*1024) || (framesize < 1024))
    {
        printf("Frame size in first header is suspicious (%zu bytes). Quitting!\n", framesize);
        exit(-1);
    }
    buf_in  = (unsigned char*)memalign(128, framesize*NUM_BUFFERED_FRAMES);
    buf_out = (unsigned char*)memalign(128, framesize*NUM_BUFFERED_FRAMES);
    groups_per_frame = (framesize-VDIF_HEADER_LEN)/(Nvsi*sizeof(uint32_t));

    /* Perform copying */
    lseek64(fdi, 0, SEEK_SET);
    setbuf(stdout, NULL);
    while (1)
    {
        size_t nframes, i, j, k;
        uint32_t *src32 = (uint32_t*)buf_in;
        uint32_t *dst32 = (uint32_t*)buf_out;

        nrd = read(fdi, (void*)buf_in, framesize*NUM_BUFFERED_FRAMES);
        if (nrd <= 0)
        {
            break;
        }
        rdpos += nrd;

        nframes = nrd / framesize;
        memset(dst32, 0x00, nframes*framesize);

        for (i=0; i<nframes; i++)
        {
            // Copy header, initialize frame contents
            memcpy(dst32, src32, VDIF_HEADER_LEN);

            // Shuffle 2-bit samples from 2 or 4 VSI together in time
            src32 += VDIF_HEADER_LEN32;
            dst32 += VDIF_HEADER_LEN32;
            for (j=0; j<groups_per_frame; j++)
            {
                if (Nvsi == 2)
                {
                    uint32_t in1 = src32[0];
                    uint32_t in2 = src32[1];
                    for (k=0; k<16; k++)
                    {
                        // 16 samples per 32-bit word, taken from 64-bit input
                        uint32_t new = ((in2 & 3) << 2) | (in1 & 3);
                        dst32[k/8] = dst32[k/8] | (new << 4*(k % 8));
                        in1 >>= 2;
                        in2 >>= 2;
                    }
                }
                else if (Nvsi == 4)
                {
                    uint32_t in1 = src32[0];
                    uint32_t in2 = src32[1];
                    uint32_t in3 = src32[2];
                    uint32_t in4 = src32[3];
                    for (k=0; k<16; k++)
                    {
                        // 16 samples per 32-bit word, taken from 128-bit input
                        uint32_t new = ((in4 & 3) << 6) | ((in3 & 3) << 4) | ((in2 & 3) << 2) | (in1 & 3);
                        dst32[k/4] = dst32[k/4] | (new << 8*(k % 4));
                        in1 >>= 2;
                        in2 >>= 2;
                        in3 >>= 2;
                        in4 >>= 2;
                    }
                }
                src32 += Nvsi;
                dst32 += Nvsi;
            }
        }

        nwr = write(fdo, (void*)buf_out, nframes*framesize);

        // putchar('.');
        // printf("\33[2K\r %.0f MByte", ((double)rwpos)/(1048576.0) );
        printf("\33[2K\r %.0f GByte", ((double)rdpos)*9.31322574615479e-10 ); // 1/2^30

    }

    printf("\nDone.\n");
    return 0;
}
