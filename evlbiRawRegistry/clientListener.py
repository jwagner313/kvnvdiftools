import socket, ssl, sys, time

CENTRAL_NODE = "localhost"
CENTRAL_NODE_PORT = 46228

class clientListener:

	def __init__(self,peername,peerport):

		self.peer = peername
		self.port = peerport

		#self.ssl_cli = ssl.create_default_context()
		self.ssl_cli = ssl._create_unverified_context()
		self.ssl_cli.options |= ssl.OP_NO_SSLv2
		self.ssl_cli.options |= ssl.OP_NO_SSLv3

		#self.ssl_srv = ssl.create_default_context()
		self.ssl_srv = ssl._create_unverified_context()
		self.ssl_srv.options |= ssl.OP_NO_SSLv2
		self.ssl_srv.options |= ssl.OP_NO_SSLv3
		self.ssl_srv.check_hostname = False

		#  openssl req -new -x509 -days 365 -nodes -out cert.pem -keyout cert.pem
		self.ssl_cli.load_cert_chain(certfile="cert.pem", keyfile="cert.pem")
		self.ssl_srv.load_cert_chain(certfile="cert.pem", keyfile="cert.pem")

		self.connUpstream = self.ssl_cli.wrap_socket(socket.socket(socket.AF_INET), server_hostname=self.peer)
		self.sockDownstream = socket.socket()
		self.connDownstream = None

		self.serverMode = True

	def connect(self):
		self.serverMode = False
		self.connUpstream.connect((self.peer,self.port))

	def listen(self):
		self.serverMode = True
		self.sockDownstream.bind(('localhost',self.port))
		self.sockDownstream.listen(5)
		while True:
			newsocket, fromaddr = self.sockDownstream.accept()
			self.connDownstream = self.ssl_srv.wrap_socket(newsocket, server_side=True)
			try:
				print (self.connDownstream)
				t = self.receive(1024)
				print t
			finally:
				self.connDownstream.shutdown(socket.SHUT_RDWR)
				self.connDownstream.close()
				self.connDownstream = None


	def close(self):
		try:
			if self.serverMode:
				pass
			else:
				self.connUpstream.shutdown(socket.SHUT_RDWR)
				self.connUpstream.close()		
		finally:
			pass

	def receive(self,N):
		if self.serverMode:
			return self.connDownstream.recv(N)
		else:
			return self.connUpstream.recv(N)

	def send(self,msg):
		if self.serverMode:
			self.connDownstream.sendall(msg)
		else:
			self.connUpstream.sendall(msg)

x = clientListener(CENTRAL_NODE,CENTRAL_NODE_PORT)
if len(sys.argv)>1:
	x.listen()
else:
	x.connect()
	x.send("hello")
	time.sleep(1)

