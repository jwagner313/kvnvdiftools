#!/usr/bin/python

import sys, time, json
import mSSLWrapper

M_VERSION = '1.00.00'

class mCommsHandler:

	def __init__(self, isClient):
		self.ssl = mSSLWrapper.mSSLWrapper(mSSLWrapper.CENTRAL_NODE, mSSLWrapper.CENTRAL_NODE_PORT)
		if isClient:
			self.ssl.connect()
		else:
			self.ssl.setHandler(self.handle)
			self.ssl.listen()

	def send(self,msg):
		self.ssl.send(msg)

	"""Handle commands from a client"""
	def handle(self,msg):
		j = json.loads(msg)
		r = {}
		print (j)
		if j['req'] == 'version':
			r['srv_version'] = M_VERSION
		else:
			pass
		return json.dumps(r)

	"""Send to server: version request"""
	def reqVersion(self):
		cmd = {}
		cmd['req'] = 'version'
		cmd['cli_version'] = M_VERSION
		self.ssl.send(json.dumps(cmd))
		r = self.ssl.receive(1024)
		j = json.loads(r)
		return j

if len(sys.argv)>1:
	x = mCommsHandler(isClient=False)
else:
	x = mCommsHandler(isClient=True)
	x.reqVersion()
        time.sleep(1)
