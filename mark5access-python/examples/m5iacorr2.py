#!/usr/bin/python
"""
m5iacorr2.py ver. 1.0   Jan Wagner  20150801
 
Produces time-averaged 2nd order autocorrelation spectra 
of *estimated* intensity from raw recorded VLBI data.

The 2nd order autocorrelation of intensity is: 
   R(t) = <E*(T) E(T) E*(T+t) E(T+t)> / <|E(T)|^2>^2
        = <I(T) I(T+t)> / <I(T)>^2
        = F^-1{ |F{I(t)}|^2 } / <I(t)>^2
where intensity I(t) = const * E*(t) E(T) = const*|E(t)|^2 
with field E(t) = E_x(t)+j*E_y(t). For an ergodic process,
2 >= R(t) >= 1, from incoherent to fully coherent.

Intensity is proportional to the squared amplitude (magnitude)
of the electric field vector. Common VLBI recordings contain only 
voltage data, not amplitude. The amplitude is estimated using 
a Hilbert transform. To reduce the level of artifacts the Hilbert
transform is taken across the entire (i.e. non-segmented) raw sample
data of each full integration period.
 
Usage : m5iacorr2.py infile <dataformat> outfile <chan> <T_int(ms)> <nlags> [<offset>]
 
  <dataformat> should be of the form: <FORMAT>-<Mbps>-<nchan>-<nbit>, e.g.:
    VLBA1_2-256-8-2
    MKIV1_4-128-2-1
    Mark5B-512-16-2
    VDIF_1000-64-1-2 (here 1000 is payload size in bytes)
 
  <chan>      is the channel to process, between 1..nchan
  <T_int>     approximate integration time per spectrum in milliseconds
  <nlags>     is the number of lags
  <offset>    is the byte offset into the file
"""
import ctypes, numpy, scipy, scipy.signal, sys
import mark5access as m5lib

def usage():
	print __doc__

MAX_SAMP = 10240000      # limit the number of samples processed in memory
doInplace = True         # enable in-place calculations (faster, lower memory use, same numerical result)
doPlot = True            # enable plotting
doLinearAutocorr = False # enable linear/acyclic autocorrelation if True, otherwise use cyclic autocorrelation 

try:
	if doPlot:
		# http://stackoverflow.com/questions/15795720/matplotlib-major-display-issue-with-dense-data-sets
		import matplotlib as mpl
		mpl.rcParams['path.simplify'] = False 
		import pylab
		pylab.ion()
except:
	doPlot = False


def m5iacorr(fn, fmt, fout, chan, T_int_ms, nlags, offset):
	"""Form time-averaged intensity autocorrelations for a subband of the input spectrum"""

	# Open file
	try:
		m5file = m5lib.new_mark5_stream_file(fn, ctypes.c_longlong(offset))
		m5fmt  = m5lib.new_mark5_format_generic_from_string(fmt)
		ms     = m5lib.new_mark5_stream_absorb(m5file, m5fmt)
		dms    = ms.contents
	except:
		print ('Error: problem opening or decoding %s\n' % (fn))
		return 1

	# Check parameters
	if (chan>dms.nchan) or (chan<=0):
		print ('Error: incorrect <chan> argument (%u). File has channels 1 to %u.'
			% (chan,dms.nchan))
		return 1
	if (nlags<4):
		print ('Error: invalid command line arguments related to Fourier transform')
		return 1

	# Settings
	Lfft  = 2*nlags # since input data are real-valued
	nint  = numpy.round(float(dms.samprate)*T_int_ms*1e-3/float(Lfft))
	nsamp = int(nint*Lfft)
	Tint  = float(nint*Lfft)/float(dms.samprate)
	iter  = 0
	print ('Intensity autocorrelations (%.4f usec/bin) are averaged to %.2fms (%u subintegrations, %u samples).'
		% (1e6/float(dms.samprate),Tint*1e3,nint,nsamp))
	if doLinearAutocorr:
		# Linear autocorrelation, does not wrap at data boundaries
		acmode = 'linear'
		npad = int(nextpow2(2*Lfft-1))
		print ('Linear %d-lag autocorrelation via zero-padded %d-point FFT.' % (nlags,npad))
	else:
		# Cyclinc autocorrelation, wraps around at data boundaries
		acmode = 'cyclic'
		npad = Lfft
		print ('Cyclic %d-lag autocorrelation with %d-point FFT.' % (nlags,npad))
	print ('Memory requirement: ~%.2f MB' % (nsamp*(2*4 + 4)/1048576.0))
	print ('Calculating intensity via %d-point Hilbert transform.' % (nsamp))

	if nsamp>MAX_SAMP:
		print ('Number of samples %d exceeds limit %d. Too slow to process. Stopping.' % (nsamp,MAX_SAMP))
		sys.exit(-1)

	# Collection of vectors for mark5access decode() raw sample output data
        pdata = m5lib.helpers.make_decoder_array(ms, nsamp, dtype=ctypes.c_float)

	# Data pointers and result arrays
	freqs  = numpy.linspace(0.0,1e-6*dms.samprate,nlags)
	lag_us = numpy.linspace(0,nlags-1,nlags) * 1e6 * 1/float(dms.samprate)
	data   = ctypes.cast(pdata[chan-1], ctypes.POINTER(ctypes.c_float*nsamp))
	R_avg  = numpy.zeros(shape=(npad), dtype='float64')
	I_avg  = 0.0

	# Write data header
	fout.write('# First data row:  starting MJD and lags[usec]\n')
	fout.write('# Other data rows: MJD in middle of averaging period, value of normalized intensity autocorrelation in each lag bin\n')
	(t0abs,t0rel) = get_m5_time(ms)
	laginfo = numpy.array_str(lag_us,max_line_width=1e99)[1:-1]
	fout.write('%.10f %s\n' % (t0abs,laginfo))

	# Process the recorded data until EOF
	while True:

		# Read data
		rc = m5lib.mark5_stream_decode(ms, nsamp, pdata)
		if (rc < 0):
			print ('\n<EOF> status=%d' % (rc))
			return 0
		x = numpy.frombuffer(data.contents, dtype='float32')

		# For testing can replace x,y with synthetic data
		#
		# Case 1: static signal of constant intensity, should produce R==1.0 in all bins
		# x = numpy.ones_like(x)
		#
		# Case 2: single tone with constant intensity, should produce R==1.0 in all bins
		# x = 1.0*numpy.cos(2*numpy.pi*numpy.linspace(0,nsamp-1,nsamp)*0.15)
		#
		# Case 3: noise, should produce R==2.0 in first bin, and R==1.0 in other bins
		# x = numpy.random.normal(size=x.shape)
		#
		# Case 4: noise + tone with varying intensity, should produce varying R seen using nlags>1024
		# x = 1.0*numpy.cos(2*numpy.pi*numpy.linspace(0,nsamp-1,nsamp)*0.15)
		# x *= 1.0 + 0.5*numpy.sin(2*numpy.pi*numpy.linspace(0,nsamp-1,nsamp)*1e-3)
		# x += numpy.random.normal(size=x.shape)

		# Intensity = squared magnitude of electric field
		if doInplace:
			I = scipy.signal.hilbert(x)
			numpy.abs(I, I)
			numpy.square(I, I)
		else:
			E = scipy.signal.hilbert(x)
			I = numpy.square(numpy.abs(E))
		I_avg = numpy.mean(I)

		# Segment the input into time-continuous segments
		I = numpy.reshape(I, (nint,Lfft)) # "numpy.reshape([1,2,3,4,5,6,7,8],(2,4)) --> ([1,2,3,4],[5,6,7,8])"

		# Calculate intensity autocorrelation <I(t) I(t+tau)>/<I>^2 using Fourier
		# because numpy.correlate() works only on 1D input and lacks an axis= parameter
		ax_time = 1
		ax_seg  = 0
		if doInplace:
			F2I = numpy.fft.fft(I,n=npad,axis=ax_time)
			numpy.absolute(F2I, F2I)
			numpy.square(F2I, F2I)
			R = numpy.real( numpy.fft.ifft(F2I,axis=ax_time) )
		else:
			FI = numpy.fft.fft(I,n=npad,axis=ax_time)
			F2I = numpy.square(numpy.abs(FI))
			R = numpy.real( numpy.fft.ifft(F2I,axis=ax_time) )
		R_avg  = numpy.squeeze(numpy.mean(R, axis=ax_seg))
		R_avg /= (I_avg**2)     # normalize by zero-lag autocorrelation
		R_avg /= float(npad)    # normalize accounting for numpy "IFFT(FFT^2)" scaling
		R_avg  = R_avg[0:nlags] # retain only the nonredundant part

		# Append to output file: <time> <bin0> <bin1> <bin2> ... \n
		(t1abs,t1rel) = get_m5_time(ms)
		tmid = (t0abs + t1abs)/2.0
		t0abs = t1abs
		s = numpy.array_str(R_avg, max_line_width=1e99)
		s = s[1:-1]
		fout.write('%.10f %s\n' % (tmid,s))

		# Diagnostic spectra
		if doPlot:
			pylab.clf()
			pylab.gcf().set_facecolor('white')
			pylab.plot(lag_us,R_avg,'k:x')
			pylab.title('Intensity Autocorrelation Spectrum in %.3f ms (via %s autocorr.)' % (Tint*1e3,acmode))
			pylab.ylabel('Int. auto (<I(t).I(t+tau)>/<I>^2)')
			pylab.xlabel('Delay (usec)')
			pylab.ylim([0.75,2.25])
			#pylab.show()
			pylab.draw()

		print ('Spectrum %4u : MJD %.10f : %f data seconds : %u frames' % (iter, tmid, t1rel, dms.nvalidatepass))

		# Next iteration
		iter = iter + 1
		I_avg = 0.0
		R_avg.fill(0.0)

	return 0


def get_m5_time(ms):
	"""Return fractional MJD timestamp of next sample to be read by mark5access"""
	(mjd,sec,ns) = m5lib.helpers.get_sample_time(ms)
	T_absolute = numpy.float64(mjd) + numpy.float64(sec + 1e-9*ns)/86400.0
	T_data = 1e-9*ms.contents.framens * ms.contents.nvalidatepass
	return (T_absolute, T_data)

def nextpow2(n):
	"""Return nearest power of two larger or equal to n."""
	m_e = numpy.log2(n)
	m_i = numpy.ceil(m_e)
	return 2**m_i

def main(argv=sys.argv):

	# Args: infile fmt outfile chan tint nlags [offset]
	if len(argv) not in [7,8]:
		usage()
		sys.exit(1)
	offset = 0
	if len(argv) == 8:
		offset = int(argv[7])
	chan  = int(argv[4])
	T_int = float(argv[5])
	nlags = int(argv[6])

	# Start processing
	flushsize = 512
	fout = open(argv[3], 'wb', flushsize)
	rc = m5iacorr(argv[1],argv[2], fout, chan, T_int, nlags, offset)
	fout.close()

	return rc

if __name__ == "__main__":
	sys.exit(main())
