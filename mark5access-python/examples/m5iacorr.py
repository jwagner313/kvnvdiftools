#!/usr/bin/python
"""
m5iacorr.py ver. 1.0   Jan Wagner  20150415
 
Produces time-averaged 2nd order autocorrelation spectra of intensity
from raw recorded VLBI data.

The 2nd order autocorrelation of intensity autocorrelation is: 
   R(t) = <E*(T) E(T) E*(T+t) E(T)> / <|E(T)|^2>^2
        = F^-1{ |F{I(t)}|^2 } / <I(t)>^2
where intensity I(t) = const * E*(t) E(T) = const*|E(t)|^2 
with field E(t) = E_x(t)+j*E_y(t). A dual-polarization VLBI recording 
is needed to capture the electric field and form the intensity I(t).

For an ergodic process 2 >= R(t) >= 1, from incoherent to fully coherent.
 
Usage : m5iacorr.py <infile> <dataformat> <outfile> <if_nrs> <T_int(ms)> <Ldft>
                    <start_bin|start_freq> <stop_bin|stop_freq> [<offset>]
 
  <dataformat> should be of the form: <FORMAT>-<Mbps>-<nchan>-<nbit>, e.g.:
    VLBA1_2-256-8-2
    MKIV1_4-128-2-1
    Mark5B-512-16-2
    VDIF_1000-64-1-2 (here 1000 is payload size in bytes)
 
  <if_nrs>    two channels (LCP,RCP) to process, each between 1..nchan (e.g., "1,2")
  <T_int>     approximate integration time per spectrum in milliseconds
  <Ldft>      length in points of Fourier transform across full bandwidth
  <start_bin> starting bin (0...Ldft-2) for forming intensity autocorrelation
  <stop_bin>  ending bin (start_bin...Ldft-1)
 
  If <start_bin> or <stop_bin> are floating point numbers they are treated as
  start and stop frequencies (in kHz) of the desired band to be analyzed.
 
  <offset> is the byte offset into the file
"""
import ctypes, numpy, sys, pylab
import mark5access as m5lib
from datetime import datetime
from scipy import stats
try:
	import matplotlib as mpl
	mpl.rcParams['path.simplify'] = False # http://stackoverflow.com/questions/15795720/matplotlib-major-display-issue-with-dense-data-sets
except:
	pass    
pylab.ion()
doPlot = True
oversampf = 2

def usage():
	print __doc__


def m5iacorr(fn, fmt, fout, if_nrs, T_int_ms, nfft, start_bin, stop_bin, offset):
	"""Form time-averaged intensity autocorrelations for a subband of the input spectrum"""

	# Open file
	try:
		m5file = m5lib.new_mark5_stream_file(fn, ctypes.c_longlong(offset))
		m5fmt  = m5lib.new_mark5_format_generic_from_string(fmt)
		ms     = m5lib.new_mark5_stream_absorb(m5file, m5fmt)
		dms    = ms.contents
	except:
		print ('Error: problem opening or decoding %s\n' % (fn))
		return 1

	if isinstance(start_bin, float):
		start_bin = int(nfft*(1e3*start_bin/dms.samprate))
		stop_bin  = int(nfft*(1e3*stop_bin/dms.samprate))

	# Check parameters
	if (max(if_nrs)>dms.nchan) or (min(if_nrs)<=0):
		print ('Error: incorrect if_nrs argument. Requested IF %u and IF %u, but file has %u IFs numbered 1 to %u.'
			% (min(if_nrs),max(if_nrs),dms.nchan,dms.nchan))
		return 1
	if (nfft<2) or (start_bin<0) or (start_bin>stop_bin) or (start_bin > nfft):
		print ('Error: invalid command line arguments related to Fourier transform')
		return 1

	# Settings
	nout  = stop_bin - start_bin + 1
	nint  = numpy.round(float(dms.samprate)*T_int_ms*1e-3/float(nfft))
	Tint  = float(nint*nfft)/float(dms.samprate)
	df    = float(dms.samprate)/float(nfft)
	f1    = df*(start_bin+0)
	f2    = df*(stop_bin+0)
	bout  = f2-f1
	iter  = 0
	print ('Using signal band centered on %.2f +- %.2f kHz out of a recorded bandwidth of %.2f kHz.' 
		% ((f1+bout/2)*1e-3,bout*0.5e-3,dms.samprate*0.5e-3) )
	print ('Intensity autocorrelations have %u delay bins (%.4f usec/bin) and are averaged to %.2f milliseconds (%u subintegrations).'
		% (nout,1e6/float(dms.samprate),Tint*1e3,nint))
	print ('Internal settings: nint=%u, FFT nfft=%u, autocorr nout=%u' % (nint,nfft,nout))

	# Collection of vectors for mark5access decode() raw sample output data
        pdata = m5lib.helpers.make_decoder_array(ms, nfft, dtype=ctypes.c_float)

	# Data pointers and result arrays
	x_data = ctypes.cast(pdata[if_nrs[0]-1], ctypes.POINTER(ctypes.c_float*nfft))
	y_data = ctypes.cast(pdata[if_nrs[1]-1], ctypes.POINTER(ctypes.c_float*nfft))
	R_avg  = numpy.zeros(shape=(nout), dtype='float64'); I_avg = 0.0
	freqs  = numpy.linspace(0.0,1e-6*dms.samprate,nfft)

	dbg_x_spec = numpy.zeros(shape=(oversampf*(nout - (nout%2))), dtype='float64'); freqs = freqs[start_bin:-1]
	#dbg_x_spec = numpy.zeros(shape=(nfft), dtype='float64')
	dbg_y_spec = numpy.zeros_like(dbg_x_spec)
	dbg_i_avg  = numpy.zeros_like(dbg_x_spec)

	# Write data header
	(t0abs,t0rel) = get_m5_time(ms)
	fout.write
	lags = (1e6/float(dms.samprate)) * numpy.linspace(0,nout-1,nout)
	lags = numpy.array_str(lags,max_line_width=1e99)[1:-1]
	fout.write('# First data row:  starting MJD and lags[usec]\n')
	fout.write('# Other data rows: MJD in middle of averaging period, value of normalized intensity autocorrelation in each lag bin\n')
	fout.write('%.10f %s\n' % (t0abs,lags))

	# Process the recorded data until EOF
	while True:

		# Read data
		rc = m5lib.mark5_stream_decode(ms, nfft, pdata)
		if (rc < 0):
			print ('\n<EOF> status=%d' % (rc))
			return 0
		x = numpy.frombuffer(x_data.contents, dtype='float32')
		y = numpy.frombuffer(y_data.contents, dtype='float32')

		# For testing can replace x,y with synthetic data
		#
		# Case 1: static signal of constant intensity, should produce R==1.0 in all bins
		#         note: extract_subband() must be skipped since it does not "pass DC"
		# x = numpy.ones_like(x); y = x;
		#
		# Case 2: orthogonal sinusoids in extracted bands,
		#         should produce R==1.0 for matched amplitudes, and
		#         R==1.0+oscillation for mismatched amplitudes
		# x = 1.0*numpy.cos(2*numpy.pi*((start_bin+nout/8)/float(nfft))*numpy.linspace(0,nfft-1,nfft))
		# y = 2.0*numpy.sin(2*numpy.pi*((start_bin+nout/8)/float(nfft))*numpy.linspace(0,nfft-1,nfft))
		#
		# Case 3: noise, should produce R==2.0 in first bin, and R==1.0 in other bins
		# x = numpy.random.normal(size=x.shape)
		# y = numpy.random.normal(size=x.shape)

		# Extract desired band slice in freq. domain and restore to time domain
		x = extract_subband(x, start_bin,stop_bin)
		y = extract_subband(y, start_bin,stop_bin)

		# Diagnostic use: time-average the data to form time integrated spectra later
		if doPlot:
			X = numpy.fft.fft(x)
			Y = numpy.fft.fft(y)
			dbg_x_spec += numpy.abs(X)
			dbg_y_spec += numpy.abs(Y)
			dbg_i_avg  += numpy.square(numpy.real(X)) + numpy.square(numpy.real(Y))

		# Apply calibrations for instrumental cross-polarization delay, phase, gain differences
		# TODO?

		# Intensity = magnitude-squared of complex-valued electric field
		I = numpy.square(x) + numpy.square(y)

		# Calculate intensity autocorrelation 'R' for time domain intensity 'I'
		F = numpy.square( numpy.abs(numpy.fft.fft(I)) )
		R = numpy.real( numpy.fft.ifft(F) )

		# Time averaging
		I_avg += numpy.mean(I)
		R_avg += R[0:nout]

		# End of averaging period
		iter = iter + 1
		print ('Time %u : %.1f%%\r' % (iter/nint,100.0*(iter%nint)/float(nint))),
		if (iter % nint)==0:
			I_avg = I_avg / float(nint)
			R_avg = R_avg / float(nint)
			R_avg = R_avg / (I_avg**2)
			R_avg = R_avg / float(len(x)) # normalization accounts for numpy "IFFT(FFT^2)"

			# Append to output file: <time> <bin0> <bin1> <bin2> ... \n
			(t1abs,t1rel) = get_m5_time(ms)
			tmid = (t0abs + t1abs)/2.0
			t0abs = t1abs
			s = numpy.array_str(R_avg, max_line_width=1e99)
			s = s[1:-1]
			fout.write('%.10f %s\n' % (tmid,s))

			# Diagnostic spectra
			if doPlot:
				nn = int(len(dbg_x_spec)/2 - 1)
				dbg_x_spec /= float(nint)
				dbg_y_spec /= float(nint)
				dbg_i_avg  /= float(nint)

				pylab.clf()
				pylab.gcf().set_facecolor('white')

				pylab.subplot(221)
				pylab.plot(freqs[1:nn],abs(dbg_x_spec)[1:nn],'r-')
				pylab.title('Spectrum of E_x')
				pylab.ylabel('Mean abs. amp')
				pylab.axis('tight')

				pylab.subplot(222)
				pylab.plot(freqs[1:nn],abs(dbg_y_spec)[1:nn],'g-')
				pylab.title('Spectrum of E_y')
				pylab.ylabel('Mean abs. amp')
				pylab.axis('tight')

				pylab.subplot(223)
				pylab.plot(freqs[1:nn],dbg_i_avg[1:nn],'k:x')
				pylab.title('Average intensity')

				pylab.subplot(224)
				pylab.plot(R_avg,'k:x')
				pylab.title('Intensity Autocorrelation Spectrum')
				pylab.ylabel('Int. auto (<I(t).I(t+tau)>/<I>^2)')
				#pylab.show()
				pylab.draw()

			# Next iteration
			I_avg = 0.0
			R_avg = numpy.zeros(shape=(nout), dtype='float64')
			dbg_x_spec = numpy.zeros_like(dbg_x_spec)
			dbg_y_spec = numpy.zeros_like(dbg_y_spec)
			dbg_i_avg  = numpy.zeros_like(dbg_i_avg)

			print ('Spectrum %3d : MJD %.10f : %u frames ok : %f data seconds' % (iter/nint, tmid, dms.nvalidatepass, t1rel))

	return 0


def extract_subband(x, start_bin,stop_bin):
	"""Extract a frequency subband from time series x using foward and inverse Fourier transforms."""
	N = stop_bin - start_bin + 1
	M = len(x)

	# FFT to extract spectral subband
	X = numpy.fft.fft(x)
	X = X[start_bin:stop_bin]
	X[0]  = numpy.real(X[0])
	X[-1] = numpy.real(X[-1])

	# IFFT with zero padding to get real-valued signal from complex spectrum
	n = oversampf*(N - (N%2)) 
	x = numpy.fft.ifft(X,n).real

	# Scale to preserve original amplitudes ; not relevant for normalized intensity autocorr
	# x = x * float(n)/(0.5*M)
	return x


def get_m5_time(ms):
	"""Return fractional MJD timestamp of next sample to be read by mark5access"""
	(mjd,sec,ns) = m5lib.helpers.get_sample_time(ms)
	T_absolute = numpy.float64(mjd) + numpy.float64(sec + 1e-9*ns)/86400.0
	T_data = 1e-9*ms.contents.framens * ms.contents.nvalidatepass
	return (T_absolute, T_data)


def main(argv=sys.argv):

	# Args
	if len(argv) not in [9,10]:
		usage()
		sys.exit(1)

	if_nrs = argv[4].split(',')   # two IFs specified by, e.g., "1,2"
	if_nrs = [int(x) for x in if_nrs]
	T_int  = abs(float(argv[5]))
	Ldft   = int(argv[6])
	if ('.' in argv[7]) and ('.' in argv[8]):
		start_bin = float(argv[7])
		stop_bin  = float(argv[8])
	else:
		start_bin = int(argv[7])
		stop_bin  = int(argv[8])

	offset = 0
	if len(argv) == 10:
		offset = int(argv[9])

	# Check args
	if (len(if_nrs)!=2):
		print ('Error: please specify two IFs, separated by a comma, e.g., "1,2"!')
		return 1

	# Start processing
	fout = open(argv[3], 'wb')
	rc = m5iacorr(argv[1],argv[2], fout, if_nrs, T_int,Ldft,start_bin,stop_bin, offset)
	fout.close()

	return rc

if __name__ == "__main__":
	sys.exit(main())
