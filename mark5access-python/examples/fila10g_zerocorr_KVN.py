#!/usr/bin/python
"""
fila10g_zerocorr_KVN.py   ver. 1.0   Jan Wagner  20150503
 
Helper to make zero baseline correlation of 4-channel KVN VDIF
recording where FILA10G was fed with four identical VSI data sources

Usage : fila10g_zerocorr_KVN.py <infile> [offset]
 
  <offset>    is the byte offset into the file
"""

import ctypes, numpy, sys, pylab
import mark5access as m5lib
from datetime import datetime
from scipy import stats
try:
	import matplotlib as mpl
	mpl.rcParams['path.simplify'] = False # http://stackoverflow.com/questions/15795720/matplotlib-major-display-issue-with-dense-data-sets
except:
	pass    

def usage():
	print __doc__


def m5zerocorr(fn, offset):
	"""Form time-averaged autocorrelation spectra"""

	# Open file
	fmt = 'VDIF_8192-8192-4-2'
	try:
		m5file = m5lib.new_mark5_stream_file(fn, ctypes.c_longlong(offset))
		m5fmt  = m5lib.new_mark5_format_generic_from_string(fmt)
		ms     = m5lib.new_mark5_stream_absorb(m5file, m5fmt)
		dms    = ms.contents
	except:
		print ('Error: problem opening or decoding %s\n' % (fn))
		return 1

	if (dms.nchan!=4):
		print ('Error: input VDIF file had %d instead of the expected 4 channels!' % (dms.nchan))
		return 1

	# Settings
	nfft  = 16384
	pairs = [(i,j) for i in range(dms.nchan) for j in range(i+1,dms.nchan)]
	iter  = 0

	# Collection of vectors for mark5access decode() raw sample output data
        pdata = m5lib.helpers.make_decoder_array(ms, nfft, dtype=ctypes.c_float)

	# Result arrays
	ch_data = [ctypes.cast(pdata[ii],ctypes.POINTER(ctypes.c_float*nfft)) for ii in range(dms.nchan)]
	F_data  = numpy.zeros(shape=(dms.nchan,nfft), dtype='complex128')
	acorrs  = numpy.zeros(shape=(dms.nchan,nfft), dtype='float64')
	xcorrs  = numpy.zeros(shape=(len(pairs),nfft), dtype='complex128')
	freqs   = numpy.linspace(0.0, dms.samprate*1e-6, num=nfft)

	# Process the recorded data
	while True:

		# Read data
		rc = m5lib.mark5_stream_decode(ms, nfft, pdata)
		if (rc < 0):
			print ('\n<EOF> status=%d' % (rc))
			break
		if (iter > 500):
			print ('\nGot 500 FFTs, finishing...')
			break

		# Brute force averaging of xcorrs
		for ii in range(dms.nchan):
			x = numpy.frombuffer(ch_data[ii].contents, dtype='float32')
			F_data[ii] = numpy.fft.fft(x)
			acorrs[ii] += numpy.abs(numpy.multiply(F_data[ii], numpy.conj(F_data[ii])))
		for ii in range(len(pairs)):
			A = F_data[pairs[ii][0]]
			B = F_data[pairs[ii][1]]
			xc = numpy.multiply(A, numpy.conj(B))
			xcorrs[ii] += xc
		iter = iter + 1

		(mjd0,sec0,ns0) = m5lib.helpers.get_sample_time(ms)
		print ('%d/%d.%f       \r' % (mjd0,sec0,ns0)),

	N = int(numpy.floor(float(nfft)/2 + 1))
	f = freqs[0:N]

	xcorrs /= float(iter)
	acorrs /= float(iter)
	xcoeffs = numpy.copy(xcorrs)
	for ii in range(len(pairs)):
		pair = pairs[ii]
		xcoeffs[ii] /= numpy.sqrt(numpy.multiply(acorrs[pair[0]],acorrs[pair[1]]))

	rows = 3
	cols = len(pairs)

	# Cross-corr plot: Phase, Amplitude, Coefficient
	A_lims = [numpy.amin(numpy.abs(xcorrs)), numpy.amax(numpy.abs(xcorrs))]
	pylab.figure()
	pylab.gcf().set_facecolor('white')
	for ii in range(len(pairs)):

		pair = pairs[ii]
		xc = xcorrs[ii,0:N]
		xcoeff = xcoeffs[ii,0:N]

		pylab.subplot(rows,cols,ii+0*cols+1)
		pylab.plot(f,numpy.angle(xc,deg=True),'k-')
		pylab.axis('tight')
		pylab.ylim([-180,180])
		pylab.gca().set_xticklabels([])
		if ii>0:
			pylab.gca().set_yticklabels([])
		else:
			pylab.ylabel('Phase')
		pylab.title('Pair %d-%d' % (pair[0],pair[1]))

		pylab.subplot(rows,cols,ii+1*cols+1)
		pylab.plot(f,numpy.abs(xc),'k-')
		pylab.axis('tight')
		pylab.ylim(A_lims)
		if ii>0:
			pylab.gca().set_yticklabels([])
		else:
			pylab.ylabel('Cross-amp.')

		pylab.subplot(rows,cols,ii+2*cols+1)
		pylab.plot(f,numpy.abs(xcoeff),'k-')
		pylab.axis('tight')
		pylab.ylim([0,1.2])
		pylab.xlabel('Frequency (MHz)')
		if ii>0:
			pylab.gca().set_yticklabels([])
		else:
			pylab.ylabel('Cross-corr coeff.')

	# Auto-corr plot
	A_lims = [numpy.amin(acorrs), numpy.amax(acorrs)]
	pylab.figure()
	pylab.gcf().set_facecolor('white')
	for ii in range(dms.nchan):
		pylab.subplot(1,dms.nchan,ii+1)
		pylab.plot(f,acorrs[ii,0:N],'k-')
		pylab.axis('tight')
		#pylab.ylim(A_lims)
		pylab.xlabel('Frequency (MHz)')
		if ii>0:
			#pylab.gca().set_yticklabels([])
			pass
		else:
			pylab.ylabel('Auto-corr. amp.')
		pylab.title('Baseband %d' % (ii))

	pylab.show()

	return 0

def main(argv=sys.argv):

	# Args
	if len(argv) not in [2,3]:
		usage()
		sys.exit(1)
	offset = 0
	if len(argv) == 3:
		offset = int(argv[2])

	# Start processing
	rc = m5zerocorr(argv[1],offset)

	return rc

if __name__ == "__main__":
	sys.exit(main())
