#!/usr/bin/python
"""
m5power.py ver. 1.0   Jan Wagner  20150723

Calculates signal power in intervals of 'dt'. The definition of 
signal power used here is dt * sum(V(t)^2), where V(t) is the
baseband voltage signal with several samples during each 'dt'.

If the VLBI recording contains multiple channels (subbands) then
their powers will be summed. Power is *not* normalized by the number 
of accumulated samples-squared.

The produced power-data file can be used as input data for a search for 
impulses or periodic signals. The file contains 32-bit float values.
The first two are MJD start and MJD stop times, the third is 'dt' in seconds,
and the remaning values are the power samples of every time interval.

Usage : m5power.py <infile> <dataformat> <dt (seconds)> <len (seconds)>
                   <outfile> [<offset>]
 
  <dataformat> should be of the form: <FORMAT>-<Mbps>-<nchan>-<nbit>, e.g.:
    VLBA1_2-256-8-2
    MKIV1_4-128-2-1
    Mark5B-512-16-2
    VDIF_1000-64-1-2 (here 1000 is payload size in bytes)
 
  <dt (s)>    approximate integration time per power sample in seconds

  <len (s)>   how many seconds of input data to process

  <outfile>   output file, binary

  <offset>    byte offset into input file
"""

import ctypes, numpy, struct, sys
import mark5access as m5lib
from datetime import datetime

def usage():
	print __doc__

def date_to_mjd(dtime):
	"""Returns the Julian day number of a date."""
	a = (14 - dtime.month)//12
	y = dtime.year + 4800 - a
	m = dtime.month + 12*a - 3
	j = dtime.day + ((153*m + 2)//5) + 365*y + y//4 - y//100 + y//400 - 32045
	return j - 2400000.5


def m5power(fn, fmt, fout, dt_sec, len_sec, offset):
	"""Form power data"""

	# Open file
	try:
		m5file = m5lib.new_mark5_stream_file(fn, ctypes.c_longlong(offset))
		m5fmt  = m5lib.new_mark5_format_generic_from_string(fmt)
		ms     = m5lib.new_mark5_stream_absorb(m5file, m5fmt)
		dms    = ms.contents
	except:
		print ('Error: problem opening or decoding %s\n' % (fn))
		return 1

	# Settings
        nsamp  = int(numpy.round(dt_sec*float(dms.samprate)))
        nstep  = numpy.ceil(8/dms.nbit) # granularity to avoid VLBI data decoding at sub-byte levels
        nsamp  = int(nstep * numpy.ceil(nsamp/nstep))
        dt_sec = float(nsamp) / float(dms.samprate)
	print ('Calculating total power in %f millisecond (%d-sample) intervals.' % (dt_sec*1e3,nsamp))

	# Collection of vectors for mark5access decode() raw sample output data
        pdata = m5lib.helpers.make_decoder_array(ms, nsamp, dtype=ctypes.c_float)
	ch_data = [ctypes.cast(pdata[ii],ctypes.POINTER(ctypes.c_float*nsamp)) for ii in range(dms.nchan)]

	# Get timestamp
	mjd0 = dms.mjd
	sec0 = dms.sec + 1e-9*dms.ns
	if dms.format in [m5lib.MK5_FORMAT_VLBA,m5lib.MK5_FORMAT_MARK5B]:
		now     = datetime.utcnow()
		now_mjd = date_to_mjd(now)
		mjd_fix = mjd0 + (now_mjd-(now_mjd%1000))
		if (mjd_fix > now_mjd):
			mjd_fix = mjd_fix - 1000
	else:
		mjd_fix = mjd0
	print ('File started at MJD %d + %.4f seconds' % (mjd_fix,sec0))

	# Write header (MJD start, MJD stop, sample time resolution)
	fout.write(struct.pack('d',mjd_fix + sec0/86400.0))
	fout.write(struct.pack('d',mjd_fix + (sec0+len_sec)/86400.0)) # TODO: adjust to actual value after run?
	fout.write(struct.pack('f',dt_sec))

	# Process the recorded data
	iter = 0
	while True:

		# Read data
		rc = m5lib.mark5_stream_decode(ms, nsamp, pdata)
		if (rc < 0):
			print ('\n<EOF> status=%d' % (rc))
			return 0

		# Power across subbands, not normalized by number of samples!
		# P = dt * sum(V(t)^2)
		# as in http://www.aip.de/groups/soe/local/numres/bookcpdf/c13-4.pdf
                power = 0.0
		for ii in range(dms.nchan):
			x = numpy.frombuffer(ch_data[ii].contents, dtype='float32')
                        power += numpy.dot(x,x)
                power = dt_sec * (power / float(dms.nchan))

		# Write results
		fout.write(struct.pack('f',power))

		# Get the expected VLBI raw data time stamp of next sample
		(mjd,sec,ns) = m5lib.helpers.get_sample_time(ms)
		sec = sec + 1e-9*ns
		datatime = (mjd-mjd0)*86400.0 + (sec-sec0)
		print '%f %f %f \r' % (datatime-dt_sec,iter*dt_sec,power),
		if (datatime >= len_sec):
			break
		iter = iter + 1

	return 0


def main(argv=sys.argv):
	offset = 0

	# Args of 'm5power.py <infile> <dataformat> <dt (sec)> <len (sec)> <outfile> [<byte offset>]'
	if len(argv) not in [6,7]:
		usage()
		sys.exit(1)
	if len(argv) == 7:
		offset = int(argv[6])

	# Start processing
	fout = open(argv[5], 'w')
	rc = m5power(argv[1],argv[2], fout, abs(float(argv[3])), abs(float(argv[4])), offset)
	fout.close()

	return rc

if __name__ == "__main__":
	sys.exit(main())
