/********************************************************************************
 **
 ** removeinvalidVDIF.c
 **
 ** Usage:    removeinvalidVDIF.c <in.vdif> <out.vdif>
 **
 ** Removes all Invalid-marked VDIF frames.
 **
 ** (C) 2019 Jan Wagner
 **
 *******************************************************************************/

// Compile with:
//    gcc -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE -D_LARGEFILE_SOURCE -O3 removeinvalidVDIF.c -o removeinvalidVDIF

// Number of VDIF frames in memory. Keep it small (512*8032byte = ~4 MB) to fit into CPU cache.
#define NUM_BUFFERED_FRAMES   512

#include <malloc.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <fcntl.h>
#include <errno.h>
#include <endian.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char** argv)
{
    int fi, fo, i;

    ssize_t nrd, nwr;
    off64_t rwpos = 0;
    size_t  framesize;
    int     nframes;

    unsigned char* buf = NULL;
    uint32_t vdif_header_w32[4];
    uint32_t t0_orig;
    uint32_t t0_new   = 0;
    uint8_t  ep_new   = 0;
    uint32_t ep_new32 = 0;

    /* Arguments */
    if (argc != 3)
    {
        printf("Program removeinvalidVDIF Version 1.0\n\n"
               "Usage: removeinvalidVDIF <in.vdif> <out.vdif>\n"
               "\n"
               "Removes all Invalid-marked VDIF frames.\n");
        return -1;
    }

    fi = open(argv[1], O_RDONLY|O_LARGEFILE);
    fo = open(argv[2], O_RDWR|O_LARGEFILE|O_CREAT);
    if (fi < 0)
    {
        printf("Error opening file '%s': %s\n", argv[1], strerror(errno));
        return -1;
    }

    /* Determine the frame size and original starting time */
    read(fi, (void*)&vdif_header_w32, sizeof(vdif_header_w32));
    lseek64(fi, 0, SEEK_SET);

    // VDIF Header word0: [Invalid data flag:1 | reserved:1 | seconds in reference epoch:30]
    // VDIF Header word1: [Unassigned:2 | reference epoch:6 | data frame nr:24]
    // VDIF Header word2: [VDIF Version:3bit | log2 channels: 5bit | frame size in 8-byte units: 24bit]
    // ...
    t0_orig   = vdif_header_w32[0] & 0x3FFFFFFF;
    framesize = (vdif_header_w32[2] & 0x00FFFFFF) * 8;
    fprintf(stderr, "The VDIF frame size seems to be %zu bytes, start time %u seconds, ref. epoch %u.\n", 
            framesize, t0_orig, (vdif_header_w32[1]>>24) & 0x3F);

    if (framesize > 32768) 
    {
       fprintf(stderr, "Suspiciously large frame size. Aborting.\n");
       return -1;
    }

    buf = (unsigned char*)memalign(16, framesize*NUM_BUFFERED_FRAMES);

    uint32_t ongoing_sec = t0_orig;

    /* Modify the file */
    while (1)
    {
        nrd = read(fi, (void*)buf, framesize*NUM_BUFFERED_FRAMES);
        if (nrd <= 0) 
        { 
            break; 
        }

        nframes = nrd / framesize;
        putchar('.');

        for (i=0; i<nframes; i++)
        {
            uint32_t* tptr = (uint32_t*)(buf + i*framesize);

            // check the Invalid bit
            if (*tptr & ((int32_t)1<<31))
            {
                printf("invalid\n");
            }
            else
            {
                nwr = write(fo, (void*)tptr, framesize);
            }

            ongoing_sec = *tptr & 0x3FFFFFFF;
        }
    }

    printf("\nDone.\n");
    return 0;
}
