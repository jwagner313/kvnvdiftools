# CFLAGS = -Wall -O4 -msse -msse2 -msse3 -mavx -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE -D_LARGEFILE_SOURCE  -lm
CFLAGS = -g -Wall -O3 -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE -D_LARGEFILE_SOURCE  -lm

TARGETS = kvnMark5B2VDIF kvnVDIF2VDIF retimeVDIF stripVDIFheaders addVDIFheaders dvp2VDIF modifyVDIF shrinkVDIF \
	m5tvg mark5B2VDIF fixMultiIFVDIF forwardUDP interleaveVDIF renumberVDIF unfillpatternVDIF swapendianVDIF \
	removeinvalidVDIF timeshiftYJ timeshiftWF

all: $(TARGETS)

install: $(TARGETS)
	#cp $(TARGETS) /usr/local/bin
	cp -a $(TARGETS) /home/oper/bin

clean:
	rm -f *.o $(TARGETS)
