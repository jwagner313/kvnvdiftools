// sectors () returns information about the number
// of disk sectors actually written to a list of
// devices
//
// returns non-zero on error
//
// initial code                    rjc 2012.10.31 (Boo!)

#include "dplane.h"

#define BUFSIZE 10000

int sectors (char *diskparts,       // ptr to array of disk partition names (e.g. sdg1)
             int ndisks,            // number of disks to look up
             long int *nsects)      // ptr to int array containing results (#sectors)
    {
    int n,
        i,
        nbytes,
        nbprev;

    long int n1, n2, n3, n4, n5, n6, n7;

    static int fd,
           opened = FALSE;

    char *ptr;

    char statbuff[BUFSIZE];

                                    // open the diskstats pseudo-file only once
    if (opened)
        {
        if (lseek (fd, 0, SEEK_SET) < 0)    // rewind to start
            {
            printf ("problem rewinding /proc/diskstats file\n");
            perror ("sectors");
            }
        }
    else
        {
        fd = open ("/proc/diskstats", O_RDONLY);
        if (fd < 0)
            {
            printf ("major problem -- error opening /proc/diskstats\n");
            perror ("sectors");
            return -1;
            }
        opened = TRUE;
        }
                                    // read in a fresh copy of the data
    nbytes = read (fd, statbuff, BUFSIZE);
    // printf ("read %d bytes from /proc/diskstats\n", nbytes);
                                    // hack to get around partial-read problem
    nbprev = nbytes;
    for (i=0; i<100; i++)
        {
        nbytes += read (fd, statbuff+nbytes, BUFSIZE-nbytes);
        // printf ("read more of /proc/diskstats: total nbytes %d\n", nbytes);
                                    // jump out if no more bytes were read
        if (nbytes == nbprev)
            break;
        nbprev = nbytes;
        }
  
    if (nbytes <= 0 || i == 100)
        {
        printf ("major problem -- error reading /proc/diskstats - nbytes %d i %d\n", nbytes, i);
        perror ("sectors");
        return -1;
        }
    statbuff[nbytes] = 0;           // terminate, so lines are one long string
                                    // loop over devices, looking each up
    for (n=0; n<ndisks; n++)
        {
        ptr = strstr (statbuff, diskparts+8*n);
        if (ptr == NULL)
            {
            printf ("couldn't find %s in /proc/diskstats\n", diskparts+8*n);
            return -1;
            }
        sscanf (ptr + strlen(diskparts+8*n), "%ld %ld %ld %ld %ld %ld %ld",
                &n1, &n2, &n3, &n4, &n5, &n6, &n7);
        //printf ("n %d n7 %d\n", n, n7);
        nsects[n] = n7;
        }
    return 0;
    }
