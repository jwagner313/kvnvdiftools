
To build dplane_voa:
 
  make
  make install


The cplane program of Mark6 looks for running
processes called 'dplane' (rather than checking
whether the dplane TCP/UDP control ports are open).

Because the dplane for VOA is called 'dplane_voa'
there are two options to make it recognized by cplane:

1) Utils.py of the "M6 Utils" package can be modified. 

The dplane_status() function checks whether dplane is 
running or not, but this check looks for the process 
instead of for open TCP&UDP ports. That means 
dplane_status() has to be edited so that it checks for 
both 'dplane' and 'dplane_voa'. 

A modified Utils.py is included here.

2) Alternatively, M6 Utils can be left unmodified.

In the dplane.c source code for VOA included here,
the name of the program is changed at runtime using
two methods,

    const char fakename[] = "dplane\0";
    prctl(PR_SET_NAME, (unsigned long)&fakename[0]);
    memcpy(argv[0], fakename, strlen(fakename)+1);

The prctl() changes the process name tidily. 
But this alone seemed not to be enough. 
For cplane to recognize the VOA dplane the program
name in argv[0] has to be overwritten at runtime, too.
The result of the latter looks quite messy in the output 
of say 'ps' or 'top' -- but at least dplane seems happy.


