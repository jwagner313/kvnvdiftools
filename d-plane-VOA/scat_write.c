// scat_write ()  performs scattered write from circular 
// buffer to multiple files
//
// created  -                              rjc 2012.9.5 
// modify to cache-equalization algorithm  rjc 2012.11.1
// concept is that by keeping the difference between sectors written
// from the program's pov, and sectors actually written to disk, we
// can keep any slow disk from getting overused and dominating the
// cache

#include "dplane.h"
#define NOT_USED_AT_ALL 9999999999999999L

extern struct global_tag glob;

int scat_write (u_char *pbuf,       // pointer to buffer containing data
                int wr_size)        // size of write block (bytes)
    {
    int i,
        ii,
        ileast;

    static int ilast = 0;

    long int least_used;

                                    // find least-used disk 
    least_used = NOT_USED_AT_ALL;

    while (least_used == NOT_USED_AT_ALL)
        {
        for (i=0; i<glob.nfils; i++)
            {
                                    // use cyclical starting point
            ii = (ilast + i) % glob.nfils;
            if (glob.sfile[ii].file_open
             && glob.sfile[ii].status == READY
             && glob.sfile[ii].nbytes < least_used)
                {
                least_used = glob.sfile[ii].nbytes;
                ileast = ii;
                }
            }
                                    // check for time-outs on file write
        for (i=0; i<glob.nfils; i++)
            if (glob.sfile[i].wr_pending == TRUE 
             && glob.tnow > glob.sfile[i].wr_t_init + WRITER_TIMEOUT)
                {
                glob.status.file_timeout |= (1 << i);
                                    // reset timeout clock to space error messages
                glob.sfile[i].wr_t_init = glob.tnow;
                }

        if (least_used < NOT_USED_AT_ALL)
            {
         
                                    // write to it
            glob.sfile[ileast].pbuf = pbuf;
            glob.sfile[ileast].wr_size = wr_size;
            glob.sfile[ileast].status = BUSY;
            glob.sfile[ileast].wr_pending = TRUE;
                                    // keep track of file size (# bytes written)
            glob.sfile[ileast].nbytes += wr_size;
            }
        else                        // wait for a drive to be READY (tuning???)
            usleep ((useconds_t) 100);
        }
                                    // update cyclical starting point
    ilast = (ilast + 1) % glob.nfils;
                                    // successful return of index of file written
    return ileast;
    }

