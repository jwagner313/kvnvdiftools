/****************************************************************
 **
 ** Mark5B VLBI Data Conversion 
 ** and Data Streaming in VDIF UDP/IP Format
 **
 ** Version 1.50
 ** 
 *****************************************************************
 **
 ** Usage: ./ctl_octa.jw <IPaddr> <Port#>  <Infile[Mark5B format]>
 **                      <rec.rate#>  <send.rate#>  
 **                      <yyyy> <ddd> <hh> <mm> <ss>
 **
 ** The time (yyyy-ddd(DOY)-hh-mm-ss) must be identical to
 ** the time of the first data frame in the Mark5B input file.
 ** Can be used to send a file
 **   input Mark5B file --> VDIF UDP/IP 10GbE --> 10GbE OCTA recorder
 **
 ** The VLBI data timestamp in 16-byte Mark5B headers are ignored
 ** and are simply thrown away. The program invents its own VDIF
 ** timestamps. If unlucky/not careful then the VDIF data will have
 ** incorrect timestamps. Data are then not be usable for VLBI.
 **
 *****************************************************************
 **
 ** Test: 
 **   ./ctl_octa.jw  127.0.0.1 70123   VSITVG32.mk5b 1024 1  2014 186 11 12 00
 **
 ** Test file VSITVG32.mk5b has a start mjd/sec = 112 40320.00000000
 ** and MJD=56112 is the 04 July 2012 (doy=186) at  time 11h12m00s
 **
 *****************************************************************
 ** Version 1.0 from ?? 2014 by ??
 **    First version, produces wrong VDIF data
 **
 ** Version 1.5 from 05Aug2014 by JanW
 **    Attempt to clean up code, fix VDIF mistakes,
 **    and figure out what the original code is really doing.
 **    Did local testing (works!) but did not test with OCTA.
 ****************************************************************/

#define _LARGEFILE64_SOURCE
#define _FILE_OFFSET_BITS 64
#define _GNU_SOURCE
#define _XOPEN_SOURCE 600

#include "vdif_io.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <malloc.h>
#include <memory.h>


///////////////////////////////////////////////////////////////////////
// Switches
///////////////////////////////////////////////////////////////////////

#define DO_CONVERT_MARK5B_ENCODING  1  // JanW: set to 1 to convert from Mark5B 2-bit sample encoding into VDIF 2-bit encoding

#define DEBUG_WR_TEST               1  // JanW: set to 1 to write some kind of debug file used in the original code
#define DEBUG_SHOW_5B_TIMESTAMPS    0  // JanW: set to 1 to print out the (ignored) true VLBI timestamps of input Mark5B data
#define DEBUG_SHOW_VDIF_TIMESTAMPS  0  // Janw: set to 1 to print the invented VDIF timestamps (allows comparison to Mark5B timestamps)

/////////////////////////////////////////////////////////////////////// // Defines 
///////////////////////////////////////////////////////////////////////

#define DEBUG_WR_FILENAME           "ctl_octa_UDPcopy.vdif"
#define VFCP_SR_MAGIC               0x80108010

#define tv_diff_usec(newer,older) ((newer.tv_sec-older.tv_sec)*1e6 + (newer.tv_usec-older.tv_usec)) // Adopted from Tsunami UDP protocol


///////////////////////////////////////////////////////////////////////
// Helpers
///////////////////////////////////////////////////////////////////////

// Global flag to exit after I/O errors
static uint8_t flag_finfile = 0;   

// Current data byte offsets into Mark5B input file
static off64_t m_m5b_readpos = 0;

// A copy of the latest Mark5B header
static unsigned char m_m5b_currheader[16];


/** Helper: show error message and exit */
static void bail(const char *on_what)
{
   if (errno != 0)
      fprintf(stderr, "%s : ", strerror(errno));
   fprintf(stderr, "%s\n", on_what);
   exit(1);
}

/** Helper: read N data bytes from Mark5B-formatted file, while taking care to remove Mark5B headers */
static int get_headerless_from_mark5b(int fd, char* outbuf, size_t nbytes)
{
   ssize_t nread;
   char *b = outbuf;

   while (nbytes > 0) {

      // Mark5B:  16 byte header + 10.000 byte payload

      // Strip the header
      if ((m_m5b_readpos % 10016) == 0) {  // TODO: could also allow for read position that is inside header?

         nread = read(fd, (void*)&m_m5b_currheader, 16);
         m_m5b_readpos += nread;
         if (nread != 16) {
             return -1;
         }

         #if DEBUG_SHOW_5B_TIMESTAMPS
         fprintf(stderr, "Mk5B frame#=%05d    BCD=%02X%02X%02X%02X.%02X%02X\n",
                         m_m5b_currheader[4] + m_m5b_currheader[5]*256,
                         m_m5b_currheader[11], m_m5b_currheader[10], m_m5b_currheader[9], m_m5b_currheader[8],
                         m_m5b_currheader[15], m_m5b_currheader[14]
                );
         #endif
      }
  
      // Read more data until next header
      size_t navail = 10016 - (m_m5b_readpos % 10016);
      nread = read(fd, (void*)b,  (nbytes < navail) ? nbytes : navail );
      if (nread <= 0) {
         return -1;
      }

      m_m5b_readpos += nread;
      nbytes -= nread;
      b += nread;

   }
   return 0;
}

/** Replacement fast calculation of delta-time adopted from Tsunami UDP protocol */
u_int64_t get_usec_since(struct timeval *old_time)
{
    struct timeval now;
    u_int64_t result = 0;

    /* get the current time */
    gettimeofday(&now, NULL);

    /* return the elapsed time */
    while (now.tv_sec > old_time->tv_sec) {
        result += 1000000;
        --now.tv_sec;
    }
    return result + (now.tv_usec - old_time->tv_usec);
}

/** Replacement usleep() adopted from Tsunami UDP protocol */
void usleep_that_works(u_int64_t usec)
{
    u_int64_t      sleep_time = (usec / 10000) * 10000;  /* the amount of time to sleep */
    struct timeval delay, now;

    /* get the current time */
    gettimeofday(&now, NULL);

    /* do the basic sleep, 10ms granularity */
    if (sleep_time >= 10000) {
        delay.tv_sec  = sleep_time / 1000000;
        delay.tv_usec = sleep_time % 1000000;
        select(0, NULL, NULL, NULL, &delay);
    }

    /* and spin for the rest of the time */
    while (get_usec_since(&now) < usec);
}


/** Prepare bit swapping to convert sign/magnitude bit order from Mark5B to VDIF (necessary in *some* cases) */
static unsigned char m_lut_signmagswap[255]  __attribute__ ((aligned (16)));
void init_signmag_swap_LUT(void)
{
    int i;
    unsigned char b;
    for (i=0; i<256; i++)
    {
        // test case
        //   b = 0x1B; // 0b00 01 10 11  so expecting  swp = 0b00 10 01 11 = 0x27 as output
        b = (unsigned char)i;
        unsigned char s0 = b & 0xAA;  // 0xAA = 0b101010
        unsigned char m0 = b & 0x55;  // 0x55 = 0b010101
        unsigned char swp = (s0 >> 1) | (m0 << 1);
        m_lut_signmagswap[b] = swp;
    }
    printf("Note: swapping of sign/magnitude bits during conversion is enabled.\n");
}


///////////////////////////////////////////////////////////////////////
// MAIN
///////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
   threadarg_t *arg;

   int fi = -1, fo = -1;

   char *srvr_port = "60000";
   struct sockaddr_in adr_srvr;
   struct sockaddr_in adr_ctl;
   struct sockaddr_in adr_clnt;

   int len_inet;
   int sd;
   int s;
   int c;
   uint16_t year = 2013;
   uint16_t day =343;
   uint8_t hh = 6;
   uint8_t mm = 47;
   uint8_t ss = 1;

   unsigned int frame_no;
   unsigned int sec_refepoch;
   unsigned int src_secrefepoch;
   unsigned char refepoch;
   unsigned char sec_count;
   unsigned int recrate;
   unsigned int frames_per_second = 0;
   double sendrate = 0;
   double sendrate_approx_Mbps;
  
   int64_t send_framedelay_usec = 0;
   int64_t ipd_usleep_diff = 0;
   int64_t ipd_time = 0;
   struct timeval Tprevpacket = {0, 0};
   struct timeval Tcurrpacket = {0, 0};
   
   vdif_cp_sr_t vfcp_sr;
   vdif_cp_rr_t vfcp_rr;
   vdif_frame_t frame;      // new VDIF frame
   unsigned char* m5;       // piece of Mark5B-format payload data read from file


   /* Init */
   len_inet = sizeof(adr_srvr);
   memset(&adr_srvr, 0, sizeof(adr_srvr));
   memset(&adr_ctl,  0, sizeof(adr_ctl));
   memset(&adr_clnt, 0, sizeof(adr_clnt));
   memset(&frame,    0, sizeof(vdif_frame_t));
   memset(&vfcp_sr,  0, sizeof(vdif_cp_sr_t));
   memset(&vfcp_rr,  0, sizeof(vdif_cp_rr_t));
   memset(&m_m5b_currheader, 0, sizeof(m_m5b_currheader));
#if DO_CONVERT_MARK5B_ENCODING
   init_signmag_swap_LUT();
#endif

   /* Command Line Parameters */
   if(argc != 11) {
     fprintf(stderr, "\n"
             "Usage: $ %s IPaddr Port# Infile[Mark format] rec.rate# send.rate# yyyy ddd hh mm ss\n"
             "     ---> IPaddr     : 127.0.0.1 or other\n"
             "     ---> Port#      : usually %s\n"
             "     ---> rec.rate#  : 2048 or 1024 (Mbps)\n"
             "     ---> send.rate# : slow-down factor for UDP/IP stream, options are\n"
             "                       1 [normal/real-time], 2 [half], 4 [quad], 8 [1/8], 16 [1/16]\n"
             "\n",
             argv[0], srvr_port
     );
     exit(1);
   }

   // Numerical command line parameters
   recrate  = (unsigned int)atoi(argv[4]);
   sendrate = atof(argv[5]);
   year = (uint16_t)atoi(argv[6]);
   day  = (uint16_t)atoi(argv[7]);
   hh   = (uint8_t)atoi(argv[8]);
   mm   = (uint8_t)atoi(argv[9]);
   ss   = (uint8_t)atoi(argv[10]);

   // Derive the real-time duration of VDIF frame, in microseconds (used for UDP/IP throttling)
   send_framedelay_usec = 5*2 * sendrate;
   sendrate_approx_Mbps = 8.0 * ((double)KVN_VDIF_FRAME_SIZE) / ((double)send_framedelay_usec);

   fprintf(stderr, "The target rate has been set to one %d byte frame in %ld microseconds = %.0f Mbit/s\n",
                   KVN_VDIF_FRAME_SIZE, send_framedelay_usec, sendrate_approx_Mbps);

   // Own IP and port (?)
   srvr_port = argv[2];
   printf("Sender IP:port : %s:%s\n", argv[1], argv[2]);
   adr_srvr.sin_addr.s_addr = inet_addr(argv[1]);
   if (INADDR_NONE == adr_srvr.sin_addr.s_addr) {
       bail("Bad server IP address.");
   }
   adr_ctl.sin_addr.s_addr = inet_addr(argv[1]);
   if (INADDR_NONE == adr_ctl.sin_addr.s_addr) {
       bail("Bad controller IP address.");
   }
   // adr_srvr.sin_addr.s_addr = inet_addr("127.0.0.1");
   // adr_ctl.sin_addr.s_addr  = inet_addr("127.0.0.1");



   /* Open the "block file" */
   int f_opts = O_RDONLY | O_LARGEFILE;
   f_opts    |= O_LARGEFILE;
   // f_opts |= O_RSYNC|O_SYNC; // immensely slow, and, input file most unlikely to be written to!
   // f_opts |= O_DIRECT;
   fprintf(stderr, "Opening '%s' for reading...\n", argv[3]);
   fi = open(argv[3], f_opts);
   if (fi < 0) {
      bail("Error opening input file");
   }
   lseek64(fi, 0, SEEK_SET);

#if DEBUG_WR_TEST
   fprintf(stderr, "Opening '%s' for debug-writing...\n", DEBUG_WR_FILENAME);
   f_opts = O_CREAT | O_WRONLY | O_LARGEFILE;
   // f_opts |= O_RSYNC; // extremely slow
   fo = open(DEBUG_WR_FILENAME, f_opts, S_IRUSR | S_IRGRP | S_IWUSR | S_IROTH
   );
   if (fo < 0) {
      bail("Error opening output file");
   }
#endif


   /* Prepare networking */
   // Make a UDP socket on server
   sd = socket(PF_INET, SOCK_DGRAM, 0);
   if (-1 == sd) { 
      bail("socket()"); 
   }
   if (0) {
      timeout.tv_sec = 30;
      timeout.tv_usec = 0;   
      z = setsockopt(sd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));
      if (z == -1) { bail("setsocketopt of timeout"); }
   }
   s = 1;
   setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &s, sizeof(s));
   s = 16777216;
   setsockopt(sd, SOL_SOCKET, SO_RCVBUF,    &s, sizeof(s));
   s = 16777216;
   setsockopt(sd, SOL_SOCKET, SO_SNDBUF,    &s, sizeof(s));

   // "Client" socket??
   if (0) {
      adr_clnt.sin_addr.s_addr = htonl(INADDR_ANY);
      adr_clnt.sin_family      = AF_INET;
      adr_clnt.sin_port        = htons(atoi(srvr_port) + 1);
      z = bind(sd, (struct sockaddr *)&adr_clnt, len_inet);
      if (z == -1) { bail("bind(2) of server socket addr"); }
   }

   // Server and control ports
   adr_srvr.sin_family = AF_INET;
   adr_srvr.sin_port   = htons(atoi(srvr_port));
   adr_ctl.sin_family  = AF_INET;
   adr_ctl.sin_port    = htons(atoi(srvr_port) + 1);
   len_inet            = sizeof(adr_srvr);



   /* Fill out thread arguments */              // JanW: doesn't look like multithreading is *actually* used...
   arg = (threadarg_t*)malloc(sizeof(threadarg_t));
   if (NULL == arg) { 
      bail("malloc(2) of 'thread' execution context"); 
   }
   arg->sd = sd;


   /* START timestamp */
   time(&etd);
   etm = *localtime(&etd);
   strftime(etstr, sizeof(etstr), tfmt, &etm);

   printf("Ready to SEND(UDP) START at %s. Press any key to continue.\n", etstr);
   getchar();

   /* Configure "server report" */
   vfcp_sr.ver_kind = 0x11;  // VDIFCP version(0x1), SR(0x1)
   // vfcp_sr.port = 0x10;   // VSI port(1 - 4), reserved
   // vfcp_sr.plen = 0x001c; // SR packet length(default 28byte)
   // vfcp_sr.plen = 0x1c00; // SR packet length(default 28byte)
   vfcp_sr.plen = 0x5000;    // SR packet length(default 80byte) , 0x5000 = 20480 byte
   vfcp_sr.seqno = 0;        // packet sequence number

   // Time code: 'yyyy' 'ddd' 'hh' 'mm' 'ss' in BCD format
   vfcp_sr.mode_timeupper = 0x10 | ((year / 1000) & 0x0f); // VSI mode 1, year(2013=>2)
   vfcp_sr.time[0] = ((((year / 100) % 10) << 4) & 0xf0) | (((year / 10) % 10) & 0x0f);
   vfcp_sr.time[1] = (((year % 10) << 4) & 0xf0) | ((day / 100) & 0x000f);
   vfcp_sr.time[2] = ((((day / 10) % 10) << 4) & 0xf0) | ((day % 10) & 0x0f);
   vfcp_sr.time[3] = (((hh / 10) << 4) & 0xf0) | ((hh % 10) & 0x0f);
   vfcp_sr.time[4] = (((mm / 10) << 4) & 0xf0) | ((mm % 10) & 0x0f);
   vfcp_sr.time[5] = (((ss / 10) << 4) & 0xf0) | ((ss % 10) & 0x0f);
   vfcp_sr.reserve2 = VFCP_SR_MAGIC; // don't understand what the value (0x10801080) does

   // Format? Version? What?
   strncpy((char*)&vfcp_sr.aux[0], "MARK53.3", sizeof(vfcp_sr.aux));

   // Configure VFCP pdata with timestamp (JanW: shouldn't this have %04X instead of %04d, because of BCD encoding like above??)
   if (recrate == 2048) {
      // 2G
      frames_per_second = 200000;  // assumes KVN_VDIF_PAYLOAD_SIZE==1280 so that 1280*200000*8bit = 2048e9 bit/s
      snprintf((char*)&vfcp_sr.pdata[0], sizeof(vfcp_sr.pdata),
            "#1234,%04u%03u%02u%02u%02u,1024,32,2,ACQ,PDATAVP1,PDATAVP2;",
            year, day, hh, mm, ss);  // 2G
      fprintf(stderr,"Sending %52s\n", vfcp_sr.pdata);
   } else if (recrate == 1024) {
      // 1G
      frames_per_second = 100000;  // assummes KVN_VDIF_PAYLOAD_SIZE==1280 so that 128*100000*8 = 1024e9 bit/s
      snprintf((char*)&vfcp_sr.pdata[0], sizeof(vfcp_sr.pdata),
            "#1234,%04u%03u%02u%02u%02u,512,32,2,ACQ,PDATAVP1,PDATAVP2;.", 
            year, day, hh, mm, ss);  // 1G
      fprintf(stderr,"Sending %52s\n", vfcp_sr.pdata);
   }



// JanW: not sure what the below things do. They were already commented out in the original code.
//
//   z = sendto(arg->sd, &vfcp_sr, sizeof(vfcp_sr), 0,
//         (struct sockaddr *)&adr_ctl, len_inet);
//
//   do{
//      z = recvfrom(arg->sd, &vfcp_rr, sizeof(vfcp_rr), 0,
//            (struct sockaddr *)&adr_clnt, &len_inet);
//   }while(vfcp_rr.ver_kind != 0x12);
//   }while(vfcp_rr.ver_kind != 0x21);
//
//   printf("port #: %d, pkt len: %d, seq #: %d\n",
//         (int)vfcp_rr.port, (int)vfcp_rr.plen, (int)vfcp_rr.seqno);
//
//   printf("mode_timeup: %X, time: %X%X%X%X%X%X\n",
//         (int)vfcp_rr.mode_timeupper, (int)vfcp_rr.time[0],
//         (int)vfcp_rr.time[1], (int)vfcp_rr.time[2], (int)vfcp_rr.time[3],
//         (int)vfcp_rr.time[4], (int)vfcp_rr.time[5]);
//
//   getchar();



   /* Calculate the VDIF Reference Epoch */
   //   refepoch     : the most recent 6-month period from 0:00 UTC 01 Jan 2010
   //   sec_refepoch : total seconds from refepoch day until current data time
   if ((year % 4) == 0) {
      if(day >= 183){ // ddd >= 1, always
         refepoch = ((year - 2000) * 2) + 1;
         sec_refepoch = ((day-183) * 86400) + (hh*3600) + (mm*60) + ss;
      }
      else{
         refepoch = (year - 2000) * 2;
         sec_refepoch = ((day-1) * 86400) + (hh*3600) + (mm*60) + ss;
      }
   } else {
      if(day >= 182){ // ddd >= 1, always
         refepoch = ((year - 2000) * 2) + 1;
         sec_refepoch = ((day-182) * 86400) + (hh*3600) + (mm*60) + ss;
      }
      else{
         refepoch = (year - 2000) * 2;
         sec_refepoch = ((day-1) * 86400) + (hh*3600) + (mm*60) + ss;
      }
   }

   frame_no = 0;
   sec_count = 0;



   /* Fill out template VDIF header */

   // Standard header fields
   frame.hdr.invalid_sec_epoch = 0;                                  // Invalid='0', Legacy='0', Data Second=0
   frame.hdr.dframe_len        = sizeof(vdif_frame_t) / 8;           // VDIF payload length in 8-byte units
   frame.hdr.dframe_len       |= (KVN_VDIF_LOG_N_CHANNELS << 24);    // Number N of freq. channels (2^N channels)
   frame.hdr.epoch_dframe_no   = (refepoch % 64) << 24;              // Fixed VDIF reference epoch
   frame.hdr.bits_tid          = ((KVN_VDIF_BITSPERSAMPLE-1) << 26); // Data bits/sample (default:2)
   frame.hdr.bits_tid         |= (KVN_VDIF_THREAD_ID << 16);         // Thread ID (default:0)

   // Non-standard Extended User Data fields for the KVN 
   // frame.hdr.valbit_sample_split = 0x001f10dc;
   // frame.hdr.valbit_sample_split = 0xdc101f00;
   frame.hdr.valbit_sample_split = (0xc0101f00  | (refepoch << 24)); // JanW: not sure what the magic bits mean
   frame.hdr.src_sec_epoch       = 0;
   frame.hdr.sec_counts          = 0;



   /* Wait for 1.0 seconds (not sure why!) */ 
   scl = clock();
   do {
      ecl = clock();
      runtime = (ecl - scl)/CLOCKS_PER_SEC;
   } while (runtime < 1.0);
   gettimeofday(&t_start,    NULL);
   gettimeofday(&t_ctlstart, NULL);
   gettimeofday(&Tprevpacket, NULL);


   /* Sending loop -- send out 1.0s pieces of data */
   while (1) {

      // UTC in VDICP Sender Report, sent to control port (default port: 60001)
      z = sendto(arg->sd, &vfcp_sr, sizeof(vfcp_sr), 0, (struct sockaddr *)&adr_ctl, len_inet);

      // JanW: CRITICAL?
      // Should we wait here until UDP/IP packet reaches the VDICP "server" and has been processed?
      // Should we also wait for a "server" OK or other reply?
      // How to be sure the UDP/IP has not been lost?

      // Update VDIF header: standard fields
      frame.hdr.invalid_sec_epoch = sec_refepoch & ~0xC0000000;  // JanW: removed conversion of LSB->MSB (equivalent to htonl())
      // sec_refepoch_swap = 0x80000000 | (sec_refepoch);        // JanW: not sure why 0x80 (Invalid='1') was used here

      // Update VDIF header: user-specific fields
      src_secrefepoch = sec_refepoch + 1;
      frame.hdr.src_sec_epoch = htonl(src_secrefepoch);    // JanW: KVN seems to want MSB-first?


      /* Frame-by-frame read and UDP/IP-send of 1.0 seconds of data */

      for (frame_no=0; frame_no<frames_per_second; frame_no++) {

         // Send rate throttling (adapted from Tsunami UDP protocol)
         gettimeofday(&Tcurrpacket, NULL);
         ipd_usleep_diff = send_framedelay_usec - tv_diff_usec(Tcurrpacket, Tprevpacket);
         Tprevpacket = Tcurrpacket;
         if (ipd_usleep_diff > 0 || ipd_time > 0) {
             ipd_time += ipd_usleep_diff;
         }

         // Update VDIF header: frame count
         frame.hdr.epoch_dframe_no = (frame.hdr.epoch_dframe_no & 0xFF000000) | frame_no;

         // JanW: CRITICAL part 2?
         // Frame count and data-second in VDIF : the original code does not check
         // if they match at all with the underlying Mark5B VLBI time stamps!
         // What if Mark5B file has missing data, shifts in time, or starts
         // with an unexpected time stamp?
         // Then the OCTA VDIF data can not be used for VLBI experiments.

#if DEBUG_SHOW_VDIF_TIMESTAMPS
         {
            double Tframe = ((double)sec_refepoch) + ((double)frame_no)/((double)frames_per_second);
            fprintf(stderr, "VDIF frame#=%05d second= %.5f\n", frame_no, Tframe);
         }
#endif

         // Read next VDIF payload from disk (JanW version)
         flag_finfile = get_headerless_from_mark5b(fi, (void*)&(frame.data[0]), KVN_VDIF_PAYLOAD_SIZE);
         if (flag_finfile) { break; }

#if DO_CONVERT_MARK5B_ENCODING
         // Swap sign and magnitude
         // VDIF 1.1 page 11: 10. Sample representation
         //    00, 01, 10, 11 = -Mag, -1.0, +1.0, +Mag
         // Mark5B in mark5 memo #019:
         //    00, 01, 10, 11 = same as VDIF, follows Mark4/VLBA encoding
         // Mark5access library decoder, mark5_format_mark5b.c definition of lut4level[4]:
         //    00, 01, 10, 11 = -Mag, +1.0, -1.0, +Mag
         m5 = &(frame.data[0]);
         for (c = 0; c < KVN_VDIF_PAYLOAD_SIZE; c+=4)
         {
             *(m5 + c + 0) = m_lut_signmagswap[*(m5 + c + 0)];
             *(m5 + c + 1) = m_lut_signmagswap[*(m5 + c + 1)];
             *(m5 + c + 2) = m_lut_signmagswap[*(m5 + c + 2)];
             *(m5 + c + 3) = m_lut_signmagswap[*(m5 + c + 3)];
         }
#endif

         // Update VDIF header: some KVN-specific info
         if(recrate == 2048){
            frame.hdr.sec_counts = (((sec_count) & 0x000000ff) | 0x00000000); // 2G Normal
         } else if (recrate == 1024) {
            frame.hdr.sec_counts = (((sec_count) & 0x000000ff) | 0x00003000); // 1G Normal
         }

         /* Send new VDIF UDP/IP packet to OCTA */
         //
         // Note: VDIF data should be Little Endian. This is the same as the native format on x86 platforms.
         //       That means that the VDIF 32-bit words in the (char*)&frame byte-array sent via sendto()
         //       already have the correct, standard VDIF byte order. No extra htonl() is necessary!
         //
         z = sendto(arg->sd, &frame, KVN_VDIF_FRAME_SIZE, 0, (struct sockaddr *)&adr_srvr, len_inet);

         /* Write new VDIF UDP/IP packet to local debug file */
#if DEBUG_WR_TEST
         z = write(fo, &frame, KVN_VDIF_FRAME_SIZE);
#endif

         /* Throttle the UDP/IP send data rate */
         if (ipd_time > 0) {
             // printf("ipd_time = %ld\n", ipd_time);
             usleep_that_works(ipd_time);
         }

     } // for(frame no)

     if (flag_finfile) { break; }


     /* Next VDIF data second */
     // JanW note: nothing is done here to check that the VDIF data-second matches underlying Mark5B data time!
     sec_count++;
     sec_refepoch++; 
//      vfcp_sr.time[3] = (atoi(&etstr[12]) - 0x30) << 4 
//                      | atoi(&etstr[13]) - 0x30; // ASCII -> BCD code
//      vfcp_sr.time[4] = (atoi(&etstr[15]) - 0x30) << 4 
//                      | atoi(&etstr[16]) - 0x30; // ASCII -> BCD code
//      vfcp_sr.time[5] = (atoi(&etstr[18]) - 0x30) << 4 
//                      | atoi(&etstr[19]) - 0x30; // ASCII -> BCD code
      ss++;
      if( ss == 60 ){
         ss = 0;
         mm++;
         if( mm == 60){
            mm = 0;
            hh++;
            if( hh == 24){
               hh = 0;
               day++;
               if(day == 366){
                  day = 1;
                  year++;
               }
            }
         }
      }

      /* Prepare next Sender Report to be sent later to the server */
      // vfcp_sr.mode_timeupper = 0x10 | ((year / 1000) & 0x0f); // VSI mode 1, year(2013=>2)
      // vfcp_sr.time[0] = ((((year / 100) % 10) << 4) & 0xf0) | (((year / 10) % 10) & 0x0f);
      vfcp_sr.time[1] = (((year % 10) << 4) & 0xf0) | ((day / 100) & 0x000f);
      vfcp_sr.time[2] = ((((day / 10) % 10) << 4) & 0xf0) | ((day % 10) & 0x0f);
      vfcp_sr.time[3] = (((hh / 10) << 4) & 0xf0) | ((hh % 10) & 0x0f);
      vfcp_sr.time[4] = (((mm / 10) << 4) & 0xf0) | ((mm % 10) & 0x0f);
      vfcp_sr.time[5] = (((ss / 10) << 4) & 0xf0) | ((ss % 10) & 0x0f);

      if (recrate == 2048) {
         snprintf((char*)&vfcp_sr.pdata[0], sizeof(vfcp_sr.pdata),
               "#1234,%04u%03u%02u%02u%02u,1024,32,2,ACQ,PDATAVP1,PDATAVP2;", // 2G
               year, day, hh, mm, ss); 
         fprintf(stderr, "Sending %52s\n", vfcp_sr.pdata);
      } else if (recrate == 1024) {
         snprintf((char*)&vfcp_sr.pdata[0], sizeof(vfcp_sr.pdata),
               "#1234,%04u%03u%02u%02u%02u,512,32,2,ACQ,PDATAVP1,PDATAVP2;.", // 1G
               year, day, hh, mm, ss); 
         fprintf(stderr, "Sending %52s\n", vfcp_sr.pdata);
      }
   }

   /* END timestamp */
   time(&etd);
   etm = *localtime(&etd);
   strftime(etstr, sizeof(etstr), tfmt, &etm);
   printf("SEND(UDP) END at %s.\n", etstr);

   close(arg->fd);
   close(fi);
#if DEBUG_WR_TEST
   close(fo);
#endif

   free(arg);
   return 0;
}
