#define _LARGEFILE64_SOURCE // open-> O_LARGEFILE
#define _GNU_SOURCE // open -> O_DIRECT
#define _XOPEN_SOURCE 600

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include <sys/stat.h>
#include <fcntl.h>
//#include <malloc.h>

#include "vdif_io_orig.h"

static uint8_t flag_finfile = 0;

int main(int argc, char *argv[])
{
   int rc;
   int m, n;

   tharg *arg;

   int fi;
//   int fo;

   char *srvr_port = "60000";
   struct sockaddr_in adr_srvr;
   struct sockaddr_in adr_ctl;
   struct sockaddr_in adr_clnt;
   struct VDIF_HEAD_DF vf_headdf;
//   struct VDIF_HEAD_DF vf_headdf[10];
//   svdif_headdf *vf_headdf;
   struct VDIFCP_SR vfcp_sr;
   struct VDIFCP_RR vfcp_rr;
   int len_inet;
   int s;
   uint16_t year = 2013;
   uint16_t day =343;
   uint8_t hh = 6;
   uint8_t mm = 47;
   uint8_t ss = 1;
   int mepoch, mtime;
//   char src_time[13] = "2014001000000";
   uint32_t seqno_swap;

   unsigned int frame_no;
   unsigned int frame_ch;
   unsigned int sec_refepoch;
   unsigned int src_secrefepoch;
   unsigned char refepoch;
   unsigned char sec_count;
   unsigned int sec_refepoch_swap;
   unsigned int frame_no_swap;
   uint8_t* p_vf_dat;
   uint8_t* p_bit_swap;
   unsigned int recrate;
   unsigned int frame_length;
   unsigned int sendrate;
   unsigned int send_count;
//   int pagesize = getpagesize();
//   char *realbuff = malloc(1312000 + pagesize);
//   char *alignedbuff = (char*)((unsigned)(realbuff + pagesize - 1) / pagesize * pagesize);

//   vf_headdf = (svdif_headdf *)alignedbuff;
//   fprintf(stderr, "page size = %d\r\n", pagesize);
//   fprintf(stderr, "realbuf addr = %X, aligned addr = %X, vf_headdf addr = %X\r\n",
//                         realbuff, alignedbuff, vf_headdf);
////   posix_memalign((void*)vf_headdf, getpagesize(), getpagesize());
////   posix_memalign((void*)vf_headdf, 4096, 4096); // 512(2^9) * 25625 = 13120000

   len_inet = sizeof(adr_srvr);
   memset(&adr_srvr, 0, sizeof(adr_srvr));
   memset(&adr_ctl, 0, sizeof(adr_ctl));
   memset(&adr_clnt, 0, sizeof(adr_clnt));
   memset(&vf_headdf.vdif_hdr, 0, sizeof(vf_headdf.vdif_hdr));
   memset(&vf_headdf.vdif_dat, 0, sizeof(vf_headdf.vdif_dat));
//   memset(vf_headdf, 0, 13120);
   memset(&vfcp_sr, 0, sizeof(vfcp_sr));
   memset(&vfcp_rr, 0, sizeof(vfcp_rr));

   // Command Line Parameters
   if(argc != 11) {
     fprintf(stderr,"Usage: $ %s IPaddr Port# Infile[Mark format] rec.rate# send.rate# yyyy ddd hh mm ss\r\n     ---> rec.rate# : 2048 or 1024\r\n     ---> send.rate# : 1[normal], 2[half], 4[quad], 8[1/8], 16[1/16]\r\n",argv[0]);
     exit(1);
   }

   /* Getting THIS server IP */
   if (argc > 1) {
      printf("Sender IP address : %s\r\n", argv[1]);
      adr_srvr.sin_addr.s_addr = inet_addr(argv[1]);
      if (adr_srvr.sin_addr.s_addr == INADDR_NONE) bail("Bad address.");

      adr_ctl.sin_addr.s_addr = inet_addr(argv[1]);
      if (adr_ctl.sin_addr.s_addr == INADDR_NONE) bail("Bad address.");
   }
   else
   {
      adr_srvr.sin_addr.s_addr = inet_addr("127.0.0.1");
      adr_ctl.sin_addr.s_addr = inet_addr("127.0.0.1");
   }

   /* Getting THIS server port number */
   if (argc > 2){
      srvr_port = argv[2];
   }
//   printf("Sender Port : %s\r\n", srvr_port);

//   if((fi = fopen(argv[3],"r")) == NULL) {
//   if((fi = open(argv[3], O_RDONLY | O_LARGEFILE | O_DIRECT)) <  0) {
   if((fi = open(argv[3], O_RDONLY | O_LARGEFILE | O_RSYNC)) <  0) {
//   if((fi = open(argv[3], O_RDONLY | O_RSYNC)) <  0) {
//   if((fi = open(argv[3], O_RDONLY)) <  0) {
     fprintf(stderr,"Fail to open '%s'\n",argv[3]);
     exit(1);
   }
//   if((fo = open("wr_test.dat", O_CREAT | O_WRONLY | O_LARGEFILE | O_RSYNC)) <  0) {
////   if((fi = open(argv[3], O_RDONLY)) <  0) {
//     fprintf(stderr,"Fail to open '%s'\n","wr_test.dat");
//     exit(1);
//   }
   //fseek(fi, 0, SEEK_SET);
   recrate = (unsigned int)atoi(argv[4]);
   sendrate = (unsigned int)atoi(argv[5]);
   year = (uint16_t)atoi(argv[6]);
   day  = (uint16_t)atoi(argv[7]);
   hh   = (uint8_t)atoi(argv[8]);
   mm   = (uint8_t)atoi(argv[9]);
   ss   = (uint8_t)atoi(argv[10]);

   /* Creating thread argument */
   arg = (tharg *)malloc(sizeof(tharg));
   if (arg == NULL) bail("malloc(2)");

   /* Making a UDP socket on server */
   arg->sd = socket(PF_INET, SOCK_DGRAM, 0);
   if (arg->sd == -1) bail("socket()");

   adr_srvr.sin_family = AF_INET;
   adr_srvr.sin_port = htons(atoi(srvr_port));
   len_inet = sizeof(adr_srvr);

   adr_ctl.sin_family = AF_INET;
   adr_ctl.sin_port = htons(atoi(srvr_port) + 1);

//   adr_clnt.sin_addr.s_addr = htonl(INADDR_ANY);
//   adr_clnt.sin_family = AF_INET;
//   adr_clnt.sin_port = htons(atoi(srvr_port) + 1);

//   timeout.tv_sec = 30;
//   timeout.tv_usec = 0;
//   z = setsockopt(arg->sd, SOL_SOCKET, SO_RCVTIMEO, &timeout, sizeof(timeout));
//   if (z == -1) bail("setsocketopt");

   s = 1;
   setsockopt(arg->sd, SOL_SOCKET, SO_REUSEADDR, &s, sizeof(s));
   s = 16777216;
   setsockopt(arg->sd, SOL_SOCKET, SO_RCVBUF, &s, sizeof(s));
   s = 16777216;
   setsockopt(arg->sd, SOL_SOCKET, SO_SNDBUF, &s, sizeof(s));

//   z = bind(arg->sd, (struct sockaddr *)&adr_clnt, len_inet);
//   if (z == -1) bail("bind(2)");

   /* START timestamp */
   time(&etd);
   etm = *localtime(&etd);
   strftime(etstr, sizeof(etstr), tfmt, &etm);
   printf("Ready to SEND(UDP) START at %s.\r\n", etstr);

   getchar();

   vfcp_sr.ver_kind = 0x11; // VDIFCP version(0x1), SR(0x1)
//   vfcp_sr.port = 0x10; // VSI port(1 - 4), reserved
//   vfcp_sr.plen = 0x001c; // SR packet length(default 28byte)
//   vfcp_sr.plen = 0x1c00; // SR packet length(default 28byte)
   vfcp_sr.plen = 0x5000; // SR packet length(default 80byte)
   vfcp_sr.seqno = 0; // packet sequence number
   seqno_swap = 0;

   vfcp_sr.mode_timeupper = 0x10 | ((year / 1000) & 0x0f); // VSI mode 1, year(2013=>2)
   vfcp_sr.time[0] = ((((year / 100) % 10) << 4) & 0xf0) | (((year / 10) % 10) & 0x0f);
   vfcp_sr.time[1] = (((year % 10) << 4) & 0xf0) | ((day / 100) & 0x000f);
   vfcp_sr.time[2] = ((((day / 10) % 10) << 4) & 0xf0) | ((day % 10) & 0x0f);
   vfcp_sr.time[3] = (((hh / 10) << 4) & 0xf0) | ((hh % 10) & 0x0f);
   vfcp_sr.time[4] = (((mm / 10) << 4) & 0xf0) | ((mm % 10) & 0x0f);
   vfcp_sr.time[5] = (((ss / 10) << 4) & 0xf0) | ((ss % 10) & 0x0f);
   vfcp_sr.reserve2 = 0x80108010; // don't understand(0x10801080)

   sprintf(&vfcp_sr.aux[0], "MARK53.3");

   if(recrate == 2048){
      // 2G
      frame_length = 200000;
      sprintf(&vfcp_sr.pdata[0],
            "#1234,%04d%03d%02d%02d%02d,1024,32,2,ACQ,PDATAVP1,PDATAVP2;",
            year, day, hh, mm, ss); 
      fprintf(stderr,
            "#1234,%04d%03d%02d%02d%02d,1024,32,2,ACQ,PDATAVP1,PDATAVP2;\r\n", // 2G
            year, day, hh, mm, ss); 
   }
   else if(recrate == 1024){
      // 1G
      frame_length = 100000;
      sprintf(&vfcp_sr.pdata[0],
            "#1234,%04d%03d%02d%02d%02d,512,32,2,ACQ,PDATAVP1,PDATAVP2;.", 
            year, day, hh, mm, ss); 
      fprintf(stderr,
            "#1234,%04d%03d%02d%02d%02d,512,32,2,ACQ,PDATAVP1,PDATAVP2;.\r\n", // 1G
            year, day, hh, mm, ss); 
   }

   if(sendrate == 1){
      send_count = 5;
   }
   else if(sendrate == 2){
      send_count = 10;
   }
   else if(sendrate == 4){
      send_count = 20;
   }
   else if(sendrate == 8){
      send_count = 40;
   }
   else if(sendrate == 16){
      send_count = 80;
   }
//   z = sendto(arg->sd, &vfcp_sr, sizeof(vfcp_sr), 0,
//         (struct sockaddr *)&adr_ctl, len_inet);

//   do{
//      z = recvfrom(arg->sd, &vfcp_rr, sizeof(vfcp_rr), 0,
//            (struct sockaddr *)&adr_clnt, &len_inet);

//   }while(vfcp_rr.ver_kind != 0x12);
//   }while(vfcp_rr.ver_kind != 0x21);

//   printf("port #: %d, pkt len: %d, seq #: %d\r\n",
//         (int)vfcp_rr.port, (int)vfcp_rr.plen, (int)vfcp_rr.seqno);

//   printf("mode_timeup: %X, time: %X%X%X%X%X%X\r\n",
//         (int)vfcp_rr.mode_timeupper, (int)vfcp_rr.time[0],
//         (int)vfcp_rr.time[1], (int)vfcp_rr.time[2], (int)vfcp_rr.time[3],
//         (int)vfcp_rr.time[4], (int)vfcp_rr.time[5]);

//   getchar();

   if((year % 4) == 0){
      if(day >= 183){ // ddd >= 1, always
         refepoch = ((year - 2000) * 2) + 1;
         sec_refepoch = ((day-183) * 86400) + (hh*3600) + (mm*60) + ss;
      }
      else{
         refepoch = (year - 2000) * 2;
         sec_refepoch = ((day-1) * 86400) + (hh*3600) + (mm*60) + ss;
      }
   }
   else{
      if(day >= 182){ // ddd >= 1, always
         refepoch = ((year - 2000) * 2) + 1;
         sec_refepoch = ((day-182) * 86400) + (hh*3600) + (mm*60) + ss;
      }
      else{
         refepoch = (year - 2000) * 2;
         sec_refepoch = ((day-1) * 86400) + (hh*3600) + (mm*60) + ss;
      }
   }

   frame_no = 0;
   sec_count = 0;

//   vf_headdf.vdif_hdr.invalid_sec_epoch = 0x80000000; //
//   vf_headdf.vdif_hdr.epoch_dframe_no = 0x1c000000; // 2014. 01
//   vf_headdf.vdif_hdr.dframe_len = 0x00000520; // VDIF Header(32bytes)+Data(1280bytes)=1312bytes
   vf_headdf.vdif_hdr.dframe_len = 0x20050000; // VDIF Header(32bytes)+Data(1280bytes)=1312bytes
//   vf_headdf.vdif_hdr.bits_tid = 0x00010000; //
//   vf_headdf.vdif_hdr.bits_tid = 0x00000100; //
//   vf_headdf.vdif_hdr.valbit_sample_split = 0x001f10dc; //
//   vf_headdf.vdif_hdr.valbit_sample_split = 0xdc101f00; //
   vf_headdf.vdif_hdr.valbit_sample_split = (0xc0101f00  | (refepoch << 24)); //
//   vf_headdf.vdif_hdr.src_sec_epoch = 0;
//   vf_headdf.vdif_hdr.sec_counts = 0;

   scl = clock();

   do{
      ecl = clock();
      runtime = (ecl - scl)/CLOCKS_PER_SEC;
   }while( runtime < 1.0 );

   gettimeofday(&t_start, NULL);
   gettimeofday(&t_ctlstart, NULL);

   while(1) {

      // send VDICP/SR using control port(60001), UTC
      z = sendto(arg->sd, &vfcp_sr, sizeof(vfcp_sr), 0,
            (struct sockaddr *)&adr_ctl, len_inet);

      src_secrefepoch = sec_refepoch + 1;

      sec_refepoch_swap = 0x80000000 | (sec_refepoch);
      vf_headdf.vdif_hdr.invalid_sec_epoch = ((sec_refepoch_swap >> 24) & 0x000000ff) |
                                             ((sec_refepoch_swap >> 8) & 0x0000ff00) |
                                             ((sec_refepoch_swap << 8) & 0x00ff0000) |
                                             ((sec_refepoch_swap << 24) & 0xff000000); //

//      vf_headdf.vdif_hdr.src_sec_epoch = src_secrefepoch;
      vf_headdf.vdif_hdr.src_sec_epoch = ((src_secrefepoch >> 24) & 0x000000ff) |
                                         ((src_secrefepoch >> 8) & 0x0000ff00) |
                                         ((src_secrefepoch << 8) & 0x00ff0000) |
                                         ((src_secrefepoch << 24) & 0xff000000);

      lseek64(fi, 16, SEEK_CUR);  // large file
      n = 0;

      for( frame_no=0; frame_no < frame_length; frame_no++){
         if(n > 8720){
            if(n != 10000){
//               fread(&vf_headdf.vdif_dat[0], 1, (10000 - n), fi);
               read(fi, &vf_headdf.vdif_dat[0], (10000 - n));
            }

//            fseek(fi, 16, SEEK_CUR);  // large file
//            n = fread(&vf_headdf.vdif_dat[(10000 - n)], 1, (n - 8720), fi);
            lseek64(fi, 16, SEEK_CUR);  // large file
            n = read(fi, &vf_headdf.vdif_dat[(10000 - n)], (n - 8720));
         }
         else{
//            n += fread(&vf_headdf.vdif_dat[0], 1, 1280, fi);
            n += read(fi, &vf_headdf.vdif_dat[0], 1280);
         }

         if(n <= 0){
            flag_finfile = 1;
            break;
         }

         p_vf_dat = &vf_headdf.vdif_dat[0];
         p_bit_swap = &byte_bit_swap[0];
         for(m=0; m<1280; m++){
//            vf_headdf.vdif_dat[m] = byte_bit_swap[vf_headdf.vdif_dat[m]];
            *p_vf_dat = *(p_bit_swap + *p_vf_dat);
            p_vf_dat++;
//            vf_headdf.vdif_dat[m] = ((vf_headdf.vdif_dat[m]&0x01)<<7)
//                  |((vf_headdf.vdif_dat[m]&0x02)<<5)
//                  |((vf_headdf.vdif_dat[m]&0x04)<<3)
//                  |((vf_headdf.vdif_dat[m]&0x08)<<1)
//                  |((vf_headdf.vdif_dat[m]&0x10)>>1)
//                  |((vf_headdf.vdif_dat[m]&0x20)>>3)
//                  |((vf_headdf.vdif_dat[m]&0x40)>>5)
//                  |((vf_headdf.vdif_dat[m]&0x80)>>7);
         }

//         vf_headdf.vdif_hdr.epoch_dframe_no = 0x1c000000 | frame_no; // 2014. 01
         frame_no_swap = (0x3f000000 & (refepoch << 24)) | frame_no;
         vf_headdf.vdif_hdr.epoch_dframe_no = ((frame_no_swap >> 24) & 0x000000ff) |
                                              ((frame_no_swap >> 8) & 0x0000ff00) |
                                              ((frame_no_swap << 8) & 0x00ff0000) |
                                              ((frame_no_swap << 24) & 0xff000000); // 2014. 01

//         vf_headdf.vdif_hdr.bits_tid = (frame_ch << 8) & 0x00000f00; // port(channel) #
         vf_headdf.vdif_hdr.bits_tid = 0x00000100; // port(channel) #
//         for(n=0; n<10; n++){
         if(recrate == 2048){
            vf_headdf.vdif_hdr.sec_counts = (((sec_count) & 0x000000ff) | 0x00000000); // 2G Normal
         }
         else if(recrate == 1024){
            vf_headdf.vdif_hdr.sec_counts = (((sec_count) & 0x000000ff) | 0x00003000); // 1G Normal
         }

            vf_headdf.vdif_hdr.dframe_len = 0x20050000; // VDIF Header(32bytes)+Data(1280bytes)=1312bytes

            z = sendto(arg->sd, &vf_headdf, 1312, 0,
//            z = sendto(arg->sd, &vf_headdf[n], 1312, 0,
                         (struct sockaddr *)&adr_srvr, len_inet);
//              write(fo, &vf_headdf[n], 1312);
//         }

////         vf_headdf.vdif_hdr.bits_tid = ((frame_ch+1) << 8) & 0x00000f00; // port(channel) #
////         vf_headdf.vdif_hdr.bits_tid = 0x00000200; // port(channel) #
////         z = sendto(arg->sd, &vf_headdf, 1312, 0,
////                      (struct sockaddr *)&adr_srvr, len_inet);
////         vf_headdf.vdif_hdr.bits_tid = 0x00000300; // port(channel) #
////         z = sendto(arg->sd, &vf_headdf, 1312, 0,
////                      (struct sockaddr *)&adr_srvr, len_inet);
////         vf_headdf.vdif_hdr.bits_tid = 0x00000400; // port(channel) #
////         z = sendto(arg->sd, &vf_headdf, 1312, 0,
////                      (struct sockaddr *)&adr_srvr, len_inet);

         do{
            gettimeofday(&t_end, NULL);
            t_diff.tv_usec = t_end.tv_usec - t_start.tv_usec;

            if(t_diff.tv_usec < 0){
               t_diff.tv_usec = t_diff.tv_usec + 1000000;
            }
         } while (t_diff.tv_usec < send_count); // 
//         printf("run time : %d[us]\r\n", (int)t_diff.tv_usec);
         t_start.tv_usec = t_end.tv_usec;

//         printf("SEND time : %10.2lf seconds. \n", runtime);
//      } // frame channel
      } // frame no

      if(flag_finfile == 1) break;

//     do{
//        gettimeofday(&t_ctlend, NULL);
//        t_ctldiff.tv_sec = t_ctlend.tv_sec - t_ctlstart.tv_sec;
////        t_ctldiff.tv_usec = t_ctlend.tv_usec - t_ctlstart.tv_usec;

////        if(t_ctldiff.tv_usec < 0){
////           t_ctldiff.tv_usec = t_ctldiff.tv_usec + 1000000;
////        }
//     } while (t_ctldiff.tv_sec < 1); // normal
////     } while (t_ctldiff.tv_sec < 2); // 1/2 rate
////     } while (t_ctldiff.tv_sec < 4); // 1/4 rate
//     t_ctlstart.tv_sec = t_ctlend.tv_sec;
////     t_ctlstart.tv_usec = t_ctlend.tv_usec;

     /* End timestamp */
//     time(&etd);
//     etm = *localtime(&etd);
//     strftime(etstr, sizeof(etstr), tfmt, &etm);

//     printf("SEND(UDP) at %s.\n", etstr);

     sec_count++;
     sec_refepoch++;

//      vfcp_sr.time[3] = (atoi(&etstr[12]) - 0x30) << 4 \
//                      | atoi(&etstr[13]) - 0x30; // ASCII -> BCD code
//      vfcp_sr.time[4] = (atoi(&etstr[15]) - 0x30) << 4 \
//                      | atoi(&etstr[16]) - 0x30; // ASCII -> BCD code
//      vfcp_sr.time[5] = (atoi(&etstr[18]) - 0x30) << 4 \
//                      | atoi(&etstr[19]) - 0x30; // ASCII -> BCD code

      ss++;
      if( ss == 60 ){
         ss = 0;
         mm++;
         if( mm == 60){
            mm = 0;
            hh++;
            if( hh == 24){
               hh = 0;
               day++;
               if(day == 366){
                  day = 1;
                  year++;
               }
            }
         }
      }

//      vfcp_sr.mode_timeupper = 0x10 | ((year / 1000) & 0x0f); // VSI mode 1, year(2013=>2)
//      vfcp_sr.time[0] = ((((year / 100) % 10) << 4) & 0xf0) | (((year / 10) % 10) & 0x0f);
      vfcp_sr.time[1] = (((year % 10) << 4) & 0xf0) | ((day / 100) & 0x000f);
      vfcp_sr.time[2] = ((((day / 10) % 10) << 4) & 0xf0) | ((day % 10) & 0x0f);
      vfcp_sr.time[3] = (((hh / 10) << 4) & 0xf0) | ((hh % 10) & 0x0f);
      vfcp_sr.time[4] = (((mm / 10) << 4) & 0xf0) | ((mm % 10) & 0x0f);
      vfcp_sr.time[5] = (((ss / 10) << 4) & 0xf0) | ((ss % 10) & 0x0f);

      if(recrate == 2048){
         sprintf(&vfcp_sr.pdata[0],
               "#1234,%04d%03d%02d%02d%02d,1024,32,2,ACQ,PDATAVP1,PDATAVP2;", // 2G
               year, day, hh, mm, ss); 
         fprintf(stderr,
               "#1234,%04d%03d%02d%02d%02d,1024,32,2,ACQ,PDATAVP1,PDATAVP2;\r\n", // 2G
               year, day, hh, mm, ss); 
      }
      else if(recrate == 1024){
         sprintf(&vfcp_sr.pdata[0],
               "#1234,%04d%03d%02d%02d%02d,512,32,2,ACQ,PDATAVP1,PDATAVP2;.", // 1G
               year, day, hh, mm, ss); 
         fprintf(stderr,
               "#1234,%04d%03d%02d%02d%02d,512,32,2,ACQ,PDATAVP1,PDATAVP2;.\r\n", // 1G
               year, day, hh, mm, ss); 
      }
   }
   /* START timestamp */
   time(&etd);
   etm = *localtime(&etd);
   strftime(etstr, sizeof(etstr), tfmt, &etm);
   printf("SEND(UDP) END at %s.\n", etstr);

   close(arg->fd);
//   fclose(fi);
   close(fi);
//   close(fo);

   free(arg);
//   free(vf_headdf);
   exit(0);
}

static void bail(const char *on_what)
{
        if (errno != 0) {
                fputs(strerror(errno), stderr);
                fputs(": ", stderr);
        }
        fputs(on_what, stderr);
        fputc('\n', stderr);
        exit(1);
}

