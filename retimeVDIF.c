/********************************************************************************
 **
 ** retimeVDIF
 **
 ** Usage:    retimeVDIF <in_out.vdif> [<new starting second> [<new ref epoch>]]
 **
 ** Can print (without modifying) or also modify the VDIF file timestamps.
 **
 ** Examples: 
 ** 
 **   Print the time in the first VDIF frame without changing the file:
 **   $  retimeVDIF r13340b_KVNTN_No00001.vdif
 **
 **   Modify the timestamps in the VDIF file:
 **   $  retimeVDIF r13340b_KVNTN_No00001.vdif 84299
 **   $  retimeVDIF r13340b_KVNTN_No00001.vdif 84299 29
 **
 ** The program reads the VDIF seconds time of the first VDIF frame (T0),
 ** changes its value to the specified new starting second,
 ** and updates all other VDIF frames by shifting their
 ** time stamps to   "new starting sec + (current sec - T0)".
 **
 ** Can be used to modify large offsets in recorded data
 ** if the DiFX 'clock_offset', if GPS UTC time sync was not proper.
 **
 ** (C) 2014 Jan Wagner
 **
 *******************************************************************************/

// Compile with:
//    gcc -D_FILE_OFFSET_BITS=64 -D_LARGEFILE64_SOURCE -D_LARGEFILE_SOURCE -O3 retimeVDIF.c -o retimeVDIF

// Number of VDIF frames in memory. Keep it small (512*8032byte = ~4 MB) to fit into CPU cache.
#define NUM_BUFFERED_FRAMES   512

#include <malloc.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <fcntl.h>
#include <errno.h>
#include <endian.h>
#include <sys/types.h>
#include <sys/stat.h>

int main(int argc, char** argv)
{
    int fi, fo, i;

    ssize_t nrd, nwr;
    off64_t rwpos = 0;
    size_t  framesize;
    int     nframes;

    unsigned char* buf = NULL;
    uint32_t vdif_header_w32[4];
    uint32_t t0_orig;
    uint32_t t0_new   = 0;
    uint8_t  ep_new   = 0;
    uint32_t ep_new32 = 0;

    /* Arguments */
    if (argc < 2 || argc > 4)
    {
        printf("Program retimeVDIF Version 1.0\n\n"
               "Usage: retimeVDIF <in_out.vdif> [<new starting second> [<new epoch>]]\n"
               "\n"
               "Shifts the VDIF time stamps in the file, such that the VDIF time stamps\n"
               "start from the specified new starting second. Can also change the VDIF epoch.\n");
        return -1;
    }

    fi = open(argv[1], O_RDWR|O_LARGEFILE);
    fo = fi;
    if (fi < 0)
    {
        printf("Error opening file '%s': %s\n", argv[1], strerror(errno));
        return -1;
    }
    if (argc >= 3)
    {
        t0_new = atol(argv[2]);
    }
    if (argc >= 4)
    {
        ep_new   = ((uint8_t)atoi(argv[3])) & 0b00111111;
        ep_new32 = ((uint32_t)ep_new) << 24;
        printf("Will update VDIF Reference Epoch to %u.\n", ep_new);
    }

    /* Determine the frame size and original starting time */
    read(fi, (void*)&vdif_header_w32, sizeof(vdif_header_w32));
    lseek64(fi, 0, SEEK_SET);

    // VDIF Header word0: [Invalid data flag:1 | reserved:1 | seconds in reference epoch:30]
    // VDIF Header word1: [Unassigned:2 | reference epoch:6 | data frame nr:24]
    // VDIF Header word2: [VDIF Version:3bit | log2 channels: 5bit | frame size in 8-byte units: 24bit]
    // ...
    t0_orig   = vdif_header_w32[0] & 0x3FFFFFFF;
    framesize = (vdif_header_w32[2] & 0x00FFFFFF) * 8;
    fprintf(stderr, "The VDIF frame size seems to be %zu bytes, start time %u seconds, ref. epoch %u.\n", 
            framesize, t0_orig, (vdif_header_w32[1]>>24) & 0x3F);

    if (!(argc >= 3))
    {
       return 0;
    }
 
    if (framesize > 32768) 
    {
       fprintf(stderr, "Suspiciously large frame size. Aborting.\n");
       return -1;
    }

    buf = (unsigned char*)memalign(16, framesize*NUM_BUFFERED_FRAMES);


    /* Modify the file */
    while (1)
    {
        lseek64(fi, rwpos, SEEK_SET); 
        nrd = read(fi, (void*)buf, framesize*NUM_BUFFERED_FRAMES);
        if (nrd <= 0) 
        { 
            break; 
        }

        nframes = nrd / framesize;
        putchar('.');

        for (i=0; i<nframes; i++)
        {
            uint32_t* tptr = (uint32_t*)(buf + i*framesize);

            // Update the 'Seconds' field: low 24 bits of word 0
            uint32_t tcurr = *tptr & 0x3FFFFFFF;
            uint32_t flags = *tptr & ~0x3FFFFFFF;
            *tptr = flags | (tcurr - t0_orig + t0_new);

            // Update the 'Ref Epoch' field: bits 24:30 of word 1
            if (argc >= 4)
            {
               uint32_t framenr_etc = *(tptr + 1) & ~0x3F000000;
               *(tptr + 1) = framenr_etc | ep_new32;
            }
        }

        lseek64(fo, rwpos, SEEK_SET); 
        nwr = write(fo, (void*)buf, nrd);

        rwpos += nwr;
    }

    printf("\nDone.\n");
    return 0;
}
